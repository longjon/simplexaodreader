#ifndef CUTFLOW_H
#define CUTFLOW_H

#include<vector>
#include<string>
#include<map>
#include<set>
#include<algorithm>


class cutflow
{

    public:

        // Print out a summary in order that was manually set
        void printSummary();

        // Print out a summary in order following first time each cut is called
        void printSummaryFIFO();

        // 
        int& operator[](const std::string &cutname);

        // Function to add or increment cutflow lines eg m_cutflow(__LINE__,"Total Events")++;
        int& operator()(const int &order, const std::string &cutname, bool expression=false, int increment=1);

        void setName(std::string name){m_name = name;};

    private:

        std::map<std::string, int> cut_yields;

        std::vector<std::string> order_fifo;

        std::set<int> order_values;

        std::vector< std::pair<int, std::string> > order_manual;

        std::string m_name;


};


#endif
