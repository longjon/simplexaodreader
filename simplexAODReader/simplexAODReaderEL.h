#ifndef SIMPLEXAODREADEREL_H
#define SIMPLEXAODREADEREL_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include "xAODEventInfo/EventInfo.h"
//#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTrigger/EnergySumRoI.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTrigMissingET/TrigMissingET.h"

#include "MCTruthClassifier/IMCTruthClassifier.h"

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "xAODTrigger/TrigCompositeContainer.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronSelectorTool.h"

//#include "TrigSteeringEventTPCnv/TrigRoiDescriptorCollection_p3.h"

//#include <TrigConfInterfaces/ITrigConfigTool.h>
//#include <TrigDecisionTool/TrigDecisionTool.h>
//#include "TriggerMatchingTool/MatchingTool.h"

#include "TGraphAsymmErrors.h"
#include <TTree.h>

#include "simplexAODReader/cutflow.h"

#include <vector>

using namespace MCTruthPartClassifier;

typedef ElementLink<xAOD::TruthParticleContainer> ElementTruthLink_t;
class simplexAODReaderEL : public EL::AnaAlgorithm
{
public:
    // this is a standard algorithm constructor
    simplexAODReaderEL(const std::string &name, ISvcLocator *pSvcLocator);

    ~simplexAODReaderEL();

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize() override;
    virtual StatusCode execute() override;
    virtual StatusCode finalize() override;

private:
    // Configuration, and any other types of variables go here.

    // testing
    enum matchType
    {
        err = 0,
        fullMatch,
        fo_NoTmatch,
        fo_noFT,
        fo_noOT,
        fo_noOTnoFT,
        noFO_Tmatch,
        noFO_noTmatch
    };
    /*
    ftf=offline ftfT=OffT : fullMatch
    ftf=offline ftfT!=OffT : fo_NoTmatch
    ftf=offline no ftftruth: fo_noFT
    ftf=offline no offtruth: fo_noOT
    ftf=offline no ftftruth noofftruth: fo_noOTnoFT
    ftf!=offline ftf has truth: noFO_ftfT


    */

    // Per event collections
    const xAOD::EventInfo *ei;
    const xAOD::TruthParticleContainer *truthParticles;
    const xAOD::TruthEventContainer *truthEvents;
    const xAOD::TrackParticleContainer *inDetTracksOffline;
    const xAOD::TrackParticleContainer *inDetTracksLRTOffline;
    const xAOD::TrackParticleContainer *HLT_FTF_Tracks;
    const xAOD::TrackParticleContainer *HLT_lrtFTF_Tracks;
    const xAOD::TrackParticleContainer *HLT_FTF_Muon_Tracks;
    const xAOD::TrackParticleContainer *HLT_FTFLRT_Muon_Tracks;
    const xAOD::TrackParticleContainer *HLT_PTLRT_Muon_Tracks;
    const xAOD::TrackParticleContainer *HLT_PT_Muon_Tracks;
    const xAOD::L2CombinedMuonContainer *HLT_MuonsL2LRT;
    const xAOD::MuonContainer *HLT_MuonsCBLRT;
    const xAOD::MuonContainer *HLT_MuonsCB;
    const xAOD::TrackParticleContainer *HLT_FTF_Electron_Tracks;
    const xAOD::TrackParticleContainer *HLT_FTFLRT_Electron_Tracks;
    const xAOD::TrackParticleContainer *HLT_PTLRT_Electron_Tracks;
    const xAOD::TrackParticleContainer *HLT_PT_Electron_Tracks;
    const xAOD::ElectronContainer *HLT_Electrons_LRT;
    const xAOD::ElectronContainer *HLT_Electrons;
    const xAOD::EnergySumRoI *l1MetObject;
    const xAOD::JetContainer *Jets;
    const xAOD::TrigMissingETContainer *HLT_trkmht;
    const xAOD::TrigMissingETContainer *HLT_pufit;
    const xAOD::MuonContainer *Muons;
    const xAOD::MuonContainer *MuonsLRT;
    const xAOD::ElectronContainer *Electrons;
    const xAOD::ElectronContainer *LRTElectrons;
    const xAOD::MuonSegmentContainer *MuonSegments;
    const xAOD::VertexContainer *PrimaryVertices;

    const xAOD::CaloClusterContainer *caloClusters;

    // Retrieval flags, if retrieval fails on a file, this gets set to false so we can keep going
    bool m_flag_ei;
    bool m_flag_truthParticles;
    bool m_flag_truthEvents;
    bool m_flag_inDetTracksOffline;
    bool m_flag_inDetTracksLRTOffline;
    bool m_flag_HLT_FTF_Tracks;
    bool m_flag_HLT_lrtFTF_Tracks;
    bool m_flag_HLT_Electrons;
    bool m_flag_HLT_Electrons_lrt;
    bool m_flag_HLT_FTF_Muon_Tracks;
    bool m_flag_HLT_FTFLRT_Muon_Tracks;
    bool m_flag_HLT_PTLRT_Muon_Tracks;
    bool m_flag_HLT_PT_Muon_Tracks;
    bool m_flag_HLT_MuonsL2LRT;
    bool m_flag_HLT_MuonsCBLRT;
    bool m_flag_HLT_MuonsCB;
    bool m_flag_HLT_FTF_Electron_Tracks;
    bool m_flag_HLT_FTFLRT_Electron_Tracks;
    bool m_flag_HLT_PTLRT_Electron_Tracks;
    bool m_flag_HLT_PT_Electron_Tracks;
    bool m_flag_l1MetObject;
    bool m_flag_Jets;
    bool m_flag_HLT_trkmht;
    bool m_flag_HLT_pufit;
    bool m_flag_Muons;
    bool m_flag_MuonsLRT;
    bool m_flag_Electrons;
    bool m_flag_LRTElectrons;
    bool m_flag_MuonSegments;
    bool m_flag_PrimaryVertices;

    bool m_flag_caloClusters;

    // Flags and options
    int m_eventCounter;
    bool m_debug;
    bool m_makeNtuple;
    bool m_isSimulation;
    bool m_isDAOD;

    bool m_printTriggerDecCount;

    double m_xbeam;
    double m_ybeam;

    float m_ROISize;
    float m_EMPhiAdjust;
    float m_Highd0Cut;

    double m_minTrackd0;
    double m_minTrackpT;
    double m_maxTrackEta;
    double m_truthMaxProdR;
    double m_truthMinDecayR;
    int m_minTruthLayers;
    std::vector<float> m_SiLayers;
    int m_maxBarcode;
    int m_minHitsFTF;
    float m_minJetpT;
    float m_minHLTtrkmht;
    float m_minHLTpufit;
    bool m_useOnlyOfflineSignal;
    bool m_extrapolateTruth;

    bool m_includeLRT;
    bool m_includeExtraLLH;

    double m_matchSizeR;
    std::vector<int> m_signalParentPDGIDs;
    std::vector<int> m_excludeParentPDGIDs;
    std::vector<int> m_signalPDGIDs;

    cutflow m_cutflow, m_cutflow_met;

    std::vector<TGraphAsymmErrors *> efficiencyGraphs;

    ToolHandle<IMCTruthClassifier> m_truthClassifier;

    //ToolHandle<TrigConf::ITrigConfigTool> m_configTool;
    ToolHandle<Trig::TrigDecisionTool> m_trigDecTool;
    ToolHandle<Trig::IMatchingTool> m_trigMatchTool;
    ToolHandle<Trig::IMatchingTool> m_trigMatchToolLRT;
    ToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool;
    ToolHandle<AsgElectronLikelihoodTool> m_ElectronLLHTool;
    ToolHandle<AsgElectronSelectorTool> m_ElectronDNNTool;
    ToolHandle<AsgElectronLikelihoodTool> m_ElectronLLHTool_newtune;
    ToolHandle<AsgElectronLikelihoodTool> m_ElectronLLHTool_offline;

    bool m_doTruth;
    bool m_firstEvent;

    // Maps to keep track of matched tracks and their inverse relations
    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_trackOffline_to_FTF_map; // map from offline track to matched ftf track
    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_trackFTF_to_Offline_map; // map from ftf track to matched offline track

    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_trackOfflineSignal_to_FTF_map; // map from offline signal track to matched ftf track
    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_trackFTF_to_OfflineSignal_map; // map from ftf track to matched offline signal track

    std::map<const xAOD::TruthParticle *, const xAOD::TrackParticle *> m_trackTruth_to_FTF_map; // map from truth track to matched ftf track
    std::map<const xAOD::TrackParticle *, const xAOD::TruthParticle *> m_trackFTF_to_Truth_map; // map from ftf track to matched truth track

    std::map<const xAOD::TruthParticle *, const xAOD::TrackParticle *> m_trackTruth_to_Offline_map; // map from truth track to matched offline track
    std::map<const xAOD::TrackParticle *, const xAOD::TruthParticle *> m_trackOffline_to_Truth_map; // map from offline track to matched truth track

    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_FSFTF_to_MUFTF_map; // map from fullscan FTF to muon roi FTF
    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_FSFTF_to_ELFTF_map; // map from fullscan FTF to muon roi FTF

    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_offlinesig_to_MUFTFlrt_map; // map from offline sigs to muon roi lrt FTF
    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_offlinesig_to_MUFTF_map;    // map from offline sigs to muon roi FTF
    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_offlinesig_to_MUPTLRT_map;  // map from offline sigs to muon roi precision track LRT
    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_offlinesig_to_MUPT_map;     // map from offline sigs to muon roi precision track LRT

    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_offlinesig_to_ELFTFlrt_map; // map from offline sigs to muon roi lrt FTF
    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_offlinesig_to_ELFTF_map;    // map from offline sigs to muon roi FTF
    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_offlinesig_to_ELPTLRT_map;  // map from offline sigs to muon roi FTF
    std::map<const xAOD::TrackParticle *, const xAOD::TrackParticle *> m_offlinesig_to_ELPT_map;     // map from offline sigs to muon roi FTF

    std::map<const xAOD::TruthParticle *, const xAOD::TrackParticle *> m_trackTruth_to_ELPTLRT_map; // map from truth track to matched electron LRT PT track
    std::map<const xAOD::TrackParticle *, const xAOD::TruthParticle *> m_ELPTLRT_to_trackTruth_map; // map from electron LRT PT track to matched truth track
    std::map<const xAOD::TruthParticle *, const xAOD::TrackParticle *> m_trackTruth_to_ELPT_map;    // map from truth track to matched electron PT track

    std::map<const xAOD::TruthParticle *, const xAOD::TrackParticle *> m_trackTruth_to_MUPTLRT_map; // map from truth track to matched muon LRT PT track
    std::map<const xAOD::TrackParticle *, const xAOD::TruthParticle *> m_MUPTLRT_to_trackTruth_map; // map from muon LRT PT track to matched truth track
    std::map<const xAOD::TruthParticle *, const xAOD::TrackParticle *> m_trackTruth_to_MUPT_map;    // map from truth track to matched muon PT track

    std::map<const xAOD::Electron *, const xAOD::Electron *> m_hlt_to_off_electrons_map; // map from online HLT electrons to offline electrons

    int m_totalEventCounter;
    // Count trigger accepts
    std::map<std::string, int> m_L1_Trig_Pass;
    std::map<std::string, int> m_HLT_Trig_Pass;
    std::map<std::string, int> m_L1_Trig_Prescale;
    std::map<std::string, int> m_HLT_Trig_Prescale;

    // vectors of selected tracks
    std::vector<const xAOD::TrackParticle *> m_FTFTracksComb;
    std::set<const xAOD::TrackParticle *> m_FTFLRTTracks;
    std::vector<const xAOD::TrackParticle *> m_muonFTFLRT;
    std::vector<const xAOD::TrackParticle *> m_muonPTLRT;
    std::vector<const xAOD::TrackParticle *> m_muonPT;
    std::vector<const xAOD::TrackParticle *> m_electronFTFLRT;
    std::vector<const xAOD::TrackParticle *> m_muonFTF;
    std::vector<const xAOD::TrackParticle *> m_electronFTF;
    std::vector<const xAOD::TrackParticle *> m_electronPTLRT;
    std::vector<const xAOD::TrackParticle *> m_electronPT;
    std::vector<const xAOD::TrackParticle *> m_OfflineTracks;
    std::vector<const xAOD::TrackParticle *> m_OfflineTracksSignal;
    std::vector<const xAOD::TruthParticle *> m_TruthParticlesSignal;

    std::vector<const xAOD::Muon *> m_Muons;
    // std::vector<const xAOD::Muon*> m_MuonsLRT;
    std::vector<const xAOD::Muon *> m_HLT_Muons;
    // std::vector<const xAOD::Muon*> m_HLT_MuonsLRT;
    std::vector<const xAOD::Electron *> m_Electrons;
    // std::vector<const xAOD::Electron*> m_LRTElectrons;
    std::vector<const xAOD::Electron *> m_HLT_Electrons;
    // std::vector<const xAOD::Electron*> m_HLT_LRTElectrons;

    std::vector<const TrigRoiDescriptor *> m_roid_em;
    std::vector<const TrigRoiDescriptor *> m_roid_mu;

    // Histograms that can't easily be booked
    // mostly Efficiencies
    TGraphAsymmErrors *h_d0_eff_FTFcomb_r_offline;
    TGraphAsymmErrors *h_d0_el_eff_FTFcomb_r_offline;
    TGraphAsymmErrors *h_d0_mu_eff_FTFcomb_r_offline;
    TGraphAsymmErrors *h_pt_eff_FTFcomb_r_offline;
    TGraphAsymmErrors *h_eta_eff_FTFcomb_r_offline;
    TGraphAsymmErrors *h_vtxR_eff_FTFcomb_r_offline;

    TGraphAsymmErrors *h_d0_pur_FTFcomb_r_offline;
    TGraphAsymmErrors *h_pt_pur_FTFcomb_r_offline;
    TGraphAsymmErrors *h_eta_pur_FTFcomb_r_offline;
    TGraphAsymmErrors *h_vtxR_pur_FTFcomb_r_offline;

    TGraphAsymmErrors *h_d0_eff_FTFcomb_r_truth;
    TGraphAsymmErrors *h_d0_eff_FTFcomb_r_truth_el;
    TGraphAsymmErrors *h_d0_eff_FTFcomb_r_truth_mu;
    TGraphAsymmErrors *h_pt_eff_FTFcomb_r_truth;
    TGraphAsymmErrors *h_vtxR_eff_FTFcomb_r_truth;

    TGraphAsymmErrors *h_d0_eff_offline_r_truth;
    TGraphAsymmErrors *h_pt_eff_offline_r_truth;
    TGraphAsymmErrors *h_vtxR_eff_offline_r_truth;

    TGraphAsymmErrors *h_vtxR_L1MU14FCH_r_offline;
    TGraphAsymmErrors *h_pt_L1MU14FCH_r_offline;
    TGraphAsymmErrors *h_d0_L1MU14FCH_r_offline;
    TGraphAsymmErrors *h_vtxR_L1eEM26M_r_offline;
    TGraphAsymmErrors *h_pt_L1eEM26M_r_offline;
    TGraphAsymmErrors *h_d0_L1eEM26M_r_offline;

    TGraphAsymmErrors *h_vtxR_L1MU14FCH_r_truth;
    TGraphAsymmErrors *h_pt_L1MU14FCH_r_truth;
    TGraphAsymmErrors *h_d0_L1MU14FCH_r_truth;
    TGraphAsymmErrors *h_vtxR_L1eEM26M_r_truth;
    TGraphAsymmErrors *h_pt_L1eEM26M_r_truth;
    TGraphAsymmErrors *h_d0_L1eEM26M_r_truth;

    TGraphAsymmErrors *h_d0_HLT_e24med_r_truth;
    TGraphAsymmErrors *h_d0_HLT_e24loose_r_truth;

    TGraphAsymmErrors *h_pt_MUFTF_r_FSFTF;
    TGraphAsymmErrors *h_d0_MUFTF_r_FSFTF;
    TGraphAsymmErrors *h_dEta_MUFTF_r_FSFTF;
    TGraphAsymmErrors *h_dPhi_MUFTF_r_FSFTF;

    TGraphAsymmErrors *h_pt_ELFTF_r_FSFTF;
    TGraphAsymmErrors *h_d0_ELFTF_r_FSFTF;
    TGraphAsymmErrors *h_dEta_ELFTF_r_FSFTF;
    TGraphAsymmErrors *h_dPhi_ELFTF_r_FSFTF;

    TGraphAsymmErrors *h_d0_eff_HLT_el_PT_LRTloose_r_truth;
    TGraphAsymmErrors *h_prodR_eff_HLT_el_PT_LRTloose_r_truth;
    TGraphAsymmErrors *h_d0_eff_HLT_el_PT_LRTloose_r_offlinesig;
    TGraphAsymmErrors *h_prodR_eff_HLT_el_PT_LRTloose_r_offlinesig;

    // Ntuple TTree output variables
    bool L1_EM22_Passed;
    bool L1_MU14FCH_Passed;
    bool L1_XE50_Passed;
    bool L1_J100_Passed;
    float l1_met;
    float hlt_met_trkmht;
    float hlt_met_pufit;
    float jet50_n, jet75_n, jet100_n;
    std::vector<float> *jet_pt, *jet_eta, *jet_phi;
    std::vector<float> *truthSignal_pt, *truthSignal_eta, *truthSignal_phi, *truthSignal_d0;
    std::vector<uint> *truthSignal_pdgid, *truthSignal_motherpdgid;

    // electron chain debugging
    SG::ReadHandleKey<xAOD::TrackParticleContainer> m_electronTracksKey{this, "ElectronTracksKey", "HLT_IDTrack_ElecLRT_IDTrig"};

    //////////////////
    // Functions
    //////////////////

    template <class A>
    void retrieveCollection(bool &flag, A *&collection, std::string collectionName);

    StatusCode::ErrorCode bookHisto(std::string info, std::vector<double> params);

    StatusCode::ErrorCode efficiencyHelper(std::string nameTitle, std::vector<double> params);
    StatusCode::ErrorCode finalizeEfficiencies();

    bool selectOfflineTrack(const xAOD::TrackParticle *track);
    bool selectOfflineTrackSignal(const xAOD::TrackParticle *track, uint matchParticleOrigin, const xAOD::TruthParticle *&part);
    bool selectFTFTrack(const xAOD::TrackParticle *track);
    bool selectlrtFTFTrack(const xAOD::TrackParticle *track);
    bool selectTruthTrack(const xAOD::TruthParticle *part);
    int countTruthLayers(const xAOD::TruthParticle *part);

    bool selectMuon(const xAOD::Muon *muon, bool isTrigger = false);
    bool selectElectron(const xAOD::Electron *electron, bool isTrigger = false);

    const std::vector<const xAOD::TruthParticle *> findTruthParticles(const xAOD::TruthParticleContainer *particles, std::vector<int> pdgid_vec, std::vector<int> parentPdgid_vec = std::vector<int>(), std::vector<int> excludeParentPdgid_vec = std::vector<int>(), int status = 1, bool selectTracks = false);

    bool testTruthParticle(const xAOD::TruthParticle *part, std::vector<int> pdgid_vec, std::vector<int> excludeParentPdgid_vec = std::vector<int>(), std::vector<int> parentPdgid_vec = std::vector<int>(), int status = 1);

    const xAOD::TruthParticle *particleHasParentPdgid(const xAOD::TruthParticle *part, std::vector<int> parentPdgid, bool directParent = false, int status = 0, bool firstLevel = true);

    // Return production radius in mm of truth particle based on production vertex
    double getProdR(const xAOD::TruthParticle *part);
    double getProdR(const xAOD::TrackParticle *track);
    double getDecayR(const xAOD::TruthParticle *part);

    double getProdZ(const xAOD::TruthParticle *part);
    double getProdZ(const xAOD::TrackParticle *track);


    double getCorrectedD0(const xAOD::TrackParticle *offPart);
    double getCorrectedD0Sig(const xAOD::TrackParticle *offPart);

    double getTruthPartD0(const xAOD::TrackParticle *offPart);
    double getTruthPartD0(const xAOD::TruthParticle *part);
    // double getTruthPartD0_straight_displep(const xAOD::TruthParticle *part);
    // double getTruthPartD0_article(const xAOD::TruthParticle *part);
    double getTruthPartD0_tida(const xAOD::TrackParticle *offPart);
    double getTruthPartD0_tida(const xAOD::TruthParticle *part);

    double getTruthPartD0_extrap(const xAOD::TrackParticle *offPart);
    double getTruthPartD0_extrap(const xAOD::TruthParticle *part);

    // Wrapper to get extrapolated decoration info if desired
    double getTruthEta(const xAOD::TruthParticle *part)
    {
        if (m_extrapolateTruth)
        {
            return -std::log(std::tan(part->auxdataConst<float>("theta") / 2));
        }
        else
        {
            return part->eta();
        }
    }

    // Wrapper to get extrapolated decoration info if desired
    double getTruthPhi(const xAOD::TruthParticle *part)
    {
        if (m_extrapolateTruth)
        {
            return part->auxdataConst<float>("phi");
        }
        else
        {
            return part->phi();
        }
    }

    //////////////////////////////////////
    // Delta R from eta and phi
    double getDeltaR(double etaA, double phiA, double etaB, double phiB)
    {
        double dEta = etaA - etaB;
        double dPhi = phiA - phiB;

        if (dPhi > M_PI)
            dPhi -= 2 * M_PI;
        if (dPhi < -M_PI)
            dPhi += 2 * M_PI;

        return sqrt(dEta * dEta + dPhi * dPhi);
    }

    ///////////////////////////////////
    // Return deltaR between two objects
    // General template
    ////////////////////////////////////
    template <class A, class B>
    double getDeltaR(const A *partA, const B *partB)
    {
        return getDeltaR(partA->eta(), partA->phi(), partB->eta(), partB->phi());
    }

    // Template, non pointer
    template <class A, class B>
    double getDeltaR(const A partA, const B partB)
    {
        return getDeltaR(partA.eta(), partA.phi(), partB.eta(), partB.phi());
    }

    //////////////////////////
    // Specialize for one object being a truth part
    template <class A>
    double getDeltaR(const A *partA, const xAOD::TruthParticle *partB)
    {
        return getDeltaR(partA->eta(), partA->phi(), getTruthEta(partB), getTruthPhi(partB));
    }

    // Flipped case for one objcet being truth part
    template <class B>
    double getDeltaR(const xAOD::TruthParticle *partA, const B *partB)
    {
        return getDeltaR(partB, partA);
    }

    // DeltaR for caching one part
    template <class B>
    double getDeltaR(double etaA, double phiA, const B *partB)
    {
        return getDeltaR(etaA, phiA, partB->eta(), partB->phi());
    }

    // Specialize for one object being a truth part
    //template <>
    double getDeltaR(double etaA, double phiA, const xAOD::TruthParticle *partB)
    {
        return getDeltaR(etaA, phiA, getTruthEta(partB), getTruthPhi(partB));
    }

    // For element link (for muon->track links).  
    double getDeltaR(const ElementLink<DataVector<xAOD::TrackParticle_v1>> &partALink, const ElementLink<DataVector<xAOD::TrackParticle_v1>> &partBLink)
    {
        if (!partALink.isValid())
        {
            std::cout << "Error in getDeltaR, partA link is invalid!" << std::endl;
            return -999.;
        }

        if (!partBLink.isValid())
        {
            std::cout << "Error in getDeltaR, partB link is invalid!" << std::endl;
            return -999.;
        }

        return getDeltaR((*partALink), (*partBLink));
    }

    // Return deltaR between two objects, with extra options
    template <class A, class B>
    bool matchDeltaR(const A *partA, const B *partB, double maxDR, double phiAdjust = 0, double etaAdjust = 0)
    {
        double dEta = partA->eta() - partB->eta();
        double dPhi = partA->phi() - partB->phi();

        if (dPhi > M_PI)
            dPhi -= 2 * M_PI;
        if (dPhi < -M_PI)
            dPhi += 2 * M_PI;

        // Adjust differences
        if (dEta > etaAdjust)
            dEta -= etaAdjust;
        else
            dEta = 0.;

        if (dPhi > phiAdjust)
            dPhi -= phiAdjust;
        else
            dPhi = 0.;

        return sqrt(dEta * dEta + dPhi * dPhi) < maxDR;
    }

    //////////////////////////////////////
    // Return deltaEta between two objects
    template <class A, class B>
    double getDeltaEta(const A *partA, const B *partB)
    {
        return partA->eta() - partB->eta();
    }

    // specialize for truth part
    template <class A>
    double getDeltaEta(const A *partA, const xAOD::TruthParticle *partB)
    {
        return partA->eta() - getTruthEta(partB);
    }

    template <class B>
    double getDeltaEta(const xAOD::TruthParticle *partA, const B *partB)
    {
        return getDeltaEta(partB, partA);
    }

    //////////////////////////////////////
    // Return deltaPhi between two objects
    template <class A, class B>
    double getDeltaPhi(const A *partA, const B *partB)
    {
        double dPhi = partA->phi() - partB->phi();

        if (dPhi > M_PI)
            dPhi -= 2 * M_PI;
        if (dPhi < -M_PI)
            dPhi += 2 * M_PI;

        return dPhi;
    }

    // specialize for truth part
    template <class A>
    double getDeltaPhi(const A *partA, const xAOD::TruthParticle *partB)
    {
        double dPhi = partA->phi() - getTruthPhi(partB);

        if (dPhi > M_PI)
            dPhi -= 2 * M_PI;
        if (dPhi < -M_PI)
            dPhi += 2 * M_PI;

        return dPhi;
    }

    template <class B>
    double getDeltaPhi(const xAOD::TruthParticle *partA, const B *partB)
    {
        return getDeltaPhi(partB, partA);
    }

    // Invert maps
    template <class A, class B>
    std::map<B, A> invertMap(std::map<A, B> mapIn)
    {
        std::map<B, A> mapOut;
        for (auto &i : mapIn)
        {
            mapOut.emplace(i.second, i.first);
        }
        return mapOut;
    }

    void matchAndFillHistos(std::vector<const xAOD::TrackParticle *> FTFtracks, std::vector<const xAOD::TrackParticle *> OfflineTracks, std::vector<const xAOD::TrackParticle *> OfflineTracksSignal);

    void fillPurHistos();

    template <class A, class B>
    std::map<A, B> matchCollections_dR(std::vector<A> ref, std::vector<B> test, double maxdR);

    template <class B>
    std::map<const xAOD::TruthParticle *, B> matchCollections_dR(std::vector<const xAOD::TruthParticle *> ref, std::vector<B> test, double maxdR);

    template <class T>
    bool findInVec(std::vector<T> vec, T item) { return (std::find(vec.begin(), vec.end(), item) != vec.end()); }

    std::vector<const xAOD::Electron *> getElectronChainFeatures(std::string chainName, bool isLRT = false, int leg = -1);
    std::vector<const xAOD::TrackParticle *> getElectronChainTrackFeatures(std::string chainName, int leg = -1);
    std::vector<const xAOD::IParticle *> getPhotonChainFeatures(std::string chainName, int leg = -1);

    std::vector<const TrigRoiDescriptor *> getElectronTnPRoI();

    std::vector<const xAOD::Muon *> getMuonChainFeatures(std::string chainName, bool isLRT = false, int leg = -1);
    std::vector<const xAOD::TrackParticle *> getMuonChainTrackFeatures(std::string chainName, bool isLRT = false, int leg = -1);
    std::vector<const xAOD::TrackParticle *> getMuonChainIDTrackFeatures(std::string chainName, bool isLRT = false, int leg = -1);
};

#endif
