#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
#inputFilePath = "/afs/cern.ch/work/l/longjon/Trigger/Athena_LRT/run/"
#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/399007_wMenu/"
#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/399108_wMenu/"
#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/399007_6hits/"

# centrally produced RDOs
#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/central_399007/"

#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.399023.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_500_0_1ns.AOD.020920_v0_AOD/"
#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.399056.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_700_0_1ns.AOD.020920_v0_AOD/"
#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.6hits.070920_v1_AOD/"


#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.500962.MGPy8EG_A14N23LO_N2C1p_150p7_150p35_150p0_MET75.AOD.140920_v4_AOD/"
#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.500963.MGPy8EG_A14N23LO_N2C1p_151p0_150p5_150p0_MET75.AOD.011020_v0_AOD/"

#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.140920_v4_AOD/"
#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.6hits.140920_v4_AOD"

# ttbar
#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.AOD.300920_v0_AOD/"

#data
# original
#inputFilePath = "/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.data18_13TeV.00360026.physics_EnhancedBias.r11276.combined/"

# with muon id track extended
inputFilePath= "/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.data18_13TeV.00360026.physics_EnhancedBias.AOD.121120_v1_muonIDd0ext_EXT0/"

#ROOT.SH.ScanDir().filePattern( 'AOD.399108.pool.root' ).scan( sh, inputFilePath )
#ROOT.SH.ScanDir().filePattern( 'AOD.399007.pool.root' ).scan( sh, inputFilePath )
#ROOT.SH.ScanDir().filePattern( 'AOD.399007_r100.pool.root' ).scan( sh, inputFilePath )
ROOT.SH.ScanDir().filePattern( '*AOD*.root*' ).scan( sh, inputFilePath )

#inputFilePath = "/afs/cern.ch/user/l/longjon/longjon_eos/Trigger/mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.recon.AOD.e5916_s3126_r9364"
#ROOT.SH.ScanDir().filePattern( 'AOD.11502452._001591.pool.root.1' ).scan( sh, inputFilePath )
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, -1)
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'simplexAODReaderEL', 'AnalysisAlg' )

alg.Debug = False # Extra printout
alg.makeNtuple = False # produce output ntuple
if alg.makeNtuple: job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))
alg.includeLRT = True # Include FTF LRT tracks
alg.printTriggerDecCount = False # Print out all trigger decisions
alg.doTruth = True # Loop over truth particles
alg.doJets = False # Loop over jets
alg.matchSizeR = 0.02 # Delta R used for matching FTF tracks to truth/offline
alg.Highd0Cut = 2 # Cut to be considered high d0
alg.EM_ChainName = "HLT_e26_lhmedium_nod0" # Chain used to retrieve ROIs
alg.MU_ChainName = "HLT_mu20_idperf" # Chain used to retrieve ROIs
alg.roiSize = 0.2 # ROI matching size
alg.EMPhiAdjust = 0. #subtract off from EM dphi in ROI mathcing to compute dR
alg.minTrackd0 = -1 # Min track d0
alg.minTrackpT = 20 # Min track pT
alg.maxTrackEta = 3 # Max track eta
alg.truthMaxProdR = 520 # matched truth track max production vertex radius in mm
alg.minHitsFTF = 8 # min hits on FTF tracks
alg.minJetpT = 80 # Jet pT Cut
alg.minHLTtrkmht = 60 # MET trkmht cut
alg.minHLTpufit = 60 # MET pufit cut
alg.signalParentPDGIDs = [1000015, 2000015]
alg.signalPDGIDs = [11,13]
if "N2" in inputFilePath: 
    alg.signalParentPDGIDs = [1000023, 1000024]
    alg.signalPDGIDs = [211,11,13]
if "Slep" in inputFilePath: alg.signalParentPDGIDs = [1000011, 1000013, 1000015, 2000011, 2000013, 2000015]

# Add our algorithm to the job
job.algsAdd( alg )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
