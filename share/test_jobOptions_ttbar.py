import datetime
from glob import glob
import hashlib

import AthenaPoolCnvSvc.ReadAthenaPool

runTimeStr = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")

if 'key' not in dir():
    key = "test_"

if 'isRun3' not in dir():
    isRun3 = False
isRun3 = True

#ART from https://atlas-art-data.web.cern.ch/atlas-art-data/local-output/master/Athena/x86_64-centos7-gcc8-opt/2021-02-16T2101/TrigAnalysisTest/test_trigAna_RDOtoAOD_v1Dev_build/AOD.pool.root
#inputFile = "/eos/atlas/user/l/longjon/Trigger/AODs/AOD.art.pool.root"

inputFiles = [
    "AOD.pool.root"
]


#inputFiles = glob("/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.090221_v0_EXT0/user*root")

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
if athenaCommonFlags.FilesInput() != []:
    inputFiles = athenaCommonFlags.FilesInput()

    if 'test' in key:
        if 'afs' in inputFiles[0]:
            key = "test_" 
        elif 'user' in inputFiles[0]:
            # works ok for user.longjon.28365459.EXT0._000004.AOD.pool.root
            key+=inputFiles[0].split(".")[2]+"-"+inputFiles[0].split(".")[-4]
        else:
            # AOD.28429144._000001.pool.root.1
            try:
                key+=inputFiles[0].split(".")[1]+"-"+inputFiles[0].split(".")[-5]+"-"+inputFiles[0].split(".")[-4]
            except IndexError:
                print("Issue generating key, setting to test_")
                key = "test_"
        
else:
    ServiceMgr.EventSelector.InputCollections = inputFiles

from AthenaConfiguration.AllConfigFlags import ConfigFlags
if ConfigFlags.Input.Files != []:
    ConfigFlags.Input.Files = inputFiles
    ConfigFlags.lock()

# Determine if input is DAOD, probably a better way
isDAOD = "daod" in inputFiles[0].lower()

# Setup histogram output
svcMgr += CfgMgr.THistSvc()
#svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms
hash = hashlib.md5(inputFiles[0].encode("utf-8")).hexdigest()[0:4]
svcMgr.THistSvc.Output += ["ANALYSIS DATAFILE='simplexAODReaderEL.hists-{}-{}-{}.root' OPT='RECREATE'".format(runTimeStr,key,hash)]


# New setup suggested by Tim (needed for newer releases eg 22.0.43)
from AthenaCommon.Configurable import Configurable
from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
from AthenaCommon.Configurable import ConfigurableCABehavior
with ConfigurableCABehavior():
    tdtAcc = TrigDecisionToolCfg(ConfigFlags)
from AthenaConfiguration.ComponentAccumulator import conf2toConfigurable, appendCAtoAthena
appendCAtoAthena( tdtAcc )
tdt = conf2toConfigurable(tdtAcc.getPrimary())

# Setup MC Truth Classifier
from  MCTruthClassifier.MCTruthClassifierConf import MCTruthClassifier
mctc = CfgMgr.MCTruthClassifier("MCTruthClassifier", ParticleCaloExtensionTool = "")
if isDAOD: mctc.xAODTruthParticleContainerName = "TruthBSMWithDecayParticles"
ToolSvc += mctc

# Setup trigger matching tools
mfct = CfgMgr.Trig__MatchFromCompositeTool("MatchFromCompositeTool")
ToolSvc += mfct

mfctLRT = CfgMgr.Trig__MatchFromCompositeTool("MatchFromCompositeToolLRT",
                                               InputPrefix="LRTTrigMatch_")    
ToolSvc += mfctLRT

r3mt = CfgMgr.Trig__R3MatchingTool("R3MatchingTool",
                                       #,OutputLevel=DEBUG
                                       TrigDecisionTool = tdt)
ToolSvc += r3mt

# Create a MuonSelectionTool if we do not yet have one 
if not hasattr(ToolSvc,"MyMuonSelectionTool"):        
    from MuonSelectorTools.MuonSelectorToolsConf import CP__MuonSelectionTool
    ToolSvc += CP__MuonSelectionTool("MyMuonSelectionTool",
                                    MaxEta=2.7,
                                    MuQuality=1, # Medium Muon
                                    PixCutOff=True,
                                    TurnOffMomCorr = True,
                                    IsRun3Geo = isRun3,
                                    AllowSettingGeometryOnTheFly = True) 

# Electron LLH Tool
# Simliar to here https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Trigger/TrigHypothesis/TrigEgammaHypo/python/TrigEgammaPrecisionElectronHypoTool.py#0347
# https://its.cern.ch/jira/browse/ATLINFR-4131
# /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/ElectronPhotonSelectorTools/trigger/rel22_20210611
from ElectronPhotonSelectorTools.ElectronPhotonSelectorToolsConf import AsgElectronLikelihoodTool
ToolSvc += AsgElectronLikelihoodTool("AsgElectronLHLooseSelectorNoPix",
                                     ConfigFile = "ElectronPhotonSelectorTools/trigger/rel22_20210611/ElectronLikelihoodLooseTriggerConfig_NoPix.conf",
                                     skipDeltaPoverP = True)

# limit the number of events (for testing purposes)
theApp.EvtMax = -1

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'simplexAODReaderEL', 'AnalysisAlg' )
alg.trigDecTool = tdt
alg.trigMatchTool    = r3mt if isRun3 else mfct
alg.trigMatchToolLRT = r3mt if isRun3 else mfctLRT
alg.MuonSelectionTool = ToolSvc.MyMuonSelectionTool
alg.ElectronLLHTool = ToolSvc.AsgElectronLHLooseSelectorNoPix
alg.truthClassifier = ToolSvc.MCTruthClassifier

alg.Debug = False # Extra printout
alg.isDAOD = isDAOD # run on DAOD
alg.makeNtuple = False # produce output ntuple
if alg.makeNtuple: job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))
alg.RootStreamName = 'ANALYSIS'
alg.includeLRT = True # Include FTF LRT tracks
alg.printTriggerDecCount = True # Print out all trigger decisions
alg.doTruth = True # Loop over truth particles
alg.extrapolateTruth = False # Extrapolate truth particles back to origin for dR matching
alg.matchSizeR = 0.02 # Delta R used for matching FTF tracks to truth/offline
alg.Highd0Cut = 2 # Cut to be considered high d0
alg.roiSize = 0.4 # ROI matching size
alg.EMPhiAdjust = 0 #subtract off from EM dphi in ROI mathcing to compute dR
alg.minTrackd0 = -1 # Min track d0
alg.minTrackpT = 24 # Min track pT
alg.maxTrackEta = 2.5 # Max track eta
alg.truthMaxProdR = 300 # max truth track production vertex radius in mm, was 520
alg.truthMinDecayR = -1 # min truth track decay vertex radius in mm
alg.minTruthLayers = 8 # min number of layers truth particle should pass
alg.SiLayers = [33.25, 50.5, 88.5, 122.5, 299, 299.01, 371, 371.01, 443, 443.01, 514, 514.01] # radius layers of silicon detector in mm.  SCT are double layers--this is a hack
alg.maxBarcode = -1 # max barcode for truth particles.  This is probably signal model dependent 
alg.minHitsFTF = -1 # min hits on FTF tracks, was 8
alg.minJetpT = 80 # Jet pT Cut
alg.minHLTtrkmht = 60 # MET trkmht cut
alg.minHLTpufit = 60 # MET pufit cut
alg.useOnlyOfflineSignal = False # only fill offline track container with signal tracks
alg.excludeParentPDGIDs = [22] # if truth particle has one of these as a parent, it's not a real signal
alg.signalParentPDGIDs = [6] # 16, 46
alg.signalPDGIDs = [11,13]

#if "N2" in inputFiles[0]: 
#    alg.signalParentPDGIDs = [1000023, 1000024]
#    alg.signalPDGIDs = [211,11,13]
#if "Slep" in inputFiles[0]: alg.signalParentPDGIDs = [1000011, 1000013, 1000015, 2000011, 2000013, 2000015]

athAlgSeq += alg



# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")
