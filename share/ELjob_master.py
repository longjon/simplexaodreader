#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'test',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

# Master
# local test
#inputFilePath= "/afs/cern.ch/work/l/longjon/Trigger/Athena_master/run/"
#ROOT.SH.ScanDir().filePattern( 'AOD.pool.root' ).scan( sh, inputFilePath )

# larger grid job
#inputFilePath="/eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.090221_v0_EXT0/"
#ROOT.SH.ScanDir().filePattern( '*10.AOD.pool.root' ).scan( sh, inputFilePath )

# trigger reprocessing
#inputFilePath="/eos/atlas/user/l/longjon/Trigger/AODs/data18_13TeV.00360026.physics_EnhancedBias.recon.AOD.r12367_tid24019881_00_reprotest/"
#ROOT.SH.ScanDir().filePattern( 'AOD.24019881._001288.pool.root.1' ).scan( sh, inputFilePath )

#art
inputFilePath="/eos/atlas/user/l/longjon/Trigger/AODs/"
ROOT.SH.ScanDir().filePattern( 'AOD.art.pool.root' ).scan( sh, inputFilePath )

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 100)
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'simplexAODReaderEL', 'AnalysisAlg' )

# Add private tools
#from AnaAlgorithm.DualUseConfig import addPrivateTool
#addPrivateTool(alg, "xAODConfigTool","xAODConfigTool")
#addPrivateTool(alg, "TrigDecisionTool","TrigDecisionTool")
#alg.TrigDecisionTool.TrigDecisionKey = "xTrigDecision"
#alg.TrigDecisionTool.ConfigTool = alg.xAODConfigTool


alg.Debug = False # Extra printout
alg.makeNtuple = False # produce output ntuple
if alg.makeNtuple: job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))
alg.includeLRT = True # Include FTF LRT tracks
alg.printTriggerDecCount = True # Print out all trigger decisions
alg.doTruth = True # Loop over truth particles
alg.doJets = False # Loop over jets
alg.matchSizeR = 0.02 # Delta R used for matching FTF tracks to truth/offline
alg.Highd0Cut = 2 # Cut to be considered high d0
#alg.EM_ChainName = "HLT_e5_idperf_L1EM3" # Chain used to retrieve ROIs
#alg.MU_ChainName = "HLT_mu6_idperf_L1MU6" # Chain used to retrieve ROIs
alg.roiSize = 0.2 # ROI matching size
alg.EMPhiAdjust = 0 #subtract off from EM dphi in ROI mathcing to compute dR
alg.minTrackd0 = -1 # Min track d0
alg.minTrackpT = 1 # Min track pT
alg.maxTrackEta = 3 # Max track eta
alg.truthMaxProdR = 520 # match truth track production vertex radius in mm
alg.minHitsFTF = 8 # min hits on FTF tracks
alg.minJetpT = 80 # Jet pT Cut
alg.minHLTtrkmht = 60 # MET trkmht cut
alg.minHLTpufit = 60 # MET pufit cut
alg.signalParentPDGIDs = [1000015, 2000015]
alg.signalPDGIDs = [11,13]
if "N2" in inputFilePath: 
    alg.signalParentPDGIDs = [1000023, 1000024]
    alg.signalPDGIDs = [211,11,13]
if "Slep" in inputFilePath: alg.signalParentPDGIDs = [1000011, 1000013, 1000015, 2000011, 2000013, 2000015]

# Add our algorithm to the job
job.algsAdd( alg )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
