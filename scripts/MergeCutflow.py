#! /usr/bin/env python

from glob import glob

files = glob("*pool*.txt")
outname = "merged_cutflow.txt"

order = []
values = {}

for fname in files:

    with open(fname, 'r') as f:
        for line in f:
            key = line.split('=')[0]
            try:
                value = int(line.split('=')[1].split()[0].strip())
                
                if key not in values:
                    order.append(key)
                    values[key] = value
                else:
                    values[key] += value
            except:
                if key not in order:
                    order.append(key)


with open(outname, 'w') as f:

    for key in order:
        if key in values:
            f.write("{} = {}\n".format(key, values[key]))
        else:
            f.write(key)

