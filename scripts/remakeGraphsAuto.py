#!/usr/bin/env python

from ROOT import TGraphAsymmErrors
from ROOT import TFile
from ROOT import TH1F


inputMergedFile = "merged.root"

f_in = TFile.Open(inputMergedFile, "read")
f_out = TFile.Open("merged_graphs.root","recreate")

def hist(histname):
    h = TH1F(f_in.Get(histname))
    #h.Rebin(2)

    return h


listOfEffHistos = []

for k in f_in.GetListOfKeys():
    # last part of name is _n
    if k.GetName()[-2:] == "_n":
        listOfEffHistos.append(k.GetName()[:-2])
        



for effName in listOfEffHistos:
    tg  = TGraphAsymmErrors()
    tg.SetName(effName)
    
    xtitle = hist(effName+"_n").GetXaxis().GetTitle()
    tg.SetTitle("{};{}; Efficiency".format(effName,xtitle))

    tg.Divide( hist(effName + "_n"), hist(effName + "_d") )
    tg.Write()


f_in.Close()
f_out.Close()

