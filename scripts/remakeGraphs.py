#!/usr/bin/env python

from ROOT import TGraphAsymmErrors
from ROOT import TFile
from ROOT import TH1F

import inspect
#print(inspect.currentframe().f_lineno)

inputMergedFile = "merged.root"

f_in = TFile.Open(inputMergedFile, "read")
f_out = TFile.Open("merged_graphs.root","recreate")

def hist(histname):
    h = TH1F(f_in.Get(histname) )
    #h.Rebin(2)

    return h
    

# Setup tgraphs

# Efficiency
h_d0_eff_FTFcomb_r_offline =  TGraphAsymmErrors()
h_d0_eff_FTFcomb_r_offline.SetName("h_d0_eff_FTFcomb_r_offline")
h_d0_eff_FTFcomb_r_offline.SetTitle("h_d0_eff_FTFcomb_r_offline; d_{0} [mm]; Efficiency")

h_d0_el_eff_FTFcomb_r_offline =  TGraphAsymmErrors()
h_d0_el_eff_FTFcomb_r_offline.SetName("h_d0_el_eff_FTFcomb_r_offline")
h_d0_el_eff_FTFcomb_r_offline.SetTitle("h_d0_el_eff_FTFcomb_r_offline; d_{0} [mm]; Efficiency")

h_d0_mu_eff_FTFcomb_r_offline =  TGraphAsymmErrors()
h_d0_mu_eff_FTFcomb_r_offline.SetName("h_d0_mu_eff_FTFcomb_r_offline")
h_d0_mu_eff_FTFcomb_r_offline.SetTitle("h_d0_mu_eff_FTFcomb_r_offline; d_{0} [mm]; Efficiency")

h_pt_eff_FTFcomb_r_offline =  TGraphAsymmErrors()
h_pt_eff_FTFcomb_r_offline.SetName("h_pt_eff_FTFcomb_r_offline")
h_pt_eff_FTFcomb_r_offline.SetTitle("h_pt_eff_FTFcomb_r_offline; p_{T} [GeV]; Efficiency")

h_eta_eff_FTFcomb_r_offline =  TGraphAsymmErrors()
h_eta_eff_FTFcomb_r_offline.SetName("h_eta_eff_FTFcomb_r_offline")
h_eta_eff_FTFcomb_r_offline.SetTitle("h_eta_eff_FTFcomb_r_offline; #eta; Efficiency")

h_vtxR_eff_FTFcomb_r_offline =  TGraphAsymmErrors()
h_vtxR_eff_FTFcomb_r_offline.SetName("h_vtxR_eff_FTFcomb_r_offline")
h_vtxR_eff_FTFcomb_r_offline.SetTitle("h_vtxR_eff_FTFcomb_r_offline; Prod Vertex Radius [mm]; Efficiency")

# Purity
h_d0_pur_FTFcomb_r_offline =  TGraphAsymmErrors()
h_d0_pur_FTFcomb_r_offline.SetName("h_d0_pur_FTFcomb_r_offline")
h_d0_pur_FTFcomb_r_offline.SetTitle("h_d0_pur_FTFcomb_r_offline; d_{0} [mm]; Purity")

h_pt_pur_FTFcomb_r_offline =  TGraphAsymmErrors()
h_pt_pur_FTFcomb_r_offline.SetName("h_pt_pur_FTFcomb_r_offline")
h_pt_pur_FTFcomb_r_offline.SetTitle("h_pt_pur_FTFcomb_r_offline; p_{T} [GeV]; Purity")

h_eta_pur_FTFcomb_r_offline =  TGraphAsymmErrors()
h_eta_pur_FTFcomb_r_offline.SetName("h_eta_pur_FTFcomb_r_offline")
h_eta_pur_FTFcomb_r_offline.SetTitle("h_eta_pur_FTFcomb_r_offline; #eta; Purity")

h_vtxR_pur_FTFcomb_r_offline =  TGraphAsymmErrors()
h_vtxR_pur_FTFcomb_r_offline.SetName("h_vtxR_pur_FTFcomb_r_offline")
h_vtxR_pur_FTFcomb_r_offline.SetTitle("h_vtxR_pur_FTFcomb_r_offline; Prod Vertex Radius [mm]; Purity")

# truth eff
h_d0_eff_FTFcomb_r_truth =  TGraphAsymmErrors()
h_d0_eff_FTFcomb_r_truth.SetName("h_d0_eff_FTFcomb_r_truth")
h_d0_eff_FTFcomb_r_truth.SetTitle("h_d0_eff_FTFcomb_r_truth; d_{0} [mm]; Efficiency")

h_d0_eff_FTFcomb_r_truth_el =  TGraphAsymmErrors()
h_d0_eff_FTFcomb_r_truth_el.SetName("h_d0_eff_FTFcomb_r_truth_el")
h_d0_eff_FTFcomb_r_truth_el.SetTitle("h_d0_eff_FTFcomb_r_truth_el; d_{0} [mm]; Efficiency")

h_d0_eff_FTFcomb_r_truth_mu =  TGraphAsymmErrors()
h_d0_eff_FTFcomb_r_truth_mu.SetName("h_d0_eff_FTFcomb_r_truth_mu")
h_d0_eff_FTFcomb_r_truth_mu.SetTitle("h_d0_eff_FTFcomb_r_truth_mu; d_{0} [mm]; Efficiency")

h_pt_eff_FTFcomb_r_truth =  TGraphAsymmErrors()
h_pt_eff_FTFcomb_r_truth.SetName("h_pt_eff_FTFcomb_r_truth")
h_pt_eff_FTFcomb_r_truth.SetTitle("h_pt_eff_FTFcomb_r_truth; p_{T} [GeV]; Efficiency")

h_vtxR_eff_FTFcomb_r_truth =  TGraphAsymmErrors()
h_vtxR_eff_FTFcomb_r_truth.SetName("h_vtxR_eff_FTFcomb_r_truth")
h_vtxR_eff_FTFcomb_r_truth.SetTitle("h_vtxR_eff_FTFcomb_r_truth; Prod Vertex Radius [mm]; Efficiency")

# offline eff
h_d0_eff_offline_r_truth = TGraphAsymmErrors()
h_d0_eff_offline_r_truth.SetName("h_d0_eff_offline_r_truth")
h_d0_eff_offline_r_truth.SetTitle("h_d0_eff_offline_r_truth; d_{0} [mm]; Efficiency")

h_pt_eff_offline_r_truth = TGraphAsymmErrors()
h_pt_eff_offline_r_truth.SetName("h_pt_eff_offline_r_truth")
h_pt_eff_offline_r_truth.SetTitle("h_pt_eff_offline_r_truth; p_{T} [GeV]; Efficiency")

h_vtxR_eff_offline_r_truth = TGraphAsymmErrors()
h_vtxR_eff_offline_r_truth.SetName("h_vtxR_eff_offline_r_truth")
h_vtxR_eff_offline_r_truth.SetTitle("h_vtxR_eff_offline_r_truth; Prod Vertex Radius [mm]; Efficiency")
    


# Trigger Eff Offline
# L1MU
h_pt_L1MU14FCH_r_offline =  TGraphAsymmErrors()
h_pt_L1MU14FCH_r_offline.SetName("h_pt_L1MU14FCH_r_offline")
h_pt_L1MU14FCH_r_offline.SetTitle("h_pt_L1MU14FCH_r_offline; p_{T} [GeV]; Efficiency")

h_d0_L1MU14FCH_r_offline =  TGraphAsymmErrors()
h_d0_L1MU14FCH_r_offline.SetName("h_d0_L1MU14FCH_r_offline")
h_d0_L1MU14FCH_r_offline.SetTitle("h_d0_L1MU14FCH_r_offline; d_{0} [mm]; Efficiency")

h_vtxR_L1MU14FCH_r_offline =  TGraphAsymmErrors()
h_vtxR_L1MU14FCH_r_offline.SetName("h_vtxR_L1MU14FCH_r_offline")
h_vtxR_L1MU14FCH_r_offline.SetTitle("h_vtxR_L1MU14FCH_r_offline; Prod Radius [mm]; Efficiency")

# L1_EM
h_pt_L1eEM26M_r_offline =  TGraphAsymmErrors()
h_pt_L1eEM26M_r_offline.SetName("h_pt_L1eEM26M_r_offline")
h_pt_L1eEM26M_r_offline.SetTitle("h_pt_L1eEM26M_r_offline; p_{T} [GeV]; Efficiency")

h_d0_L1eEM26M_r_offline =  TGraphAsymmErrors()
h_d0_L1eEM26M_r_offline.SetName("h_d0_L1eEM26M_r_offline")
h_d0_L1eEM26M_r_offline.SetTitle("h_d0_L1eEM26M_r_offline; d_{0} [mm]; Efficiency")

h_vtxR_L1eEM26M_r_offline =  TGraphAsymmErrors()
h_vtxR_L1eEM26M_r_offline.SetName("h_vtxR_L1eEM26M_r_offline")
h_vtxR_L1eEM26M_r_offline.SetTitle("h_vtxR_L1eEM26M_r_offline; Prod Radius [mm]; Efficiency")

# Trigger Eff Truth
# L1MU
h_pt_L1MU14FCH_r_truth =  TGraphAsymmErrors()
h_pt_L1MU14FCH_r_truth.SetName("h_pt_L1MU14FCH_r_truth")
h_pt_L1MU14FCH_r_truth.SetTitle("h_pt_L1MU14FCH_r_truth; p_{T} [GeV]; Efficiency")

h_d0_L1MU14FCH_r_truth =  TGraphAsymmErrors()
h_d0_L1MU14FCH_r_truth.SetName("h_d0_L1MU14FCH_r_truth")
h_d0_L1MU14FCH_r_truth.SetTitle("h_d0_L1MU14FCH_r_truth; d_{0} [mm]; Efficiency")

h_vtxR_L1MU14FCH_r_truth =  TGraphAsymmErrors()
h_vtxR_L1MU14FCH_r_truth.SetName("h_vtxR_L1MU14FCH_r_truth")
h_vtxR_L1MU14FCH_r_truth.SetTitle("h_vtxR_L1MU14FCH_r_truth; Prod Radius [mm]; Efficiency")

# L1_EM
h_pt_L1eEM26M_r_truth =  TGraphAsymmErrors()
h_pt_L1eEM26M_r_truth.SetName("h_pt_L1eEM26M_r_truth")
h_pt_L1eEM26M_r_truth.SetTitle("h_pt_L1eEM26M_r_truth; p_{T} [GeV]; Efficiency")

h_d0_L1eEM26M_r_truth =  TGraphAsymmErrors()
h_d0_L1eEM26M_r_truth.SetName("h_d0_L1eEM26M_r_truth")
h_d0_L1eEM26M_r_truth.SetTitle("h_d0_L1eEM26M_r_truth; d_{0} [mm]; Efficiency")

h_vtxR_L1eEM26M_r_truth =  TGraphAsymmErrors()
h_vtxR_L1eEM26M_r_truth.SetName("h_vtxR_L1eEM26M_r_truth")
h_vtxR_L1eEM26M_r_truth.SetTitle("h_vtxR_L1eEM26M_r_truth; Prod Radius [mm]; Efficiency")



# Muon FTF eff vs FS FTF
h_pt_MUFTF_r_FSFTF = TGraphAsymmErrors()
h_pt_MUFTF_r_FSFTF.SetName("h_pt_MUFTF_r_FSFTF")
h_pt_MUFTF_r_FSFTF.SetTitle("h_pt_MUFTF_r_FSFTF; p_{T} [GeV]; Efficiency")

h_d0_MUFTF_r_FSFTF = TGraphAsymmErrors()
h_d0_MUFTF_r_FSFTF.SetName("h_d0_MUFTF_r_FSFTF")
h_d0_MUFTF_r_FSFTF.SetTitle("h_d0_MUFTF_r_FSFTF; d_{0} [mm]; Efficiency")

h_dEta_MUFTF_r_FSFTF = TGraphAsymmErrors()
h_dEta_MUFTF_r_FSFTF.SetName("h_dEta_MUFTF_r_FSFTF")
h_dEta_MUFTF_r_FSFTF.SetTitle("h_dEta_MUFTF_r_FSFTF; #Delta#eta(track,RoI); Efficiency")

h_dPhi_MUFTF_r_FSFTF = TGraphAsymmErrors()
h_dPhi_MUFTF_r_FSFTF.SetName("h_dPhi_MUFTF_r_FSFTF")
h_dPhi_MUFTF_r_FSFTF.SetTitle("h_dPhi_MUFTF_r_FSFTF; #Delta#phi(track,RoI); Efficiency")


# new muon triggers
h_eta_eff_HLT_mu24_r_offlinesig = TGraphAsymmErrors()
h_eta_eff_HLT_mu24_r_offlinesig.SetName("h_eta_eff_HLT_mu24_r_offlinesig")
h_eta_eff_HLT_mu24_r_offlinesig.SetTitle("h_eta_eff_HLT_mu24_r_offlinesig; #eta; Efficiency")

h_eta_eff_HLT_mu24_LRTloose_r_offlinesig = TGraphAsymmErrors()
h_eta_eff_HLT_mu24_LRTloose_r_offlinesig.SetName("h_eta_eff_HLT_mu24_LRTloose_r_offlinesig")
h_eta_eff_HLT_mu24_LRTloose_r_offlinesig.SetTitle("h_eta_eff_HLT_mu24_LRTloose_r_offlinesig; #eta; Efficiency")

h_eta_eff_HLT_mu24_comb_r_offlinesig = TGraphAsymmErrors()
h_eta_eff_HLT_mu24_comb_r_offlinesig.SetName("h_eta_eff_HLT_mu24_comb_r_offlinesig")
h_eta_eff_HLT_mu24_comb_r_offlinesig.SetTitle("h_eta_eff_HLT_mu24_comb_r_offlinesig; #eta; Efficiency")

h_z0_eff_HLT_mu24_r_offlinesig = TGraphAsymmErrors()
h_z0_eff_HLT_mu24_r_offlinesig.SetName("h_z0_eff_HLT_mu24_r_offlinesig")
h_z0_eff_HLT_mu24_r_offlinesig.SetTitle("h_z0_eff_HLT_mu24_r_offlinesig; z_{0} [mm]; Efficiency")

h_z0_eff_HLT_mu24_LRTloose_r_offlinesig = TGraphAsymmErrors()
h_z0_eff_HLT_mu24_LRTloose_r_offlinesig.SetName("h_z0_eff_HLT_mu24_LRTloose_r_offlinesig")
h_z0_eff_HLT_mu24_LRTloose_r_offlinesig.SetTitle("h_z0_eff_HLT_mu24_LRTloose_r_offlinesig; z_{0} [mm]; Efficiency")

h_z0_eff_HLT_mu24_comb_r_offlinesig = TGraphAsymmErrors()
h_z0_eff_HLT_mu24_comb_r_offlinesig.SetName("h_z0_eff_HLT_mu24_comb_r_offlinesig")
h_z0_eff_HLT_mu24_comb_r_offlinesig.SetTitle("h_z0_eff_HLT_mu24_comb_r_offlinesig; z_{0} [mm]; Efficiency")

h_d0_eff_HLT_mu24_r_offlinesig = TGraphAsymmErrors()
h_d0_eff_HLT_mu24_r_offlinesig.SetName("h_d0_eff_HLT_mu24_r_offlinesig")
h_d0_eff_HLT_mu24_r_offlinesig.SetTitle("h_d0_eff_HLT_mu24_r_offlinesig; d_{0} [mm]; Efficiency")

h_d0_eff_HLT_mu24_LRTloose_r_offlinesig = TGraphAsymmErrors()
h_d0_eff_HLT_mu24_LRTloose_r_offlinesig.SetName("h_d0_eff_HLT_mu24_LRTloose_r_offlinesig")
h_d0_eff_HLT_mu24_LRTloose_r_offlinesig.SetTitle("h_d0_eff_HLT_mu24_LRTloose_r_offlinesig; d_{0} [mm]; Efficiency")

h_d0_eff_HLT_mu24_comb_r_offlinesig = TGraphAsymmErrors()
h_d0_eff_HLT_mu24_comb_r_offlinesig.SetName("h_d0_eff_HLT_mu24_comb_r_offlinesig")
h_d0_eff_HLT_mu24_comb_r_offlinesig.SetTitle("h_d0_eff_HLT_mu24_comb_r_offlinesig; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_mu24_r_offlinesig = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_mu24_r_offlinesig.SetName("h_d0_zoom_eff_HLT_mu24_r_offlinesig")
h_d0_zoom_eff_HLT_mu24_r_offlinesig.SetTitle("h_d0_zoom_eff_HLT_mu24_r_offlinesig; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig.SetName("h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig")
h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig.SetTitle("h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig.SetName("h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig")
h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig.SetTitle("h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig; d_{0} [mm]; Efficiency")

h_prodR_eff_HLT_mu24_r_offlinesig = TGraphAsymmErrors()
h_prodR_eff_HLT_mu24_r_offlinesig.SetName("h_prodR_eff_HLT_mu24_r_offlinesig")
h_prodR_eff_HLT_mu24_r_offlinesig.SetTitle("h_prodR_eff_HLT_mu24_r_offlinesig; Prod Radius [mm]; Efficiency")

h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig = TGraphAsymmErrors()
h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig.SetName("h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig")
h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig.SetTitle("h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig; Prod Radius [mm]; Efficiency")

h_prodR_eff_HLT_mu24_comb_r_offlinesig = TGraphAsymmErrors()
h_prodR_eff_HLT_mu24_comb_r_offlinesig.SetName("h_prodR_eff_HLT_mu24_comb_r_offlinesig")
h_prodR_eff_HLT_mu24_comb_r_offlinesig.SetTitle("h_prodR_eff_HLT_mu24_comb_r_offlinesig; Prod Radius [mm]; Efficiency")

h_pt_eff_HLT_mu24_r_offlinesig = TGraphAsymmErrors()
h_pt_eff_HLT_mu24_r_offlinesig.SetName("h_pt_eff_HLT_mu24_r_offlinesig")
h_pt_eff_HLT_mu24_r_offlinesig.SetTitle("h_pt_eff_HLT_mu24_r_offlinesig; p_{T} [GeV]; Efficiency")

h_pt_eff_HLT_mu24_LRTloose_r_offlinesig = TGraphAsymmErrors()
h_pt_eff_HLT_mu24_LRTloose_r_offlinesig.SetName("h_pt_eff_HLT_mu24_LRTloose_r_offlinesig")
h_pt_eff_HLT_mu24_LRTloose_r_offlinesig.SetTitle("h_pt_eff_HLT_mu24_LRTloose_r_offlinesig; p_{T} [GeV]; Efficiency")

h_pt_eff_HLT_mu24_comb_r_offlinesig = TGraphAsymmErrors()
h_pt_eff_HLT_mu24_comb_r_offlinesig.SetName("h_pt_eff_HLT_mu24_comb_r_offlinesig")
h_pt_eff_HLT_mu24_comb_r_offlinesig.SetTitle("h_pt_eff_HLT_mu24_comb_r_offlinesig; p_{T} [GeV]; Efficiency")
##########################

# muon triggers with respect to offline muon
h_eta_eff_HLT_mu24_r_muon = TGraphAsymmErrors()
h_eta_eff_HLT_mu24_r_muon.SetName("h_eta_eff_HLT_mu24_r_muon")
h_eta_eff_HLT_mu24_r_muon.SetTitle("h_eta_eff_HLT_mu24_r_muon; #eta; Efficiency")

h_eta_eff_HLT_mu24_LRTloose_r_muon = TGraphAsymmErrors()
h_eta_eff_HLT_mu24_LRTloose_r_muon.SetName("h_eta_eff_HLT_mu24_LRTloose_r_muon")
h_eta_eff_HLT_mu24_LRTloose_r_muon.SetTitle("h_eta_eff_HLT_mu24_LRTloose_r_muon; #eta; Efficiency")

h_eta_eff_HLT_mu24_comb_r_muon = TGraphAsymmErrors()
h_eta_eff_HLT_mu24_comb_r_muon.SetName("h_eta_eff_HLT_mu24_comb_r_muon")
h_eta_eff_HLT_mu24_comb_r_muon.SetTitle("h_eta_eff_HLT_mu24_comb_r_muon; #eta; Efficiency")

h_z0_eff_HLT_mu24_r_muon = TGraphAsymmErrors()
h_z0_eff_HLT_mu24_r_muon.SetName("h_z0_eff_HLT_mu24_r_muon")
h_z0_eff_HLT_mu24_r_muon.SetTitle("h_z0_eff_HLT_mu24_r_muon; z_{0} [mm]; Efficiency")

h_z0_eff_HLT_mu24_LRTloose_r_muon = TGraphAsymmErrors()
h_z0_eff_HLT_mu24_LRTloose_r_muon.SetName("h_z0_eff_HLT_mu24_LRTloose_r_muon")
h_z0_eff_HLT_mu24_LRTloose_r_muon.SetTitle("h_z0_eff_HLT_mu24_LRTloose_r_muon; z_{0} [mm]; Efficiency")

h_z0_eff_HLT_mu24_comb_r_muon = TGraphAsymmErrors()
h_z0_eff_HLT_mu24_comb_r_muon.SetName("h_z0_eff_HLT_mu24_comb_r_muon")
h_z0_eff_HLT_mu24_comb_r_muon.SetTitle("h_z0_eff_HLT_mu24_comb_r_muon; z_{0} [mm]; Efficiency")

h_d0_eff_HLT_mu24_r_muon = TGraphAsymmErrors()
h_d0_eff_HLT_mu24_r_muon.SetName("h_d0_eff_HLT_mu24_r_muon")
h_d0_eff_HLT_mu24_r_muon.SetTitle("h_d0_eff_HLT_mu24_r_muon; d_{0} [mm]; Efficiency")

h_d0_eff_HLT_mu24_LRTloose_r_muon = TGraphAsymmErrors()
h_d0_eff_HLT_mu24_LRTloose_r_muon.SetName("h_d0_eff_HLT_mu24_LRTloose_r_muon")
h_d0_eff_HLT_mu24_LRTloose_r_muon.SetTitle("h_d0_eff_HLT_mu24_LRTloose_r_muon; d_{0} [mm]; Efficiency")

h_d0_eff_HLT_mu24_comb_r_muon = TGraphAsymmErrors()
h_d0_eff_HLT_mu24_comb_r_muon.SetName("h_d0_eff_HLT_mu24_comb_r_muon")
h_d0_eff_HLT_mu24_comb_r_muon.SetTitle("h_d0_eff_HLT_mu24_comb_r_muon; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_mu24_r_muon = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_mu24_r_muon.SetName("h_d0_zoom_eff_HLT_mu24_r_muon")
h_d0_zoom_eff_HLT_mu24_r_muon.SetTitle("h_d0_zoom_eff_HLT_mu24_r_muon; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon.SetName("h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon")
h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon.SetTitle("h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_mu24_comb_r_muon = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_mu24_comb_r_muon.SetName("h_d0_zoom_eff_HLT_mu24_comb_r_muon")
h_d0_zoom_eff_HLT_mu24_comb_r_muon.SetTitle("h_d0_zoom_eff_HLT_mu24_comb_r_muon; d_{0} [mm]; Efficiency")

h_prodR_eff_HLT_mu24_r_muon = TGraphAsymmErrors()
h_prodR_eff_HLT_mu24_r_muon.SetName("h_prodR_eff_HLT_mu24_r_muon")
h_prodR_eff_HLT_mu24_r_muon.SetTitle("h_prodR_eff_HLT_mu24_r_muon; Prod Radius [mm]; Efficiency")

h_prodR_eff_HLT_mu24_LRTloose_r_muon = TGraphAsymmErrors()
h_prodR_eff_HLT_mu24_LRTloose_r_muon.SetName("h_prodR_eff_HLT_mu24_LRTloose_r_muon")
h_prodR_eff_HLT_mu24_LRTloose_r_muon.SetTitle("h_prodR_eff_HLT_mu24_LRTloose_r_muon; Prod Radius [mm]; Efficiency")

h_prodR_eff_HLT_mu24_comb_r_muon = TGraphAsymmErrors()
h_prodR_eff_HLT_mu24_comb_r_muon.SetName("h_prodR_eff_HLT_mu24_comb_r_muon")
h_prodR_eff_HLT_mu24_comb_r_muon.SetTitle("h_prodR_eff_HLT_mu24_comb_r_muon; Prod Radius [mm]; Efficiency")

h_pt_eff_HLT_mu24_r_muon = TGraphAsymmErrors()
h_pt_eff_HLT_mu24_r_muon.SetName("h_pt_eff_HLT_mu24_r_muon")
h_pt_eff_HLT_mu24_r_muon.SetTitle("h_pt_eff_HLT_mu24_r_muon; p_{T} [GeV]; Efficiency")

h_pt_eff_HLT_mu24_LRTloose_r_muon = TGraphAsymmErrors()
h_pt_eff_HLT_mu24_LRTloose_r_muon.SetName("h_pt_eff_HLT_mu24_LRTloose_r_muon")
h_pt_eff_HLT_mu24_LRTloose_r_muon.SetTitle("h_pt_eff_HLT_mu24_LRTloose_r_muon; p_{T} [GeV]; Efficiency")

h_pt_eff_HLT_mu24_comb_r_muon = TGraphAsymmErrors()
h_pt_eff_HLT_mu24_comb_r_muon.SetName("h_pt_eff_HLT_mu24_comb_r_muon")
h_pt_eff_HLT_mu24_comb_r_muon.SetTitle("h_pt_eff_HLT_mu24_comb_r_muon; p_{T} [GeV]; Efficiency")

#############################


h_pt_offlinesig_mu20_msonly_eff = TGraphAsymmErrors()
h_pt_offlinesig_mu20_msonly_eff.SetNameTitle("h_pt_mu20_msonly_r_offlinesig_eff","h_pt_mu20_msonly_r_offlinesig_eff; p_{T} [GeV]; Efficiency")

h_prodR_mu20_msonly_r_offlinesig_eff = TGraphAsymmErrors()
h_prodR_mu20_msonly_r_offlinesig_eff.SetNameTitle("h_prodR_mu20_msonly_r_offlinesig_eff","h_prodR_mu20_msonly_r_offlinesig_eff; Prod Radius [mm]; Efficiency")

h_d0_mu20_msonly_r_offlinesig_eff = TGraphAsymmErrors()
h_d0_mu20_msonly_r_offlinesig_eff.SetNameTitle("h_d0_mu20_msonly_r_offlinesig_eff","h_d0_mu20_msonly_r_offlinesig_eff; d_{0} [mm]; Efficiency")

# Electron FTF eff vs FS FTF
h_pt_ELFTF_r_FSFTF = TGraphAsymmErrors()
h_pt_ELFTF_r_FSFTF.SetName("h_pt_ELFTF_r_FSFTF")
h_pt_ELFTF_r_FSFTF.SetTitle("h_pt_ELFTF_r_FSFTF; p_{T} [GeV]; Efficiency")

h_d0_ELFTF_r_FSFTF = TGraphAsymmErrors()
h_d0_ELFTF_r_FSFTF.SetName("h_d0_ELFTF_r_FSFTF")
h_d0_ELFTF_r_FSFTF.SetTitle("h_d0_ELFTF_r_FSFTF; d_{0} [mm]; Efficiency")

h_dEta_ELFTF_r_FSFTF = TGraphAsymmErrors()
h_dEta_ELFTF_r_FSFTF.SetName("h_dEta_ELFTF_r_FSFTF")
h_dEta_ELFTF_r_FSFTF.SetTitle("h_dEta_ELFTF_r_FSFTF; #Delta#eta(track,RoI); Efficiency")

h_dPhi_ELFTF_r_FSFTF = TGraphAsymmErrors()
h_dPhi_ELFTF_r_FSFTF.SetName("h_dPhi_ELFTF_r_FSFTF")
h_dPhi_ELFTF_r_FSFTF.SetTitle("h_dPhi_ELFTF_r_FSFTF; #Delta#phi(track,RoI); Efficiency")


# new electron triggers
h_eta_eff_HLT_el26_r_offlinesig = TGraphAsymmErrors()
h_eta_eff_HLT_el26_r_offlinesig.SetName("h_eta_eff_HLT_el26_r_offlinesig")
h_eta_eff_HLT_el26_r_offlinesig.SetTitle("h_eta_eff_HLT_el26_r_offlinesig; #eta; Efficiency")

h_eta_eff_HLT_el30_LRTmedium_r_offlinesig = TGraphAsymmErrors()
h_eta_eff_HLT_el30_LRTmedium_r_offlinesig.SetName("h_eta_eff_HLT_el30_LRTmedium_r_offlinesig")
h_eta_eff_HLT_el30_LRTmedium_r_offlinesig.SetTitle("h_eta_eff_HLT_el30_LRTmedium_r_offlinesig; #eta; Efficiency")

h_eta_eff_HLT_el26_comb_r_offlinesig = TGraphAsymmErrors()
h_eta_eff_HLT_el26_comb_r_offlinesig.SetName("h_eta_eff_HLT_el26_comb_r_offlinesig")
h_eta_eff_HLT_el26_comb_r_offlinesig.SetTitle("h_eta_eff_HLT_el26_comb_r_offlinesig; #eta; Efficiency")

h_z0_eff_HLT_el26_r_offlinesig = TGraphAsymmErrors()
h_z0_eff_HLT_el26_r_offlinesig.SetName("h_z0_eff_HLT_el26_r_offlinesig")
h_z0_eff_HLT_el26_r_offlinesig.SetTitle("h_z0_eff_HLT_el26_r_offlinesig; z_{0} [mm]; Efficiency")

h_z0_eff_HLT_el30_LRTmedium_r_offlinesig = TGraphAsymmErrors()
h_z0_eff_HLT_el30_LRTmedium_r_offlinesig.SetName("h_z0_eff_HLT_el30_LRTmedium_r_offlinesig")
h_z0_eff_HLT_el30_LRTmedium_r_offlinesig.SetTitle("h_z0_eff_HLT_el30_LRTmedium_r_offlinesig; z_{0} [mm]; Efficiency")

h_z0_eff_HLT_el26_comb_r_offlinesig = TGraphAsymmErrors()
h_z0_eff_HLT_el26_comb_r_offlinesig.SetName("h_z0_eff_HLT_el26_comb_r_offlinesig")
h_z0_eff_HLT_el26_comb_r_offlinesig.SetTitle("h_z0_eff_HLT_el26_comb_r_offlinesig; z_{0} [mm]; Efficiency")

h_d0_eff_HLT_el26_r_offlinesig = TGraphAsymmErrors()
h_d0_eff_HLT_el26_r_offlinesig.SetName("h_d0_eff_HLT_el26_r_offlinesig")
h_d0_eff_HLT_el26_r_offlinesig.SetTitle("h_d0_eff_HLT_el26_r_offlinesig; d_{0} [mm]; Efficiency")

h_d0_eff_HLT_el30_LRTmedium_r_offlinesig = TGraphAsymmErrors()
h_d0_eff_HLT_el30_LRTmedium_r_offlinesig.SetName("h_d0_eff_HLT_el30_LRTmedium_r_offlinesig")
h_d0_eff_HLT_el30_LRTmedium_r_offlinesig.SetTitle("h_d0_eff_HLT_el30_LRTmedium_r_offlinesig; d_{0} [mm]; Efficiency")

h_d0_eff_HLT_el26_comb_r_offlinesig = TGraphAsymmErrors()
h_d0_eff_HLT_el26_comb_r_offlinesig.SetName("h_d0_eff_HLT_el26_comb_r_offlinesig")
h_d0_eff_HLT_el26_comb_r_offlinesig.SetTitle("h_d0_eff_HLT_el26_comb_r_offlinesig; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_el26_r_offlinesig = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_el26_r_offlinesig.SetName("h_d0_zoom_eff_HLT_el26_r_offlinesig")
h_d0_zoom_eff_HLT_el26_r_offlinesig.SetTitle("h_d0_zoom_eff_HLT_el26_r_offlinesig; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig.SetName("h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig")
h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig.SetTitle("h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_el26_comb_r_offlinesig = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_el26_comb_r_offlinesig.SetName("h_d0_zoom_eff_HLT_el26_comb_r_offlinesig")
h_d0_zoom_eff_HLT_el26_comb_r_offlinesig.SetTitle("h_d0_zoom_eff_HLT_el26_comb_r_offlinesig; d_{0} [mm]; Efficiency")

h_prodR_eff_HLT_el26_r_offlinesig = TGraphAsymmErrors()
h_prodR_eff_HLT_el26_r_offlinesig.SetName("h_prodR_eff_HLT_el26_r_offlinesig")
h_prodR_eff_HLT_el26_r_offlinesig.SetTitle("h_prodR_eff_HLT_el26_r_offlinesig; Prod Radius [mm]; Efficiency")

h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig = TGraphAsymmErrors()
h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig.SetName("h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig")
h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig.SetTitle("h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig; Prod Radius [mm]; Efficiency")

h_prodR_eff_HLT_el26_comb_r_offlinesig = TGraphAsymmErrors()
h_prodR_eff_HLT_el26_comb_r_offlinesig.SetName("h_prodR_eff_HLT_el26_comb_r_offlinesig")
h_prodR_eff_HLT_el26_comb_r_offlinesig.SetTitle("h_prodR_eff_HLT_el26_comb_r_offlinesig; Prod Radius [mm]; Efficiency")

h_pt_eff_HLT_el26_r_offlinesig = TGraphAsymmErrors()
h_pt_eff_HLT_el26_r_offlinesig.SetName("h_pt_eff_HLT_el26_r_offlinesig")
h_pt_eff_HLT_el26_r_offlinesig.SetTitle("h_pt_eff_HLT_el26_r_offlinesig; p_{T} [GeV]; Efficiency")

h_pt_eff_HLT_el30_LRTmedium_r_offlinesig = TGraphAsymmErrors()
h_pt_eff_HLT_el30_LRTmedium_r_offlinesig.SetName("h_pt_eff_HLT_el30_LRTmedium_r_offlinesig")
h_pt_eff_HLT_el30_LRTmedium_r_offlinesig.SetTitle("h_pt_eff_HLT_el30_LRTmedium_r_offlinesig; p_{T} [GeV]; Efficiency")

h_pt_eff_HLT_el26_comb_r_offlinesig = TGraphAsymmErrors()
h_pt_eff_HLT_el26_comb_r_offlinesig.SetName("h_pt_eff_HLT_el26_comb_r_offlinesig")
h_pt_eff_HLT_el26_comb_r_offlinesig.SetTitle("h_pt_eff_HLT_el26_comb_r_offlinesig; p_{T} [GeV]; Efficiency")
###################


# electron triggers with respect to offline electrons
h_eta_eff_HLT_el26_r_electron = TGraphAsymmErrors()
h_eta_eff_HLT_el26_r_electron.SetName("h_eta_eff_HLT_el26_r_electron")
h_eta_eff_HLT_el26_r_electron.SetTitle("h_eta_eff_HLT_el26_r_electron; #eta; Efficiency")

h_eta_eff_HLT_el30_LRTmedium_r_electron = TGraphAsymmErrors()
h_eta_eff_HLT_el30_LRTmedium_r_electron.SetName("h_eta_eff_HLT_el30_LRTmedium_r_electron")
h_eta_eff_HLT_el30_LRTmedium_r_electron.SetTitle("h_eta_eff_HLT_el30_LRTmedium_r_electron; #eta; Efficiency")

h_eta_eff_HLT_el26_comb_r_electron = TGraphAsymmErrors()
h_eta_eff_HLT_el26_comb_r_electron.SetName("h_eta_eff_HLT_el26_comb_r_electron")
h_eta_eff_HLT_el26_comb_r_electron.SetTitle("h_eta_eff_HLT_el26_comb_r_electron; #eta; Efficiency")

h_z0_eff_HLT_el26_r_electron = TGraphAsymmErrors()
h_z0_eff_HLT_el26_r_electron.SetName("h_z0_eff_HLT_el26_r_electron")
h_z0_eff_HLT_el26_r_electron.SetTitle("h_z0_eff_HLT_el26_r_electron; z_{0} [mm]; Efficiency")

h_z0_eff_HLT_el30_LRTmedium_r_electron = TGraphAsymmErrors()
h_z0_eff_HLT_el30_LRTmedium_r_electron.SetName("h_z0_eff_HLT_el30_LRTmedium_r_electron")
h_z0_eff_HLT_el30_LRTmedium_r_electron.SetTitle("h_z0_eff_HLT_el30_LRTmedium_r_electron; z_{0} [mm]; Efficiency")

h_z0_eff_HLT_el26_comb_r_electron = TGraphAsymmErrors()
h_z0_eff_HLT_el26_comb_r_electron.SetName("h_z0_eff_HLT_el26_comb_r_electron")
h_z0_eff_HLT_el26_comb_r_electron.SetTitle("h_z0_eff_HLT_el26_comb_r_electron; z_{0} [mm]; Efficiency")

h_d0_eff_HLT_el26_r_electron = TGraphAsymmErrors()
h_d0_eff_HLT_el26_r_electron.SetName("h_d0_eff_HLT_el26_r_electron")
h_d0_eff_HLT_el26_r_electron.SetTitle("h_d0_eff_HLT_el26_r_electron; d_{0} [mm]; Efficiency")

h_d0_eff_HLT_el30_LRTmedium_r_electron = TGraphAsymmErrors()
h_d0_eff_HLT_el30_LRTmedium_r_electron.SetName("h_d0_eff_HLT_el30_LRTmedium_r_electron")
h_d0_eff_HLT_el30_LRTmedium_r_electron.SetTitle("h_d0_eff_HLT_el30_LRTmedium_r_electron; d_{0} [mm]; Efficiency")

h_d0_eff_HLT_el26_comb_r_electron = TGraphAsymmErrors()
h_d0_eff_HLT_el26_comb_r_electron.SetName("h_d0_eff_HLT_el26_comb_r_electron")
h_d0_eff_HLT_el26_comb_r_electron.SetTitle("h_d0_eff_HLT_el26_comb_r_electron; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_el26_r_electron = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_el26_r_electron.SetName("h_d0_zoom_eff_HLT_el26_r_electron")
h_d0_zoom_eff_HLT_el26_r_electron.SetTitle("h_d0_zoom_eff_HLT_el26_r_electron; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron.SetName("h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron")
h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron.SetTitle("h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron; d_{0} [mm]; Efficiency")

h_d0_zoom_eff_HLT_el26_comb_r_electron = TGraphAsymmErrors()
h_d0_zoom_eff_HLT_el26_comb_r_electron.SetName("h_d0_zoom_eff_HLT_el26_comb_r_electron")
h_d0_zoom_eff_HLT_el26_comb_r_electron.SetTitle("h_d0_zoom_eff_HLT_el26_comb_r_electron; d_{0} [mm]; Efficiency")

h_prodR_eff_HLT_el26_r_electron = TGraphAsymmErrors()
h_prodR_eff_HLT_el26_r_electron.SetName("h_prodR_eff_HLT_el26_r_electron")
h_prodR_eff_HLT_el26_r_electron.SetTitle("h_prodR_eff_HLT_el26_r_electron; Prod Radius [mm]; Efficiency")

h_prodR_eff_HLT_el30_LRTmedium_r_electron = TGraphAsymmErrors()
h_prodR_eff_HLT_el30_LRTmedium_r_electron.SetName("h_prodR_eff_HLT_el30_LRTmedium_r_electron")
h_prodR_eff_HLT_el30_LRTmedium_r_electron.SetTitle("h_prodR_eff_HLT_el30_LRTmedium_r_electron; Prod Radius [mm]; Efficiency")

h_prodR_eff_HLT_el26_comb_r_electron = TGraphAsymmErrors()
h_prodR_eff_HLT_el26_comb_r_electron.SetName("h_prodR_eff_HLT_el26_comb_r_electron")
h_prodR_eff_HLT_el26_comb_r_electron.SetTitle("h_prodR_eff_HLT_el26_comb_r_electron; Prod Radius [mm]; Efficiency")

h_pt_eff_HLT_el26_r_electron = TGraphAsymmErrors()
h_pt_eff_HLT_el26_r_electron.SetName("h_pt_eff_HLT_el26_r_electron")
h_pt_eff_HLT_el26_r_electron.SetTitle("h_pt_eff_HLT_el26_r_electron; p_{T} [GeV]; Efficiency")

h_pt_eff_HLT_el30_LRTmedium_r_electron = TGraphAsymmErrors()
h_pt_eff_HLT_el30_LRTmedium_r_electron.SetName("h_pt_eff_HLT_el30_LRTmedium_r_electron")
h_pt_eff_HLT_el30_LRTmedium_r_electron.SetTitle("h_pt_eff_HLT_el30_LRTmedium_r_electron; p_{T} [GeV]; Efficiency")

h_pt_eff_HLT_el26_comb_r_electron = TGraphAsymmErrors()
h_pt_eff_HLT_el26_comb_r_electron.SetName("h_pt_eff_HLT_el26_comb_r_electron")
h_pt_eff_HLT_el26_comb_r_electron.SetTitle("h_pt_eff_HLT_el26_comb_r_electron; p_{T} [GeV]; Efficiency")
###################

h_pt_offlinesig_g20_eff = TGraphAsymmErrors()
h_pt_offlinesig_g20_eff.SetNameTitle("h_pt_g20_r_offlinesig_eff","h_pt_g20_r_offlinesig_eff; p_{T} [GeV]; Efficiency")

h_prodR_g20_r_offlinesig_eff = TGraphAsymmErrors()
h_prodR_g20_r_offlinesig_eff.SetNameTitle("h_prodR_g20_r_offlinesig_eff","h_prodR_g20_r_offlinesig_eff; Prod Radius [mm]; Efficiency")

h_d0_g20_r_offlinesig_eff = TGraphAsymmErrors()
h_d0_g20_r_offlinesig_eff.SetNameTitle("h_d0_g20_r_offlinesig_eff","h_d0_g20_r_offlinesig_eff; d_{0} [mm]; Efficiency")



### el support chains
h_pt_el_lrt_tnp_phtag_eff = TGraphAsymmErrors()
h_pt_el_lrt_tnp_phtag_eff.SetName("h_pt_el_lrt_tnp_phtag_eff")
h_pt_el_lrt_tnp_phtag_eff.SetTitle("h_pt_el_lrt_tnp_phtag_eff; p_{T} [GeV]; Efficiency")

h_prodR_el_lrt_tnp_phtag_eff = TGraphAsymmErrors()
h_prodR_el_lrt_tnp_phtag_eff.SetName("h_prodR_el_lrt_tnp_phtag_eff")
h_prodR_el_lrt_tnp_phtag_eff.SetTitle("h_prodR_el_lrt_tnp_phtag_eff; Prod. Radius [mm]; Efficiency")


# Electron PT tracks
h_d0_eff_HLT_el_PT_LRTloose_r_truth = TGraphAsymmErrors()
h_d0_eff_HLT_el_PT_LRTloose_r_truth.SetName("h_d0_eff_HLT_el_PT_LRTloose_r_truth")
h_d0_eff_HLT_el_PT_LRTloose_r_truth.SetTitle("h_d0_eff_HLT_el_PT_LRTloose_r_truth; d_{0} [mm]; Efficiency")

h_prodR_eff_HLT_el_PT_LRTloose_r_truth = TGraphAsymmErrors()
h_prodR_eff_HLT_el_PT_LRTloose_r_truth.SetName("h_prodR_eff_HLT_el_PT_LRTloose_r_truth")
h_prodR_eff_HLT_el_PT_LRTloose_r_truth.SetTitle("h_prodR_eff_HLT_el_PT_LRTloose_r_truth; Prod Radius [mm]; Efficiency")


# LRT Electron26 eff vs truth
h_d0_HLT_el30_LRT_r_truth_eff = TGraphAsymmErrors()
h_d0_HLT_el30_LRT_r_truth_eff.SetNameTitle("h_d0_HLT_el30_LRT_r_truth_eff","h_d0_HLT_el30_LRT_r_truth_eff; d_{0} [mm]; Efficiency")

h_pt_HLT_el30_LRT_r_truth_eff = TGraphAsymmErrors()
h_pt_HLT_el30_LRT_r_truth_eff.SetNameTitle("h_pt_HLT_el30_LRT_r_truth_eff","h_pt_HLT_el30_LRT_r_truth_eff; p_{T} [GeV]; Efficiency")

h_eta_HLT_el30_LRT_r_truth_eff = TGraphAsymmErrors()
h_eta_HLT_el30_LRT_r_truth_eff.SetNameTitle("h_eta_HLT_el30_LRT_r_truth_eff","h_eta_HLT_el30_LRT_r_truth_eff; #eta; Efficiency")

h_phi_HLT_el30_LRT_r_truth_eff = TGraphAsymmErrors()
h_phi_HLT_el30_LRT_r_truth_eff.SetNameTitle("h_phi_HLT_el30_LRT_r_truth_eff","h_phi_HLT_el30_LRT_r_truth_eff; #phi; Efficiency")

h_vtxR_HLT_el30_LRT_r_truth_eff = TGraphAsymmErrors()
h_vtxR_HLT_el30_LRT_r_truth_eff.SetNameTitle("h_vtxR_HLT_el30_LRT_r_truth_eff","h_vtxR_HLT_el30_LRT_r_truth_eff; Prod Radius [mm]; Efficiency")

# Prompt Electron26 eff vs truth
h_d0_HLT_el26_r_truth_eff = TGraphAsymmErrors()
h_d0_HLT_el26_r_truth_eff.SetNameTitle("h_d0_HLT_el26_r_truth_eff","h_d0_HLT_el26_r_truth_eff; d_{0} [mm]; Efficiency")

h_pt_HLT_el26_r_truth_eff = TGraphAsymmErrors()
h_pt_HLT_el26_r_truth_eff.SetNameTitle("h_pt_HLT_el26_r_truth_eff","h_pt_HLT_el26_r_truth_eff; p_{T} [GeV]; Efficiency")

h_eta_HLT_el26_r_truth_eff = TGraphAsymmErrors()
h_eta_HLT_el26_r_truth_eff.SetNameTitle("h_eta_HLT_el26_r_truth_eff","h_eta_HLT_el26_r_truth_eff; #eta; Efficiency")

h_phi_HLT_el26_r_truth_eff = TGraphAsymmErrors()
h_phi_HLT_el26_r_truth_eff.SetNameTitle("h_phi_HLT_el26_r_truth_eff","h_phi_HLT_el26_r_truth_eff; #phi; Efficiency")

h_vtxR_HLT_el26_r_truth_eff = TGraphAsymmErrors()
h_vtxR_HLT_el26_r_truth_eff.SetNameTitle("h_vtxR_HLT_el26_r_truth_eff","h_vtxR_HLT_el26_r_truth_eff; Prod Radius [mm]; Efficiency")

# LRT Muon24 eff vs truth
h_d0_HLT_mu24_LRT_r_truth_eff = TGraphAsymmErrors()
h_d0_HLT_mu24_LRT_r_truth_eff.SetNameTitle("h_d0_HLT_mu24_LRT_r_truth_eff","h_d0_HLT_mu24_LRT_r_truth_eff; d_{0} [mm]; Efficiency")

h_pt_HLT_mu24_LRT_r_truth_eff = TGraphAsymmErrors()
h_pt_HLT_mu24_LRT_r_truth_eff.SetNameTitle("h_pt_HLT_mu24_LRT_r_truth_eff","h_pt_HLT_mu24_LRT_r_truth_eff; p_{T} [GeV]; Efficiency")

h_eta_HLT_mu24_LRT_r_truth_eff = TGraphAsymmErrors()
h_eta_HLT_mu24_LRT_r_truth_eff.SetNameTitle("h_eta_HLT_mu24_LRT_r_truth_eff","h_eta_HLT_mu24_LRT_r_truth_eff; #eta; Efficiency")

h_phi_HLT_mu24_LRT_r_truth_eff = TGraphAsymmErrors()
h_phi_HLT_mu24_LRT_r_truth_eff.SetNameTitle("h_phi_HLT_mu24_LRT_r_truth_eff","h_phi_HLT_mu24_LRT_r_truth_eff; #phi; Efficiency")

h_vtxR_HLT_mu24_LRT_r_truth_eff = TGraphAsymmErrors()
h_vtxR_HLT_mu24_LRT_r_truth_eff.SetNameTitle("h_vtxR_HLT_mu24_LRT_r_truth_eff","h_vtxR_HLT_mu24_LRT_r_truth_eff; Prod Radius [mm]; Efficiency")

# prompt Muon24 eff vs truth
h_d0_HLT_mu24_r_truth_eff = TGraphAsymmErrors()
h_d0_HLT_mu24_r_truth_eff.SetNameTitle("h_d0_HLT_mu24_r_truth_eff","h_d0_HLT_mu24_r_truth_eff; d_{0} [mm]; Efficiency")

h_pt_HLT_mu24_r_truth_eff = TGraphAsymmErrors()
h_pt_HLT_mu24_r_truth_eff.SetNameTitle("h_pt_HLT_mu24_r_truth_eff","h_pt_HLT_mu24_r_truth_eff; p_{T} [GeV]; Efficiency")

h_eta_HLT_mu24_r_truth_eff = TGraphAsymmErrors()
h_eta_HLT_mu24_r_truth_eff.SetNameTitle("h_eta_HLT_mu24_r_truth_eff","h_eta_HLT_mu24_r_truth_eff; #eta; Efficiency")

h_phi_HLT_mu24_r_truth_eff = TGraphAsymmErrors()
h_phi_HLT_mu24_r_truth_eff.SetNameTitle("h_phi_HLT_mu24_r_truth_eff","h_phi_HLT_mu24_r_truth_eff; #phi; Efficiency")

h_vtxR_HLT_mu24_r_truth_eff = TGraphAsymmErrors()
h_vtxR_HLT_mu24_r_truth_eff.SetNameTitle("h_vtxR_HLT_mu24_r_truth_eff","h_vtxR_HLT_mu24_r_truth_eff; Prod Radius [mm]; Efficiency")


# msonly trigger validation
h_pt_sublead_HLT_2mu50_r_offline_eff = TGraphAsymmErrors()
h_pt_sublead_HLT_2mu50_r_offline_eff.SetNameTitle("h_pt_sublead_HLT_2mu50_r_offline_eff","h_pt_sublead_HLT_2mu50_r_offline_eff; p_{T} [GeV]; Efficiency")

h_eta_sublead_HLT_2mu50_r_offline_eff = TGraphAsymmErrors()
h_eta_sublead_HLT_2mu50_r_offline_eff.SetNameTitle("h_eta_sublead_HLT_2mu50_r_offline_eff","h_eta_sublead_HLT_2mu50_r_offline_eff; #eta; Efficiency")

h_phi_sublead_HLT_2mu50_r_offline_eff = TGraphAsymmErrors()
h_phi_sublead_HLT_2mu50_r_offline_eff.SetNameTitle("h_phi_sublead_HLT_2mu50_r_offline_eff","h_phi_sublead_HLT_2mu50_r_offline_eff; #phi; Efficiency")

h_prodR_HLT_2mu50_r_offline = TGraphAsymmErrors()
h_prodR_HLT_2mu50_r_offline.SetNameTitle("h_prodR_HLT_2mu50_r_offline","h_prodR_HLT_2mu50_r_offline; Prod. Radius [mm]; Efficiency")

h_pt_el_HLT_g40mu40_r_offline_eff = TGraphAsymmErrors()
h_pt_el_HLT_g40mu40_r_offline_eff.SetNameTitle("h_pt_el_HLT_g40mu40_r_offline_eff","h_pt_el_HLT_g40mu40_r_offline_eff; p_{T} [GeV]; Efficiency")

h_pt_mu_HLT_g40mu40_r_offline_eff = TGraphAsymmErrors()
h_pt_mu_HLT_g40mu40_r_offline_eff.SetNameTitle("h_pt_mu_HLT_g40mu40_r_offline_eff","h_pt_mu_HLT_g40mu40_r_offline_eff; p_{T} [GeV]; Efficiency")

h_prodR_el_HLT_g40mu40_r_offline = TGraphAsymmErrors()
h_prodR_el_HLT_g40mu40_r_offline.SetNameTitle("h_prodR_el_HLT_g40mu40_r_offline","h_prodR_el_HLT_g40mu40_r_offline; Prod. Radius [mm]; Efficiency")

h_prodR_mu_HLT_g40mu40_r_offline = TGraphAsymmErrors()
h_prodR_mu_HLT_g40mu40_r_offline.SetNameTitle("h_prodR_mu_HLT_g40mu40_r_offline","h_prodR_mu_HLT_g40mu40_r_offline; Prod. Radius [mm]; Efficiency")

h_eta_mu_HLT_g40mu40_r_offline_eff = TGraphAsymmErrors()
h_eta_mu_HLT_g40mu40_r_offline_eff.SetNameTitle("h_eta_mu_HLT_g40mu40_r_offline_eff","h_eta_mu_HLT_g40mu40_r_offline_eff; #eta; Efficiency")

h_phi_mu_HLT_g40mu40_r_offline_eff = TGraphAsymmErrors()
h_phi_mu_HLT_g40mu40_r_offline_eff.SetNameTitle("h_phi_mu_HLT_g40mu40_r_offline_eff","h_phi_mu_HLT_g40mu40_r_offline_eff; #phi; Efficiency")

# single leg g40
h_pt_el_HLT_g40_r_offline_eff = TGraphAsymmErrors()
h_pt_el_HLT_g40_r_offline_eff.SetNameTitle("h_pt_el_HLT_g40_r_offline_eff","h_pt_el_HLT_g40_r_offline_eff; p_{T} [GeV]; Efficiency")

h_eta_el_HLT_g40_r_offline_eff = TGraphAsymmErrors()
h_eta_el_HLT_g40_r_offline_eff.SetNameTitle("h_eta_el_HLT_g40_r_offline_eff","h_eta_el_HLT_g40_r_offline_eff; #eta; Efficiency")

h_prodR_el_HLT_g40_r_offline_eff = TGraphAsymmErrors()
h_prodR_el_HLT_g40_r_offline_eff.SetNameTitle("h_prodR_el_HLT_g40_r_offline_eff","h_prodR_el_HLT_g40_r_offline_eff; Prod. Radius [mm]; Efficiency")



#//////////////////////////

# Divide numerator and denominator to make efficiency
h_d0_eff_FTFcomb_r_offline.Divide( hist("h_d0_eff_FTFcomb_r_offline_n"), hist("h_d0_eff_FTFcomb_r_offline_d") );    
h_d0_eff_FTFcomb_r_offline.Write()

h_d0_el_eff_FTFcomb_r_offline.Divide( hist("h_d0_el_eff_n"), hist("h_d0_el_eff_d") );    
h_d0_el_eff_FTFcomb_r_offline.Write()

h_d0_mu_eff_FTFcomb_r_offline.Divide( hist("h_d0_mu_eff_n"), hist("h_d0_mu_eff_d") );    
h_d0_mu_eff_FTFcomb_r_offline.Write()

h_pt_eff_FTFcomb_r_offline.Divide( hist("h_pt_eff_n"), hist("h_pt_eff_d") );    
h_pt_eff_FTFcomb_r_offline.Write()

h_eta_eff_FTFcomb_r_offline.Divide( hist("h_eta_eff_n"), hist("h_eta_eff_d") );    
h_eta_eff_FTFcomb_r_offline.Write()

h_vtxR_eff_FTFcomb_r_offline.Divide( hist("h_vtxR_eff_n"), hist("h_vtxR_eff_d") );    
h_vtxR_eff_FTFcomb_r_offline.Write()



# truth
h_d0_eff_FTFcomb_r_truth.Divide( hist("h_d0_truth_eff_n"), hist("h_d0_truth_eff_d") )
h_d0_eff_FTFcomb_r_truth.Write()

h_d0_eff_FTFcomb_r_truth_el.Divide( hist("h_d0_truth_el_eff_n"), hist("h_d0_truth_el_eff_d") )
h_d0_eff_FTFcomb_r_truth_el.Write()

h_d0_eff_FTFcomb_r_truth_mu.Divide( hist("h_d0_truth_mu_eff_n"), hist("h_d0_truth_mu_eff_d") )
h_d0_eff_FTFcomb_r_truth_mu.Write()

h_pt_eff_FTFcomb_r_truth.Divide( hist("h_pt_truth_eff_n"), hist("h_pt_truth_eff_d") )
h_pt_eff_FTFcomb_r_truth.Write()

h_vtxR_eff_FTFcomb_r_truth.Divide( hist("h_vtxR_truth_eff_n"), hist("h_vtxR_truth_eff_d") )
h_vtxR_eff_FTFcomb_r_truth.Write()

# offline eff
h_d0_eff_offline_r_truth.Divide( hist("h_d0_offline_eff_n"), hist("h_d0_offline_eff_d") )
h_d0_eff_offline_r_truth.Write()

h_pt_eff_offline_r_truth.Divide( hist("h_pt_offline_eff_n"), hist("h_pt_offline_eff_d") )
h_pt_eff_offline_r_truth.Write()

h_vtxR_eff_offline_r_truth.Divide( hist("h_vtxR_offline_eff_n"), hist("h_vtxR_offline_eff_d") )
h_vtxR_eff_offline_r_truth.Write()

# purity
h_d0_pur_FTFcomb_r_offline.Divide( hist("h_d0_pur_n"), hist("h_d0_pur_d") );    
h_d0_pur_FTFcomb_r_offline.Write()

h_pt_pur_FTFcomb_r_offline.Divide( hist("h_pt_pur_n"), hist("h_pt_pur_d") );    
h_pt_pur_FTFcomb_r_offline.Write()

h_eta_pur_FTFcomb_r_offline.Divide( hist("h_eta_pur_n"), hist("h_eta_pur_d") );    
h_eta_pur_FTFcomb_r_offline.Write()



# L1 Eff Offline
h_pt_L1eEM26M_r_offline.Divide(hist("h_pt_offline_L1eEM26M_eff_n"), hist("h_pt_offline_L1eEM26M_eff_d") )
h_pt_L1eEM26M_r_offline.Write()

h_d0_L1eEM26M_r_offline.Divide(hist("h_d0_offline_L1eEM26M_eff_n"), hist("h_d0_offline_L1eEM26M_eff_d") )
h_d0_L1eEM26M_r_offline.Write()

h_vtxR_L1eEM26M_r_offline.Divide(hist("h_vtxR_offline_L1eEM26M_eff_n"), hist("h_vtxR_offline_L1eEM26M_eff_d") )
h_vtxR_L1eEM26M_r_offline.Write()

h_pt_L1MU14FCH_r_offline.Divide(hist("h_pt_offline_L1MU14FCH_eff_n"), hist("h_pt_offline_L1MU14FCH_eff_d") )
h_pt_L1MU14FCH_r_offline.Write()

h_d0_L1MU14FCH_r_offline.Divide(hist("h_d0_offline_L1MU14FCH_eff_n"), hist("h_d0_offline_L1MU14FCH_eff_d") )
h_d0_L1MU14FCH_r_offline.Write()

h_vtxR_L1MU14FCH_r_offline.Divide(hist("h_vtxR_offline_L1MU14FCH_eff_n"), hist("h_vtxR_offline_L1MU14FCH_eff_d") )
h_vtxR_L1MU14FCH_r_offline.Write()


# L1 Eff Truth
h_pt_L1eEM26M_r_truth.Divide(hist("h_pt_truth_L1eEM26M_eff_n"), hist("h_pt_truth_L1eEM26M_eff_d") )
h_pt_L1eEM26M_r_truth.Write()

h_d0_L1eEM26M_r_truth.Divide(hist("h_d0_truth_L1eEM26M_eff_n"), hist("h_d0_truth_L1eEM26M_eff_d") )
h_d0_L1eEM26M_r_truth.Write()

h_vtxR_L1eEM26M_r_truth.Divide(hist("h_vtxR_truth_L1eEM26M_eff_n"), hist("h_vtxR_truth_L1eEM26M_eff_d") )
h_vtxR_L1eEM26M_r_truth.Write()

h_pt_L1MU14FCH_r_truth.Divide(hist("h_pt_truth_L1MU14FCH_eff_n"), hist("h_pt_truth_L1MU14FCH_eff_d") )
h_pt_L1MU14FCH_r_truth.Write()


h_d0_L1MU14FCH_r_truth.Divide(hist("h_d0_truth_L1MU14FCH_eff_n"), hist("h_d0_truth_L1MU14FCH_eff_d") )
h_d0_L1MU14FCH_r_truth.Write()

h_vtxR_L1MU14FCH_r_truth.Divide(hist("h_vtxR_truth_L1MU14FCH_eff_n"), hist("h_vtxR_truth_L1MU14FCH_eff_d") )
h_vtxR_L1MU14FCH_r_truth.Write()



# muon ftf vs fs ftf
h_pt_MUFTF_r_FSFTF.Divide( hist("h_pt_MUFTF_r_FSFTF_n"), hist("h_pt_MUFTF_r_FSFTF_d") )
h_pt_MUFTF_r_FSFTF.Write()

h_d0_MUFTF_r_FSFTF.Divide( hist("h_d0_MUFTF_r_FSFTF_n"), hist("h_d0_MUFTF_r_FSFTF_d") )
h_d0_MUFTF_r_FSFTF.Write()

h_dEta_MUFTF_r_FSFTF.Divide( hist("h_dEta_MUFTF_r_FSFTF_n"), hist("h_dEta_MUFTF_r_FSFTF_d") )
h_dEta_MUFTF_r_FSFTF.Write()

h_dPhi_MUFTF_r_FSFTF.Divide( hist("h_dPhi_MUFTF_r_FSFTF_n"), hist("h_dPhi_MUFTF_r_FSFTF_d") )
h_dPhi_MUFTF_r_FSFTF.Write()

# new muon triggers
h_eta_eff_HLT_mu24_r_offlinesig.Divide( hist("h_eta_eff_HLT_mu24_r_offlinesig_n"),hist("h_eta_eff_HLT_mu24_r_offlinesig_d") ) 
h_eta_eff_HLT_mu24_r_offlinesig.Write()

h_eta_eff_HLT_mu24_LRTloose_r_offlinesig.Divide(hist("h_eta_eff_HLT_mu24_LRTloose_r_offlinesig_n"),hist("h_eta_eff_HLT_mu24_LRTloose_r_offlinesig_d") )
h_eta_eff_HLT_mu24_LRTloose_r_offlinesig.Write()

h_eta_eff_HLT_mu24_comb_r_offlinesig.Divide(hist("h_eta_eff_HLT_mu24_comb_r_offlinesig_n"),hist("h_eta_eff_HLT_mu24_comb_r_offlinesig_d") )
h_eta_eff_HLT_mu24_comb_r_offlinesig.Write()

h_z0_eff_HLT_mu24_r_offlinesig.Divide( hist("h_z0_eff_HLT_mu24_r_offlinesig_n"),hist("h_z0_eff_HLT_mu24_r_offlinesig_d") ) 
h_z0_eff_HLT_mu24_r_offlinesig.Write()

h_z0_eff_HLT_mu24_LRTloose_r_offlinesig.Divide(hist("h_z0_eff_HLT_mu24_LRTloose_r_offlinesig_n"),hist("h_z0_eff_HLT_mu24_LRTloose_r_offlinesig_d") )
h_z0_eff_HLT_mu24_LRTloose_r_offlinesig.Write()

h_z0_eff_HLT_mu24_comb_r_offlinesig.Divide(hist("h_z0_eff_HLT_mu24_comb_r_offlinesig_n"),hist("h_z0_eff_HLT_mu24_comb_r_offlinesig_d") )
h_z0_eff_HLT_mu24_comb_r_offlinesig.Write()


h_d0_eff_HLT_mu24_r_offlinesig.Divide(hist("h_d0_eff_HLT_mu24_r_offlinesig_n"), hist("h_d0_eff_HLT_mu24_r_offlinesig_d") )
h_d0_eff_HLT_mu24_r_offlinesig.Write()

h_d0_eff_HLT_mu24_LRTloose_r_offlinesig.Divide(hist("h_d0_eff_HLT_mu24_LRTloose_r_offlinesig_n"), hist("h_d0_eff_HLT_mu24_LRTloose_r_offlinesig_d") )
h_d0_eff_HLT_mu24_LRTloose_r_offlinesig.Write()

h_d0_eff_HLT_mu24_comb_r_offlinesig.Divide(hist("h_d0_eff_HLT_mu24_comb_r_offlinesig_n"), hist("h_d0_eff_HLT_mu24_comb_r_offlinesig_d") )
h_d0_eff_HLT_mu24_comb_r_offlinesig.Write()

h_d0_zoom_eff_HLT_mu24_r_offlinesig.Divide(hist("h_d0_zoom_eff_HLT_mu24_r_offlinesig_n"), hist("h_d0_zoom_eff_HLT_mu24_r_offlinesig_d") )
h_d0_zoom_eff_HLT_mu24_r_offlinesig.Write()

h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig.Divide(hist("h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig_n"), hist("h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig_d") )
h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig.Write()

h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig.Divide(hist("h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig_n"), hist("h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig_d") )
h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig.Write()

h_prodR_eff_HLT_mu24_r_offlinesig.Divide(hist("h_prodR_eff_HLT_mu24_r_offlinesig_n"), hist("h_prodR_eff_HLT_mu24_r_offlinesig_d") )
h_prodR_eff_HLT_mu24_r_offlinesig.Write()

h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig.Divide(hist("h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig_n"), hist("h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig_d") )
h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig.Write()

h_prodR_eff_HLT_mu24_comb_r_offlinesig.Divide(hist("h_prodR_eff_HLT_mu24_comb_r_offlinesig_n"), hist("h_prodR_eff_HLT_mu24_comb_r_offlinesig_d") )
h_prodR_eff_HLT_mu24_comb_r_offlinesig.Write()

h_pt_eff_HLT_mu24_r_offlinesig.Divide(hist("h_pt_eff_HLT_mu24_r_offlinesig_n"), hist("h_pt_eff_HLT_mu24_r_offlinesig_d") )
h_pt_eff_HLT_mu24_r_offlinesig.Write()

h_pt_eff_HLT_mu24_LRTloose_r_offlinesig.Divide(hist("h_pt_eff_HLT_mu24_LRTloose_r_offlinesig_n"), hist("h_pt_eff_HLT_mu24_LRTloose_r_offlinesig_d") )
h_pt_eff_HLT_mu24_LRTloose_r_offlinesig.Write()

h_pt_eff_HLT_mu24_comb_r_offlinesig.Divide(hist("h_pt_eff_HLT_mu24_comb_r_offlinesig_n"), hist("h_pt_eff_HLT_mu24_comb_r_offlinesig_d") )
h_pt_eff_HLT_mu24_comb_r_offlinesig.Write()
##################


# muon triggers with respect to offline muons
h_eta_eff_HLT_mu24_r_muon.Divide(hist("h_eta_eff_HLT_mu24_r_muon_n"),hist("h_eta_eff_HLT_mu24_r_muon_d") )
h_eta_eff_HLT_mu24_r_muon.Write()

h_eta_eff_HLT_mu24_LRTloose_r_muon.Divide(hist("h_eta_eff_HLT_mu24_LRTloose_r_muon_n"),hist("h_eta_eff_HLT_mu24_LRTloose_r_muon_d") )
h_eta_eff_HLT_mu24_LRTloose_r_muon.Write()

h_eta_eff_HLT_mu24_comb_r_muon.Divide(hist("h_eta_eff_HLT_mu24_comb_r_muon_n"),hist("h_eta_eff_HLT_mu24_comb_r_muon_d") )
h_eta_eff_HLT_mu24_comb_r_muon.Write()

h_z0_eff_HLT_mu24_r_muon.Divide(hist("h_z0_eff_HLT_mu24_r_muon_n"),hist("h_z0_eff_HLT_mu24_r_muon_d") )
h_z0_eff_HLT_mu24_r_muon.Write()

h_z0_eff_HLT_mu24_LRTloose_r_muon.Divide(hist("h_z0_eff_HLT_mu24_LRTloose_r_muon_n"),hist("h_z0_eff_HLT_mu24_LRTloose_r_muon_d") )
h_z0_eff_HLT_mu24_LRTloose_r_muon.Write()

h_z0_eff_HLT_mu24_comb_r_muon.Divide(hist("h_z0_eff_HLT_mu24_comb_r_muon_n"),hist("h_z0_eff_HLT_mu24_comb_r_muon_d") )
h_z0_eff_HLT_mu24_comb_r_muon.Write()

h_d0_eff_HLT_mu24_r_muon.Divide(hist("h_d0_eff_HLT_mu24_r_muon_n"), hist("h_d0_eff_HLT_mu24_r_muon_d") )
h_d0_eff_HLT_mu24_r_muon.Write()

h_d0_eff_HLT_mu24_LRTloose_r_muon.Divide(hist("h_d0_eff_HLT_mu24_LRTloose_r_muon_n"), hist("h_d0_eff_HLT_mu24_LRTloose_r_muon_d") )
h_d0_eff_HLT_mu24_LRTloose_r_muon.Write()

h_d0_eff_HLT_mu24_comb_r_muon.Divide(hist("h_d0_eff_HLT_mu24_comb_r_muon_n"), hist("h_d0_eff_HLT_mu24_comb_r_muon_d") )
h_d0_eff_HLT_mu24_comb_r_muon.Write()

h_d0_zoom_eff_HLT_mu24_r_muon.Divide(hist("h_d0_zoom_eff_HLT_mu24_r_muon_n"), hist("h_d0_zoom_eff_HLT_mu24_r_muon_d") )
h_d0_zoom_eff_HLT_mu24_r_muon.Write()

h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon.Divide(hist("h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon_n"), hist("h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon_d") )
h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon.Write()

h_d0_zoom_eff_HLT_mu24_comb_r_muon.Divide(hist("h_d0_zoom_eff_HLT_mu24_comb_r_muon_n"), hist("h_d0_zoom_eff_HLT_mu24_comb_r_muon_d") )
h_d0_zoom_eff_HLT_mu24_comb_r_muon.Write()

h_prodR_eff_HLT_mu24_r_muon.Divide(hist("h_prodR_eff_HLT_mu24_r_muon_n"), hist("h_prodR_eff_HLT_mu24_r_muon_d") )
h_prodR_eff_HLT_mu24_r_muon.Write()

h_prodR_eff_HLT_mu24_LRTloose_r_muon.Divide(hist("h_prodR_eff_HLT_mu24_LRTloose_r_muon_n"), hist("h_prodR_eff_HLT_mu24_LRTloose_r_muon_d") )
h_prodR_eff_HLT_mu24_LRTloose_r_muon.Write()

h_prodR_eff_HLT_mu24_comb_r_muon.Divide(hist("h_prodR_eff_HLT_mu24_comb_r_muon_n"), hist("h_prodR_eff_HLT_mu24_comb_r_muon_d") )
h_prodR_eff_HLT_mu24_comb_r_muon.Write()

h_pt_eff_HLT_mu24_r_muon.Divide(hist("h_pt_eff_HLT_mu24_r_muon_n"), hist("h_pt_eff_HLT_mu24_r_muon_d") )
h_pt_eff_HLT_mu24_r_muon.Write()

h_pt_eff_HLT_mu24_LRTloose_r_muon.Divide(hist("h_pt_eff_HLT_mu24_LRTloose_r_muon_n"), hist("h_pt_eff_HLT_mu24_LRTloose_r_muon_d") )
h_pt_eff_HLT_mu24_LRTloose_r_muon.Write()

h_pt_eff_HLT_mu24_comb_r_muon.Divide(hist("h_pt_eff_HLT_mu24_comb_r_muon_n"), hist("h_pt_eff_HLT_mu24_comb_r_muon_d") )
h_pt_eff_HLT_mu24_comb_r_muon.Write()
##################


h_pt_offlinesig_mu20_msonly_eff.Divide( hist("h_pt_mu20_msonly_r_offlinesig_eff_n"), hist("h_pt_mu20_msonly_r_offlinesig_eff_d") )
h_pt_offlinesig_mu20_msonly_eff.Write()

h_prodR_mu20_msonly_r_offlinesig_eff.Divide( hist("h_prodR_mu20_msonly_r_offlinesig_eff_n"), hist("h_prodR_mu20_msonly_r_offlinesig_eff_d") )
h_prodR_mu20_msonly_r_offlinesig_eff.Write()

h_d0_mu20_msonly_r_offlinesig_eff.Divide( hist("h_d0_mu20_msonly_r_offlinesig_eff_n"), hist("h_d0_mu20_msonly_r_offlinesig_eff_d") )
h_d0_mu20_msonly_r_offlinesig_eff.Write()



# electron ftf vs fs ftf
h_pt_ELFTF_r_FSFTF.Divide( hist("h_pt_ELFTF_r_FSFTF_n"), hist("h_pt_ELFTF_r_FSFTF_d") )
h_pt_ELFTF_r_FSFTF.Write()

h_d0_ELFTF_r_FSFTF.Divide( hist("h_d0_ELFTF_r_FSFTF_n"), hist("h_d0_ELFTF_r_FSFTF_d") )
h_d0_ELFTF_r_FSFTF.Write()

h_dEta_ELFTF_r_FSFTF.Divide( hist("h_dEta_ELFTF_r_FSFTF_n"), hist("h_dEta_ELFTF_r_FSFTF_d") )
h_dEta_ELFTF_r_FSFTF.Write()

h_dPhi_ELFTF_r_FSFTF.Divide( hist("h_dPhi_ELFTF_r_FSFTF_n"), hist("h_dPhi_ELFTF_r_FSFTF_d") )
h_dPhi_ELFTF_r_FSFTF.Write()

# new electron triggers
h_eta_eff_HLT_el26_r_offlinesig.Divide(hist("h_eta_eff_HLT_el26_r_offlinesig_n"),hist("h_eta_eff_HLT_el26_r_offlinesig_d") )
h_eta_eff_HLT_el26_r_offlinesig.Write()

h_eta_eff_HLT_el30_LRTmedium_r_offlinesig.Divide(hist("h_eta_eff_HLT_el30_LRTmedium_r_offlinesig_n"),hist("h_eta_eff_HLT_el30_LRTmedium_r_offlinesig_d") )
h_eta_eff_HLT_el30_LRTmedium_r_offlinesig.Write()

h_eta_eff_HLT_el26_comb_r_offlinesig.Divide(hist("h_eta_eff_HLT_el26_comb_r_offlinesig_n"),hist("h_eta_eff_HLT_el26_comb_r_offlinesig_d") )
h_eta_eff_HLT_el26_comb_r_offlinesig.Write()

h_z0_eff_HLT_el26_r_offlinesig.Divide(hist("h_z0_eff_HLT_el26_r_offlinesig_n"),hist("h_z0_eff_HLT_el26_r_offlinesig_d") )
h_z0_eff_HLT_el26_r_offlinesig.Write()

h_z0_eff_HLT_el30_LRTmedium_r_offlinesig.Divide(hist("h_z0_eff_HLT_el30_LRTmedium_r_offlinesig_n"),hist("h_z0_eff_HLT_el30_LRTmedium_r_offlinesig_d") )
h_z0_eff_HLT_el30_LRTmedium_r_offlinesig.Write()

h_z0_eff_HLT_el26_comb_r_offlinesig.Divide(hist("h_z0_eff_HLT_el26_comb_r_offlinesig_n"),hist("h_z0_eff_HLT_el26_comb_r_offlinesig_d") )
h_z0_eff_HLT_el26_comb_r_offlinesig.Write()

h_d0_eff_HLT_el26_r_offlinesig.Divide(hist("h_d0_eff_HLT_el26_r_offlinesig_n"), hist("h_d0_eff_HLT_el26_r_offlinesig_d") )
h_d0_eff_HLT_el26_r_offlinesig.Write()

h_d0_eff_HLT_el30_LRTmedium_r_offlinesig.Divide(hist("h_d0_eff_HLT_el30_LRTmedium_r_offlinesig_n"), hist("h_d0_eff_HLT_el30_LRTmedium_r_offlinesig_d") )
h_d0_eff_HLT_el30_LRTmedium_r_offlinesig.Write()

h_d0_eff_HLT_el26_comb_r_offlinesig.Divide(hist("h_d0_eff_HLT_el26_comb_r_offlinesig_n"), hist("h_d0_eff_HLT_el26_comb_r_offlinesig_d") )
h_d0_eff_HLT_el26_comb_r_offlinesig.Write()

h_d0_zoom_eff_HLT_el26_r_offlinesig.Divide(hist("h_d0_zoom_eff_HLT_el26_r_offlinesig_n"), hist("h_d0_zoom_eff_HLT_el26_r_offlinesig_d") )
h_d0_zoom_eff_HLT_el26_r_offlinesig.Write()

h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig.Divide(hist("h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig_n"), hist("h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig_d") )
h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig.Write()

h_d0_zoom_eff_HLT_el26_comb_r_offlinesig.Divide(hist("h_d0_zoom_eff_HLT_el26_comb_r_offlinesig_n"), hist("h_d0_zoom_eff_HLT_el26_comb_r_offlinesig_d") )
h_d0_zoom_eff_HLT_el26_comb_r_offlinesig.Write()

h_prodR_eff_HLT_el26_r_offlinesig.Divide(hist("h_prodR_eff_HLT_el26_r_offlinesig_n"), hist("h_prodR_eff_HLT_el26_r_offlinesig_d") )
h_prodR_eff_HLT_el26_r_offlinesig.Write()

h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig.Divide(hist("h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig_n"), hist("h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig_d") )
h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig.Write()

h_prodR_eff_HLT_el26_comb_r_offlinesig.Divide(hist("h_prodR_eff_HLT_el26_comb_r_offlinesig_n"), hist("h_prodR_eff_HLT_el26_comb_r_offlinesig_d") )
h_prodR_eff_HLT_el26_comb_r_offlinesig.Write()

h_pt_eff_HLT_el26_r_offlinesig.Divide(hist("h_pt_eff_HLT_el26_r_offlinesig_n"), hist("h_pt_eff_HLT_el26_r_offlinesig_d") )
h_pt_eff_HLT_el26_r_offlinesig.Write()

h_pt_eff_HLT_el30_LRTmedium_r_offlinesig.Divide(hist("h_pt_eff_HLT_el30_LRTmedium_r_offlinesig_n"), hist("h_pt_eff_HLT_el30_LRTmedium_r_offlinesig_d") )
h_pt_eff_HLT_el30_LRTmedium_r_offlinesig.Write()

h_pt_eff_HLT_el26_comb_r_offlinesig.Divide(hist("h_pt_eff_HLT_el26_comb_r_offlinesig_n"), hist("h_pt_eff_HLT_el26_comb_r_offlinesig_d") )
h_pt_eff_HLT_el26_comb_r_offlinesig.Write()
####################

# electron triggers with respect to offline electrons
h_eta_eff_HLT_el26_r_electron.Divide(hist("h_eta_eff_HLT_el26_r_electron_n"),hist("h_eta_eff_HLT_el26_r_electron_d") )
h_eta_eff_HLT_el26_r_electron.Write()

h_eta_eff_HLT_el30_LRTmedium_r_electron.Divide(hist("h_eta_eff_HLT_el30_LRTmedium_r_electron_n"),hist("h_eta_eff_HLT_el30_LRTmedium_r_electron_d") )
h_eta_eff_HLT_el30_LRTmedium_r_electron.Write()

h_eta_eff_HLT_el26_comb_r_electron.Divide(hist("h_eta_eff_HLT_el26_comb_r_electron_n"),hist("h_eta_eff_HLT_el26_comb_r_electron_d") )
h_eta_eff_HLT_el26_comb_r_electron.Write()

h_z0_eff_HLT_el26_r_electron.Divide(hist("h_z0_eff_HLT_el26_r_electron_n"),hist("h_z0_eff_HLT_el26_r_electron_d") )
h_z0_eff_HLT_el26_r_electron.Write()

h_z0_eff_HLT_el30_LRTmedium_r_electron.Divide(hist("h_z0_eff_HLT_el30_LRTmedium_r_electron_n"),hist("h_z0_eff_HLT_el30_LRTmedium_r_electron_d") )
h_z0_eff_HLT_el30_LRTmedium_r_electron.Write()

h_z0_eff_HLT_el26_comb_r_electron.Divide(hist("h_z0_eff_HLT_el26_comb_r_electron_n"),hist("h_z0_eff_HLT_el26_comb_r_electron_d") )
h_z0_eff_HLT_el26_comb_r_electron.Write()

h_d0_eff_HLT_el26_r_electron.Divide(hist("h_d0_eff_HLT_el26_r_electron_n"), hist("h_d0_eff_HLT_el26_r_electron_d") )
h_d0_eff_HLT_el26_r_electron.Write()

h_d0_eff_HLT_el30_LRTmedium_r_electron.Divide(hist("h_d0_eff_HLT_el30_LRTmedium_r_electron_n"), hist("h_d0_eff_HLT_el30_LRTmedium_r_electron_d") )
h_d0_eff_HLT_el30_LRTmedium_r_electron.Write()

h_d0_eff_HLT_el26_comb_r_electron.Divide(hist("h_d0_eff_HLT_el26_comb_r_electron_n"), hist("h_d0_eff_HLT_el26_comb_r_electron_d") )
h_d0_eff_HLT_el26_comb_r_electron.Write()

h_d0_zoom_eff_HLT_el26_r_electron.Divide(hist("h_d0_zoom_eff_HLT_el26_r_electron_n"), hist("h_d0_zoom_eff_HLT_el26_r_electron_d") )
h_d0_zoom_eff_HLT_el26_r_electron.Write()

h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron.Divide(hist("h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron_n"), hist("h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron_d") )
h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron.Write()

h_d0_zoom_eff_HLT_el26_comb_r_electron.Divide(hist("h_d0_zoom_eff_HLT_el26_comb_r_electron_n"), hist("h_d0_zoom_eff_HLT_el26_comb_r_electron_d") )
h_d0_zoom_eff_HLT_el26_comb_r_electron.Write()

h_prodR_eff_HLT_el26_r_electron.Divide(hist("h_prodR_eff_HLT_el26_r_electron_n"), hist("h_prodR_eff_HLT_el26_r_electron_d") )
h_prodR_eff_HLT_el26_r_electron.Write()

h_prodR_eff_HLT_el30_LRTmedium_r_electron.Divide(hist("h_prodR_eff_HLT_el30_LRTmedium_r_electron_n"), hist("h_prodR_eff_HLT_el30_LRTmedium_r_electron_d") )
h_prodR_eff_HLT_el30_LRTmedium_r_electron.Write()

h_prodR_eff_HLT_el26_comb_r_electron.Divide(hist("h_prodR_eff_HLT_el26_comb_r_electron_n"), hist("h_prodR_eff_HLT_el26_comb_r_electron_d") )
h_prodR_eff_HLT_el26_comb_r_electron.Write()

h_pt_eff_HLT_el26_r_electron.Divide(hist("h_pt_eff_HLT_el26_r_electron_n"), hist("h_pt_eff_HLT_el26_r_electron_d") )
h_pt_eff_HLT_el26_r_electron.Write()

h_pt_eff_HLT_el30_LRTmedium_r_electron.Divide(hist("h_pt_eff_HLT_el30_LRTmedium_r_electron_n"), hist("h_pt_eff_HLT_el30_LRTmedium_r_electron_d") )
h_pt_eff_HLT_el30_LRTmedium_r_electron.Write()

h_pt_eff_HLT_el26_comb_r_electron.Divide(hist("h_pt_eff_HLT_el26_comb_r_electron_n"), hist("h_pt_eff_HLT_el26_comb_r_electron_d") )
h_pt_eff_HLT_el26_comb_r_electron.Write()
####################

h_pt_offlinesig_g20_eff.Divide( hist("h_pt_g20_r_offlinesig_eff_n"), hist("h_pt_g20_r_offlinesig_eff_d") )
h_pt_offlinesig_g20_eff.Write()

h_prodR_g20_r_offlinesig_eff.Divide( hist("h_prodR_g20_r_offlinesig_eff_n"), hist("h_prodR_g20_r_offlinesig_eff_d") )
h_prodR_g20_r_offlinesig_eff.Write()

h_d0_g20_r_offlinesig_eff.Divide( hist("h_d0_g20_r_offlinesig_eff_n"), hist("h_d0_g20_r_offlinesig_eff_d") )
h_d0_g20_r_offlinesig_eff.Write()




## el lrt tnp support
h_pt_el_lrt_tnp_phtag_eff.Divide(hist("h_pt_el_lrt_tnp_phtag_eff_n"), hist("h_pt_el_lrt_tnp_phtag_eff_d") )
h_pt_el_lrt_tnp_phtag_eff.Write()

h_prodR_el_lrt_tnp_phtag_eff.Divide(hist("h_prodR_el_lrt_tnp_phtag_eff_n"), hist("h_prodR_el_lrt_tnp_phtag_eff_d") ) 
h_prodR_el_lrt_tnp_phtag_eff.Write()





# LRT Electron26 eff vs truth
h_d0_HLT_el30_LRT_r_truth_eff.Divide(hist("h_d0_HLT_el30_LRT_r_truth_eff_n"),hist("h_d0_HLT_el30_LRT_r_truth_eff_d") )
h_d0_HLT_el30_LRT_r_truth_eff.Write()

h_pt_HLT_el30_LRT_r_truth_eff.Divide(hist("h_pt_HLT_el30_LRT_r_truth_eff_n"),hist("h_pt_HLT_el30_LRT_r_truth_eff_d") )
h_pt_HLT_el30_LRT_r_truth_eff.Write()

h_eta_HLT_el30_LRT_r_truth_eff.Divide(hist("h_eta_HLT_el30_LRT_r_truth_eff_n"),hist("h_eta_HLT_el30_LRT_r_truth_eff_d") )
h_eta_HLT_el30_LRT_r_truth_eff.Write()

h_phi_HLT_el30_LRT_r_truth_eff.Divide(hist("h_phi_HLT_el30_LRT_r_truth_eff_n"),hist("h_phi_HLT_el30_LRT_r_truth_eff_d") )
h_phi_HLT_el30_LRT_r_truth_eff.Write()

h_vtxR_HLT_el30_LRT_r_truth_eff.Divide(hist("h_vtxR_HLT_el30_LRT_r_truth_eff_n"),hist("h_vtxR_HLT_el30_LRT_r_truth_eff_d") )
h_vtxR_HLT_el30_LRT_r_truth_eff.Write()

# Prompt Electron26 eff vs truth
h_d0_HLT_el26_r_truth_eff.Divide(hist("h_d0_HLT_el26_r_truth_eff_n"),hist("h_d0_HLT_el26_r_truth_eff_d") )
h_d0_HLT_el26_r_truth_eff.Write()

h_pt_HLT_el26_r_truth_eff.Divide(hist("h_pt_HLT_el26_r_truth_eff_n"),hist("h_pt_HLT_el26_r_truth_eff_d") )
h_pt_HLT_el26_r_truth_eff.Write()

h_eta_HLT_el26_r_truth_eff.Divide(hist("h_eta_HLT_el26_r_truth_eff_n"),hist("h_eta_HLT_el26_r_truth_eff_d") )
h_eta_HLT_el26_r_truth_eff.Write()

h_phi_HLT_el26_r_truth_eff.Divide(hist("h_phi_HLT_el26_r_truth_eff_n"),hist("h_phi_HLT_el26_r_truth_eff_d") )
h_phi_HLT_el26_r_truth_eff.Write()

h_vtxR_HLT_el26_r_truth_eff.Divide(hist("h_vtxR_HLT_el26_r_truth_eff_n"),hist("h_vtxR_HLT_el26_r_truth_eff_d") )
h_vtxR_HLT_el26_r_truth_eff.Write()

# LRT Muon24 eff vs truth
h_d0_HLT_mu24_LRT_r_truth_eff.Divide(hist("h_d0_HLT_mu24_LRT_r_truth_eff_n"),hist("h_d0_HLT_mu24_LRT_r_truth_eff_d") )
h_d0_HLT_mu24_LRT_r_truth_eff.Write()

h_pt_HLT_mu24_LRT_r_truth_eff.Divide(hist("h_pt_HLT_mu24_LRT_r_truth_eff_n"),hist("h_pt_HLT_mu24_LRT_r_truth_eff_d") )
h_pt_HLT_mu24_LRT_r_truth_eff.Write()

h_eta_HLT_mu24_LRT_r_truth_eff.Divide(hist("h_eta_HLT_mu24_LRT_r_truth_eff_n"),hist("h_eta_HLT_mu24_LRT_r_truth_eff_d") )
h_eta_HLT_mu24_LRT_r_truth_eff.Write()

h_phi_HLT_mu24_LRT_r_truth_eff.Divide(hist("h_phi_HLT_mu24_LRT_r_truth_eff_n"),hist("h_phi_HLT_mu24_LRT_r_truth_eff_d") )
h_phi_HLT_mu24_LRT_r_truth_eff.Write()

h_vtxR_HLT_mu24_LRT_r_truth_eff.Divide(hist("h_vtxR_HLT_mu24_LRT_r_truth_eff_n"),hist("h_vtxR_HLT_mu24_LRT_r_truth_eff_d") )
h_vtxR_HLT_mu24_LRT_r_truth_eff.Write()

# prompt Muon24 eff vs truth
h_d0_HLT_mu24_r_truth_eff.Divide(hist("h_d0_HLT_mu24_r_truth_eff_n"),hist("h_d0_HLT_mu24_r_truth_eff_d") )
h_d0_HLT_mu24_r_truth_eff.Write()

h_pt_HLT_mu24_r_truth_eff.Divide(hist("h_pt_HLT_mu24_r_truth_eff_n"),hist("h_pt_HLT_mu24_r_truth_eff_d") )
h_pt_HLT_mu24_r_truth_eff.Write()

h_eta_HLT_mu24_r_truth_eff.Divide(hist("h_eta_HLT_mu24_r_truth_eff_n"),hist("h_eta_HLT_mu24_r_truth_eff_d") )
h_eta_HLT_mu24_r_truth_eff.Write()

h_phi_HLT_mu24_r_truth_eff.Divide(hist("h_phi_HLT_mu24_r_truth_eff_n"),hist("h_phi_HLT_mu24_r_truth_eff_d") )
h_phi_HLT_mu24_r_truth_eff.Write()

h_vtxR_HLT_mu24_r_truth_eff.Divide(hist("h_vtxR_HLT_mu24_r_truth_eff_n"),hist("h_vtxR_HLT_mu24_r_truth_eff_d") )
h_vtxR_HLT_mu24_r_truth_eff.Write()


# msonly trigger validation
h_pt_sublead_HLT_2mu50_r_offline_eff.Divide(hist("h_pt_sublead_HLT_2mu50_r_offline_eff_n"), hist("h_pt_sublead_HLT_2mu50_r_offline_eff_d") )
h_pt_sublead_HLT_2mu50_r_offline_eff.Write()

h_eta_sublead_HLT_2mu50_r_offline_eff.Divide(hist("h_eta_sublead_HLT_2mu50_r_offline_eff_n"), hist("h_eta_sublead_HLT_2mu50_r_offline_eff_d") )
h_eta_sublead_HLT_2mu50_r_offline_eff.Write()

h_phi_sublead_HLT_2mu50_r_offline_eff.Divide(hist("h_phi_sublead_HLT_2mu50_r_offline_eff_n"), hist("h_phi_sublead_HLT_2mu50_r_offline_eff_d") )
h_phi_sublead_HLT_2mu50_r_offline_eff.Write()

h_prodR_HLT_2mu50_r_offline.Divide(hist("h_prodR_HLT_2mu50_r_offline_n"), hist("h_prodR_HLT_2mu50_r_offline_d") )
h_prodR_HLT_2mu50_r_offline.Write()

h_pt_el_HLT_g40mu40_r_offline_eff.Divide(hist("h_pt_el_HLT_g40mu40_r_offline_eff_n"), hist("h_pt_el_HLT_g40mu40_r_offline_eff_d") )
h_pt_el_HLT_g40mu40_r_offline_eff.Write()

h_pt_mu_HLT_g40mu40_r_offline_eff.Divide(hist("h_pt_mu_HLT_g40mu40_r_offline_eff_n"), hist("h_pt_mu_HLT_g40mu40_r_offline_eff_d") )
h_pt_mu_HLT_g40mu40_r_offline_eff.Write()

h_prodR_el_HLT_g40mu40_r_offline.Divide(hist("h_prodR_el_HLT_g40mu40_r_offline_n"), hist("h_prodR_el_HLT_g40mu40_r_offline_d") )
h_prodR_el_HLT_g40mu40_r_offline.Write()

h_prodR_mu_HLT_g40mu40_r_offline.Divide(hist("h_prodR_mu_HLT_g40mu40_r_offline_n"), hist("h_prodR_mu_HLT_g40mu40_r_offline_d") )
h_prodR_mu_HLT_g40mu40_r_offline.Write()

h_eta_mu_HLT_g40mu40_r_offline_eff.Divide(hist("h_eta_mu_HLT_g40mu40_r_offline_eff_n"), hist("h_eta_mu_HLT_g40mu40_r_offline_eff_d") )
h_eta_mu_HLT_g40mu40_r_offline_eff.Write()

h_phi_mu_HLT_g40mu40_r_offline_eff.Divide(hist("h_phi_mu_HLT_g40mu40_r_offline_eff_n"), hist("h_phi_mu_HLT_g40mu40_r_offline_eff_d") )
h_phi_mu_HLT_g40mu40_r_offline_eff.Write()


# single leg g40
h_pt_el_HLT_g40_r_offline_eff.Divide(hist("h_pt_el_HLT_g40_r_offline_eff_n"),hist("h_pt_el_HLT_g40_r_offline_eff_d") )
h_pt_el_HLT_g40_r_offline_eff.Write()


h_eta_el_HLT_g40_r_offline_eff.Divide(hist("h_eta_el_HLT_g40_r_offline_eff_n"),hist("h_eta_el_HLT_g40_r_offline_eff_d") )
h_eta_el_HLT_g40_r_offline_eff.Write()

h_prodR_el_HLT_g40_r_offline_eff.Divide(hist("h_prodR_el_HLT_g40_r_offline_eff_n"),hist("h_prodR_el_HLT_g40_r_offline_eff_d") )
h_prodR_el_HLT_g40_r_offline_eff.Write()



f_in.Close()
f_out.Close()

