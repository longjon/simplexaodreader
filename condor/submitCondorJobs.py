#!/usr/bin/env python3

"""
Generic script for submitting batch jobs to condor on lxplus.
Makes directories in ./logs/ per job and submits from that directory.

April 6th, 2020
Jonathan Long
"""


import os
#import sys
import argparse
import subprocess as sp
from pathlib import Path
import shutil
from datetime import datetime

########## Settings ################### 
parser = argparse.ArgumentParser()
parser.add_argument("--dryRun",   help="Don't actually submit job", action="store_true")
parser.add_argument("--tag", "-t", default="")

args = parser.parse_args()

dryRun = False
if args.dryRun:
    dryRun = True


jobScript = "job.sh" # bash script that gets run on batch system

currentDir = Path.cwd()
logDir = currentDir / "logs"

tag = args.tag



############ Deeper Settings ###############
timeNow = datetime.now()

if not logDir.exists():
    print("Creating log directory: ",logDir)
    Path.mkdir(logDir)

    

#################################################
# Define something to loop over that will be your jobs
basedir = "/eos/atlas/user/l/longjon/Trigger/AODs/"
jo_default = "test_jobOptions_mu.py"
listOfJobs = [
#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210512_v0_EXT0",
#"user.longjon.312571.Py8EG_A14_NNPDF23LO_LLztau_all_m200_t1ns.AOD.210507_v0_EXT0",
#"user.longjon.data18_13TeV.00360026.physics_EnhancedBias.210512_v0_EXT0",

# test roi size
#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210526_v2_EXT0",
#"user.longjon.312571.Py8EG_A14_NNPDF23LO_LLztau_all_m200_t1ns.AOD.210526_phi0.6_v2_EXT0",
#"user.longjon.312571.Py8EG_A14_NNPDF23LO_LLztau_all_m200_t1ns.AOD.210526_v2_EXT0",
#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210526_phi0.6_v2_EXT0"

#"user.longjon.399011.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_1ns.AOD.210526_v2_EXT0",
#"user.longjon.399032.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_0_1ns.AOD.210526_v2_EXT0"

#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210601_electronChainTest_v7_EXT0",
#"user.longjon.data18_13TeV.00360026.physics_EnhancedBias.210601_electronChainTest_v7_EXT0"

#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210602_electronChainTestMore_v8_EXT0",
#"user.longjon.data18_13TeV.00360026.physics_EnhancedBias.210602_electronChainTestMore_v8_EXT0"

#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210606_electronChainTest_v0_EXT0",
#"user.longjon.data18_13TeV.00360026.physics_EnhancedBias.210606_electronChainTest_v0_EXT0"

#"user.bkerridg.rhadron_20210423_b_EXT0",

#"user.longjon.data18_13TeV.00360026.physics_EnhancedBias.210607_electronChainTest_v0_EXT0",
#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210607_electronChainTest_v0_EXT0"

#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210608_electronChainTest_d0hack_v1_EXT0",
#"user.longjon.data18_13TeV.00360026.physics_EnhancedBias.210608_electronChainTest_d0hack_v1_EXT0"

###"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210609_electronChainTest_noftfd0_v1_EXT0",
####"user.longjon.data18_13TeV.00360026.physics_EnhancedBias.210615_eLRT_v0_EXT0",
###"user.longjon.399032.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_0_1ns.AOD.210616_eLRT_v0_EXT0",
###"user.longjon.399056.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_700_0_1ns.AOD.210616_eLRT_v0_EXT0",
###"user.longjon.312571.Py8EG_A14_NNPDF23LO_LLztau_all_m200_t1ns.AOD.210616_eLRT_v0_EXT0",

########
##"mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.recon.AOD.e6348_e5984_s3126_d1663_r12711",
##"mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.recon.AOD.e3601_s3126_d1663_r12711",
##"mc16_13TeV.301325.Pythia8EvtGen_A14NNPDF23LO_zprime1000_tt.recon.AOD.e4061_e5984_s3126_d1663_r12711",
##"user.longjon.312571.Py8EG_A14_NNPDF23LO_LLztau_all_m200_t1ns.AOD.210922_r22039_v0_EXT0",
##"user.longjon.399011.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_1ns.AOD.210922_r22039_v0_EXT0",
##"user.longjon.399032.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_0_1ns.AOD.210922_r22039_v0_EXT0",
##"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210922_r22039_v0_EXT0",

#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.210922_r22043_v0_EXT0",
#"user.longjon.399032.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_0_1ns.AOD.210922_r22043_v0_EXT0",
#"user.longjon.312571.Py8EG_A14_NNPDF23LO_LLztau_all_m200_t1ns.AOD.210922_r22043_v0_EXT0",
#"user.longjon.399011.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_1ns.AOD.210922_r22043_v0_EXT0"

##"user.longjon.399032.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_0_1ns.AOD.211108_gsf_v2_EXT0",
##"user.longjon.312571.Py8EG_A14_NNPDF23LO_LLztau_all_m200_t1ns.AOD.211108_gsf_v2_EXT0",
##"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.211108_gsf_v2_EXT0"

#"user.longjon.448024.MGPy8EG_A14NNPDF23LO_TT_RPVdirectBL_1200_tau_1ns.AOD.211116_v1_22047_EXT0",
#"user.longjon.399032.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_0_1ns.AOD.211116_v2_22047_EXT0",
#"user.longjon.399056.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_700_0_1ns.AOD.211116_v2_22047_EXT0",
#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.211116_v2_22047_EXT0",
#"user.longjon.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.AOD.211116_v2_22047_EXT0",
#"user.longjon.313465.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_15G_lt10dd.AOD.211116_v2_22047_EXT0",
#"user.longjon.313423.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_3G_lt10dd.AOD.211116_v2_22047_EXT0",

#"user.longjon.313478.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_20G_lt100dd.AOD.211118_v2_22047_EXT0",
#"user.longjon.313015.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_20G_lt100dd_emu.AOD.211118_v2_22047_EXT0",
#"user.longjon.399011.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_1ns.AOD.211116_v2_22047_EXT0",
#"user.longjon.399015.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_1ns.AOD.211116_v2_22047_EXT0",
#"user.longjon.312571.Py8EG_A14_NNPDF23LO_LLztau_all_m200_t1ns.AOD.211116_v2_22047_EXT0",
#"user.longjon.301325.Pythia8EvtGen_A14NNPDF23LO_zprime1000_tt.211116_v2_22047_EXT0",
#"user.longjon.313477.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_20G_lt10dd.AOD.211118_v2_22047_EXT0",

#"mc20_13TeV.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.merge.AOD.e7067_e5984_s3338_d1708_r13044_r13208_tid27413341_00",

#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.220218_r22056_v1_EXT0"
#"user.longjon.399032.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_0_1ns.AOD.030122n_v1_EXT0",
#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.030122n_v1_EXT0"

#"valid1.399056.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_700_0_1ns.merge.AOD.e6633_e5984_s3775_r13476_r13491",
#"valid1.399005.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_0p01ns.merge.AOD.e6633_e5984_s3775_r13476_r13491",
#"valid1.399068.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_500_0_10ns.merge.AOD.e7312_e5984_s3775_r13476_r13491",
#"valid1.399067.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_500_0_3ns.merge.AOD.e7312_e5984_s3775_r13476_r13491",
#"valid1.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.merge.AOD.e6633_s3775_r13476_r13491",
#"valid1.399069.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_500_0_30ns.merge.AOD.e7312_e5984_s3775_r13476_r13491",

#"user.longjon.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.AOD.220331_gsf_v1_EXT0",
#"user.longjon.399056.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_700_0_1ns.AOD.220331_gsf_v1_EXT0",
#"user.longjon.399011.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_1ns.AOD.220331_gsf_v1_EXT0",
#"user.longjon.399032.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_0_1ns.AOD.220331_gsf_v1_EXT0",

"user.kazheng.399007.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_1ns.simul.HITS.e7067_s3380_r11944.AOD.Feb05.v0.0_EXT0_EXT0"

]

#basedir = "/eos/home-l/longjon/Trigger/AOD/" 


for job in listOfJobs:
    if "LLztau" in job or "zprime" in job:
        jo = "test_jobOptions_zprime.py"
    elif "ttbar" in job:
        jo = "test_jobOptions_ttbar.py"
    elif "Zmumu" in job or "Zee" in job:
        jo = "test_jobOptions_Z.py"
    elif "TT" in job:
        jo = "test_jobOptions_rhad.py"
    elif "HNL" in job:
        jo = "test_jobOptions_hnl.py"
    else:
        jo = jo_default

    if 'user' in job:
        jobNamePrefix = ".".join([job.split('.')[2], job.split('AOD.')[-1]])
    else:
        jobNamePrefix = ".".join([job.split('.')[1], job.split('AOD.')[-1]])

    # Setup Job Name
    jobName = (jobNamePrefix + "_" if jobNamePrefix!="" else "") + jo + "_" + str(timeNow.year) + "-" + str(timeNow.month).zfill(2) + "-" + str(timeNow.day).zfill(2) + ("_"+tag if tag!="" else "") #+ "-" + str(timeNow.hour).zfill(2) + "-" + str(timeNow.minute).zfill(2) + "-" + str(timeNow.second).zfill(2)

    Files = sp.check_output(f"ls {basedir}/{job}", shell=True).decode('UTF-8').split()
    nFiles = len(Files)


    jobDir = logDir / jobName
    if not jobDir.exists():
        print("Creating job directory: ", jobDir)
        Path.mkdir(jobDir)

    os.chdir(jobDir)    

    ### Copy job script to jobdir
    shutil.copy(currentDir / jobScript, jobDir / jobScript)
    
    ### Make condor file
    #espresso     = 20 minutes
    #microcentury = 1 hour
    #longlunch    = 2 hours
    #workday      = 8 hours
    #tomorrow     = 1 day
    #testmatch    = 3 days
    #nextweek     = 1 week

    condorFileName = str(jobDir / (jobName+".condor"))
    condorFile = open(condorFileName ,"w")
    condorFile.write(
f"""
executable      = {jobScript}
universe        = vanilla
arguments       = "{jobDir} {job} $(ProcId) {jo} {basedir}"

output          = {jobDir}/job.$(ProcId).out
error           = {jobDir}/job.$(ProcId).err
log             = {jobDir}/job.$(ProcId).log

#send_credential = True
+JobFlavour = "espresso"
Queue {nFiles}
""" #.format(jobScript, jobDir, job, nFiles, jo)
)
    condorFile.close()

    ### Submit job
    if not dryRun:
        print(str(sp.check_output("condor_submit " + condorFileName, shell=True, encoding="ASCII")))
    



print("I'm finished!")

