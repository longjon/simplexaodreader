#!/bin/bash 

logDir=${1}
sample=${2}
id=${3}
jo=${4}
basedir=${5}

# Setup Environment
pushd .

#athena_version="Athena,22.0.47"
export ALRB_localConfigDir="/etc/hepix/sh/GROUP/zp/alrb";
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh;
#source ${AtlasSetup}/scripts/asetup.sh ${athena_version}
#TestArea=${logDir}/../../../build_ath/
echo ${logDir}/../../../build/x86_64-*/setup.sh
#source ${logDir}/../../../build_ath/x86_64-centos7-gcc8-opt/setup.sh

cd ${logDir}/../../../build/
asetup
source x86_64-*/setup.sh
popd


#echo $TestArea
#echo `which athena`
#echo `pwd`
#echo $PATH

#AtlasSetup=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/AtlasSetup/V02-00-24/AtlasSetup
#source $AtlasSetup/scripts/asetup.sh $athena_version


# Run code
echo $jobName
echo $logDir
echo $sample
echo $jo
echo $basedir

i=`ls ${basedir}/${sample} | sed -n $(expr ${id} + 1)p`


athena simplexAODReader/${jo} --filesInput ${basedir}/${sample}/${i} |tee ${i}.txt

mv simplexAODReaderEL.hists*root ${i}.txt ${logDir}/

