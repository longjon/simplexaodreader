import datetime
from glob import glob
import hashlib
import sys

runTimeStr = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")

if 'key' not in dir():
    key = "test_"

if 'isRun3' not in dir():
    isRun3 = False
isRun3 = True

#ART from https://atlas-art-data.web.cern.ch/atlas-art-data/local-output/master/Athena/x86_64-centos7-gcc8-opt/2021-02-16T2101/TrigAnalysisTest/test_trigAna_RDOtoAOD_v1Dev_build/AOD.pool.root
#inputFile = "/eos/atlas/user/l/longjon/Trigger/AODs/AOD.art.pool.root"

inputFiles = [
    "AOD.pool.root"
]

from AthenaConfiguration.AllConfigFlags import initConfigFlags
flags = initConfigFlags()
# this filles in the input files array
args = flags.fillFromArgs()


if args.filesInput != []:
    #inputFiles = args.filesInput #.split(',') # not needed?
    print(f"InputFile[0] = {inputFiles[0]}")

    if 'test' in key:
        if 'afs' in inputFiles[0]:
            key = "test_" 
        elif 'root://eosatlas.cern.ch' in inputFiles[0]:
            key = "grid_"
        elif 'user' in inputFiles[0]:
            # works ok for user.longjon.28365459.EXT0._000004.AOD.pool.root
            #split if slash
            tmp_if = inputFiles[0].split('/')[-1]
            try:
                key+=tmp_if[0].split(".")[2]+"-"+tmp_if[0].split(".")[-4]
            except IndexError:
                print("Issue generating key with user, setting to test_")
                key = "test_"
        else:
            # AOD.28429144._000001.pool.root.1
            try:
                tmp_if = inputFiles[0].split('/')[-1]
                key+=tmp_if[0].split(".")[1]+"-"+tmp_if[0].split(".")[-5]+"-"+tmp_if[0].split(".")[-4]
            except IndexError:
                print("Issue generating key, setting to test_")
                key = "test_"

flags.lock()

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
acc = MainServicesCfg(flags)

from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
acc.merge(PoolReadCfg(flags))

# Determine if input is DAOD, probably a better way
isDAOD = "daod" in args.filesInput[0].lower()
print(f"isDAOD = {isDAOD}")

# Setup histogram output
THistSvc=CompFactory.THistSvc()
#svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms
hash = hashlib.md5(inputFiles[0].encode("utf-8")).hexdigest()[0:4]
THistSvc.Output += ["ANALYSIS DATAFILE='simplexAODReaderEL.hists-{}-{}-{}.root' OPT='RECREATE'".format(runTimeStr,key,hash)]
acc.addService(THistSvc)


# Setup Trigger Decision Tool
from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
tdt = acc.getPrimaryAndMerge(TrigDecisionToolCfg(flags))
#tdt = acc.getPrimaryAndMerge(tdtAcc) # was poptoolsand merge, what is going on why won't this work


# Setup MC Truth Classifier
from  MCTruthClassifier.MCTruthClassifierConfig import MCTruthClassifierCfg
mctc = acc.popToolsAndMerge(MCTruthClassifierCfg(flags,
                            ParticleCaloExtensionTool = "",
                            xAODTruthParticleContainerName = "TruthBSMWithDecayParticles" if isDAOD else "TruthParticles") )

# Setup trigger matching tools
match_comp = CompFactory.Trig.MatchFromCompositeTool("MatchFromCompositeTool") 
match_compLRT = CompFactory.Trig.MatchFromCompositeTool("MatchFromCompositeToolLRT",InputPrefix="LRTTrigMatch_") 
match_R3 = CompFactory.Trig.R3MatchingTool("R3MatchingTool") #,OutputLevel=DEBUG)

# Create a MuonSelectionTool if we do not yet have one 
from MuonSelectorTools.MuonSelectorToolsConfig import MuonSelectionToolCfg
#mst = acc.getPrimaryAndMerge(
#    MuonSelectionToolCfg(flags,
#                         name = "MyMuonSelectionTool",
#                         MaxEta=2.8,
#                         MuQuality=1, # Medium Muon
#                         PixCutOff=True,
#                         TurnOffMomCorr = True,
#                         IsRun3Geo = True, 
#                         AllowSettingGeometryOnTheFly = True)
#) # why doesn't this work anymore???? why doesn't the tool use any of the config arguments?
mst = CompFactory.CP.MuonSelectionTool(name = "MyMuonSelectionTool",
                         MaxEta=2.7,
                         MuQuality=1, # Medium Muon
                         PixCutOff=True,
                         TurnOffMomCorr = True,
                         IsRun3Geo = True, 
                         AllowSettingGeometryOnTheFly = True)

#flags.Trigger.EDMVersion == 3,
acc.addPublicTool(mst)


# Electron LLH Tool
# Simliar to here https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Trigger/TrigHypothesis/TrigEgammaHypo/python/TrigEgammaPrecisionElectronHypoTool.py#0347
# https://its.cern.ch/jira/browse/ATLINFR-4131
# /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/ElectronPhotonSelectorTools/trigger/rel22_20210611
from ElectronPhotonSelectorTools.ElectronPhotonSelectorToolsConf import AsgElectronLikelihoodTool
llht = CompFactory.AsgElectronLikelihoodTool(
    name = "AsgElectronLHLooseSelectorNoPix",
    ConfigFile = "ElectronPhotonSelectorTools/trigger/rel22_20210611/ElectronLikelihoodLooseTriggerConfig_NoPix.conf",
    skipDeltaPoverP = True
    )    
acc.addPublicTool(llht)                                                      

# TODO misisng new LLH tools

# Electron merger tool
#from AnaAlgorithm.DualUseConfig import createAlgorithm
#MergedElectronContainer = "StdWithLRTElectrons"
#alg = createAlgorithm('CP::ElectronLRTMergingAlg','ElectronLRTMergingAlg')
#alg.PromptElectronLocation  = "Electrons"
#alg.LRTElectronLocation     = "LRTElectrons"
#alg.OutputCollectionName    = MergedElectronContainer
#alg.isDAOD                  = True
#alg.CreateViewCollection    = False
#athAlgSeq += alg


# limit the number of events (for testing purposes)
acc.EvtMax = -1

# Create the algorithm's configuration.
alg = CompFactory.simplexAODReaderEL('simplexAODReaderAlg')

alg.trigDecTool = tdt
alg.trigMatchTool    = match_R3 if isRun3 else match_comp
alg.trigMatchToolLRT = match_R3 if isRun3 else match_compLRT
alg.MuonSelectionTool = mst
alg.ElectronLLHTool = llht
alg.truthClassifier = mctc

alg.Debug = False # Extra printout
alg.isDAOD = isDAOD # run on DAOD
alg.makeNtuple = False # produce output ntuple
if alg.makeNtuple: job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))
alg.RootStreamName = 'ANALYSIS'
alg.includeExtraLLH = False # include extra LLH version for checks
alg.includeLRT = True # Include FTF LRT tracks
alg.printTriggerDecCount = True # Print out all trigger decisions
alg.doTruth = True # Loop over truth particles
alg.extrapolateTruth = False # Extrapolate truth particles back to origin for dR matching
alg.matchSizeR = 0.02 # Delta R used for matching FTF tracks to truth/offline
alg.Highd0Cut = 2 # Cut to be considered high d0
alg.roiSize = 0.4 # ROI matching size
alg.EMPhiAdjust = 0 #subtract off from EM dphi in ROI mathcing to compute dR
alg.minTrackd0 = -1 # Min track d0
alg.minTrackpT = 1 # Min track pT
alg.maxTrackEta = 2.5 # Max track eta
alg.truthMaxProdR = 300 # max truth track production vertex radius in mm, was 520
alg.truthMinDecayR = -1 # min truth track decay vertex radius in mm
alg.minTruthLayers = 8 # min number of layers truth particle should pass
alg.SiLayers = [33.25, 50.5, 88.5, 122.5, 299, 299.01, 371, 371.01, 443, 443.01, 514, 514.01] # radius layers of silicon detector in mm.  SCT are double layers--this is a hack
alg.maxBarcode = -1 # max barcode for truth particles.  This is probably signal model dependent 
alg.minHitsFTF = -1 # min hits on FTF tracks, was 8
alg.minJetpT = 80 # Jet pT Cut
alg.minHLTtrkmht = 60 # MET trkmht cut
alg.minHLTpufit = 60 # MET pufit cut
alg.useOnlyOfflineSignal = False # only fill offline track container with signal tracks
alg.excludeParentPDGIDs = [22] # if truth particle has one of these as a parent, it's not a real signal
alg.signalParentPDGIDs = [1000015, 2000015, 1000011, 1000013, 2000011, 2000013] + [1000006, 2000006]
alg.signalPDGIDs = [11,13]

#if "N2" in inputFiles[0]: 
#    alg.signalParentPDGIDs = [1000023, 1000024]
#    alg.signalPDGIDs = [211,11,13]
#if "Slep" in inputFiles[0]: alg.signalParentPDGIDs = [1000011, 1000013, 1000015, 2000011, 2000013, 2000015]

acc.addEventAlgo(alg)

# optional include for reducing printout from athena
acc.getService("AthenaEventLoopMgr").EventPrintoutInterval = 1000


# Return a status code
sys.exit(not acc.run().isSuccess())
