# Overview

Some fairly simple code to run over xAODs, mostly for unconventional track trigger studies.  Based on simple setup here: https://indico.cern.ch/event/811664/contributions/3382388/attachments/1855860/3048195/Tutorial-2019-06-04-Krumnack.pdf .  Don't forget to add includes in cmake file when using additional xAOD classes.

# Setup Athena
Make a build, run, and source directory, setup a release, cmake, and build:

```
mkdir build run source
cd source
git clone ssh://git@gitlab.cern.ch:7999/longjon/simplexaodreader.git
cp simplexaodreader/CMakeLists.txt_common CMakeLists.txt
cd ..; ln -s source/simplexaodreader/condor .
cd build
# Try to match the release to the one used for the sample to avoid problems
# Example from 24.0 series on lxplus9
asetup Athena,24.0.29
cmake ../source/ -DATLAS_GCC_CHECKERS=none
make
source x86_64-el9-gcc13-opt/setup.sh
cd ../run
python -m simplexaodreader.test_jobOptions_mu_CA --filesInput file.root
```
If you are running on a Run3 sample, you need to add `-c "isRun3=True"` to get the muonselectiontool to work.  `AllowSettingGeometryOnTheFly=True` should get around this.


There was a bug that requires running on one input file a at time; this seems to be fixed now.  Scripts to merge the root files into graphs and merge the output cutflows live in the parent scripts directory: `remakeGraphs.py` and `MergeCutflow.py`.  These can still be used for running in parallel.

## Non CA setup for old releases < 24
`athena simplexAODReader/test_jobOptions_mu.py | tee log`

## CA configuration is different
because of course it is and why would it be worth making it easy to transition.  Note the dot not slash below and the need to update the cmake file to install the python files, so the job options need to be in the python not share sub-directory.
```python -m simplexaodreader.test_jobOptions_mu_CA --filesInput /afs/cern.ch/work/l/longjon/SUSY/Run3_LLP/Derivations/run/DAOD_LLP1.output_daod.llp1.pool.root```


## Example loop over files
`for i in `ls /eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.399011.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_1ns.AOD.110329_muRoI0.2_v0_EXT0`; do athena simplexAODReader/test_jobOptions.py --filesInput /eos/atlas/user/l/longjon/Trigger/AODs/user.longjon.399011.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_1ns.AOD.110329_muRoI0.2_v0_EXT0/${i} |tee ${i}.txt; done`


# Setup AnalysisBase (old)
Not sure if this still works.

Make a build and run directory, setup a release in source, cmake, and build:
```
cd source
asetup 22.2.11,AnalysisBase,here
cd ../
mkdir build run
cd build
cmake ../source
make
source x86_64-centos7-gcc8-opt/setup.sh
```



Everytime you log in or start a fresh shell you need to setup the release and source the setup.sh files above.


# Run
Run `athena simplexAODReader/test_jobOptions_mu.py`, as above, from the run directory, edit the file list in this script or use `--filesInput` to run over different inputs.  Histograms end up in the `hist-*` file and the TTree ends up in the file under `data-ANALYSIS/`.

# Grid
You need a linked file in your submission directory called AOD.pool.root
`pathena --inDS=data22_13p6TeV.00431885.physics_EnhancedBias.merge.AOD.r13926_r13927_p5286 --outDS=user.longjon.data22_13p6TeV.00431885.physics_EnhancedBias.merge.AOD.r13926_r13927_p5286.simpleAna.310822_v0   simplexAODReader/test_jobOptions_mu.py `

To copy extra files use the `--extFile fileA,fileB` option.

## CA Grid
`prun --exec="python -m simplexaodreader.test_jobOptions_mu_CA --filesInput=%IN" --inDS=mc20_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_PHYSVAL.e3601_e5984_s3126_s3136_r15250_p5979 --outDS=user.longjon.mc20_13TeV.361107.DAOD_PHYSVAL.test.simpleAna.240201_v2 --useAthenaPackage --outputs="*root" --nJobs=20`


# Editing 

https://atlassoftwaredocs.web.cern.ch/ABtutorial/basic_xaod_content/

## Getting containers and key names
`checkxAOD.py xAOD.pool.root`

## Variables inside containers 
```
root -l xAOD.pool.root
root [1] CollectionTree->Print("egammaClusters*")
```

## Samples
mc21 stau DAOD_LLP1:
`/eos/atlas/user/l/longjon/DAODs/mc21_13p6TeV.516640.MGPy8EG_A14NNPDF23LO_StauStauLLP_100_0_1ns.deriv.DAOD_LLP1.e8481_s3928_r13829_p5333`

mc23 stau DAOD_LLP1 from validation samples:
`/eos/atlas/user/l/longjon/DAODs/user.longjon.516640.MGPy8EG_A14NNPDF23LO_StauStauLLP_100_0_1ns.DAOD_LLP1.2406_230522_v1_EXT0`

## Debugging Tools
You can set the message level of individual tools when they are configured.  I have not found a consistent way to do tihs.  One example https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection/test/ut_ElectronPhotonFourMomentumCorrection_maintest.py#0049:
```tool.msg().setLevel(ROOT.MSG.FATAL)```

See also https://atlassoftwaredocs.web.cern.ch/ABtutorial/el_message_levels/, where 
```alg.setProperty ("OutputLevel", MSG::DEBUG);```
is used where: Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL



### Modifying the column width for tool names
see: `https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/SUSYPhys/SUSYTools/share/jobOptions.py#L103`
I think this needs to be in postexec:
```from AthenaCommon.AppMgr import ServiceMgr as svcMgr; svcMgr.MessageSvc.Format="% F%60W%S%7W%R%T %0W%M"```



### Turn off message limit
Should set this in postexec, not preexec
```from AthenaCommon.AppMgr import ServiceMgr;ServiceMgr.MessageSvc.enableSuppression=False;```