#include "simplexAODReader/cutflow.h"

#include<iostream>

// Usage example
// cutflow_object(1, "Cut name")++;
// cutflow_object(2, "Next Cut name")++;
// calling cutflow( #,"name") without ++ or a selection will ensure the line is printed even if the yield is 0

// Print out a summary in order that was manually set
void cutflow :: printSummary()
{

    std::sort(order_manual.begin(), order_manual.end());

    std::cout<<"=================================="<<std::endl;
    std::cout<<m_name << " cutflow summary"<<std::endl;


    for (std::pair<int, std::string> tuple  : order_manual)
    {
        std::string cut = tuple.second;
        std::cout<<cut << " = " << cut_yields[cut]<< std::endl;
    }
    std::cout<<"=================================="<<std::endl;

}

// Print out a summary in order following first time each cut is called
void cutflow :: printSummaryFIFO()
{

    std::cout<<"=================================="<<std::endl;
    std::cout<<m_name << " cutflow summary"<<std::endl;

    for (std::string cut : order_fifo)
    {
        std::cout<<cut << " = " << cut_yields[cut]<< std::endl;
    }
    std::cout<<"=================================="<<std::endl;

}

// Add line to cutflow, return object to increment.  Method to automatically set order
int& cutflow :: operator[](const std::string &cutname)
{
    // First time we saw this cut
    if (cut_yields.find(cutname) == cut_yields.end() )
    {
        order_fifo.push_back(cutname);
        order_manual.push_back(std::make_pair(-1, cutname));
        cut_yields[cutname] = 0; //initialize value
    }

    return cut_yields[cutname];
}

// Add line to cutflow, return object to increment, and manually set order
int& cutflow :: operator()(const int &order, const std::string &cutname, bool expression, int increment)
{
    // First time we saw this cut or it's a different order
    if (cut_yields.find(cutname) == cut_yields.end() || order_values.find(order) == order_values.end())
    {
        order_fifo.push_back(cutname);
        order_values.insert(order);

        order_manual.push_back(std::make_pair(order, cutname));
        cut_yields[cutname] = 0; //initialize value
    }

    if (expression) cut_yields[cutname]+=increment;

    return cut_yields[cutname];
}