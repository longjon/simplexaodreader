#define _USE_MATH_DEFINES


#include <algorithm> 
#include <iostream>

#define DEBUG_HERE std::cout << "DEBUG: in " __FILE__ ":" << __LINE__ << std::endl;
//#define DEBUG_HERE std::cout << "DEBUG: "<< __LINE__ << std::endl;
//#define DEBUG_HERE ;

#include <AsgMessaging/MessageCheck.h>
#include <simplexAODReader/simplexAODReaderEL.h>
#include <AsgTools/ToolHandle.h>
#include "AsgTools/AnaToolHandle.h"
//#include <EventLoop/Worker.h>


//#include "xAODRootAccess/Init.h"
//#include "xAODRootAccess/TEvent.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "MCTruthClassifier/MCTruthClassifier.h"

#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"



simplexAODReaderEL :: simplexAODReaderEL (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
    ei(nullptr), 
    truthParticles(nullptr), 
    truthEvents(nullptr), 
    inDetTracksOffline(nullptr), 
    inDetTracksLRTOffline(nullptr),
    HLT_FTF_Tracks(nullptr), 
    HLT_lrtFTF_Tracks(nullptr),
    HLT_FTF_Muon_Tracks(nullptr),
    HLT_FTFLRT_Muon_Tracks(nullptr),
    HLT_FTF_Electron_Tracks(nullptr),
    HLT_FTFLRT_Electron_Tracks(nullptr),
    HLT_Electrons(nullptr),
    l1MetObject(nullptr),
    Jets(nullptr),
    HLT_trkmht(nullptr),
    HLT_pufit(nullptr),
    Muons(nullptr),
    MuonSegments(nullptr),
    PrimaryVertices(nullptr),
    m_flag_ei(true),
    m_flag_truthParticles(true),
    m_flag_truthEvents(true),
    m_flag_inDetTracksOffline(true),
    m_flag_inDetTracksLRTOffline(true),
    m_flag_HLT_FTF_Tracks(true),
    m_flag_HLT_lrtFTF_Tracks(true),
    m_flag_HLT_Electrons(true),
    m_flag_HLT_Electrons_lrt(true),
    m_flag_HLT_FTF_Muon_Tracks(true),
    m_flag_HLT_FTFLRT_Muon_Tracks(true),
    m_flag_HLT_PTLRT_Muon_Tracks(true),
    m_flag_HLT_PT_Muon_Tracks(true),
    m_flag_HLT_MuonsL2LRT(true),
    m_flag_HLT_MuonsCBLRT(true),
    m_flag_HLT_MuonsCB(true),
    m_flag_HLT_FTF_Electron_Tracks(true),
    m_flag_HLT_FTFLRT_Electron_Tracks(true),
    m_flag_HLT_PTLRT_Electron_Tracks(true),
    m_flag_HLT_PT_Electron_Tracks(true),
    m_flag_l1MetObject(true),
    m_flag_Jets(true),
    m_flag_HLT_trkmht(true),
    m_flag_HLT_pufit(true),
    m_flag_Muons(true),
    m_flag_MuonsLRT(true),
    m_flag_Electrons(true),
    m_flag_LRTElectrons(true),
    m_flag_MuonSegments(true),
    m_flag_PrimaryVertices(true),
    m_flag_caloClusters(true),
    m_eventCounter(0),
    m_debug(false),
    m_makeNtuple(false),
    m_isSimulation(false),
    m_isDAOD(false),
    m_printTriggerDecCount(true),
    m_xbeam(0),
    m_ybeam(0),
    m_ROISize(0.1),
    m_minTrackd0(-1),
    m_minHitsFTF(8),
    m_useOnlyOfflineSignal(false),
    m_extrapolateTruth(true),
    m_includeLRT(true),
    m_includeExtraLLH(false),
    m_matchSizeR(0),
    //m_truthClassifier("MCTruthClassifier/MCTruthClassifier"),
    //m_trigDecTool("Trig::TrigDecisionTool/TrigDecisionTool"),
    //m_muonSelectionTool("CP::MuonSelectionTool/MuonSelectionTool"),
    //m_ElectronLLHTool("AsgElectronLikelihoodTool/AsgElectronLHLooseSelectorNoPix"),
    m_firstEvent(true),
    m_totalEventCounter(0),
    h_d0_eff_FTFcomb_r_offline(nullptr),
    h_pt_eff_FTFcomb_r_offline(nullptr),
    h_eta_eff_FTFcomb_r_offline(nullptr),
    h_vtxR_eff_FTFcomb_r_offline(nullptr),
    h_d0_pur_FTFcomb_r_offline(nullptr),
    h_pt_pur_FTFcomb_r_offline(nullptr),
    h_eta_pur_FTFcomb_r_offline(nullptr),
    h_vtxR_pur_FTFcomb_r_offline(nullptr)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.

    declareProperty( "matchSizeR", m_matchSizeR = 0.1, "DeltaR cut to match FTF to offline tracks" );

    declareProperty( "roiSize", m_ROISize = 0.1, "dR size of ROI to consider track in it." );

    declareProperty( "EMPhiAdjust", m_EMPhiAdjust = 0., "Value is subtracted from EM dphi number for dR computation for ROI matching." );

    declareProperty( "Highd0Cut", m_Highd0Cut = 2, "d0 cut to count as a high d0 track." );

    declareProperty( "minTrackd0", m_minTrackd0 = -1, "Min d0 requirement in track selection in mm. Set to -1 to disable." );

    declareProperty( "minTrackpT", m_minTrackpT = 1, "Min track pT" );

    declareProperty( "maxTrackEta", m_maxTrackEta = 2.5, "Track eta" );

    declareProperty( "truthMaxProdR", m_truthMaxProdR = 300, "Max production radius in mm for truth particles [mm]." );

    declareProperty( "truthMinDecayR", m_truthMinDecayR = 520, "Minimum decay radius for truth particles [mm]." );

    declareProperty( "minTruthLayers", m_minTruthLayers = 8, "Minimum number of layers, raddii of Si, that truth particles has to pass through");

    declareProperty( "SiLayers", m_SiLayers, "Radius of layers of silicon detector in ATLAS, in mm.");

    declareProperty( "maxBarcode", m_maxBarcode = -1, "Largest barcode on truth particles allowed.  Set to -1 to disable." );

    declareProperty( "minHitsFTF", m_minHitsFTF = 8, "Min number of hits on FTF tracks." );

    declareProperty( "minJetpT", m_minJetpT = 20, "Min Jet pT." );

    declareProperty( "minHLTtrkmht", m_minHLTtrkmht = 0, "HLT met trkmht cut." );

    declareProperty( "minHLTpufit", m_minHLTpufit = 0, "HLT met pufit cut." );

    declareProperty( "useOnlyOfflineSignal", m_useOnlyOfflineSignal = false, "Use only offline stignal tracks matched to truth.");

    declareProperty( "extrapolateTruth", m_extrapolateTruth = true, "Extrapolate truth particles back to origin for dR matching.");

    declareProperty( "includeLRT", m_includeLRT = true, "Include LRT tracks in FTF collection by default, turn off to look at nominal FRF tracks only" );

    declareProperty( "Debug", m_debug = false, "Debug printout" );

    declareProperty( "isDAOD", m_isDAOD = false, "Running on DAOD instead of AOD" );

    declareProperty( "makeNtuple", m_makeNtuple = false, "Make output ntuple" );

    declareProperty( "doTruth", m_doTruth = true, "Do truth particle loops" );

    declareProperty( "printTriggerDecCount", m_printTriggerDecCount = false, "Print counters of each passing trigger chain." );

    declareProperty( "excludeParentPDGIDs", m_excludeParentPDGIDs, "Exclude these particle as parent in truth record.");

    declareProperty( "signalParentPDGIDs", m_signalParentPDGIDs, "Signal-like list of PDGIDs to search truth record for.");

    declareProperty( "signalPDGIDs", m_signalPDGIDs, "PDGIDs of final state to accept as signal.");

    declareProperty( "trigDecTool", m_trigDecTool, "Trig decision tool" );

    declareProperty( "trigMatchTool", m_trigMatchTool, "Trigger matching tool");

    declareProperty( "trigMatchToolLRT", m_trigMatchToolLRT, "Trigger matching tool for LRT");

    declareProperty( "truthClassifier", m_truthClassifier, "MC Truth Classifier" );

    declareProperty( "MuonSelectionTool", m_muonSelectionTool, "Muon Selection Tool");

    declareProperty( "ElectronLLHTool", m_ElectronLLHTool, "Electron LLH Tool");

    declareProperty( "ElectronDNNTool", m_ElectronDNNTool, "Electron DNN Tool");

    declareProperty( "ElectronLLHTool_newtune", m_ElectronLLHTool_newtune, "Electron LLH Tool");

    declareProperty( "ElectronLLHTool_offline", m_ElectronLLHTool_offline, "Electron LLH Tool");

    declareProperty( "includeExtraLLH", m_includeExtraLLH, "Include the extra LLH tools and plots");

}


simplexAODReaderEL :: ~simplexAODReaderEL()
{
    // Clean up
    if (m_makeNtuple)
    {
        delete jet_pt;
        delete jet_eta;
        delete jet_phi;

        delete truthSignal_pt;
        delete truthSignal_eta;
        delete truthSignal_phi;
        delete truthSignal_d0;
        delete truthSignal_pdgid;
        delete truthSignal_motherpdgid;
    }
}


StatusCode simplexAODReaderEL :: initialize ()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    if (m_debug) std::cout<<"isDAOD flag = " << m_isDAOD<<std::endl;

    ANA_CHECK(m_trigDecTool.retrieve());
    ANA_CHECK(m_trigMatchTool.retrieve());
    ANA_CHECK(m_trigMatchToolLRT.retrieve());

    ANA_CHECK(m_truthClassifier.retrieve());
    ANA_CHECK(m_muonSelectionTool.retrieve());
    ANA_CHECK(m_ElectronLLHTool.retrieve());
    //ANA_CHECK(m_ElectronDNNTool.retrieve());
    if (m_includeExtraLLH)
    {
        ANA_CHECK(m_ElectronLLHTool_newtune.retrieve());
        //ANA_CHECK(m_ElectronLLHTool_offline.retrieve());
    }

    // Setup tree for ntuple
    if (m_makeNtuple)
    {
        ANA_CHECK(book (TTree("utt_tree","utt_tree") ));
        TTree* m_tree = tree("utt_tree");

        m_tree->Branch("L1_EM22_Passed", &L1_EM22_Passed );
        m_tree->Branch("L1_MU14FCH_Passed", &L1_MU14FCH_Passed );
        m_tree->Branch("L1_XE50_Passed", &L1_XE50_Passed );
        m_tree->Branch("L1_J100_Passed", &L1_J100_Passed);
        m_tree->Branch("l1_met", &l1_met);
        m_tree->Branch("hlt_met_trkmht", &hlt_met_trkmht);
        m_tree->Branch("hlt_met_pufit", &hlt_met_pufit );
        m_tree->Branch("jet50_n", &jet50_n);
        m_tree->Branch("jet75_n", &jet75_n);
        m_tree->Branch("jet100_n", &jet100_n);

        jet_pt = new std::vector<float>();
        m_tree->Branch("jet_pt", &jet_pt); 

        jet_eta = new std::vector<float>();
        m_tree->Branch("jet_eta", &jet_eta); 

        jet_phi = new std::vector<float>();
        m_tree->Branch("jet_phi", &jet_phi); 

        truthSignal_pt = new std::vector<float>();
        m_tree->Branch("truthSignal_pt", &truthSignal_pt);

        truthSignal_eta = new std::vector<float>();
        m_tree->Branch("truthSignal_eta", &truthSignal_eta);

        truthSignal_phi = new std::vector<float>();
        m_tree->Branch("truthSignal_phi", &truthSignal_phi);

        truthSignal_pdgid = new std::vector<uint>();
        m_tree->Branch("truthSignal_pdgid", &truthSignal_pdgid);

        truthSignal_motherpdgid = new std::vector<uint>();
        m_tree->Branch("truthSignal_motherpdgid", &truthSignal_motherpdgid);

        truthSignal_d0 = new std::vector<float>();
        m_tree->Branch("truthSignal_d0", &truthSignal_d0);
    }

    ////////////////////////////////////
    //     Book a lot of histograms   //
    ////////////////////////////////////

    //// 1D histograms

    // ID histograms

    // dnn testing
    //bookHisto("h_dnn_llh_overlap; Category (1=llh, 2=dnn); Entries",{4,0,4});
    //efficiencyHelper("h_dnn_El_d0; d_{0} [mm]; Efficiency", {25, 0, 300});

    if (m_includeExtraLLH)
    {
        // llh test
        efficiencyHelper("h_llh_El_d0; d_{0} [mm]; Efficiency", {25, 0, 300});
        efficiencyHelper("h_llh_new_El_d0; d_{0} [mm]; Efficiency", {25, 0, 300});
        efficiencyHelper("h_llh_off_El_d0; d_{0} [mm]; Efficiency", {25, 0, 300});

        efficiencyHelper("h_llh_El_pt; p_{T} [GeV]; Efficiency", {50, 0, 500});
        efficiencyHelper("h_llh_new_El_pt; p_{T} [GeV]; Efficiency", {50, 0, 500});
        efficiencyHelper("h_llh_off_El_pt; p_{T} [GeV]; Efficiency", {50, 0, 500});

        // truth match plus offline llh
        efficiencyHelper("h_llh_El_d0_r_off; d_{0} [mm]; Efficiency", {25, 0, 300});
        efficiencyHelper("h_llh_new_El_d0_r_off; d_{0} [mm]; Efficiency", {25, 0, 300});

        efficiencyHelper("h_llh_El_pt_r_off; p_{T} [GeV]; Efficiency", {50, 0, 500});
        efficiencyHelper("h_llh_new_El_pt_r_off; p_{T} [GeV]; Efficiency", {50, 0, 500});

        // hlt electrons
        efficiencyHelper("h_hltlrt_llh_El_d0; d_{0} [mm]; Efficiency", {25, 0, 300});
        efficiencyHelper("h_hltlrt_llh_new_El_d0; d_{0} [mm]; Efficiency", {25, 0, 300});

        efficiencyHelper("h_hltlrt_llh_El_pt; p_{T} [GeV]; Efficiency", {50, 0, 500});
        efficiencyHelper("h_hltlrt_llh_new_El_pt; p_{T} [GeV]; Efficiency", {50, 0, 500});
    }

    // Basic distributions
    //SetTitle() conveniently lets you set the title; x title; y title
    bookHisto("h_nTracks_offline; N Tracks; Events",{50, 0, 1000});
    bookHisto("h_nTracks_offlineLRT; N LRT Tracks; Events", {50, 0, 1000});
    bookHisto("h_nTracks_FTFcomb; N Tracks; Events", {50, 0, 1000});
    bookHisto("h_nTracks_FTFLRT; N Tracks; Events", {50, 0, 1000});
    bookHisto("h_nTracks_ELPT; N Tracks; Events", {50, 0, 1000});

    bookHisto("h_deltaR_offline_FTFcomb; dR; N Tracks", {100, 0, 0.1});
    bookHisto("h_deltaR_truthsignal_FTFcomb; dR; N Tracks", {100, 0, 0.1});

    bookHisto("h_d0_FTF; d_{0} [mm]; N Tracks", {50, -300, 300});
    bookHisto("h_d0_lrtFTF; d_{0} [mm]; N Tracks", {50, -300, 300});
    bookHisto("h_d0_offline; d_{0} [mm]; N Tracks", {50, -300, 300});
    bookHisto("h_d0_muonFTF; d_{0} [mm]; N Tracks", {50, -300, 300});
    bookHisto("h_d0_electronFTF; d_{0} [mm]; N Tracks", {50, -300, 300});
    bookHisto("h_d0sig_FTFcomb; d^{sig}_{0}; N Tracks", {20, 0, 20});
    bookHisto("h_d0sig_offline; d^{sig}_{0}; N Tracks", {20, 0, 20});

    bookHisto("h_eta_FTFcomb; #eta; N Tracks", {30, -3, 3});
    bookHisto("h_eta_offline; #eta; N Tracks", {30, -3, 3});
    bookHisto("h_pt_FTFcomb; p_{T} [GeV]; N Tracks", {50, 0, 500});
    bookHisto("h_pt_offline; p_{T} [GeV]; N Tracks", {50, 0, 500});
    
    bookHisto("h_nhits_offline; N Hits; Entries", {20,0,20});
    bookHisto("h_nhits_offlineSignal; N Hits; Entries", {20,0,20});
    bookHisto("h_nhits_FTF; N Hits; Entries", {20,0,20});
    bookHisto("h_nhits_lrtFTF; N Hits; Entries", {20,0,20});
        
    bookHisto("h_deltaR_ROI_offlineSignal_em;dR(ROI,em)", {100,0,0.5});
    bookHisto("h_deltaR_ROI_offlineSignal_mu;dR(ROI,mu)",{100,0,0.5});

    
    //ANA_CHECK(book(TH1F("h_nFTFcombTrack_in_ROI_mu", "h_nFTFcombTrack_in_ROI_mu", 50,0,50)));
    //ANA_CHECK(book(TH1F("h_nFTFcombTrack_in_ROI_em", "h_nFTFcombTrack_in_ROI_em", 50,0,50)));
              
    bookHisto("h_nROI_mu; N MU ROI", {10,0,10});
    bookHisto("h_nROI_em; N EM ROI", {10,0,10});
    

    // Resolutions
    bookHisto("h_d0_res_offlineSignal_truth; d0_{off} - d0_{tru}; Entries", {80, -20, 20});
    bookHisto("h_d0_res_FTFoffmatch_truth; d0_{FTF} - d0_{tru}; Entries", {80, -20, 20});
    bookHisto("h_d0_res_FTF_truthSignal; d0_{FTF} - d0_{tru}; Entries", {80, -20, 20});
    bookHisto("h_pt_res_FTF_truthSignal; pt_{FTF} - pt_{tru}; Entries", {80, -20, 20});
    bookHisto("h_eta_res_FTF_truthSignal; eta_{FTF} - eta_{tru}; Entries", {100, -0.5, 0.5});
    bookHisto("h_phi_res_FTF_truthSignal; phi_{FTF} - phi_{tru}; Entries", {100, -0.5, 0.5});

    // jets
    //bookHisto("h_njets; N Jets; Entries", {10, 0, 10});
    //bookHisto("h_jet_pt; p_{T} [GeV]; Entries", {20, 0, 200});
    //bookHisto("h_jet_eta; #eta; Entries", {20, -4, 4});

    // muons
    bookHisto("h_nmuons; N muon; Entries", {10, 0, 10});   
    bookHisto("h_nmuonsLRT; N LRT muon; Entries", {10, 0, 10});   
    bookHisto("h_muon_pt; p_{T} [GeV]; Entries", {20, 0, 200});    
    bookHisto("h_muon_eta; #eta; Entries", {20, -4, 4});    
    bookHisto("h_muon_d0; d_{0}; Entries", {300, 0, 300});    
    bookHisto("h_muon_auth; Muon Author; Entries", {20, 0, 20});    
    bookHisto("h_muon_isLRT; Trk is LRT; Entries", {2, 0, 2});    
    bookHisto("h_nmuonsegments; N muon segments; Entries", {10, 0, 10});    
    bookHisto("h_deltaR_muon_offlineSignal; dR(muon,track); Entries", {20, 0, 0.2});    
    bookHisto("h_deltaR_muon_FTF; dR(muon,track); Entries", {20, 0, 0.2});
    

    bookHisto("h_muon_lrtPT_d0; d_{0}; Entries", {300, 0, 300});    


    bookHisto("h_electron_lrtPT_pt; p_{T} [GeV]; Entries", {50, 0, 50});
    bookHisto("h_electron_lrtPT_d0; d_{0}; Entries", {300, -300, 300});


    // electrons
    bookHisto("h_nelectrons; N electron; Entries", {10, 0, 10});   
    bookHisto("h_nelectronsLRT; N LRT electron; Entries", {10, 0, 10});   
    bookHisto("h_electron_pt; p_{T} [GeV]; Entries", {20, 0, 200});    
    bookHisto("h_electron_eta; #eta; Entries", {20, -4, 4});    
    bookHisto("h_electron_d0; d_{0}; Entries", {300, 0, 300});  

    bookHisto("h_nHLTelectrons; N HLT Electrons; Entries", {10, 0, 10});    
    bookHisto("h_HLT_electron_d0; d_{0}; Entries", {300, 0, 300});

    bookHisto("h_nHLTelectrons_LRT; N HLT Electrons; Entries", {10, 0, 10});    
    bookHisto("h_HLT_electron_LRT_d0; d_{0}; Entries", {300, 0, 300});
    
    // met
    //bookHisto("h_hlt_trkmht; HLT trkmht [GeV]; Entries", {20, 0, 200});    
    //bookHisto("h_hlt_pufit; HLT trkmht [GeV]; Entries", {20, 0, 200});
    
    // 1D Truth histos
    ANA_CHECK (book (TH1F ("h_nTracks_truthSignal", "h_nTracks_truthSignal", 30, 0, 30)));
    
    bookHisto("h_pt_truthSignal;p_{T} [GeV]", {600, 0, 300});    
    bookHisto("h_d0_truthSignal;d0 [mm]", {300, 0, 300});
    bookHisto("h_eta_truthSignal;#eta;", {30, -3, 3});
    bookHisto("h_vtxR_truthSignal;Prod R [mm]", {50, 0, 500});
    bookHisto("h_vtxZ_truthSignal;Prod Z [mm]", {50, -500, 500});
    bookHisto("h_vtxZ_truthSignal_el;Prod Z [mm]", {50, -500, 500});
    bookHisto("h_vtxZ_truthSignal_mu;Prod Z [mm]", {50, -500, 500});
    
    ANA_CHECK (book (TH1F ("h_pdgid_truthSignal", "h_pdgid_truthSignal", 300, 0, 300)));
    
    bookHisto("h_deltaR_FTF_truthSignal; dR; N Tracks", {100, 0, 0.1});    
    bookHisto("h_deltaR_ROI_truthSignal_em;dR(ROI,em)", {100,0,0.5});    
    bookHisto("h_deltaR_ROI_truthSignal_mu;dR(ROI,mu)", {100,0,0.5});    
    bookHisto("h_deltaEta_ROI_truthSignal_mu;dR(ROI,mu)", {100,0,0.5});    
    bookHisto("h_deltaPhi_ROI_truthSignal_mu;dR(ROI,mu)", {100,0,0.5});
    
    // Offline match to truth, SUSY daughter particle, i.e. trackable object
    bookHisto("h_pt_offlineSignal;p_{T} [GeV]; N Tracks", {50, 0, 500});    
    bookHisto("h_eta_offlineSignal;#eta; N Tracks", {30, -3, 3});
    bookHisto("h_phi_offlineSignal;#phi; N Tracks", {35, -3.5, 3.5});
    bookHisto("h_d0_offlineSignal;d_{0} [mm]; N Tracks", {50, -300, 300});
    bookHisto("h_d0sig_offlineSignal; d^{sig}_{0}; N Tracks", {20, 0, 20});
    bookHisto("h_z0_offlineSignal;z_{0} [mm]; N Tracks", {50, -200, 200});
    bookHisto("h_vtxR_offlineSignal;Prod Vertex Radius [mm]; N Tracks", {50, 0, 500});
    bookHisto("h_vtxZ_offlineSignal;Prod Vertex Z [mm]; N Tracks", {50, -500, 500});
    bookHisto("h_vtxZ_offlineSignal_el;Prod Vertex Z [mm]; N Tracks", {50, -500, 500});
    bookHisto("h_vtxZ_offlineSignal_mu;Prod Vertex Z [mm]; N Tracks", {50, -500, 500});


    // Offline not matched to FTF track
    //ANA_CHECK (book (TH1F ("h_pt_offline_noFTF", "h_pt_offline_noFTF", 50, 0, 500)));
    //hist("h_pt_offline_noFTF")->SetTitle("h_pt_offline_noFTF;p_{T} [GeV];N Tracks");
    //
    //ANA_CHECK (book (TH1F ("h_eta_offline_noFTF", "h_eta_offline_noFTF", 30, -3, 3)));
    //hist("h_eta_offline_noFTF")->SetTitle("h_eta_offline_noFTF;#eta;N Tracks");
    //
    //ANA_CHECK (book (TH1F ("h_d0_offline_noFTF", "h_d0_offline_noFTF", 50, -300, 300)));
    //hist("h_d0_offline_noFTF")->SetTitle("h_d0_offline_noFTF;d_{0} [mm]; N Tracks");
    //
    //ANA_CHECK (book (TH1F ("h_vtxR_offline_noFTF", "h_vtxR_offline_noFTF", 50, 0, 500)));
    //hist("h_vtxR_offline_noFTF")->SetTitle("h_vtxR_offline_noFTF; Prod Vertex Radius [mm]; N Tracks");


    // Efficiency numerator and denominators for offline
    efficiencyHelper("h_d0_eff_FTFcomb_r_offline; d_{0} [mm]; Efficiency", {50, -300, 300});

    ANA_CHECK (book (TH1F ("h_d0_el_eff_n", "h_d0_el_eff_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_el_eff_d", "h_d0_el_eff_d", 50, -300, 300)));
    h_d0_el_eff_FTFcomb_r_offline = new TGraphAsymmErrors();
    h_d0_el_eff_FTFcomb_r_offline->SetNameTitle("h_d0_el_eff_FTFcomb_r_offline","h_d0_el_eff_FTFcomb_r_offline; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_d0_mu_eff_n", "h_d0_mu_eff_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_mu_eff_d", "h_d0_mu_eff_d", 50, -300, 300)));
    h_d0_mu_eff_FTFcomb_r_offline = new TGraphAsymmErrors();
    h_d0_mu_eff_FTFcomb_r_offline->SetNameTitle("h_d0_mu_eff_FTFcomb_r_offline","h_d0_mu_eff_FTFcomb_r_offline; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_pt_eff_n", "h_pt_eff_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_pt_eff_d", "h_pt_eff_d", 50, 0, 500)));
    h_pt_eff_FTFcomb_r_offline = new TGraphAsymmErrors();
    h_pt_eff_FTFcomb_r_offline->SetNameTitle("h_pt_eff_FTFcomb_r_offline","h_pt_eff_FTFcomb_r_offline; p_{T} [GeV]; Efficiency");

    ANA_CHECK (book (TH1F ("h_eta_eff_n", "h_eta_eff_n", 30, -3, 3))); 
    ANA_CHECK (book (TH1F ("h_eta_eff_d", "h_eta_eff_d", 30, -3, 3)));
    h_eta_eff_FTFcomb_r_offline = new TGraphAsymmErrors();
    h_eta_eff_FTFcomb_r_offline->SetNameTitle("h_eta_eff_FTFcomb_r_offline","h_eta_eff_FTFcomb_r_offline; #eta; Efficiency");

    ANA_CHECK (book (TH1F ("h_vtxR_eff_n", "h_vtxR_eff_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_vtxR_eff_d", "h_vtxR_eff_d", 50, 0, 500)));
    h_vtxR_eff_FTFcomb_r_offline = new TGraphAsymmErrors();
    h_vtxR_eff_FTFcomb_r_offline->SetNameTitle("h_vtxR_eff_FTFcomb_r_offline","h_vtxR_eff_FTFcomb_r_offline; Prod Vertex Radius [mm]; Efficiency");

    // Purity numerator and denominators
    ANA_CHECK (book (TH1F ("h_d0_pur_n", "h_d0_pur_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_pur_d", "h_d0_pur_d", 50, -300, 300)));
    h_d0_pur_FTFcomb_r_offline = new TGraphAsymmErrors();
    h_d0_pur_FTFcomb_r_offline->SetNameTitle("h_d0_pur_FTFcomb_r_offline","h_d0_pur_FTFcomb_r_offline; d_{0} [mm]; Purity");

    ANA_CHECK (book (TH1F ("h_pt_pur_n", "h_pt_pur_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_pt_pur_d", "h_pt_pur_d", 50, 0, 500)));
    h_pt_pur_FTFcomb_r_offline = new TGraphAsymmErrors();
    h_pt_pur_FTFcomb_r_offline->SetNameTitle("h_pt_pur_FTFcomb_r_offline","h_pt_pur_FTFcomb_r_offline; p_{T} [GeV]; Purity");

    ANA_CHECK (book (TH1F ("h_eta_pur_n", "h_eta_pur_n", 30, -3, 3))); 
    ANA_CHECK (book (TH1F ("h_eta_pur_d", "h_eta_pur_d", 30, -3, 3)));
    h_eta_pur_FTFcomb_r_offline = new TGraphAsymmErrors();
    h_eta_pur_FTFcomb_r_offline->SetNameTitle("h_eta_pur_FTFcomb_r_offline","h_eta_pur_FTFcomb_r_offline; #eta; Purity");

    ANA_CHECK (book (TH1F ("h_vtxR_pur_n", "h_vtxR_pur_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_vtxR_pur_d", "h_vtxR_pur_d", 50, 0, 500)));
    h_vtxR_pur_FTFcomb_r_offline = new TGraphAsymmErrors();
    h_vtxR_pur_FTFcomb_r_offline->SetNameTitle("h_vtxR_pur_FTFcomb_r_offline","h_vtxR_pur_FTFcomb_r_offline; Prod Vertex Radius [mm]; Purity");

    // truth eff
    ANA_CHECK (book (TH1F ("h_d0_truth_eff_n", "h_d0_truth_eff_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_truth_eff_d", "h_d0_truth_eff_d", 50, -300, 300)));
    h_d0_eff_FTFcomb_r_truth = new TGraphAsymmErrors();
    h_d0_eff_FTFcomb_r_truth->SetNameTitle("h_d0_eff_FTFcomb_r_truth","h_d0_eff_FTFcomb_r_truth; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_d0_truth_el_eff_n", "h_d0_truth_el_eff_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_truth_el_eff_d", "h_d0_truth_el_eff_d", 50, -300, 300)));
    h_d0_eff_FTFcomb_r_truth_el = new TGraphAsymmErrors();
    h_d0_eff_FTFcomb_r_truth_el->SetNameTitle("h_d0_eff_FTFcomb_r_truth_el","h_d0_eff_FTFcomb_r_truth_el; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_d0_truth_mu_eff_n", "h_d0_truth_mu_eff_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_truth_mu_eff_d", "h_d0_truth_mu_eff_d", 50, -300, 300)));
    h_d0_eff_FTFcomb_r_truth_mu = new TGraphAsymmErrors();
    h_d0_eff_FTFcomb_r_truth_mu->SetNameTitle("h_d0_eff_FTFcomb_r_truth_mu","h_d0_eff_FTFcomb_r_truth_mu; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_pt_truth_eff_n", "h_pt_truth_eff_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_pt_truth_eff_d", "h_pt_truth_eff_d", 50, 0, 500)));
    h_pt_eff_FTFcomb_r_truth = new TGraphAsymmErrors();
    h_pt_eff_FTFcomb_r_truth->SetNameTitle("h_pt_eff_FTFcomb_r_truth","h_pt_eff_FTFcomb_r_truth; p_{T} [GeV]; Efficiency");

    ANA_CHECK (book (TH1F ("h_vtxR_truth_eff_n", "h_vtxR_truth_eff_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_vtxR_truth_eff_d", "h_vtxR_truth_eff_d", 50, 0, 500)));
    h_vtxR_eff_FTFcomb_r_truth = new TGraphAsymmErrors();
    h_vtxR_eff_FTFcomb_r_truth->SetNameTitle("h_vtxR_eff_FTFcomb_r_truth","h_vtxR_eff_FTFcomb_r_truth; Prod Vertex Radius [mm]; Efficiency");


    // Offline Eff
    ANA_CHECK (book (TH1F ("h_d0_offline_eff_n", "h_d0_offline_eff_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_offline_eff_d", "h_d0_offline_eff_d", 50, -300, 300)));
    h_d0_eff_offline_r_truth = new TGraphAsymmErrors();
    h_d0_eff_offline_r_truth->SetNameTitle("h_d0_eff_offline_r_truth","h_d0_eff_offline_r_truth; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_pt_offline_eff_n", "h_pt_offline_eff_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_pt_offline_eff_d", "h_pt_offline_eff_d", 50, 0, 500)));
    h_pt_eff_offline_r_truth = new TGraphAsymmErrors();
    h_pt_eff_offline_r_truth->SetNameTitle("h_pt_eff_offline_r_truth","h_pt_eff_offline_r_truth; p_{T} [GeV]; Efficiency");

    ANA_CHECK (book (TH1F ("h_vtxR_offline_eff_n", "h_vtxR_offline_eff_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_vtxR_offline_eff_d", "h_vtxR_offline_eff_d", 50, 0, 500)));
    h_vtxR_eff_offline_r_truth = new TGraphAsymmErrors();
    h_vtxR_eff_offline_r_truth->SetNameTitle("h_vtxR_eff_offline_r_truth","h_vtxR_eff_offline_r_truth; Prod Vertex Radius [mm]; Efficiency");


    // Trigger Eff Offline
    // L1MU
    ANA_CHECK (book (TH1F ("h_pt_offline_L1MU14FCH_eff_n", "h_pt_offline_L1MU14FCH_eff_n", 50, 0, 100))); 
    ANA_CHECK (book (TH1F ("h_pt_offline_L1MU14FCH_eff_d", "h_pt_offline_L1MU14FCH_eff_d", 50, 0, 100)));
    h_pt_L1MU14FCH_r_offline = new TGraphAsymmErrors();
    h_pt_L1MU14FCH_r_offline->SetNameTitle("h_pt_L1MU14FCH_r_offline","h_pt_L1MU14FCH_r_offline; p_{T} [GeV]; Efficiency");

    ANA_CHECK (book (TH1F ("h_d0_offline_L1MU14FCH_eff_n", "h_d0_offline_L1MU14FCH_eff_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_offline_L1MU14FCH_eff_d", "h_d0_offline_L1MU14FCH_eff_d", 50, -300, 300)));
    h_d0_L1MU14FCH_r_offline = new TGraphAsymmErrors();
    h_d0_L1MU14FCH_r_offline->SetNameTitle("h_d0_L1MU14FCH_r_offline","h_d0_L1MU14FCH_r_offline; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_vtxR_offline_L1MU14FCH_eff_n", "h_vtxR_offline_L1MU14FCH_eff_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_vtxR_offline_L1MU14FCH_eff_d", "h_vtxR_offline_L1MU14FCH_eff_d", 50, 0, 500)));
    h_vtxR_L1MU14FCH_r_offline = new TGraphAsymmErrors();
    h_vtxR_L1MU14FCH_r_offline->SetNameTitle("h_vtxR_L1MU14FCH_r_offline","h_vtxR_L1MU14FCH_r_offline; Prod Radius [mm]; Efficiency");

    // L1_EM
    ANA_CHECK (book (TH1F ("h_pt_offline_L1eEM26M_eff_n", "h_pt_offline_L1eEM26M_eff_n", 50, 0, 100))); 
    ANA_CHECK (book (TH1F ("h_pt_offline_L1eEM26M_eff_d", "h_pt_offline_L1eEM26M_eff_d", 50, 0, 100)));
    h_pt_L1eEM26M_r_offline = new TGraphAsymmErrors();
    h_pt_L1eEM26M_r_offline->SetNameTitle("h_pt_L1eEM26M_r_offline","h_pt_L1eEM26M_r_offline; p_{T} [GeV]; Efficiency");

    ANA_CHECK (book (TH1F ("h_d0_offline_L1eEM26M_eff_n", "h_d0_offline_L1eEM26M_eff_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_offline_L1eEM26M_eff_d", "h_d0_offline_L1eEM26M_eff_d", 50, -300, 300)));
    h_d0_L1eEM26M_r_offline = new TGraphAsymmErrors();
    h_d0_L1eEM26M_r_offline->SetNameTitle("h_d0_L1eEM26M_r_offline","h_d0_L1eEM26M_r_offline; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_vtxR_offline_L1eEM26M_eff_n", "h_vtxR_offline_L1eEM26M_eff_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_vtxR_offline_L1eEM26M_eff_d", "h_vtxR_offline_L1eEM26M_eff_d", 50, 0, 500)));
    h_vtxR_L1eEM26M_r_offline = new TGraphAsymmErrors();
    h_vtxR_L1eEM26M_r_offline->SetNameTitle("h_vtxR_L1eEM26M_r_offline","h_vtxR_L1eEM26M_r_offline; Prod Radius [mm]; Efficiency");

    // Trigger Eff Truth
    // L1MU
    ANA_CHECK (book (TH1F ("h_pt_truth_L1MU14FCH_eff_n", "h_pt_truth_L1MU14FCH_eff_n", 50, 0, 100))); 
    ANA_CHECK (book (TH1F ("h_pt_truth_L1MU14FCH_eff_d", "h_pt_truth_L1MU14FCH_eff_d", 50, 0, 100)));
    h_pt_L1MU14FCH_r_truth = new TGraphAsymmErrors();
    h_pt_L1MU14FCH_r_truth->SetNameTitle("h_pt_L1MU14FCH_r_truth","h_pt_L1MU14FCH_r_truth; p_{T} [GeV]; Efficiency");

    ANA_CHECK (book (TH1F ("h_d0_truth_L1MU14FCH_eff_n", "h_d0_truth_L1MU14FCH_eff_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_truth_L1MU14FCH_eff_d", "h_d0_truth_L1MU14FCH_eff_d", 50, -300, 300)));
    h_d0_L1MU14FCH_r_truth = new TGraphAsymmErrors();
    h_d0_L1MU14FCH_r_truth->SetNameTitle("h_d0_L1MU14FCH_r_truth","h_d0_L1MU14FCH_r_truth; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_vtxR_truth_L1MU14FCH_eff_n", "h_vtxR_truth_L1MU14FCH_eff_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_vtxR_truth_L1MU14FCH_eff_d", "h_vtxR_truth_L1MU14FCH_eff_d", 50, 0, 500)));
    h_vtxR_L1MU14FCH_r_truth = new TGraphAsymmErrors();
    h_vtxR_L1MU14FCH_r_truth->SetNameTitle("h_vtxR_L1MU14FCH_r_truth","h_vtxR_L1MU14FCH_r_truth; Prod Radius [mm]; Efficiency");

    // L1_EM
    ANA_CHECK (book (TH1F ("h_pt_truth_L1eEM26M_eff_n", "h_pt_truth_L1eEM26M_eff_n", 50, 0, 100))); 
    ANA_CHECK (book (TH1F ("h_pt_truth_L1eEM26M_eff_d", "h_pt_truth_L1eEM26M_eff_d", 50, 0, 100)));
    h_pt_L1eEM26M_r_truth = new TGraphAsymmErrors();
    h_pt_L1eEM26M_r_truth->SetNameTitle("h_pt_L1eEM26M_r_truth","h_pt_L1eEM26M_r_truth; p_{T} [GeV]; Efficiency");

    ANA_CHECK (book (TH1F ("h_d0_truth_L1eEM26M_eff_n", "h_d0_truth_L1eEM26M_eff_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_truth_L1eEM26M_eff_d", "h_d0_truth_L1eEM26M_eff_d", 50, -300, 300)));
    h_d0_L1eEM26M_r_truth = new TGraphAsymmErrors();
    h_d0_L1eEM26M_r_truth->SetNameTitle("h_d0_L1eEM26M_r_truth","h_d0_L1eEM26M_r_truth; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_vtxR_truth_L1eEM26M_eff_n", "h_vtxR_truth_L1eEM26M_eff_n", 50, 0, 500))); 
    ANA_CHECK (book (TH1F ("h_vtxR_truth_L1eEM26M_eff_d", "h_vtxR_truth_L1eEM26M_eff_d", 50, 0, 500)));
    h_vtxR_L1eEM26M_r_truth = new TGraphAsymmErrors();
    h_vtxR_L1eEM26M_r_truth->SetNameTitle("h_vtxR_L1eEM26M_r_truth","h_vtxR_L1eEM26M_r_truth; Prod Radius [mm]; Efficiency");


    // Muon FTF eff vs FS FTF
    ANA_CHECK (book (TH1F ("h_pt_MUFTF_r_FSFTF_n", "h_pt_MUFTF_r_FSFTF_n", 50, 0, 300))); 
    ANA_CHECK (book (TH1F ("h_pt_MUFTF_r_FSFTF_d", "h_pt_MUFTF_r_FSFTF_d", 50, 0, 300)));
    h_pt_MUFTF_r_FSFTF = new TGraphAsymmErrors();
    h_pt_MUFTF_r_FSFTF->SetNameTitle("h_pt_MUFTF_r_FSFTF","h_pt_MUFTF_r_FSFTF; p_{T} [GeV]; Efficiency");

    ANA_CHECK (book (TH1F ("h_d0_MUFTF_r_FSFTF_n", "h_d0_MUFTF_r_FSFTF_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_MUFTF_r_FSFTF_d", "h_d0_MUFTF_r_FSFTF_d", 50, -300, 300)));
    h_d0_MUFTF_r_FSFTF = new TGraphAsymmErrors();
    h_d0_MUFTF_r_FSFTF->SetNameTitle("h_d0_MUFTF_r_FSFTF","h_d0_MUFTF_r_FSFTF; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_dEta_MUFTF_r_FSFTF_n", "h_dEta_MUFTF_r_FSFTF_n", 50,  0, 1))); 
    ANA_CHECK (book (TH1F ("h_dEta_MUFTF_r_FSFTF_d", "h_dEta_MUFTF_r_FSFTF_d", 50,  0, 1)));
    h_dEta_MUFTF_r_FSFTF = new TGraphAsymmErrors();
    h_dEta_MUFTF_r_FSFTF->SetNameTitle("h_dEta_MUFTF_r_FSFTF","h_dEta_MUFTF_r_FSFTF; #Delta#eta(track,RoI); Efficiency");

    ANA_CHECK (book (TH1F ("h_dPhi_MUFTF_r_FSFTF_n", "h_dPhi_MUFTF_r_FSFTF_n", 50, 0, 1))); 
    ANA_CHECK (book (TH1F ("h_dPhi_MUFTF_r_FSFTF_d", "h_dPhi_MUFTF_r_FSFTF_d", 50, 0, 1)));
    h_dPhi_MUFTF_r_FSFTF = new TGraphAsymmErrors();
    h_dPhi_MUFTF_r_FSFTF->SetNameTitle("h_dPhi_MUFTF_r_FSFTF","h_dPhi_MUFTF_r_FSFTF; #Delta#phi(track,RoI); Efficiency");

    // mu24 lrt vs mu24 offlinesig
    efficiencyHelper("h_z0_eff_HLT_mu24_r_offlinesig; z_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_z0_eff_HLT_mu24_LRTloose_r_offlinesig; z_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_z0_eff_HLT_mu24_comb_r_offlinesig; z_{0} [mm]; Efficiency", {25, -300, 300});

    efficiencyHelper("h_d0_eff_HLT_mu24_r_offlinesig; d_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_d0_eff_HLT_mu24_LRTloose_r_offlinesig; d_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_d0_eff_HLT_mu24_comb_r_offlinesig; d_{0} [mm]; Efficiency", {25, -300, 300});

    efficiencyHelper("h_d0_zoom_eff_HLT_mu24_r_offlinesig; d_{0} [mm]; Efficiency", {40, -20, 20});
    efficiencyHelper("h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig; d_{0} [mm]; Efficiency", {40, -20, 20});
    efficiencyHelper("h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig; d_{0} [mm]; Efficiency", {40, -20, 20});

    efficiencyHelper("h_prodR_eff_HLT_mu24_r_offlinesig; Prod Radius [mm]; Efficiency", {15, 0, 300});
    efficiencyHelper("h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig; Prod Radius [mm]; Efficiency", {15, 0, 300});
    efficiencyHelper("h_prodR_eff_HLT_mu24_comb_r_offlinesig; Prod Radius [mm]; Efficiency", {15, 0, 300});

    efficiencyHelper("h_pt_eff_HLT_mu24_r_offlinesig; p_{T} [GeV]; Efficiency", {25, 0, 300});
    efficiencyHelper("h_pt_eff_HLT_mu24_LRTloose_r_offlinesig; p_{T} [GeV]; Efficiency", {25, 0, 300});
    efficiencyHelper("h_pt_eff_HLT_mu24_comb_r_offlinesig; p_{T} [GeV]; Efficiency", {25, 0, 300});

    efficiencyHelper("h_eta_eff_HLT_mu24_r_offlinesig; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_eta_eff_HLT_mu24_LRTloose_r_offlinesig; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_eta_eff_HLT_mu24_comb_r_offlinesig; #eta; Efficiency", {30, -3, 3});

    // TODO JDL check pub plot script for which ones to copy
    // mu24 lrt vs mu24 muon
    efficiencyHelper("h_z0_eff_HLT_mu24_r_muon; z_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_z0_eff_HLT_mu24_LRTloose_r_muon; z_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_z0_eff_HLT_mu24_comb_r_muon; z_{0} [mm]; Efficiency", {25, -300, 300});

    efficiencyHelper("h_d0_eff_HLT_mu24_r_muon; d_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_d0_eff_HLT_mu24_LRTloose_r_muon; d_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_d0_eff_HLT_mu24_comb_r_muon; d_{0} [mm]; Efficiency", {25, -300, 300});

    efficiencyHelper("h_d0_zoom_eff_HLT_mu24_r_muon; d_{0} [mm]; Efficiency", {40, -20, 20});
    efficiencyHelper("h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon; d_{0} [mm]; Efficiency", {40, -20, 20});
    efficiencyHelper("h_d0_zoom_eff_HLT_mu24_comb_r_muon; d_{0} [mm]; Efficiency", {40, -20, 20});

    efficiencyHelper("h_prodR_eff_HLT_mu24_r_muon; Prod Radius [mm]; Efficiency", {15, 0, 300});
    efficiencyHelper("h_prodR_eff_HLT_mu24_LRTloose_r_muon; Prod Radius [mm]; Efficiency", {15, 0, 300});
    efficiencyHelper("h_prodR_eff_HLT_mu24_comb_r_muon; Prod Radius [mm]; Efficiency", {15, 0, 300});

    efficiencyHelper("h_pt_eff_HLT_mu24_r_muon; p_{T} [GeV]; Efficiency", {25, 0, 300});
    efficiencyHelper("h_pt_eff_HLT_mu24_LRTloose_r_muon; p_{T} [GeV]; Efficiency", {25, 0, 300});
    efficiencyHelper("h_pt_eff_HLT_mu24_comb_r_muon; p_{T} [GeV]; Efficiency", {25, 0, 300});

    efficiencyHelper("h_eta_eff_HLT_mu24_r_muon; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_eta_eff_HLT_mu24_LRTloose_r_muon; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_eta_eff_HLT_mu24_comb_r_muon; #eta; Efficiency", {30, -3, 3});

    // msonly
    efficiencyHelper("h_pt_mu20_msonly_r_offlinesig_eff; p_{T} [GeV]; Efficiency", {25, 0, 300});
    efficiencyHelper("h_prodR_mu20_msonly_r_offlinesig_eff; Prod Radius [mm]; Efficiency", {15, 0, 300});
    efficiencyHelper("h_d0_mu20_msonly_r_offlinesig_eff; d_{0} [mm]; Efficiency", {25, -300, 300});


   // Electron FTF eff vs FS FTF
    ANA_CHECK (book (TH1F ("h_pt_ELFTF_r_FSFTF_n", "h_pt_ELFTF_r_FSFTF_n", 50, 0, 300))); 
    ANA_CHECK (book (TH1F ("h_pt_ELFTF_r_FSFTF_d", "h_pt_ELFTF_r_FSFTF_d", 50, 0, 300)));
    h_pt_ELFTF_r_FSFTF = new TGraphAsymmErrors();
    h_pt_ELFTF_r_FSFTF->SetNameTitle("h_pt_ELFTF_r_FSFTF","h_pt_ELFTF_r_FSFTF; p_{T} [GeV]; Efficiency");

    ANA_CHECK (book (TH1F ("h_d0_ELFTF_r_FSFTF_n", "h_d0_ELFTF_r_FSFTF_n", 50, -300, 300))); 
    ANA_CHECK (book (TH1F ("h_d0_ELFTF_r_FSFTF_d", "h_d0_ELFTF_r_FSFTF_d", 50, -300, 300)));
    h_d0_ELFTF_r_FSFTF = new TGraphAsymmErrors();
    h_d0_ELFTF_r_FSFTF->SetNameTitle("h_d0_ELFTF_r_FSFTF","h_d0_ELFTF_r_FSFTF; d_{0} [mm]; Efficiency");

    ANA_CHECK (book (TH1F ("h_dEta_ELFTF_r_FSFTF_n", "h_dEta_ELFTF_r_FSFTF_n", 50,  0, 1))); 
    ANA_CHECK (book (TH1F ("h_dEta_ELFTF_r_FSFTF_d", "h_dEta_ELFTF_r_FSFTF_d", 50,  0, 1)));
    h_dEta_ELFTF_r_FSFTF = new TGraphAsymmErrors();
    h_dEta_ELFTF_r_FSFTF->SetNameTitle("h_dEta_ELFTF_r_FSFTF","h_dEta_ELFTF_r_FSFTF; #Delta#eta(track,RoI); Efficiency");

    ANA_CHECK (book (TH1F ("h_dPhi_ELFTF_r_FSFTF_n", "h_dPhi_ELFTF_r_FSFTF_n", 50, 0, 1))); 
    ANA_CHECK (book (TH1F ("h_dPhi_ELFTF_r_FSFTF_d", "h_dPhi_ELFTF_r_FSFTF_d", 50, 0, 1)));
    h_dPhi_ELFTF_r_FSFTF = new TGraphAsymmErrors();
    h_dPhi_ELFTF_r_FSFTF->SetNameTitle("h_dPhi_ELFTF_r_FSFTF","h_dPhi_ELFTF_r_FSFTF; #Delta#phi(track,RoI); Efficiency");

    // el26 lrt vs el26 offline signal
    efficiencyHelper("h_z0_eff_HLT_el26_r_offlinesig; z_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_z0_eff_HLT_el30_LRTmedium_r_offlinesig; z_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_z0_eff_HLT_el26_comb_r_offlinesig; z_{0} [mm]; Efficiency", {25, -300, 300});

    efficiencyHelper("h_d0_eff_HLT_el26_r_offlinesig; d_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_d0_eff_HLT_el30_LRTmedium_r_offlinesig; d_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_d0_eff_HLT_el26_comb_r_offlinesig; d_{0} [mm]; Efficiency", {25, -300, 300});

    //efficiencyHelper("h_d0_eff_HLT_el30_LRTtight_vloose_r_offlinesig; d_{0} [mm]; Efficiency", {25, -300, 300}); // 

    efficiencyHelper("h_d0_zoom_eff_HLT_el26_r_offlinesig; d_{0} [mm]; Efficiency", {40, -20, 20});
    efficiencyHelper("h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig; d_{0} [mm]; Efficiency", {40, -20, 20});
    efficiencyHelper("h_d0_zoom_eff_HLT_el26_comb_r_offlinesig; d_{0} [mm]; Efficiency", {40, -20, 20});
   
    efficiencyHelper("h_prodR_eff_HLT_el26_r_offlinesig; Prod Radius [mm]; Efficiency", {15, 0, 300});
    efficiencyHelper("h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig; Prod Radius [mm]; Efficiency", {15, 0, 300});
    efficiencyHelper("h_prodR_eff_HLT_el26_comb_r_offlinesig; Prod Radius [mm]; Efficiency", {15, 0, 300});

    efficiencyHelper("h_pt_eff_HLT_el26_r_offlinesig; p_{T} [GeV]; Efficiency", {0,10,20,30,40,50,60,70,80,90,100,120,140,200,300});
    efficiencyHelper("h_pt_eff_HLT_el30_LRTmedium_r_offlinesig; p_{T} [GeV]; Efficiency", {0,10,20,30,40,50,60,70,80,90,100,120,140,200,300});
    efficiencyHelper("h_pt_eff_HLT_el26_comb_r_offlinesig; p_{T} [GeV]; Efficiency", {0,10,20,30,40,50,60,70,80,90,100,120,140,200,300});

    efficiencyHelper("h_eta_eff_HLT_el26_r_offlinesig; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_eta_eff_HLT_el30_LRTmedium_r_offlinesig; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_eta_eff_HLT_el26_comb_r_offlinesig; #eta; Efficiency", {30, -3, 3});

    // el26 lrt vs el26 offline electron
    efficiencyHelper("h_z0_eff_HLT_el26_r_electron; z_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_z0_eff_HLT_el30_LRTmedium_r_electron; z_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_z0_eff_HLT_el26_comb_r_electron; z_{0} [mm]; Efficiency", {25, -300, 300});

    efficiencyHelper("h_d0_eff_HLT_el26_r_electron; d_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_d0_eff_HLT_el30_LRTmedium_r_electron; d_{0} [mm]; Efficiency", {25, -300, 300});
    efficiencyHelper("h_d0_eff_HLT_el26_comb_r_electron; d_{0} [mm]; Efficiency", {25, -300, 300});

    efficiencyHelper("h_d0_zoom_eff_HLT_el26_r_electron; d_{0} [mm]; Efficiency", {40, -20, 20});
    efficiencyHelper("h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron; d_{0} [mm]; Efficiency", {40, -20, 20});
    efficiencyHelper("h_d0_zoom_eff_HLT_el26_comb_r_electron; d_{0} [mm]; Efficiency", {40, -20, 20});
   
    efficiencyHelper("h_prodR_eff_HLT_el26_r_electron; Prod Radius [mm]; Efficiency", {15, 0, 300});
    efficiencyHelper("h_prodR_eff_HLT_el30_LRTmedium_r_electron; Prod Radius [mm]; Efficiency", {15, 0, 300});
    efficiencyHelper("h_prodR_eff_HLT_el26_comb_r_electron; Prod Radius [mm]; Efficiency", {15, 0, 300});

    efficiencyHelper("h_pt_eff_HLT_el26_r_electron; p_{T} [GeV]; Efficiency", {0,10,20,30,40,50,60,70,80,90,100,120,140,200,300});
    efficiencyHelper("h_pt_eff_HLT_el30_LRTmedium_r_electron; p_{T} [GeV]; Efficiency", {0,10,20,30,40,50,60,70,80,90,100,120,140,200,300});
    efficiencyHelper("h_pt_eff_HLT_el26_comb_r_electron; p_{T} [GeV]; Efficiency", {0,10,20,30,40,50,60,70,80,90,100,120,140,200,300});

    efficiencyHelper("h_eta_eff_HLT_el26_r_electron; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_eta_eff_HLT_el30_LRTmedium_r_electron; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_eta_eff_HLT_el26_comb_r_electron; #eta; Efficiency", {30, -3, 3});

    // DNN Tests
    //efficiencyHelper("h_d0_zoom_eff_HLT_el26dnn_r_electron; d_{0} [mm]; Efficiency", {40, -20, 20});
    //efficiencyHelper("h_d0_zoom_eff_HLT_el26dnn_nod0_r_electron; d_{0} [mm]; Efficiency", {40, -20, 20});
    //efficiencyHelper("h_d0_zoom_eff_HLT_el30dnn_nopix_LRTmedium_r_electron; d_{0} [mm]; Efficiency", {40, -20, 20});



    // photon
    efficiencyHelper("h_pt_g20_r_offlinesig_eff; p_{T} [GeV]; Efficiency", {25, 0, 300});
    efficiencyHelper("h_prodR_g20_r_offlinesig_eff; Prod Radius [mm]; Efficiency", {15, 0, 300});
    efficiencyHelper("h_d0_g20_r_offlinesig_eff; d_{0} [mm]; Efficiency", {25, -300, 300});


    // LRT Electron26 efficiency vs truth
    efficiencyHelper("h_d0_HLT_el30_LRT_r_truth_eff; d_{0} [mm]; Efficiency", {50, -300, 300});
    efficiencyHelper("h_pt_HLT_el30_LRT_r_truth_eff; p_{T} [GeV]; Efficiency", {50, 0, 250});
    efficiencyHelper("h_eta_HLT_el30_LRT_r_truth_eff; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_phi_HLT_el30_LRT_r_truth_eff; #phi; Efficiency", {35, -3.5, 3.5});
    efficiencyHelper("h_vtxR_HLT_el30_LRT_r_truth_eff; Prod Radius [mm]; Efficiency", {50, 0, 300});

    // prompt electron vs truth
    efficiencyHelper("h_d0_HLT_el26_r_truth_eff; d_{0} [mm]; Efficiency", {50, -300, 300});
    efficiencyHelper("h_pt_HLT_el26_r_truth_eff; p_{T} [GeV]; Efficiency", {50, 0, 250});
    efficiencyHelper("h_eta_HLT_el26_r_truth_eff; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_phi_HLT_el26_r_truth_eff; #phi; Efficiency", {35, -3.5, 3.5});
    efficiencyHelper("h_vtxR_HLT_el26_r_truth_eff; Prod Radius [mm]; Efficiency", {50, 0, 300});


    // el lrt supporting chains
    efficiencyHelper("h_pt_el_lrt_tnp_phtag_eff;  p_{T} [GeV]; Efficiency", {50, 0, 300});
    efficiencyHelper("h_prodR_el_lrt_tnp_phtag_eff;  Prod Radius [mm]; Efficiency", {25, 0, 300});

    // these are problematic, filled per track not event
    // LRT Muon24 efficiency vs truth
    efficiencyHelper("h_d0_HLT_mu24_LRT_r_truth_eff; d_{0} [mm]; Efficiency", {50, -300, 300});
    efficiencyHelper("h_pt_HLT_mu24_LRT_r_truth_eff; p_{T} [GeV]; Efficiency", {50, 0, 250});
    efficiencyHelper("h_eta_HLT_mu24_LRT_r_truth_eff; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_phi_HLT_mu24_LRT_r_truth_eff; #phi; Efficiency", {35, -3.5, 3.5});
    efficiencyHelper("h_vtxR_HLT_mu24_LRT_r_truth_eff; Prod Radius [mm]; Efficiency", {50, 0, 300});

    // prompt muon eff vs truth
    efficiencyHelper("h_d0_HLT_mu24_r_truth_eff; d_{0} [mm]; Efficiency", {50, -300, 300});
    efficiencyHelper("h_pt_HLT_mu24_r_truth_eff; p_{T} [GeV]; Efficiency", {50, 0, 250});
    efficiencyHelper("h_eta_HLT_mu24_r_truth_eff; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_phi_HLT_mu24_r_truth_eff; #phi; Efficiency", {35, -3.5, 3.5});
    efficiencyHelper("h_vtxR_HLT_mu24_r_truth_eff; Prod Radius [mm]; Efficiency", {50, 0, 300});


    efficiencyHelper("h_pt_sublead_HLT_2mu50_r_offline_eff; p_{T} [GeV]; Efficiency", {20, 0, 300});
    efficiencyHelper("h_eta_sublead_HLT_2mu50_r_offline_eff; #eta; Efficiency", {15, -3, 3});
    efficiencyHelper("h_phi_sublead_HLT_2mu50_r_offline_eff; #phi; Efficiency", {15, -3.5, 3.5});
    efficiencyHelper("h_prodR_HLT_2mu50_r_offline;  Prod Radius [mm]", {20, 0, 300});


    efficiencyHelper("h_pt_el_HLT_g40mu40_r_offline_eff; p_{T} [GeV]; Efficiency", {20, 0, 300});
    efficiencyHelper("h_pt_mu_HLT_g40mu40_r_offline_eff; p_{T} [GeV]; Efficiency", {20, 0, 300});
    efficiencyHelper("h_prodR_el_HLT_g40mu40_r_offline;  Prod Radius [mm]", {20, 0, 300});
    efficiencyHelper("h_prodR_mu_HLT_g40mu40_r_offline;  Prod Radius [mm]", {20, 0, 300});
    efficiencyHelper("h_eta_mu_HLT_g40mu40_r_offline_eff; #eta; Efficiency", {15, -3, 3});
    efficiencyHelper("h_phi_mu_HLT_g40mu40_r_offline_eff; #phi; Efficiency", {15, -3.5, 3.5});

    // g40 single leg
    efficiencyHelper("h_pt_el_HLT_g40_r_offline_eff; p_{T} [GeV]; Efficiency", {20, 0, 300});
    efficiencyHelper("h_eta_el_HLT_g40_r_offline_eff; #eta; Efficiency", {30, -3, 3});
    efficiencyHelper("h_prodR_el_HLT_g40_r_offline_eff;  Prod Radius [mm]", {20, 0, 300});


    // Matching checks
    bookHisto("h_deltaR_hlt_muon_reco_muon; dR; Count", {100, 0, 0.5}); 
    bookHisto("h_deltad0_hlt_muon_reco_muon; d_d0; Count", {100, -10, 10}); 
    bookHisto("h_deltapt_hlt_muon_reco_muon; d_pt; Count", {100, -20, 20}); 
    bookHisto("h_deltad0_hlt_muon_reco_muon_drcut; d_d0; Count", {100, -10, 10}); 
    bookHisto("h_deltapt_hlt_muon_reco_muon_drcut; d_pt; Count", {100, -20, 20}); 

    bookHisto("h_deltaR_ms_hlt_muon_reco_muon; dR; Count", {100, 0, 0.5}); 
    bookHisto("h_deltad0_ms_hlt_muon_reco_muon; d_d0; Count", {100, -10, 10}); 
    bookHisto("h_deltapt_ms_hlt_muon_reco_muon; d_pt; Count", {100, -20, 20}); 
    bookHisto("h_deltad0_ms_hlt_muon_reco_muon_drcut; d_d0; Count", {100, -10, 10}); 
    bookHisto("h_deltapt_ms_hlt_muon_reco_muon_drcut; d_pt; Count", {100, -20, 20});

    bookHisto("h_deltaR_hlt_elec_reco_elec; dR; Count", {100, 0, 0.5}); 
    bookHisto("h_deltad0_hlt_elec_reco_elec; d_d0; Count", {100, -10, 10}); 
    bookHisto("h_deltapt_hlt_elec_reco_elec; d_pt; Count", {100, -20, 20}); 
    bookHisto("h_deltaE_hlt_elec_reco_elec; d_E; Count", {100, -20, 20}); 
    bookHisto("h_deltad0_hlt_elec_reco_elec_drcut; d_d0; Count", {100, -10, 10}); 
    bookHisto("h_deltapt_hlt_elec_reco_elec_drcut; d_pt; Count", {100, -20, 20}); 
    bookHisto("h_deltaE_hlt_elec_reco_elec_drcut; d_E; Count", {100, -20, 20}); 

    bookHisto("h_deltaR_clust_hlt_elec_reco_elec; dR; Count", {100, 0, 0.5}); 
    bookHisto("h_deltad0_clust_hlt_elec_reco_elec; d_d0; Count", {100, -10, 10}); 
    bookHisto("h_deltapt_clust_hlt_elec_reco_elec; d_pt; Count", {100, -20, 20}); 
    bookHisto("h_deltaE_clust_hlt_elec_reco_elec; d_E; Count", {100, -20, 20}); 
    bookHisto("h_deltad0_clust_hlt_elec_reco_elec_drcut; d_d0; Count", {100, -10, 10}); 
    bookHisto("h_deltapt_clust_hlt_elec_reco_elec_drcut; d_pt; Count", {100, -20, 20}); 
    bookHisto("h_deltaE_clust_hlt_elec_reco_elec_drcut; d_E; Count", {100, -20, 20}); 

    bookHisto("h_deltaR_clustBE_hlt_elec_reco_elec; dR; Count", {100, 0, 0.5}); 


    ///////////////////////// 

    ///////////////////////////
    // TProfiles
    ANA_CHECK(book (TProfile("hp_lrtftf_hits_v_d0", "hp_lrtftf_hits_v_d0", 50, 0, 300)));
    hist("hp_lrtftf_hits_v_d0")->SetTitle("hp_lrtftf_hits_v_d0; d0 [mm]; <SI Hits>");

    ANA_CHECK(book (TProfile("hp_lrtftf_pixhits_v_d0", "hp_lrtftf_pixhits_v_d0", 50, 0, 300)));
    hist("hp_lrtftf_pixhits_v_d0")->SetTitle("hp_lrtftf_pixhits_v_d0; d0 [mm]; <Pix Hits>");

    ANA_CHECK(book (TProfile("hp_lrtftf_scthits_v_d0", "hp_lrtftf_scthits_v_d0", 50, 0, 300)));
    hist("hp_lrtftf_scthits_v_d0")->SetTitle("hp_lrtftf_scthits_v_d0; d0 [mm]; <SCT Hits>");

    ANA_CHECK(book (TProfile("hp_offlineLRT_hits_v_d0", "hp_offlineLRT_hits_v_d0", 50, 0, 300)));
    hist("hp_offlineLRT_hits_v_d0")->SetTitle("hp_offlineLRT_hits_v_d0; d0 [mm]; <SI Hits>");

    ANA_CHECK(book (TProfile("hp_offlineLRT_pixhits_v_d0", "hp_offlineLRT_pixhits_v_d0", 50, 0, 300)));
    hist("hp_offlineLRT_pixhits_v_d0")->SetTitle("hp_offlineLRT_pixhits_v_d0; d0 [mm]; <Pix Hits>");

    ANA_CHECK(book (TProfile("hp_offlineLRT_scthits_v_d0", "hp_offlineLRT_scthits_v_d0", 50, 0, 300)));
    hist("hp_offlineLRT_scthits_v_d0")->SetTitle("hp_offlineLRT_scthits_v_d0; d0 [mm]; <SCT Hits>");


    ANA_CHECK(book (TProfile("hp_offlineSignal_hits_v_d0", "hp_offlineSignal_hits_v_d0", 50, 0, 300)));
    hist("hp_offlineSignal_hits_v_d0")->SetTitle("hp_offlineSignal_hits_v_d0; d0 [mm]; <SI Hits>");

    ANA_CHECK(book (TProfile("hp_offlineSignal_pixhits_v_d0", "hp_offlineSignal_pixhits_v_d0", 50, 0, 300)));
    hist("hp_offlineSignal_pixhits_v_d0")->SetTitle("hp_offlineSignal_pixhits_v_d0; d0 [mm]; <Pix Hits>");

    ANA_CHECK(book (TProfile("hp_offlineSignal_scthits_v_d0", "hp_offlineSignal_scthits_v_d0", 50, 0, 300)));
    hist("hp_offlineSignal_scthits_v_d0")->SetTitle("hp_offlineSignal_scthits_v_d0; d0 [mm]; <SCT Hits>");

    ////////////////////////////////////////
    ////// 2D histograms

    //ANA_CHECK (book (TH2F ("h2_truth_beam_x_v_y", "h2_truth_beam_x_v_y", 40, -2, 2, 40, -2, 2)));
    //hist("h2_truth_beam_x_v_y")->SetTitle("h2_truth_beam_x_v_y; Truth Beam x [mm]; Truth Beam y [mm]");

    //ANA_CHECK (book (TH2F ("h2_offline_prodVtx_x_v_y", "h2_offline_prodVtx_x_v_y", 40, -2, 2, 40, -2, 2)));
    //hist("h2_offline_prodVtx_x_v_y")->SetTitle("h2_offline_prodVtx_x_v_y; Prod Vtx x [mm]; Prod Vtx y [mm]");


    bookHisto("h2_ftf_d0_v_hitsSI; d_{0} [mm]; SI Hits",{50, 0, 300, 20,-0.5,19.5});
   
    bookHisto("h2_lrtftf_d0_v_hitsSI; d_{0} [mm]; SI Hits", {50, 0, 300, 20,-0.5,19.5});
    bookHisto("h2_ftf_d0_v_holesSI; d_{0} [mm]; Holes", { 50, 0, 300, 10,-0.5,9.5});
    bookHisto("h2_lrtftf_d0_v_holesSI; d_{0} [mm]; Holes", { 50, 0, 300, 10,-0.5,9.5});
    bookHisto("h2_offline_d0_v_hitsSI; d_{0} [mm]; SI Hits", { 50, 0, 300, 20,-0.5,19.5});
    bookHisto("h2_offline_d0_v_hitsPix; d_{0} [mm]; Pix Hits", { 50, 0, 300, 20,-0.5,19.5});
    bookHisto("h2_offline_d0_v_hitsSCT; d_{0} [mm]; SCT Hits", { 50, 0, 300, 20,-0.5,19.5});
    bookHisto("h2_offline_d0_v_holesSI; d_{0} [mm]; Holes", { 50, 0, 300, 10,-0.5,9.5});
    bookHisto("h2_offlineSignal_d0_v_hitsSI; d_{0} [mm]; SI hits", { 50, 0, 300, 20,-0.5,19.5});


    bookHisto("h2_offlineSignal_d0_v_hitsPix; d_{0} [mm]; Pix hits", { 50, 0, 300, 20,-0.5,19.5});
    bookHisto("h2_offlineSignal_d0_v_hitsSCT; d_{0} [mm]; SCT hits", { 50, 0, 300, 20,-0.5,19.5});    
    bookHisto("h2_lrtftf_d0_v_pt; d_{0} [mm]; p_{T} [GeV]", { 50, 0, 300, 50, 0, 500});    
    bookHisto("h2_offlineSignal_vtxR_v_hitsSI; Prod Radius [mm]; SI Hits", { 50, 0, 500, 20,-0.5,19.5});
    
    
    //bookHisto("h2_truthSignal_d0_v_l1MET; d0 [mm]; L1 MET [GeV]", { 50, 0, 50, 20,0,200});
    
    // 2D ROI hists
    bookHisto("h2_deltaR_ROI_offlineSignal_v_d0_em; dR(ROI, trac", { 50, 0, 0.5, 150, 0, 300}); 
    bookHisto("h2_deltaR_ROI_offlineSignal_v_d0_mu; dR(ROI, trac", { 50, 0, 0.5, 150, 0, 300});
    
    bookHisto("h2_FTF_inROI_d0_v_pt_em; d_{0} [mm]; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});
    bookHisto("h2_FTF_inROI_d0_v_pt_mu; d_{0} [mm]; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});
    bookHisto("h2_FTF_inROI_d0sig_v_pt_em; d_{sig}_{0}; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});
    bookHisto("h2_FTF_inROI_d0sig_v_pt_mu; d^{sig}_{0}; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});
    
    
    // em
    bookHisto("h2_offlineSignalwFTF_inROI_d0_v_pt_em; d_{0} [mm]; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});    
    bookHisto("h2_offlineSignalwFTF_inROI_d0_v_pt_mu; d_{0} [mm]; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});   
    bookHisto("h2_offlineSignalwFTF_inROI_d0sig_v_pt_em; d_{sig}_{0}; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});   
    bookHisto("h2_offlineSignalwFTF_inROI_d0sig_v_pt_mu; d^{sig}_{0}; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});

    // mu
    bookHisto("h2_truthSignalwFTF_inROI_d0_v_pt_em; d_{0} [mm]; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});
    bookHisto("h2_truthSignalwFTF_inROI_d0_v_pt_mu; d_{0} [mm]; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});
    bookHisto("h2_truthSignalwFTF_inROI_d0sig_v_pt_em; d_{sig}_{0}; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});
    bookHisto("h2_truthSignalwFTF_inROI_d0sig_v_pt_mu; d^{sig}_{0}; p_{T} [GeV]", { 50, 0, 50, 50, 0, 50});


    // ftf/offline eff debugging
    if (m_debug) bookHisto("h_ftf_offline_truth_matching; Match Type; N Entries", { 10, .5, 11});


    // electron chain debugging
    ANA_CHECK(m_electronTracksKey.initialize());


    return StatusCode::SUCCESS;
}



StatusCode simplexAODReaderEL :: execute ()
{
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.
    
    m_cutflow(__LINE__,"Total Events")++;
    m_totalEventCounter++;
    
    // Collections to fill with passing offline and FTF tracks to be used for matching and efficiency calculation
    m_OfflineTracks.clear();    
    m_OfflineTracksSignal.clear();  
    m_FTFTracksComb.clear();
    m_FTFLRTTracks.clear();
    m_TruthParticlesSignal.clear();    
    m_muonFTFLRT.clear();
    m_muonPTLRT.clear();
    m_muonPT.clear();
    m_electronFTFLRT.clear();
    m_muonFTF.clear();
    m_electronFTF.clear();
    m_electronPTLRT.clear();
    m_electronPT.clear();
    m_Muons.clear();
    //m_MuonsLRT.clear();
    m_HLT_Muons.clear();
    //m_HLT_MuonsLRT.clear();
    m_Electrons.clear();
    //m_LRTElectrons.clear();
    m_HLT_Electrons.clear();
    //m_HLT_LRTElectrons.clear();

    // Collections of ROIs
    m_roid_em.clear();
    m_roid_mu.clear();

    

    // Load xAOD info
    ANA_CHECK( evtStore()->retrieve( ei, "EventInfo" ) ); 
    bool m_isSimulation = ei->eventType( xAOD::EventInfo::IS_SIMULATION );

    if (m_isSimulation)
    {
        if (m_isDAOD) {ANA_CHECK( evtStore()->retrieve(truthParticles, "TruthBSMWithDecayParticles" )  );}
        else {ANA_CHECK( evtStore()->retrieve(truthParticles, "TruthParticles" )  );}

        ANA_CHECK( evtStore()->retrieve(truthEvents   , "TruthEvents")      );
    }

    // It's more flexible to not use ANA_CHECK, which exits the program upon an error.  
    // Instead if we just have null pointers, we can check and not use them later.  
    // The logic is to try retrieving the collection, if it fails print a warning and then don't try on the next events.

    retrieveCollection(m_flag_inDetTracksOffline, inDetTracksOffline, "InDetTrackParticles");

    retrieveCollection(m_flag_inDetTracksLRTOffline, inDetTracksLRTOffline, "InDetLargeD0TrackParticles");

    retrieveCollection(m_flag_HLT_FTF_Tracks, HLT_FTF_Tracks, "HLT_IDTrack_FS_FTF");

    retrieveCollection(m_flag_HLT_lrtFTF_Tracks, HLT_lrtFTF_Tracks, "HLT_IDTrack_FSLRT_FTF");

    retrieveCollection(m_flag_HLT_FTF_Muon_Tracks, HLT_FTF_Muon_Tracks, "HLT_IDTrack_Muon_FTF");

    retrieveCollection(m_flag_HLT_FTFLRT_Muon_Tracks, HLT_FTFLRT_Muon_Tracks, "HLT_IDTrack_MuonLRT_FTF");

    retrieveCollection(m_flag_HLT_PT_Muon_Tracks, HLT_PT_Muon_Tracks, "HLT_IDTrack_Muon_IDTrig");

    // Even though these next two lines are used, if they are commented, the code will segfault when accessing the pointer HLT_PTLRT_Muon_Tracks
    retrieveCollection(m_flag_HLT_MuonsL2LRT, HLT_MuonsL2LRT, "HLT_MuonL2CBInfoLRT");
    retrieveCollection(m_flag_HLT_MuonsCBLRT, HLT_MuonsCBLRT, "HLT_MuonsCB_LRT");
    retrieveCollection(m_flag_HLT_MuonsCB, HLT_MuonsCB, "HLT_MuonsCB_RoI");

    retrieveCollection(m_flag_HLT_PTLRT_Muon_Tracks, HLT_PTLRT_Muon_Tracks, "HLT_IDTrack_MuonLRT_IDTrig");

    std::string HLT_Electrons_lrt_name = (REL22 ? "HLT_egamma_Electrons_LRTGSF" : "");
    retrieveCollection(m_flag_HLT_Electrons_lrt, HLT_Electrons_LRT, HLT_Electrons_lrt_name);

    std::string HLT_Electrons_name = (REL22 ? "HLT_egamma_Electrons_GSF" : "HLT_xAOD__ElectronContainer_egamma_Electrons");
    retrieveCollection(m_flag_HLT_Electrons, HLT_Electrons, HLT_Electrons_name);

    retrieveCollection(m_flag_HLT_FTF_Electron_Tracks, HLT_FTF_Electron_Tracks, "HLT_IDTrack_Electron_FTF");

    retrieveCollection(m_flag_HLT_FTFLRT_Electron_Tracks, HLT_FTFLRT_Electron_Tracks, "HLT_IDTrack_ElecLRT_FTF");

    retrieveCollection(m_flag_HLT_PTLRT_Electron_Tracks, HLT_PTLRT_Electron_Tracks, "HLT_IDTrack_ElecLRT_IDTrig");

    retrieveCollection(m_flag_HLT_PT_Electron_Tracks, HLT_PT_Electron_Tracks, "HLT_IDTrack_Electron_IDTrig");

    //std::string HLT_trkmht_name = (REL22 ? "HLT_MET_trkmht" : "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_trkmht");
    //retrieveCollection(m_flag_HLT_trkmht, HLT_trkmht, HLT_trkmht_name);

    //std::string HLT_pufit_name = (REL22 ? "HLT_MET_tcpufit" : "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_topocl_PUC" );
    //retrieveCollection(m_flag_HLT_pufit, HLT_pufit, HLT_pufit_name);

    //retrieveCollection(m_flag_l1MetObject, l1MetObject, "LVL1EnergySumRoI");

    //retrieveCollection(m_flag_Jets, Jets, "AntiKt4EMTopoJets");

    retrieveCollection(m_flag_Muons, Muons, "Muons");
    retrieveCollection(m_flag_MuonsLRT, MuonsLRT, "MuonsLRT");

    retrieveCollection(m_flag_Electrons, Electrons, "Electrons");
    retrieveCollection(m_flag_LRTElectrons, LRTElectrons, "LRTElectrons");

    //retrieveCollection(m_flag_MuonSegments, MuonSegments, "MuonSegments");

    retrieveCollection(m_flag_PrimaryVertices, PrimaryVertices, "PrimaryVertices");

    retrieveCollection(m_flag_caloClusters, caloClusters, "HLT_TrigEMClusters");

    if (m_debug) 
    {
        std::cout << "\n\nProcessing run #" << ei->runNumber() << ", event #" << ei->eventNumber() << ", is SIM = " << m_isSimulation<<", event loop # "<< m_eventCounter<< std::endl;  
        m_eventCounter++;
    }
    
    if (m_isSimulation)
    {
        const xAOD::TruthVertex *beamVtx = truthEvents[0][0]->signalProcessVertex();
        //hist("h2_truth_beam_x_v_y")->Fill(beamVtx->x(), beamVtx->y());

        if (!m_isDAOD)
        {
            m_xbeam = beamVtx->x(); // DAOD segfault
            m_ybeam = beamVtx->y();
        }

    } else { // data
        if (PrimaryVertices != nullptr && (*PrimaryVertices)[0] != nullptr)
        {
            m_xbeam = (*PrimaryVertices)[0]->x();
            m_ybeam = (*PrimaryVertices)[0]->y();
            //ei->beamPosSigmaX(), ei->beamPosSigmaY()
        }
    }


    // Collect Event Trigger
    // Check lepton triggers used in displaced lepton analysis
    auto cg_g140 = m_trigDecTool->getChainGroup("HLT_g140_loose_L1eEM26M");
    bool HLT_g140_loose_L1eEM26M_passed = cg_g140->isPassed();

    auto cg_2g50 = m_trigDecTool->getChainGroup("HLT_2g50_loose_L12EM20VH");
    bool HLT_2g50_loose_L12EM20VH_pased = cg_2g50->isPassed();
    
 
    


    // Loop through triggers and add up accepts
    if (m_printTriggerDecCount)
    {
        std::vector<std::string> triggers = m_trigDecTool->getListOfTriggers();
        for (std::string trigger : triggers)
        {
            auto cg =  m_trigDecTool->getChainGroup(trigger);

            // Set count to 0 for first event
            if (m_firstEvent)
            {
                if (trigger.substr(0,2) == "L1") m_L1_Trig_Pass[trigger] = 0;
                if (trigger.substr(0,3) == "HLT") m_HLT_Trig_Pass[trigger] = 0;
            }

            // special case for L1All (fsnoseed) chain
            unsigned decisiontype;
            if (trigger.find("L1All") != std::string::npos) decisiontype = TrigDefs::requireDecision; // HLT only
            else                                           decisiontype = TrigDefs::Physics; // default HLT+L1
            
            // Grab prescale value
            if (trigger.substr(0,2) == "L1") m_L1_Trig_Prescale[trigger] = cg->getPrescale(decisiontype);
            if (trigger.substr(0,3) == "HLT") m_HLT_Trig_Prescale[trigger] = cg->getPrescale(decisiontype);

            // Increment count if trigger passed
            if (cg->isPassed(decisiontype))
            {
                if (trigger.substr(0,2) == "L1") m_L1_Trig_Pass[trigger]++;
                if (trigger.substr(0,3) == "HLT") m_HLT_Trig_Pass[trigger]++;
            } 

        } 
    }


    // Retrieve EM RoIs
    // https://cds.cern.ch/record/2683881 HLT_e26_lhmedium_nod0  HLT_e0_perf_L1EM15 (old)
    // https://atlas-trigconf.cern.ch/mc2/smkey/2274/l1key/103/hltkey/317 (old)

    unsigned feature_type = TrigDefs::allFeaturesOfType; // TrigDefs::lastFeatureOfType   TrigDefs::allFeaturesOfType
    unsigned decisiontype = TrigDefs::alsoDeactivateTEs; //TrigDefs::Physics;   TrigDefs::alsoDeactivateTEs;  TrigDefs::L1_isPassedBeforePrescale;
    std::string roi_key = "HLT_Roi_Fast.*"; // HLT_Roi_FastElectron   HLT_EMRoIs   HLT_MURoIs HLT_MuonsCB_RoI  MuonCandidates_FS_ROIs  HLT_Roi_L2SAMuon
    std::string electron_chaingroup_forROI = "HLT_e26_idperf_loose_lrtloose_L1eEM26M"; //HLT_e26_lhloose_lrtloose_L1eEM26M; HLT_e26_lhtight_ivarloose_L1eEM26M . HACK "(HLT_e25_lhvloose_nod0_L1EM15|HLT_e60_lhmedium_nod0)" "(HLT_e26_lhmedium_nod0|HLT_e60_lhmedium)" HLT_e5_idperf_L1EM3 HLT_g25_loose_L1EM20VH 



    std::vector< TrigCompositeUtils::LinkInfo<TrigRoiDescriptorCollection> > rois_em_fc;
    if (!m_isDAOD)
    {
        rois_em_fc = m_trigDecTool->template features<TrigRoiDescriptorCollection>( 
                    electron_chaingroup_forROI,
                    decisiontype,
                    roi_key,
                    feature_type,
                    "roi" );
    }

    // Debug
    //auto cg =  m_trigDecTool->getChainGroup("HLT_e26_lhtight_ivarloose_L1eEM26M");
    //std::cout<<"HLT_e26_lhtight_ivarloose_L1eEM26M passed "<<cg->isPassed()<<std::endl;
    //ANA_MSG_ALWAYS(" N " << rois_em_fc.size());
    //for (auto& r : rois_em_fc) {
    //    ANA_MSG_ALWAYS( r.link.dataID() << " #" << r.link.index() << " eta:" << (*r.link)->eta() << " phi:" << (*r.link)->phi());
    //}



    // Pull out trig roi descriptor
    for (const TrigCompositeUtils::LinkInfo<TrigRoiDescriptorCollection> &RoI_info : rois_em_fc)
    {
        if (RoI_info.isValid()) 
        {
            m_roid_em.push_back(*RoI_info.link);
        }
        else std::cout<<"Warning: em roi descriptor link wasn't valid"<<std::endl;
    }



    hist("h_nROI_em")->Fill(m_roid_em.size());



    ////// MU RoIs
    std::string muon_chaingroup_forROI = "HLT_mu20_idperf_L1MU14FCH"; // HLT_mu26_ivarmedium_L1MU20 "(HLT_mu6_idperf_L1MU6|HLT_mu26_ivarmedium_L1MU20|HLT_mu50_L1MU20)"; 
    bool HLT_MU_chainPassed = m_trigDecTool->getChainGroup(muon_chaingroup_forROI)->isPassed();
    roi_key = "HLT_Roi_L2SAMuon"; // jdl debugging try putting .* to see if you get more ftf track in roi later

    std::vector< TrigCompositeUtils::LinkInfo<TrigRoiDescriptorCollection> > rois_mu_fc;
    if (!m_isDAOD)
    {
        rois_mu_fc = m_trigDecTool->template features<TrigRoiDescriptorCollection>( 
                    muon_chaingroup_forROI,
                    decisiontype,
                    roi_key,
                    feature_type,
                    "roi" );
    }

    // Debug
    //ANA_MSG_ALWAYS(" N " << rois_mu_fc.size());
    //for (auto& r : rois_mu_fc) {
    //    ANA_MSG_ALWAYS( r.link.dataID() << " #" << r.link.index() << " eta:" << (*r.link)->eta() << " phi:" << (*r.link)->phi());
    //}

    // Pull out trig roi descriptor
    for (const TrigCompositeUtils::LinkInfo<TrigRoiDescriptorCollection> &RoI_info : rois_mu_fc)
    {
        if (RoI_info.isValid()) 
        {
            m_roid_mu.push_back(*RoI_info.link);
        }             
        else std::cout<<"Warning: MU roi descriptor link wasn't valid"<<std::endl; 
    }

    hist("h_nROI_mu")->Fill(m_roid_mu.size());



    bool HLT_EL_chainPassed = m_trigDecTool->getChainGroup(electron_chaingroup_forROI)->isPassed(); 

    auto cg_L1_EM22 = m_trigDecTool->getChainGroup("L1_EM22VH");
    L1_EM22_Passed = cg_L1_EM22->isPassed();

    auto cg_L1_EM20VH = m_trigDecTool->getChainGroup("L1_EM20VH");
    bool L1_EM20VH_Passed = cg_L1_EM20VH->isPassed();

    auto cg_L1_EM20VHI = m_trigDecTool->getChainGroup("L1_EM20VHI");
    bool L1_EM20VHI_Passed = cg_L1_EM20VHI->isPassed();

    auto cg_L1_EM22VH = m_trigDecTool->getChainGroup("L1_EM22VH");
    bool L1_EM22VH_Passed = cg_L1_EM22VH->isPassed();

    auto cg_L1_EM22VHI = m_trigDecTool->getChainGroup("L1_EM22VHI");
    bool L1_EM22VHI_Passed = cg_L1_EM22VHI->isPassed();

    auto cg_L1_eEM26M = m_trigDecTool->getChainGroup("L1_eEM26M");
    bool L1_eEM26M_Passed = cg_L1_eEM26M->isPassed();

    auto cg_HLT_g20_loose_L1EM15VH = m_trigDecTool->getChainGroup("HLT_g20_loose_L1EM15VH");
    bool HLT_g20_loose_L1EM15VH_passed = cg_HLT_g20_loose_L1EM15VH->isPassed();

    auto cg_HLT_g25_loose_L1EM20VH = m_trigDecTool->getChainGroup("HLT_g25_loose_L1EM20VH");
    bool HLT_g25_loose_L1EM20VH_Passed = cg_HLT_g25_loose_L1EM20VH->isPassed();

    auto cg_HLT_e26_etcut_L1eEM26M = m_trigDecTool->getChainGroup("HLT_e26_etcut_L1eEM26M");
    bool HLT_e26_etcut_L1eEM26M_Passed = cg_HLT_e26_etcut_L1eEM26M->isPassed();

    auto cg_HLT_g35_loose_L1eEM26M = m_trigDecTool->getChainGroup("HLT_g35_loose_L1eEM26M");
    bool HLT_g35_loose_L1eEM26M_Passed = cg_HLT_g35_loose_L1eEM26M->isPassed();

    // new muon lrt triggers
    // auto cg_HLT_mu24_LRT_l2lrt_d0tight_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu24_LRT_l2lrt_d0tight_L1MU14FCH");
    // bool HLT_mu24_LRT_l2lrt_d0tight_L1MU14FCH_passed = cg_HLT_mu24_LRT_l2lrt_d0tight_L1MU14FCH->isPassed();

    // auto cg_HLT_mu24_LRT_l2lrt_d0medium_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu24_LRT_l2lrt_d0medium_L1MU14FCH");
    // bool HLT_mu24_LRT_l2lrt_d0medium_L1MU14FCH_passed = cg_HLT_mu24_LRT_l2lrt_d0medium_L1MU14FCH->isPassed();

    // auto cg_HLT_mu24_LRT_l2lrt_d0loose_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu24_LRT_l2lrt_d0loose_L1MU14FCH");
    // bool HLT_mu24_LRT_l2lrt_d0loose_L1MU14FCH_passed = cg_HLT_mu24_LRT_l2lrt_d0loose_L1MU14FCH->isPassed();

    auto cg_HLT_mu24_LRT_d0tight_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu24_LRT_d0tight_L1MU14FCH");
    bool HLT_mu24_LRT_d0tight_L1MU14FCH_passed = cg_HLT_mu24_LRT_d0tight_L1MU14FCH->isPassed();

    auto cg_HLT_mu24_LRT_d0medium_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu24_LRT_d0medium_L1MU14FCH");
    bool HLT_mu24_LRT_d0medium_L1MU14FCH_passed = cg_HLT_mu24_LRT_d0medium_L1MU14FCH->isPassed();

    auto cg_HLT_mu24_LRT_d0loose_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu24_LRT_d0loose_L1MU14FCH");
    bool HLT_mu24_LRT_d0loose_L1MU14FCH_passed = cg_HLT_mu24_LRT_d0loose_L1MU14FCH->isPassed();

    auto cg_HLT_mu24_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu24_L1MU14FCH");
    bool HLT_mu24_L1MU14FCH_passed = cg_HLT_mu24_L1MU14FCH->isPassed();

    auto cg_HLT_mu24_ivarmedium_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu24_ivarmedium_L1MU14FCH");
    bool HLT_mu24_ivarmedium_L1MU14FCH_passed = cg_HLT_mu24_ivarmedium_L1MU14FCH->isPassed();
    
    auto cg_L1_MU14FCH = m_trigDecTool->getChainGroup("L1_MU14FCH");
    L1_MU14FCH_Passed = cg_L1_MU14FCH->isPassed();

    auto cg_HLT_mu20_msonly = m_trigDecTool->getChainGroup("HLT_mu20_msonly_L1MU14FCH");
    bool HLT_mu20_msonly_Passed = cg_HLT_mu20_msonly->isPassed();

    auto cg_HLT_mu60_0eta105_msonly_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu60_0eta105_msonly_L1MU14FCH");
    bool HLT_mu60_0eta105_msonly_L1MU14FCH_passed = cg_HLT_mu60_0eta105_msonly_L1MU14FCH->isPassed();

    auto cg_HLT_mu6_msonly_L1MU6 = m_trigDecTool->getChainGroup("HLT_mu6_msonly_L1MU6");
    bool HLT_mu6_msonly_L1MU6_Passed = cg_HLT_mu6_msonly_L1MU6->isPassed();

    // Muon LRT test triggers
    auto cg_HLT_mu20_LRT_d0loose_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu20_LRT_d0loose_L1MU14FCH");
    bool HLT_mu20_LRT_d0loose_L1MU14FCH_Passed = cg_HLT_mu20_LRT_d0loose_L1MU14FCH->isPassed();
    
    auto cg_HLT_mu20_LRT_d0tight_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_mu20_LRT_d0tight_L1MU14FCH");
    bool HLT_mu20_LRT_d0tight_L1MU14FCH_Passed = cg_HLT_mu20_LRT_d0tight_L1MU14FCH->isPassed();

    // New EL LRT Triggers
    auto cg_HLT_e26_lhtight_L1eEM26M = m_trigDecTool->getChainGroup("HLT_e26_lhtight_L1eEM26M");
    bool HLT_e26_lhtight_L1eEM26M_passed = cg_HLT_e26_lhtight_L1eEM26M->isPassed();

    auto cg_HLT_e26_lhtight_ivarloose_L1eEM26M = m_trigDecTool->getChainGroup("HLT_e26_lhtight_ivarloose_L1eEM26M");
    bool HLT_e26_lhtight_ivarloose_L1eEM26M_passed = cg_HLT_e26_lhtight_ivarloose_L1eEM26M->isPassed();

    auto cg_HLT_e26_lhloose_nopix_lrttight_L1eEM26M = m_trigDecTool->getChainGroup("HLT_e26_lhloose_nopix_lrttight_L1eEM26M");
    bool HLT_e26_lhloose_nopix_lrttight_L1eEM26M_passed = cg_HLT_e26_lhloose_nopix_lrttight_L1eEM26M->isPassed();

    auto cg_HLT_e26_lhmedium_nopix_lrttight_L1eEM26M = m_trigDecTool->getChainGroup("HLT_e26_lhmedium_nopix_lrttight_L1eEM26M");
    bool HLT_e26_lhmedium_nopix_lrttight_L1eEM26M_passed = cg_HLT_e26_lhmedium_nopix_lrttight_L1eEM26M->isPassed();

    // tighter pt looser d0
    auto cg_HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M = m_trigDecTool->getChainGroup("HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M");
    bool HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed = cg_HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M->isPassed();

    // EL support chains
    auto cg_HLT_e26_lhtight_ivarloose_e5_lhvloose_nopix_lrtloose_idperf_probe_L1eEM26M = m_trigDecTool->getChainGroup("HLT_e26_lhtight_ivarloose_e5_lhvloose_nopix_lrtloose_idperf_probe_L1eEM26M");
    bool HLT_e26_lhtight_ivarloose_e5_lhvloose_nopix_lrtloose_idperf_probe_L1eEM26M_passed = cg_HLT_e26_lhtight_ivarloose_e5_lhvloose_nopix_lrtloose_idperf_probe_L1eEM26M->isPassed();
    
    auto cg_HLT_e26_lhtight_ivarloose_e26_lhloose_nopix_lrttight_probe_L1eEM26M = m_trigDecTool->getChainGroup("HLT_e26_lhtight_ivarloose_e26_lhloose_nopix_lrttight_probe_L1eEM26M");
    bool HLT_e26_lhtight_ivarloose_e26_lhloose_nopix_lrttight_probe_L1eEM26M_passed = cg_HLT_e26_lhtight_ivarloose_e26_lhloose_nopix_lrttight_probe_L1eEM26M->isPassed();
    
    auto cg_HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH = m_trigDecTool->getChainGroup("HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH");
    bool HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH_passed = cg_HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH->isPassed();
    
    auto cg_HLT_e26_lhloose_nopix_lrttight_probe_g25_medium_L1EM20VH = m_trigDecTool->getChainGroup("HLT_e26_lhloose_nopix_lrttight_probe_g25_medium_L1EM20VH");
    bool HLT_e26_lhloose_nopix_lrttight_probe_g25_medium_L1EM20VH_passed = cg_HLT_e26_lhloose_nopix_lrttight_probe_g25_medium_L1EM20VH->isPassed();
       
    auto cg_HLT_e26_idperf_loose_lrtloose_L1eEM26M = m_trigDecTool->getChainGroup("HLT_e26_idperf_loose_lrtloose_L1eEM26M");
    bool HLT_e26_idperf_loose_lrtloose_L1eEM26M_passed = cg_HLT_e26_idperf_loose_lrtloose_L1eEM26M->isPassed();
    
    auto cg_HLT_e5_idperf_loose_lrtloose_L1EM3 = m_trigDecTool->getChainGroup("HLT_e5_idperf_loose_lrtloose_L1EM3");
    bool HLT_e5_idperf_loose_lrtloose_L1EM3_passed = cg_HLT_e5_idperf_loose_lrtloose_L1EM3->isPassed();
    
    // ms only trigger validation
    auto cg_HLT_2mu50_msonly_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_2mu50_msonly_L1MU14FCH");
    bool HLT_2mu50_msonly_L1MU14FCH_passed = cg_HLT_2mu50_msonly_L1MU14FCH->isPassed();

    auto cg_HLT_g40_loose_mu40_msonly_L1MU14FCH = m_trigDecTool->getChainGroup("HLT_g40_loose_mu40_msonly_L1MU14FCH");
    bool HLT_g40_loose_mu40_msonly_L1MU14FCH_passed = cg_HLT_g40_loose_mu40_msonly_L1MU14FCH->isPassed();

    auto cg_HLT_g40_loose_L1EM20VH = m_trigDecTool->getChainGroup("HLT_g40_loose_L1EM20VH");
    bool HLT_g40_loose_L1EM20VH_passed = cg_HLT_g40_loose_L1EM20VH->isPassed();

    // L1 MET
    auto cg_L1_XE50 = m_trigDecTool->getChainGroup("L1_XE50");
    L1_XE50_Passed = cg_L1_XE50->isPassed();

    auto cg_HLT_xe100_trkmht_xe85_pufit_xe65_L1XE50 = m_trigDecTool->getChainGroup("HLT_xe100_trkmht_xe85_pufit_xe65_L1XE50");
    bool HLT_xe100_trkmht_xe85_pufit_xe65_L1XE50_Passed = cg_HLT_xe100_trkmht_xe85_pufit_xe65_L1XE50->isPassed();

    // L1 J
    auto cg_L1_J100 = m_trigDecTool->getChainGroup("L1_J100");
    L1_J100_Passed = cg_L1_J100->isPassed();


    bool passDisplacedLepTriggers = HLT_g140_loose_L1eEM26M_passed || HLT_2g50_loose_L12EM20VH_pased || HLT_mu60_0eta105_msonly_L1MU14FCH_passed || HLT_2mu50_msonly_L1MU14FCH_passed || HLT_g40_loose_mu40_msonly_L1MU14FCH_passed;

    // Cutflow 
    m_cutflow(__LINE__,"L1_MU14FCH Fired", L1_MU14FCH_Passed);
    m_cutflow(__LINE__,"L1_EM22 Fired", L1_EM22_Passed);
    m_cutflow(__LINE__,"L1_EM20VH Fired", L1_EM20VH_Passed);
    m_cutflow(__LINE__,"L1_EM22VHI Fired", L1_EM22VHI_Passed);
    m_cutflow(__LINE__,"L1_eEM26M Fired", L1_eEM26M_Passed);
    m_cutflow(__LINE__,"HLT_mu20_msonly Fired", HLT_mu20_msonly_Passed);
    m_cutflow(__LINE__,"HLT_g25_loose_L1EM20VH Fired", HLT_g25_loose_L1EM20VH_Passed);
    m_cutflow(__LINE__,"HLT_g35_loose_L1eEM26M Fired", HLT_g35_loose_L1eEM26M_Passed);

    //m_cutflow(__LINE__,"deriv OR check", m_trigDecTool->getChainGroup("HLT_e26_lhtight_ivarloose_e5_lhvloose_nopix_lrtloose_idperf_probe_L1eEM26M")->isPassed() && m_trigDecTool->getChainGroup("HLT_e26_lhtight_ivarloose_2j20_0eta290_020jvt_boffperf_pf_ftf_L1eEM26M")->isPassed());


    /*
    m_cutflow_met(__LINE__, "L1_XE50 Fired");
    if ( L1_XE50_Passed ) m_cutflow_met(__LINE__, "L1_XE50 Fired")++;
    m_cutflow_met(__LINE__, "L1_J100 Fired");
    if ( L1_J100_Passed ) m_cutflow_met(__LINE__, "L1_J100 Fired")++;
    m_cutflow_met(__LINE__, "HLT_xe100... Fired");
    if ( HLT_xe100_trkmht_xe85_pufit_xe65_L1XE50_Passed ) m_cutflow_met(__LINE__, "HLT_xe100... Fired")++;
    */

   /*
    // Get L1 MET (this ignores the overflow check)
    float l1_mex = l1MetObject->exMiss();
    float l1_mey = l1MetObject->eyMiss();
    l1_met = sqrt(l1_mex*l1_mex + l1_mey*l1_mey) * 0.001;



    //////////////////
    // HLT MET
    //////////////////
    float hlt_met_trkmht_ex = 0;
    float hlt_met_trkmht_ey = 0;

    if (HLT_trkmht != nullptr && HLT_trkmht->size()>0)
    {
        hlt_met_trkmht_ex = HLT_trkmht->front()->ex();
        hlt_met_trkmht_ey = HLT_trkmht->front()->ey();
    }
    hlt_met_trkmht = sqrt(hlt_met_trkmht_ex*hlt_met_trkmht_ex + hlt_met_trkmht_ey*hlt_met_trkmht_ey) * 0.001;

    float hlt_met_pufit_ex = 0;
    float hlt_met_pufit_ey = 0;

    if (HLT_pufit != nullptr && HLT_pufit->size()>0)
    {
        hlt_met_pufit_ex = HLT_pufit->front()->ex();
        hlt_met_pufit_ey = HLT_pufit->front()->ey();
    }
    hlt_met_pufit = sqrt(hlt_met_pufit_ex*hlt_met_pufit_ex + hlt_met_pufit_ey*hlt_met_pufit_ey) * 0.001;

    hist("h_hlt_trkmht")->Fill( hlt_met_trkmht );
    hist("h_hlt_pufit")->Fill( hlt_met_pufit );
*/

    /*
    /////////////////////////
    // Jet Loop
    ///////////////////////// 
    int jet_n = 0;
    jet50_n = 0;
    jet75_n = 0;
    jet100_n = 0;
    if (m_makeNtuple)
    {
        jet_pt->clear();
        jet_eta->clear();
        jet_phi->clear();
    }
    for (const xAOD::Jet *jet: *Jets)
    {
        if (m_makeNtuple)
        {
            jet_pt->push_back(jet->pt() * 0.001);
            jet_eta->push_back(jet->eta());
            jet_phi->push_back(jet->phi());
        }

        if (jet->pt()*0.001 > 50) jet50_n++;
        if (jet->pt()*0.001 > 75) jet75_n++;
        if (jet->pt()*0.001 > 100) jet100_n++;

        if (jet->pt()*0.001 < m_minJetpT) continue;

        jet_n++;
        hist("h_jet_pt")->Fill( jet->pt()*0.001 );
        hist("h_jet_eta")->Fill( jet->eta() );
        
    }
    hist("h_njets")->Fill(jet_n);

    */


    ////////////////////
    // Muons loop
    ///////////////////

    //std::vector<const xAOD::Muon*> offlineMuons;
    bool muon_matches_HLT_mu60_0eta105_msonly_L1MU14FCH = false;
    bool muon_matches_HLT_2mu50_msonly_L1MU14FCH = false;
    int muon_matches_HLT_2mu50_msonly_L1MU14FCH_counter = 0;
    bool muon_pt60_eta105 = false;
    for (const xAOD::Muon *muon: *Muons)
    {

        if (selectMuon(muon))
        {
            if (m_debug)
            {
                std::cout<<"====reco======\n"
                <<"std muon pt / eta / phi: "<<muon->pt() / 1000.<< " / "<<muon->eta() << " / " <<muon->phi()<<std::endl;
                std::cout<<"std muon author / type "<<muon->author()<< " / "<<muon->muonType()<<std::endl;
                if (muon->primaryTrackParticle()) std::cout<< "std muon d0 " << muon->primaryTrackParticle()->d0()<<std::endl;

                std::cout<<"std muon indet track ptr: "<<muon->inDetTrackParticleLink() <<std::endl;
                auto link0 = muon->inDetTrackParticleLink();
                if (link0.isValid())
                {                
                    std::cout<<"std muon indet pt/eta/phi/d0 "<<(*link0)->pt()/1000. <<" / "<<(*link0)->eta() <<" / "<<(*link0)->phi() <<" / "<<(*link0)->d0()<<std::endl;
                }

                std::cout<<"std muon msonly extrap track ptr: "<<muon->msOnlyExtrapolatedMuonSpectrometerTrackParticleLink() <<std::endl;
                auto link1 = muon->msOnlyExtrapolatedMuonSpectrometerTrackParticleLink();
                if (link1.isValid())
                {                
                    std::cout<<"std muon msextrap etaphi "<<(*link1)->eta() <<" / "<<(*link1)->phi() <<std::endl;
                }

                std::cout<<"std muon extrap track ptr: "<<muon->extrapolatedMuonSpectrometerTrackParticleLink() <<std::endl;
                auto link2 = muon->extrapolatedMuonSpectrometerTrackParticleLink();
                if (link2.isValid())
                {                
                    std::cout<<"std muon extrap etaphi "<<(*link2)->eta() <<" / "<<(*link2)->phi() <<std::endl;
                }

                std::cout<<"std ms track ptr: "<<muon->muonSpectrometerTrackParticleLink() <<std::endl;
                auto link3 = muon->muonSpectrometerTrackParticleLink();

                if (link3.isValid())
                {                
                    std::cout<<"std muon ms track etaphi "<<(*link3)->eta() <<" / "<<(*link3)->phi() <<std::endl;
                }
            }


            if (muon->pt()/1000. > 10.)  // jdl was 20
            {
                m_Muons.push_back(muon);
                if (muon->pt()*0.001 > 60 && fabs(muon->eta())<1.05) muon_pt60_eta105 = true;
                /*if(m_isDAOD)
                {
                    if(HLT_mu60_0eta105_msonly_L1MU14FCH_passed && m_trigMatchTool->match(*(xAOD::IParticle*)muon, "HLT_mu60_0eta105_msonly")) muon_matches_HLT_mu60_0eta105_msonly_L1MU14FCH = true;            
                } 
                else if (HLT_mu60_0eta105_msonly_L1MU14FCH_passed && m_trigMatchTool->match(*(xAOD::IParticle*)muon, "HLT_mu60_0eta105_msonly_L1MU14FCH")) muon_matches_HLT_mu60_0eta105_msonly_L1MU14FCH = true;
                */
                if (m_debug)
                {
                    if (HLT_2mu50_msonly_L1MU14FCH_passed && m_trigMatchTool->match(*(xAOD::IParticle *)muon, "HLT_2mu50_msonly_L1MU14FCH"))
                        muon_matches_HLT_2mu50_msonly_L1MU14FCH_counter += 1;
                    std::cout << "std muon trig match" << std::endl;
                    if ((m_debug) && HLT_mu24_ivarmedium_L1MU14FCH_passed && m_trigMatchTool->match(*(xAOD::IParticle *)muon, "HLT_mu24_ivarmedium_L1MU14FCH"))
                        std::cout << "std muon matched HLT_mu24_ivarmedium_L1MU14FCH" << std::endl;
                    if ((m_debug) && HLT_mu20_LRT_d0loose_L1MU14FCH_Passed && m_trigMatchTool->match(*(xAOD::IParticle *)muon, "HLT_mu20_LRT_d0loose_L1MU14FCH"))
                        std::cout << "std muon matched HLT_mu20_LRT_d0loose_L1MU14FCH" << std::endl;
                }
            }
        }

        //if ((muon->author() == 5 || muon->author() == 1) && muon->pt()*0.001 > 24) offlineMuons.push_back(muon);
    }
    if (muon_matches_HLT_2mu50_msonly_L1MU14FCH_counter >= 2) muon_matches_HLT_2mu50_msonly_L1MU14FCH = true;
    hist("h_nmuons")->Fill(m_Muons.size());

    //if (MuonSegments != nullptr) hist("h_nmuonsegments")->Fill(MuonSegments->size());

    ////////////////////
    // LRT Muons loop
    ///////////////////
    int lrtmucounter = 0;
    for (const xAOD::Muon *muon: *MuonsLRT)
    {

        if (selectMuon(muon))
        {
            if (m_debug)
            {
                std::cout<<"====reco======\n"
                <<"lrt muon pt / eta / phi: "<<muon->pt() / 1000.<< " / "<<muon->eta() << " / " <<muon->phi()<<std::endl;
                std::cout<<"lrt muon author / type "<<muon->author()<< " / "<<muon->muonType()<<std::endl;

                std::cout<<"lrt muon track pointer "<<muon->primaryTrackParticle()<<std::endl;
                if (muon->primaryTrackParticle()) std::cout<< "lrt muon d0 " << muon->primaryTrackParticle()->d0()<<std::endl;
                //hist("h_HLT_muon_d0")->Fill(fabs( muon->trackParticle()->d0() ));

                std::cout<<"lrt muon indet track ptr: "<<muon->inDetTrackParticleLink() <<std::endl;
                auto link0 = muon->inDetTrackParticleLink();
                if (link0.isValid())
                {                
                    std::cout<<"lrt muon indet pt/eta/phi/d0 "<<(*link0)->pt()/1000. <<" / "<<(*link0)->eta() <<" / "<<(*link0)->phi() <<" / "<<(*link0)->d0()<<std::endl;
                }

                std::cout<<"lrt muon msonly extrap track ptr: "<<muon->msOnlyExtrapolatedMuonSpectrometerTrackParticleLink() <<std::endl;
                auto link1 = muon->msOnlyExtrapolatedMuonSpectrometerTrackParticleLink();
                if (link1.isValid())
                {                
                    std::cout<<"lrt muon msextrap etaphi "<<(*link1)->eta() <<" / "<<(*link1)->phi() <<std::endl;
                }

                std::cout<<"lrt muon extrap track ptr: "<<muon->extrapolatedMuonSpectrometerTrackParticleLink() <<std::endl;
                auto link2 = muon->extrapolatedMuonSpectrometerTrackParticleLink();
                if (link2.isValid())
                {                
                    std::cout<<"lrt muon extrap etaphi "<<(*link2)->eta() <<" / "<<(*link2)->phi() <<std::endl;
                }

                std::cout<<"lrt ms track ptr: "<<muon->muonSpectrometerTrackParticleLink() <<std::endl;
                auto link3 = muon->muonSpectrometerTrackParticleLink();

                if (link3.isValid())
                {                
                    std::cout<<"lrt muon ms track etaphi "<<(*link3)->eta() <<" / "<<(*link3)->phi() <<std::endl;
                }
            }

            if (muon->pt()/1000. > 10.) // jdl was 20
            {
                m_Muons.push_back(muon);
                lrtmucounter+=1;
                if (m_debug)
                {
                    std::cout<<"lrt muon trig match"<<std::endl;
                    if (HLT_mu24_ivarmedium_L1MU14FCH_passed && m_trigMatchToolLRT->match(*(xAOD::IParticle*)muon, "HLT_mu24_ivarmedium_L1MU14FCH")) std::cout<<"lrt muon matched HLT_mu24_ivarmedium_L1MU14FCH"<<std::endl;
                    if (HLT_mu20_LRT_d0loose_L1MU14FCH_Passed && m_trigMatchToolLRT->match(*(xAOD::IParticle*)muon, "HLT_mu20_LRT_d0loose_L1MU14FCH")) std::cout<<"lrt muon matched HLT_mu20_LRT_d0loose_L1MU14FCH"<<std::endl;
                    //if (HLT_mu24_ivarmedium_L1MU14FCH_passed && m_trigMatchToolLRT->match(*(xAOD::IParticle*)muon, "HLT_mu24_ivarmedium_L1MU14FCH")) std::cout<<"lrt muon test matched HLT_mu24_ivarmedium_L1MU14FCH"<<std::endl;
                    //if (HLT_mu20_LRT_d0loose_L1MU14FCH_Passed && m_trigMatchToolLRT->match(*(xAOD::IParticle*)muon, "HLT_mu20_LRT_d0loose_L1MU14FCH")) std::cout<<"lrt muon test matched HLT_mu20_LRT_d0loose_L1MU14FCH"<<std::endl;
                    if (HLT_mu20_LRT_d0loose_L1MU14FCH_Passed) std::cout<<"HLT_mu20_LRT_d0loose_L1MU14FCH passed with lrt muon >20 GeV with d0 = "<<muon->primaryTrackParticle()->d0()<<std::endl;
                }

            } 
        }
    }
    hist("h_nmuonsLRT")->Fill(lrtmucounter);



    ////////////////////
    // Electrons loop
    ///////////////////
    for (const xAOD::Electron *electron: *Electrons)
    {
        if (selectElectron(electron) && electron->pt()/1000. > 10.) // jdl was 30 
        {
            //int llh_dnn_cat = 0;
            //if (m_ElectronLLHTool->accept(electron)) llh_dnn_cat +=1;
            //if (m_ElectronDNNTool->accept(electron)) llh_dnn_cat +=2;
            //hist("h_dnn_llh_overlap")->Fill(llh_dnn_cat);

            m_Electrons.push_back(electron);
            if (m_debug)
            {
                std::cout<<"std el trig match"<<std::endl;
                if (HLT_e26_lhtight_ivarloose_L1eEM26M_passed && m_trigMatchTool->match(*(xAOD::IParticle*)electron, "HLT_e26_lhtight_ivarloose_L1eEM26M")) std::cout<<"std electron matched HLT_e26_lhtight_ivarloose_L1eEM26M"<<std::endl;
                if (HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed && m_trigMatchTool->match(*(xAOD::IParticle*)electron, "HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M")) std::cout<<"std electron matched HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M"<<std::endl;
            }
            
        }

        if (m_debug && selectElectron(electron))
        {
            std::cout<<"======reco====\n"
            <<"std electron pt: "<<electron->pt() / 1000.<<std::endl;
            std::cout<<"std el n cluster: "<<electron->nCaloClusters() << std::endl;
            std::cout<<"std el cluster ptr: "<<electron->caloCluster() << std::endl;
            if (electron->caloCluster()!=nullptr && !m_isDAOD) std::cout<<"std el cluster eta-phi: "<<electron->caloCluster()->eta0()<<" - "<<electron->caloCluster()->phi0() << std::endl;
        }
    }
    hist("h_nelectrons")->Fill(m_Electrons.size());

    ////////////////////
    // LRT Electrons loop
    ///////////////////
    int lrtelectronscounter = 0;
    for (const xAOD::Electron *electron: *LRTElectrons)
    {
        if (selectElectron(electron) && electron->pt()/1000. > 10.)  // jdl was 30
        {
            //int llh_dnn_cat = 0;
            //if (m_ElectronLLHTool->accept(electron)) llh_dnn_cat +=1;
            //if (m_ElectronDNNTool->accept(electron)) llh_dnn_cat +=2;
            //hist("h_dnn_llh_overlap")->Fill(llh_dnn_cat);

            m_Electrons.push_back(electron);
            lrtelectronscounter +=1;
            if (m_debug)
            {
                std::cout << "lrt el trig match" << std::endl;

                if (HLT_e26_lhtight_ivarloose_L1eEM26M_passed && m_trigMatchToolLRT->match(*(xAOD::IParticle *)electron, "HLT_e26_lhtight_ivarloose_L1eEM26M"))
                    std::cout << "lrt electron matched HLT_e26_lhtight_ivarloose_L1eEM26M" << std::endl;
                if (HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed && m_trigMatchToolLRT->match(*(xAOD::IParticle *)electron, "HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M"))
                    std::cout << "lrt electron matched HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M" << std::endl;
            }
        }

        if (m_debug && selectElectron(electron, true))
        {
            std::cout<<"======reco====\n"
            <<"lrt electron pt: "<<electron->pt() / 1000.<<std::endl;
            std::cout<<"lrt el n cluster: "<<electron->nCaloClusters() << std::endl;
            std::cout<<"lrt el cluster ptr: "<<electron->caloCluster() << std::endl;
            if (electron->caloCluster()!=nullptr && !m_isDAOD) std::cout<<"el cluster eta-phi: "<<electron->caloCluster()->eta0()<<" - "<<electron->caloCluster()->phi0() << std::endl;
        }
    }
    hist("h_nelectronsLRT")->Fill(lrtelectronscounter);


    /*
    ///////////////////
    // Electron FTF Loop //
    ///////////////////

    for (const xAOD::TrackParticle *track : *HLT_FTF_Electron_Tracks)
    {
        if (selectFTFTrack(track))
        {
            hist("h_d0_electronFTF")->Fill(track->d0());
        }
    }
    */


    ///////////////////////
    // HLT std Muons Loop //
    ///////////////////////
    
    for (const xAOD::Muon *muon: *HLT_MuonsCB)
    {
        if (selectMuon(muon, true)) 
        {
            m_HLT_Muons.push_back(muon);

            if (m_debug)
            {
                std::cout<<"====hlt======\n"
                <<"hlt std muon pt / eta / phi: "<<muon->pt() / 1000.<< " / "<<muon->eta() << " / " <<muon->phi()<<std::endl;
                std::cout<<"hlt std muon author / type "<<muon->author()<< " / "<<muon->muonType()<<std::endl;

                std::cout<<"hlt std muon track pointer "<<muon->primaryTrackParticle()<<std::endl;
                if (muon->primaryTrackParticle()) std::cout<< "hlt std muon d0 " << muon->primaryTrackParticle()->d0()<<std::endl;
                //hist("h_HLT_muon_d0")->Fill(fabs( muon->trackParticle()->d0() ));

                std::cout<<"hlt std muon indet track ptr: "<<muon->inDetTrackParticleLink() <<std::endl;
                auto link0 = muon->inDetTrackParticleLink();
                if (link0.isValid())
                {                
                    std::cout<<"hlt std muon indet pt/eta/phi/d0 "<<(*link0)->pt()/1000. <<" / "<<(*link0)->eta() <<" / "<<(*link0)->phi() <<" / "<<(*link0)->d0()<<std::endl;
                }

                std::cout<<"hlt std muon msonly extrap track ptr: "<<muon->msOnlyExtrapolatedMuonSpectrometerTrackParticleLink() <<std::endl;
                auto link1 = muon->msOnlyExtrapolatedMuonSpectrometerTrackParticleLink();
                if (link1.isValid())
                {                
                    std::cout<<"hlt std muon msextrap etaphi "<<(*link1)->eta() <<" / "<<(*link1)->phi() <<std::endl;
                }

                std::cout<<"hlt std muon extrap track ptr: "<<muon->extrapolatedMuonSpectrometerTrackParticleLink() <<std::endl;
                auto link2 = muon->extrapolatedMuonSpectrometerTrackParticleLink();
                if (link2.isValid())
                {                
                    std::cout<<"hlt std muon extrap etaphi "<<(*link2)->eta() <<" / "<<(*link2)->phi() <<std::endl;
                }

                std::cout<<"hlt std ms track ptr: "<<muon->muonSpectrometerTrackParticleLink() <<std::endl;
                auto link3 = muon->muonSpectrometerTrackParticleLink();

                if (link3.isValid())
                {                
                    std::cout<<"hlt std muon ms track etaphi "<<(*link3)->eta() <<" / "<<(*link3)->phi() <<std::endl;
                }
            }
        }
    }

    ///////////////////////
    // HLT LRT Muons Loop //
    ///////////////////////
    
    for (const xAOD::Muon *muon: *HLT_MuonsCBLRT)
    {

        if (selectMuon(muon, true)) 
        {
            m_HLT_Muons.push_back(muon);

            if (m_debug)
            {
                std::cout<<"====hlt======\n"
                <<"hlt lrt muon pt / eta / phi: "<<muon->pt() / 1000.<< " / "<<muon->eta() << " / " <<muon->phi()<<std::endl;
                std::cout<<"hlt lrt muon author / type "<<muon->author()<< " / "<<muon->muonType()<<std::endl;

                std::cout<<"hlt lrt muon track pointer "<<muon->primaryTrackParticle()<<std::endl;
                if (muon->primaryTrackParticle()) std::cout<< "hlt lrt muon d0 " << muon->primaryTrackParticle()->d0()<<std::endl;
                //hist("h_HLT_muon_d0")->Fill(fabs( muon->trackParticle()->d0() ));

                std::cout<<"hlt lrt muon indet track ptr: "<<muon->inDetTrackParticleLink() <<std::endl;
                auto link0 = muon->inDetTrackParticleLink();
                if (link0.isValid())
                {                
                    std::cout<<"hlt lrt muon indet pt/eta/phi/d0 "<<(*link0)->pt()/1000. <<" / "<<(*link0)->eta() <<" / "<<(*link0)->phi() <<" / "<<(*link0)->d0()<<std::endl;
                }

                std::cout<<"hlt lrt muon msonly extrap track ptr: "<<muon->msOnlyExtrapolatedMuonSpectrometerTrackParticleLink() <<std::endl;
                auto link1 = muon->msOnlyExtrapolatedMuonSpectrometerTrackParticleLink();
                if (link1.isValid())
                {                
                    std::cout<<"hlt lrt muon msextrap etaphi "<<(*link1)->eta() <<" / "<<(*link1)->phi() <<std::endl;
                }

                std::cout<<"hlt lrt muon extrap track ptr: "<<muon->extrapolatedMuonSpectrometerTrackParticleLink() <<std::endl;
                auto link2 = muon->extrapolatedMuonSpectrometerTrackParticleLink();
                if (link2.isValid())
                {                
                    std::cout<<"hlt lrt muon extrap etaphi "<<(*link2)->eta() <<" / "<<(*link2)->phi() <<std::endl;
                }

                std::cout<<"hlt lrt ms track ptr: "<<muon->muonSpectrometerTrackParticleLink() <<std::endl;
                auto link3 = muon->muonSpectrometerTrackParticleLink();

                if (link3.isValid())
                {                
                    std::cout<<"hlt lrt muon ms track etaphi "<<(*link3)->eta() <<" / "<<(*link3)->phi() <<std::endl;
                }
            }
        }
    }


    ///////////////////////
    // HLT Electron Loop //
    ///////////////////////
    
    // temp remove standard hlt_electron
    /*for (const xAOD::Electron *electron: *HLT_Electrons)
    {
        if (selectElectron(electron, true)) m_HLT_Electrons.push_back(electron);

        if (m_debug)
        {
            std::cout<<"=====hlt=====\n"
            <<"std hlt electron pt: "<<electron->pt() / 1000.<<std::endl;
            
            std::cout<<"std electron track pointer "<<electron->trackParticle()<<std::endl;
            if (electron->trackParticle()) std::cout<< electron->trackParticle()->d0()<<std::endl;
            //hist("h_HLT_electron_d0")->Fill(fabs( electron->trackParticle()->d0() ));
            std::cout<<"std electron aux d0 "<<electron->auxdata<float>("trk_d0")<<std::endl;
            ////std::cout<<"std electron aux cleta "<<electron->auxdata<float>("cl_eta2")<<std::endl;
            ////std::cout<<"std electron aux clphi "<<electron->auxdata<float>("cl_phi2")<<std::endl;
            
            std::cout<<"std el n cluster: "<<electron->nCaloClusters() << std::endl;
            std::cout<<"std el cluster ptr: "<<electron->caloCluster() << std::endl;


            if (electron->caloCluster()!=nullptr && !m_isDAOD) std::cout<<"std el cluster eta-phi: "<<electron->caloCluster()->eta0()<<" - "<<electron->caloCluster()->phi0() << std::endl;
        }

    }*/
    if (HLT_Electrons !=nullptr) hist("h_nHLTelectrons")->Fill(HLT_Electrons->size());
    

    ///////////////////////////
    // HLT LRT Electron Loop //
    ///////////////////////////
    
    for (const xAOD::Electron *electron: *HLT_Electrons_LRT)
    {
        if (selectElectron(electron, true)) m_HLT_Electrons.push_back(electron);

        if (m_debug)
        {
            std::cout<<"\n\n=====hlt=====\n"
            <<"lrt hlt electron pt: "<<electron->pt() / 1000.<<std::endl;
            std::cout<<"lrt electron track pointer "<<electron->trackParticle()<<std::endl;
            if (electron->trackParticle()) std::cout<< electron->trackParticle()->d0()<<std::endl;
            //hist("h_HLT_electron_LRT_d0")->Fill(fabs( electron->trackParticle()->d0() ));
            std::cout<<"lrt electron aux d0 "<<electron->auxdata<float>("trk_d0")<<std::endl;
            ////std::cout<<"lrt electron aux cleta "<<electron->auxdata<float>("cl_eta2")<<std::endl;
            ////std::cout<<"lrt electron aux clphi "<<electron->auxdata<float>("cl_phi2")<<std::endl;
            std::cout<<"lrt el n cluster: "<<electron->nCaloClusters() << std::endl;
            std::cout<<"lrt el cluster ptr: "<<electron->caloCluster() << std::endl;
            if (electron->caloCluster()!=nullptr && !m_isDAOD) std::cout<<"lrt el cluster eta-phi: "<<electron->caloCluster()->eta0()<<" - "<<electron->caloCluster()->phi0() << std::endl;
        }
    }
    if (HLT_Electrons_LRT !=nullptr) hist("h_nHLTelectrons_LRT")->Fill(HLT_Electrons_LRT->size());

    ///
    // JDL HACK!!!!!
    //HLT_FTF_Tracks = HLT_FTF_Electron_Tracks;





    ////////////////////////////////////////////////////
    // Now select offline, FTF, and truth tracks
    // Then perform matching between collections
    // Muon normal and LRT FTF tracks

    if (m_flag_HLT_FTF_Muon_Tracks) 
    {
        for (const xAOD::TrackParticle *track : *HLT_FTF_Muon_Tracks)
        {
            m_muonFTF.push_back(track);
            if (selectFTFTrack(track)) m_FTFTracksComb.push_back(track);
        }
    }

    if (m_flag_HLT_FTFLRT_Muon_Tracks)
    {  
        for (const xAOD::TrackParticle *track : *HLT_FTFLRT_Muon_Tracks)
        {        
            m_muonFTFLRT.push_back(track);
            if (selectFTFTrack(track)) m_FTFTracksComb.push_back(track);
        }
    }

    if(m_flag_HLT_PTLRT_Muon_Tracks)
    {
        for (const xAOD::TrackParticle *track : *HLT_PTLRT_Muon_Tracks)
        {
            m_muonPTLRT.push_back(track);
        }
    }

    if (m_flag_HLT_PT_Muon_Tracks)
    {
        for (const xAOD::TrackParticle *track : *HLT_PT_Muon_Tracks)
        {
            m_muonPT.push_back(track);
        }
    }

    // Electron nominal and LRT FTF Tracks
    if (m_flag_HLT_FTF_Electron_Tracks)
    {
        for (const xAOD::TrackParticle *track : *HLT_FTF_Electron_Tracks)
        {
            m_electronFTF.push_back(track);
            if (selectFTFTrack(track)) m_FTFTracksComb.push_back(track);
        }
    }

    if (m_flag_HLT_FTFLRT_Electron_Tracks)
    {
        for (const xAOD::TrackParticle *track : *HLT_FTFLRT_Electron_Tracks)
        {
            m_electronFTFLRT.push_back(track);
            if (selectFTFTrack(track)) m_FTFTracksComb.push_back(track);
        }
    }

    // Electron PT Tracks
    if (m_flag_HLT_PTLRT_Electron_Tracks)
    {
        for (const xAOD::TrackParticle *track : *HLT_PTLRT_Electron_Tracks)
        {
            m_electronPTLRT.push_back(track);
        }
    }
    hist("h_nTracks_ELPT")->Fill(m_electronPTLRT.size());

    if (m_flag_HLT_PT_Electron_Tracks)
    {
        for (const xAOD::TrackParticle *track : *HLT_PT_Electron_Tracks)
        {
            m_electronPT.push_back(track);
        }
    }

    ////////////////////////////
    // Select FS FTF Tracks
    //for (const xAOD::TrackParticle *track : *HLT_FTF_Tracks)
    //{
    //    // Apply quality cuts to FTF tracks
    //    if (selectFTFTrack(track)) m_FTFTracksComb.push_back(track);
    //}
    //if (m_includeLRT)
    //{
    //    for (const xAOD::TrackParticle *track : *HLT_lrtFTF_Tracks)
    //    {
    //        // Apply quality cuts to LRT FTF tracks
    //        if(selectlrtFTFTrack(track))
    //        {
    //            m_FTFTracksComb.push_back(track);
    //            m_FTFLRTTracks.insert(track);
    //        }
    //    }
    //}


    // Offline
    std::map<const xAOD::TrackParticle*, const xAOD::TruthParticle*> offlineTrackIsSignal;
    for (const xAOD::TrackParticle *track : *inDetTracksOffline)
    {
        if (selectOfflineTrack(track)) 
        {
            m_OfflineTracks.push_back(track); 
            const xAOD::TruthParticle* tempPart = nullptr;
            if (m_isSimulation && selectOfflineTrackSignal(track, ParticleOrigin::SUSY, tempPart)) // select only signal offline tracks
            {
                offlineTrackIsSignal.emplace(track, tempPart);
                m_OfflineTracksSignal.push_back(track);
            }
        }    
    }

    // Offline LRT (If offline LRT tracks are in a different collection)
    if (inDetTracksLRTOffline)
    {
        for (const xAOD::TrackParticle *track : *inDetTracksLRTOffline)
        {               
            if (selectOfflineTrack(track)) 
            {
                m_OfflineTracks.push_back(track); 
                const xAOD::TruthParticle *tempPart = nullptr;
                if (m_isSimulation && selectOfflineTrackSignal(track, ParticleOrigin::SUSY, tempPart)) // select only signal offline tracks
                {
                    offlineTrackIsSignal.emplace(track, tempPart);
                    m_OfflineTracksSignal.push_back(track);
                }
            }            
        }
    }

    ////////////////////////////////
    // Perform matching between track collections
    // Maps of matched offline tracks to FTF tracks
    if (m_useOnlyOfflineSignal) m_trackOffline_to_FTF_map = matchCollections_dR(m_OfflineTracksSignal, m_FTFTracksComb, m_matchSizeR);
    else m_trackOffline_to_FTF_map = matchCollections_dR(m_OfflineTracks, m_FTFTracksComb, m_matchSizeR);
    m_trackOfflineSignal_to_FTF_map = matchCollections_dR(m_OfflineTracksSignal, m_FTFTracksComb, m_matchSizeR);

    // Select Truth particles
    if (m_isSimulation && m_doTruth)
    {
        int status = 1; // 1 is a stable particle
        for (const xAOD::TruthParticle *part : *truthParticles)
        {  
            if(selectTruthTrack(part) && testTruthParticle(part, m_signalPDGIDs, m_signalParentPDGIDs, m_excludeParentPDGIDs, status) ) m_TruthParticlesSignal.push_back(part); 
        }

        // Match truth to offline and FTF tracks
        m_trackTruth_to_FTF_map = matchCollections_dR(m_TruthParticlesSignal, m_FTFTracksComb, m_matchSizeR);
        m_trackTruth_to_Offline_map = matchCollections_dR(m_TruthParticlesSignal, m_OfflineTracks, m_matchSizeR);

    }

    // map online electrons to offline electrons
    m_hlt_to_off_electrons_map = matchCollections_dR(m_HLT_Electrons, m_Electrons, m_matchSizeR);

    /////////////////////////
    // Invert maps for purity and other lookup uses
    m_trackFTF_to_Offline_map = invertMap(m_trackOffline_to_FTF_map);
    m_trackFTF_to_OfflineSignal_map = invertMap(m_trackOfflineSignal_to_FTF_map);
    m_trackFTF_to_Truth_map = invertMap(m_trackTruth_to_FTF_map);
    m_trackOffline_to_Truth_map = invertMap(m_trackTruth_to_Offline_map);
    
    if (m_flag_HLT_FTFLRT_Muon_Tracks) 
    {
        m_FSFTF_to_MUFTF_map = matchCollections_dR(m_FTFTracksComb, m_muonFTFLRT, m_matchSizeR);
        m_offlinesig_to_MUFTFlrt_map = matchCollections_dR(m_OfflineTracksSignal, m_muonFTFLRT, m_matchSizeR);
    }
        
    if (m_flag_HLT_FTFLRT_Electron_Tracks) 
    {
        m_FSFTF_to_ELFTF_map = matchCollections_dR(m_FTFTracksComb, m_electronFTFLRT, m_matchSizeR);
        m_offlinesig_to_ELFTFlrt_map = matchCollections_dR(m_OfflineTracksSignal, m_electronFTFLRT, m_matchSizeR);
    }

    if (m_flag_HLT_PTLRT_Electron_Tracks) 
    {
        m_trackTruth_to_ELPTLRT_map = matchCollections_dR(m_TruthParticlesSignal, m_electronPTLRT, m_matchSizeR);
        m_ELPTLRT_to_trackTruth_map = invertMap(m_trackTruth_to_ELPTLRT_map);
        m_offlinesig_to_ELPTLRT_map = matchCollections_dR(m_OfflineTracksSignal, m_electronPTLRT, m_matchSizeR);
    }
    if (m_flag_HLT_PTLRT_Muon_Tracks) 
    {
        m_trackTruth_to_MUPTLRT_map = matchCollections_dR(m_TruthParticlesSignal, m_muonPTLRT, m_matchSizeR);
        m_MUPTLRT_to_trackTruth_map = invertMap(m_trackTruth_to_MUPTLRT_map);
        m_offlinesig_to_MUPTLRT_map = matchCollections_dR(m_OfflineTracksSignal, m_muonPTLRT, m_matchSizeR);
    }

    if (m_flag_HLT_FTF_Muon_Tracks) m_offlinesig_to_MUFTF_map = matchCollections_dR(m_OfflineTracksSignal, m_muonFTF, m_matchSizeR);
    if (m_flag_HLT_FTF_Electron_Tracks) m_offlinesig_to_ELFTF_map = matchCollections_dR(m_OfflineTracksSignal, m_electronFTF, m_matchSizeR);

    if (m_flag_HLT_PT_Muon_Tracks) 
    {
        m_trackTruth_to_MUPT_map = matchCollections_dR(m_TruthParticlesSignal, m_muonPT, m_matchSizeR);
        m_offlinesig_to_MUPT_map = matchCollections_dR(m_OfflineTracksSignal, m_muonPT, m_matchSizeR);
    }
    if (m_flag_HLT_PT_Electron_Tracks) 
    {
        m_trackTruth_to_ELPT_map = matchCollections_dR(m_TruthParticlesSignal, m_electronPT, m_matchSizeR);
        m_offlinesig_to_ELPT_map = matchCollections_dR(m_OfflineTracksSignal, m_electronPT, m_matchSizeR);
    }


    /*
    Look if offline signal track is the same as the truth track
    is the truth selection for truth and offline the same?
    */

    // Ok now we have all of the match maps, let's categorize each event by how the collections matched
    if (m_debug)
    {
        std::set<const xAOD::TrackParticle*> ftf_with_matches;
        for (auto &t : m_trackFTF_to_OfflineSignal_map) ftf_with_matches.emplace(t.first);
        for (auto &t : m_trackFTF_to_Truth_map) ftf_with_matches.emplace(t.first);

        for (auto &ftf : ftf_with_matches) // ftf matched to offline signal
        {
            bool hasOff = m_trackFTF_to_Offline_map.find(ftf)!=m_trackFTF_to_Offline_map.end();
            bool hasTruth = m_trackFTF_to_Truth_map.find(ftf) != m_trackFTF_to_Truth_map.end();

            bool offHasTruth = false;
            bool truthsMatch = false;
            bool ftfTruthMatchsoff = false;

            const xAOD::TrackParticle* off = NULL;
            if (hasOff) off = m_trackFTF_to_Offline_map[ftf];

            const xAOD::TruthParticle* ftftruth = NULL;
            if (hasTruth) ftftruth = m_trackFTF_to_Truth_map[ftf];

            if (hasOff) offHasTruth = m_trackOffline_to_Truth_map.find(off) != m_trackOffline_to_Truth_map.end();
            if (hasOff && offHasTruth && hasTruth) truthsMatch = m_trackFTF_to_Truth_map[ftf] == m_trackOffline_to_Truth_map[off];
            if (!hasOff && hasTruth &&  m_trackTruth_to_Offline_map.find(ftftruth) != m_trackTruth_to_Offline_map.end()) ftfTruthMatchsoff = true; 


            matchType mt=matchType::err;
            if (hasTruth && hasOff && truthsMatch) mt = matchType::fullMatch;
            else if (hasTruth && hasOff && !truthsMatch) mt = matchType::fo_NoTmatch;
            else if (!hasTruth && hasOff) mt = matchType::fo_noFT;
            else if (hasTruth && hasOff && !offHasTruth) mt = matchType::fo_noOT;
            else if (!hasTruth && hasOff && !offHasTruth) mt = matchType::fo_noOTnoFT;
            else if (hasTruth && !hasOff && ftfTruthMatchsoff) mt = matchType::noFO_Tmatch;
            else if (hasTruth && !hasOff && !ftfTruthMatchsoff) mt = matchType::noFO_noTmatch;


            hist("h_ftf_offline_truth_matching")->Fill(mt);
        }
    }


    // Debugging matching and truth selection
    if (m_debug)
    //if (m_debug && (m_trackTruth_to_FTF_map.size() == m_trackTruth_to_Offline_map.size() && m_trackOffline_to_FTF_map.size() < m_trackTruth_to_Offline_map.size()) )
    {
        std::cout<<"N Truth Signal Tracks "<<m_TruthParticlesSignal.size() << ", N Offline Signal "<< m_OfflineTracksSignal.size()<< ", N FTF "<<m_FTFTracksComb.size()<< ", N EL PT "<<m_electronPTLRT.size()<<std::endl;
        std::cout<<"m_trackOffline_to_FTF_map size "<<m_trackOffline_to_FTF_map.size()<<std::endl;
        std::cout<<"m_trackOfflineSignal_to_FTF_map size "<<m_trackOfflineSignal_to_FTF_map.size()<<std::endl;
        std::cout<<"m_trackTruth_to_FTF_map size "<<m_trackTruth_to_FTF_map.size()<<std::endl;
        std::cout<<"m_trackTruth_to_Offline_map size "<<m_trackTruth_to_Offline_map.size()<<std::endl;        

        std::cout<<"*********** Truth particles ********"<< std::endl;
        for (auto &p : m_TruthParticlesSignal)
        {
            std::cout<<"pt="<<p->pt()*0.001<<", eta="<<getTruthEta(p)<<", phi="<<getTruthPhi(p)<<", d0="<<getTruthPartD0_extrap(p) <<", prodR="<<getProdR( p ) <<", pdgid="<<p->pdgId() <<", parent pdgid="<<p->parent()->pdgId()<< ", status="<<p->status()<< ", barcode="<<p->barcode()<<std::endl;
        }
        std::cout<<"*********** Offline signal particles ********"<< std::endl;
        for (auto &p : m_OfflineTracksSignal)
        {
            std::cout<<"pt="<<p->pt()*0.001<<", eta="<<p->eta()<<", phi="<<p->phi()<<", d0="<<p->d0()<<std::endl;

            const xAOD::TruthParticle *thePart = m_truthClassifier->getGenPart(p);

            std::cout<<"pt="<<thePart->pt()*0.001<<", eta="<<getTruthEta(thePart)<<", phi="<<getTruthPhi(thePart)<<", d0="<< getTruthPartD0_extrap(thePart)<<" (TRUTH)"<<std::endl;
            std::cout<<"--"<<std::endl;            
        }


        std::cout<<"*********** FTF match Offline Signal ********"<< std::endl;
        for (auto &m : m_trackOfflineSignal_to_FTF_map)
        {
            auto p = m.second;
            std::cout<<"pt="<<p->pt()*0.001<<", eta="<<p->eta()<<", phi="<<p->phi()<<", d0="<<p->d0()<<std::endl;
        }
        std::cout<<"*********** FTF match truth ********"<< std::endl;
        for (auto &m : m_trackTruth_to_FTF_map)
        {
            auto p = m.second;
            std::cout<<"pt="<<p->pt()*0.001<<", eta="<<p->eta()<<", phi="<<p->phi()<<", d0="<<p->d0()<<std::endl;
        }

        std::cout<<"*********** FTF ********"<< std::endl;
        for (auto &p : m_FTFTracksComb)
        {
            std::cout<<"pt="<<p->pt()*0.001<<", eta="<<p->eta()<<", phi="<<p->phi()<<", d0="<<p->d0()<<std::endl;
        }

        std::cout<<"*********** EL PT ********"<< std::endl;
        for (auto &p : m_electronPTLRT)
        {
            std::cout<<"pt="<<p->pt()*0.001<<", eta="<<p->eta()<<", phi="<<p->phi()<<", d0="<<p->d0()<<std::endl;
        }

        
        if (ei->eventNumber() == 415 && false)
        {
            std::vector<std::string> triggers = m_trigDecTool->getListOfTriggers();
            for (std::string trigger : triggers)
            {
                auto cg =  m_trigDecTool->getChainGroup(trigger);

                if ( !(trigger.find("HLT_e") || trigger.find("HLT_mu"))) continue;

                // special case for L1All (fsnoseed) chain
                unsigned decisiontype;
                if (trigger.find("L1All") != std::string::npos) decisiontype = TrigDefs::requireDecision; // HLT only
                else                                           decisiontype = TrigDefs::Physics; // default HLT+L1

                std::cout<<trigger << " passed = "<<cg->isPassed(decisiontype) <<std::endl;
            }  
        }
        
    }

    ///////////////////////////////////////////////////////////////////
    // We have all the inputs, now lets loop through the selected collections and fill histograms and cutflows
    //////////////////////////////////////


    //////////////////////
    // FTF Loop
    //////////////////////
    bool passedHighD0FTF = false;
    bool passedHighD0FTFTrackInROI_EM = false;
    bool passedHighD0FTFTrackInROI_MU = false;

    bool HighD0FTFMatchMuon_MU = false;

    std::map<const TrigRoiDescriptor*, int> em_roi_ftf_counter;
    std::map<const TrigRoiDescriptor*, int> mu_roi_ftf_counter;

    for (const xAOD::TrackParticle *track : m_FTFTracksComb)
    {
        float track_d0_corr = track->d0(); //getCorrectedD0(track);
        float d0sig = fabs(xAOD::TrackingHelpers::d0significance(track, ei->beamPosSigmaX(), ei->beamPosSigmaY(), ei->beamPosSigmaXY())); //fabs(getCorrectedD0Sig(track));
        if ( fabs( track_d0_corr ) > m_Highd0Cut) passedHighD0FTF = true;

        int nPixHits = track->auxdata<unsigned char>("numberOfPixelHits");
        int nSCTHits = track->auxdata<unsigned char>("numberOfSCTHits");
        int nDeadPix = track->auxdata<unsigned char>("numberOfPixelDeadSensors");
        int nDeadSCT = track->auxdata<unsigned char>("numberOfSCTDeadSensors");
        int nSCTHoles = track->auxdata<unsigned char>("numberOfSCTHoles");
        int nPixHoles = track->auxdata<unsigned char>("numberOfPixelHoles");

        // LRT track!
        if (m_FTFLRTTracks.find(track) !=m_FTFLRTTracks.end())
        {
            hist ("h_d0_lrtFTF")->Fill (track_d0_corr); 
            hist("h_nhits_lrtFTF")->Fill(nPixHits+nSCTHits+nDeadPix+nDeadSCT);
            hist("h2_lrtftf_d0_v_hitsSI")->Fill(fabs(track_d0_corr), nPixHits+nSCTHits+nDeadPix+nDeadSCT );
            hist("h2_lrtftf_d0_v_holesSI")->Fill(fabs(track_d0_corr), nSCTHoles+nPixHoles);
            hist("h2_lrtftf_d0_v_pt")->Fill(fabs(track_d0_corr), track->pt() / 1000.);
            hist("hp_lrtftf_hits_v_d0")->Fill(fabs(track_d0_corr), nPixHits+nSCTHits+nDeadPix+nDeadSCT );
            hist("hp_lrtftf_pixhits_v_d0")->Fill(fabs(track_d0_corr), nPixHits+nDeadPix );
            hist("hp_lrtftf_scthits_v_d0")->Fill(fabs(track_d0_corr), nSCTHits+nDeadSCT );
        } 
        else
        {
            hist ("h_d0_FTF")->Fill (track_d0_corr);
            hist("h_nhits_FTF")->Fill(nPixHits+nSCTHits+nDeadPix+nDeadSCT);
            hist("h2_ftf_d0_v_hitsSI")->Fill(fabs(track_d0_corr), nPixHits+nSCTHits+nDeadPix+nDeadSCT);
            hist("h2_ftf_d0_v_holesSI")->Fill(fabs(track_d0_corr), nSCTHoles+nPixHoles);
        }
        hist("h_eta_FTFcomb")->Fill(track->eta());
        hist("h_pt_FTFcomb")->Fill(track->pt()*0.001);
        hist("h_d0sig_FTFcomb")->Fill( d0sig );

        // Loop over ROIs and check if tracks fall within
        double track_eta = track->eta();
        double track_phi = track->phi();

        for (auto em : m_roid_em)
        {                
            em_roi_ftf_counter.try_emplace( em, 0 ); // fill out with 0s, added to layer

            if (getDeltaR(track_eta, track_phi, em) < m_ROISize)
            //if (matchDeltaR(track, em, m_ROISize, m_EMPhiAdjust))
            {
                hist("h2_FTF_inROI_d0_v_pt_em")->Fill( fabs(track_d0_corr), track->pt()*0.001);
                hist("h2_FTF_inROI_d0sig_v_pt_em")->Fill(d0sig, track->pt()*0.001); 
                if( fabs(track_d0_corr) > m_Highd0Cut ) passedHighD0FTFTrackInROI_EM=true;

                em_roi_ftf_counter[em]++;

                // FTF is truth matched
                if (m_trackFTF_to_Truth_map.find(track) != m_trackFTF_to_Truth_map.end())
                {
                    hist("h_d0_ELFTF_r_FSFTF_d")->Fill( fabs(track->d0()) );
                    hist("h_pt_ELFTF_r_FSFTF_d")->Fill( track->pt()*0.001 );
                    hist("h_dEta_ELFTF_r_FSFTF_d")->Fill( fabs(getDeltaEta(track, em)) );
                    hist("h_dPhi_ELFTF_r_FSFTF_d")->Fill( fabs(getDeltaPhi(track, em)) );

                    // Electron RoI FTF was matched to this track
                    if (m_FSFTF_to_ELFTF_map.find(track) != m_FSFTF_to_ELFTF_map.end())
                    {
                        hist("h_d0_ELFTF_r_FSFTF_n")->Fill( fabs(track->d0()) );
                        hist("h_pt_ELFTF_r_FSFTF_n")->Fill( track->pt()*0.001 );
                        hist("h_dEta_ELFTF_r_FSFTF_n")->Fill( fabs(getDeltaEta(track, em)) );
                        hist("h_dPhi_ELFTF_r_FSFTF_n")->Fill( fabs(getDeltaPhi(track, em)) );
                    }
                }

                
            }
        }


        for (auto mu : m_roid_mu)
        {
            mu_roi_ftf_counter.try_emplace( mu, 0 ); // fill out with 0s, added to layer

            if (getDeltaR(track_eta, track_phi, mu) < m_ROISize)
            {
                hist("h2_FTF_inROI_d0_v_pt_mu")->Fill( fabs(track_d0_corr), track->pt()*0.001);
                hist("h2_FTF_inROI_d0sig_v_pt_mu")->Fill(d0sig, track->pt()*0.001); 
                if( fabs(track_d0_corr) > m_Highd0Cut ) 
                {
                    passedHighD0FTFTrackInROI_MU=true;
                    for (const xAOD::Muon *muon : m_Muons)
                    {
                        float dr = getDeltaR(track_eta, track_phi, muon);
                        hist("h_deltaR_muon_FTF")->Fill(dr);
                        if ( dr < m_matchSizeR) HighD0FTFMatchMuon_MU=true;
                    }
                }

                mu_roi_ftf_counter[mu]++;

                // FTF is truth matched
                if (m_trackFTF_to_Truth_map.find(track) != m_trackFTF_to_Truth_map.end())
                {
                    hist("h_d0_MUFTF_r_FSFTF_d")->Fill( fabs(track->d0()) );
                    hist("h_pt_MUFTF_r_FSFTF_d")->Fill( track->pt()*0.001 );
                    hist("h_dEta_MUFTF_r_FSFTF_d")->Fill( fabs(getDeltaEta(track, mu)) );
                    hist("h_dPhi_MUFTF_r_FSFTF_d")->Fill( fabs(getDeltaPhi(track, mu)) );

                    // Muon RoI FTF was matched to this track
                    if (m_FSFTF_to_MUFTF_map.find(track) != m_FSFTF_to_MUFTF_map.end())
                    {
                        hist("h_d0_MUFTF_r_FSFTF_n")->Fill( fabs(track->d0()) );
                        hist("h_pt_MUFTF_r_FSFTF_n")->Fill( track->pt()*0.001 );
                        hist("h_dEta_MUFTF_r_FSFTF_n")->Fill( fabs(getDeltaEta(track, mu)) );
                        hist("h_dPhi_MUFTF_r_FSFTF_n")->Fill( fabs(getDeltaPhi(track, mu)) );
                    }
                }
                
            }
        }
    } // End of FTF Loop

    hist("h_nTracks_FTFLRT")->Fill(m_FTFLRTTracks.size());
    hist("h_nTracks_FTFcomb")->Fill(m_FTFTracksComb.size());
    //for (auto em : em_roi_ftf_counter) hist("h_nFTFcombTrack_in_ROI_em")->Fill(em.second);
    //for (auto mu : mu_roi_ftf_counter) hist("h_nFTFcombTrack_in_ROI_mu")->Fill(mu.second);



    ///////////////////////
    // Offline track loop
    ///////////////////////

    int n_offlineLRT_Tracks = 0;

    bool passedSUSYTrackEL = false;
    bool passedSUSYTrackMU = false;
    bool passedSUSYTrackOther = false;
    bool passedHighD0SUSYTrackOther = false;
    bool passedHighD0SUSYTrackEL = false;
    bool passedHighD0SUSYTrackMU = false;

    bool hasTrackInROI_em = false;
    bool hasTrackInROI_mu = false;
    bool hasMatchedFTF_em = false;
    bool hasMatchedFTF_mu = false;

    bool hasMatchedMuon_mu = false;

    // Containers to save electrons and muons
    //std::vector<const xAOD::TrackParticle*> offlineElectrons;


    for (const xAOD::TrackParticle *track : m_OfflineTracks)
    {
        float track_d0_corr = track->d0(); //getCorrectedD0(track);
        float track_z0 = track->z0();
        float d0sig = fabs(xAOD::TrackingHelpers::d0significance(track, ei->beamPosSigmaX(), ei->beamPosSigmaY(), ei->beamPosSigmaXY())); //fabs(getCorrectedD0Sig(track));

        double bestDeltaR = 999;
        const xAOD::TrackParticle *bestMatch = nullptr;
        
        if (m_trackOffline_to_FTF_map.find(track) != m_trackOffline_to_FTF_map.end())
        {
            bestMatch = m_trackOffline_to_FTF_map[track];
            bestDeltaR = getDeltaR(track, bestMatch);
            hist("h_deltaR_offline_FTFcomb")->Fill(bestDeltaR);    
        }


        // Fill histograms
        int nPixHits = track->auxdata<unsigned char>("numberOfPixelHits");
        int nSCTHits = track->auxdata<unsigned char>("numberOfSCTHits");
        int nDeadPix = track->auxdata<unsigned char>("numberOfPixelDeadSensors");
        int nDeadSCT = track->auxdata<unsigned char>("numberOfSCTDeadSensors");
        int nSCTHoles = track->auxdata<unsigned char>("numberOfSCTHoles");
        int nPixHoles = track->auxdata<unsigned char>("numberOfPixelHoles");


        hist("h_d0_offline")->Fill (track_d0_corr);
        hist("h_pt_offline")->Fill(track->pt() / 1000.);
        hist("h_eta_offline")->Fill(track->eta());
        hist("h_nhits_offline")->Fill(nPixHits+nSCTHits);
        hist("h_d0sig_offline")->Fill(d0sig);

        hist("h2_offline_d0_v_hitsSI")->Fill(fabs(track_d0_corr), nPixHits+nSCTHits+nDeadPix+nDeadSCT);
        hist("h2_offline_d0_v_hitsPix")->Fill(fabs(track_d0_corr), nPixHits+nDeadPix);
        hist("h2_offline_d0_v_hitsSCT")->Fill(fabs(track_d0_corr), nSCTHits+nDeadSCT);
        hist("h2_offline_d0_v_holesSI")->Fill(fabs(track_d0_corr), nSCTHoles+nPixHoles);


        //hist("h2_offline_prodVtx_x_v_y")->Fill(track->vx(), track->vy());

        //m_OfflineTracks.push_back(track); 

        // Select offline LRT Tracks
        if ( !m_isDAOD && track->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) )
        {
            n_offlineLRT_Tracks++;

            hist("hp_offlineLRT_hits_v_d0")->Fill(fabs(track_d0_corr), nPixHits+nSCTHits+nDeadPix+nDeadSCT );
            hist("hp_offlineLRT_pixhits_v_d0")->Fill(fabs(track_d0_corr), nPixHits+nDeadPix );
            hist("hp_offlineLRT_scthits_v_d0")->Fill(fabs(track_d0_corr), nSCTHits+nDeadSCT );
        } 


        /////////////////////
        // Select offline tracks identified as coming from signal
        //////
        if ( m_isSimulation && offlineTrackIsSignal[track] != nullptr )
        {                
            const xAOD::TruthParticle *truthPart = m_truthClassifier->getGenPart(track);

            // Get track type for matching to L1 ROIs
            int pdgid = truthPart->pdgId();

            if (abs(pdgid) == 11) 
            {
                passedSUSYTrackEL = true;
                if (fabs(track_d0_corr) > m_Highd0Cut)  
                {
                    passedHighD0SUSYTrackEL = true;
                }
            }
            else if (abs(pdgid) == 13) 
            {
                passedSUSYTrackMU = true;
                if (fabs(track_d0_corr) > m_Highd0Cut)  passedHighD0SUSYTrackMU = true;
            }
            else 
            {
                passedSUSYTrackOther = true;
                if (fabs(track_d0_corr) > m_Highd0Cut)  passedHighD0SUSYTrackOther = true;
            }

            double track_prodR = getProdR( truthPart );

            // offline efficiencies
            if (bestMatch != nullptr)  // Has FTF match
            {
                hist("h_d0_res_FTFoffmatch_truth")->Fill(fabs(bestMatch->d0()) - fabs(getTruthPartD0_extrap(truthPart)) );

                // Fill numerators
                hist("h_d0_eff_FTFcomb_r_offline_n")->Fill( track_d0_corr );
                if (passedSUSYTrackEL) hist("h_d0_el_eff_n")->Fill(track_d0_corr);
                if (passedSUSYTrackMU) hist("h_d0_mu_eff_n")->Fill(track_d0_corr);
                hist("h_pt_eff_n")->Fill( track->pt() / 1000. );
                hist("h_eta_eff_n")->Fill( track->eta() );
                hist("h_vtxR_eff_n")->Fill( track_prodR  );
            } else {
                /*
                hist("h_pt_offline_noFTF")->Fill(track->pt() / 1000.);
                hist("h_d0_offline_noFTF")->Fill(track_d0_corr );
                hist("h_eta_offline_noFTF")->Fill(track->eta() );
                hist("h_vtxR_offline_noFTF")->Fill( getProdR(truthPart) );
                */
            }

            // Fill denominators
            hist("h_d0_eff_FTFcomb_r_offline_d")->Fill( track_d0_corr );
            if (passedSUSYTrackEL) hist("h_d0_el_eff_d")->Fill(track_d0_corr);
            if (passedSUSYTrackMU) hist("h_d0_mu_eff_d")->Fill(track_d0_corr);
            hist("h_pt_eff_d")->Fill( track->pt() / 1000. );
            hist("h_eta_eff_d")->Fill( track->eta() );
            hist("h_vtxR_eff_d")->Fill( track_prodR );


            // New trigger eff vs offline Muons
            // already a signal track
            if (abs(pdgid) == 13 && L1_MU14FCH_Passed) // is muon and pt > 24 GeV and d0 > 2 
            {   
                
                // denominators
                if (track->pt()/1000. >20 && fabs(track_d0_corr) > 2. )
                {
                    hist("h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig_d")->Fill(track_prodR);
                    hist("h_prodR_eff_HLT_mu24_r_offlinesig_d")->Fill(track_prodR);
                    hist("h_prodR_eff_HLT_mu24_comb_r_offlinesig_d")->Fill(track_prodR);

                    hist("h_z0_eff_HLT_mu24_LRTloose_r_offlinesig_d")->Fill(track_z0);
                    hist("h_z0_eff_HLT_mu24_r_offlinesig_d")->Fill(track_z0);
                    hist("h_z0_eff_HLT_mu24_comb_r_offlinesig_d")->Fill(track_z0);

                    hist("h_eta_eff_HLT_mu24_LRTloose_r_offlinesig_d")->Fill(track->eta());
                    hist("h_eta_eff_HLT_mu24_r_offlinesig_d")->Fill(track->eta());
                    hist("h_eta_eff_HLT_mu24_comb_r_offlinesig_d")->Fill(track->eta());
                }

                if (fabs(track_d0_corr) > 2. )
                {
                    hist("h_pt_eff_HLT_mu24_LRTloose_r_offlinesig_d")->Fill(track->pt() / 1000.);
                    hist("h_pt_eff_HLT_mu24_r_offlinesig_d")->Fill(track->pt() / 1000.);
                    hist("h_pt_eff_HLT_mu24_comb_r_offlinesig_d")->Fill(track->pt() / 1000.);
                }

                if (track->pt()/1000. >20 )
                {
                    hist("h_d0_eff_HLT_mu24_r_offlinesig_d")->Fill(track_d0_corr);
                    hist("h_d0_eff_HLT_mu24_LRTloose_r_offlinesig_d")->Fill(track_d0_corr);
                    hist("h_d0_eff_HLT_mu24_comb_r_offlinesig_d")->Fill(track_d0_corr);
                    hist("h_d0_zoom_eff_HLT_mu24_r_offlinesig_d")->Fill(track_d0_corr);
                    hist("h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig_d")->Fill(track_d0_corr);
                    hist("h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig_d")->Fill(track_d0_corr);



                    hist("h_pt_mu20_msonly_r_offlinesig_eff_d")->Fill(track->pt() / 1000.);
                    hist("h_prodR_mu20_msonly_r_offlinesig_eff_d")->Fill(track_prodR);
                    hist("h_d0_mu20_msonly_r_offlinesig_eff_d")->Fill(track_d0_corr);                    
                }




                bool fillOR = false;
                // numerators
                if (m_offlinesig_to_MUPT_map.find(track) != m_offlinesig_to_MUPT_map.end()) // matched to roi muon track
                {
                    if (HLT_mu24_ivarmedium_L1MU14FCH_passed)
                    {
                        if (track->pt()/1000. >20 )
                        {
                            hist("h_d0_eff_HLT_mu24_r_offlinesig_n")->Fill(track_d0_corr);
                            hist("h_d0_zoom_eff_HLT_mu24_r_offlinesig_n")->Fill(track_d0_corr);                            
                        }
                        if (track->pt()/1000. >20 && fabs(track_d0_corr) > 2. ) 
                        {
                            hist("h_prodR_eff_HLT_mu24_r_offlinesig_n")->Fill(track_prodR);                        
                            hist("h_z0_eff_HLT_mu24_r_offlinesig_n")->Fill(track_z0);                        
                            hist("h_eta_eff_HLT_mu24_r_offlinesig_n")->Fill(track->eta());                        
                        }
                        if (fabs(track_d0_corr) > 2. ) hist("h_pt_eff_HLT_mu24_r_offlinesig_n")->Fill(track->pt() / 1000.);

                        fillOR = true;
                    }
                }

                if (m_offlinesig_to_MUPTLRT_map.find(track) != m_offlinesig_to_MUPTLRT_map.end()) // matched to roi LRT muon track
                {
                    if (HLT_mu20_LRT_d0loose_L1MU14FCH_Passed)
                    {
                        if (track->pt()/1000. >20 )
                        {
                            hist("h_d0_eff_HLT_mu24_LRTloose_r_offlinesig_n")->Fill(track_d0_corr);
                            hist("h_d0_zoom_eff_HLT_mu24_LRTloose_r_offlinesig_n")->Fill(track_d0_corr);
                        }
                        if (track->pt()/1000. >20 && fabs(track_d0_corr) > 2. ) {
                            hist("h_prodR_eff_HLT_mu24_LRTloose_r_offlinesig_n")->Fill(track_prodR);
                            hist("h_z0_eff_HLT_mu24_LRTloose_r_offlinesig_n")->Fill(track_z0);
                            hist("h_eta_eff_HLT_mu24_LRTloose_r_offlinesig_n")->Fill(track->eta());
                        }
                        if (fabs(track_d0_corr) > 2. ) hist("h_pt_eff_HLT_mu24_LRTloose_r_offlinesig_n")->Fill(track->pt() / 1000.);

                        fillOR = true;
                    }
                }

                if (fillOR) // matched to roi muon track , OR lrt track
                {
                    if (track->pt()/1000. >20 )
                    {
                        hist("h_d0_eff_HLT_mu24_comb_r_offlinesig_n")->Fill(track_d0_corr);
                        hist("h_d0_zoom_eff_HLT_mu24_comb_r_offlinesig_n")->Fill(track_d0_corr);
                           
                    } 
                    if (track->pt()/1000. >20 && fabs(track_d0_corr) > 2. ) {
                        hist("h_prodR_eff_HLT_mu24_comb_r_offlinesig_n")->Fill(track_prodR); 
                        hist("h_z0_eff_HLT_mu24_comb_r_offlinesig_n")->Fill(track_z0); 
                        hist("h_eta_eff_HLT_mu24_comb_r_offlinesig_n")->Fill(track->eta()); 
                    }
                    if (fabs(track_d0_corr) > 2. ) hist("h_pt_eff_HLT_mu24_comb_r_offlinesig_n")->Fill(track->pt() / 1000.);
           
                }

                if (HLT_mu20_msonly_Passed && track->pt()/1000. >20)
                {
                    hist("h_pt_mu20_msonly_r_offlinesig_eff_n")->Fill(track->pt() / 1000.);
                    hist("h_prodR_mu20_msonly_r_offlinesig_eff_n")->Fill(track_prodR);
                    hist("h_d0_mu20_msonly_r_offlinesig_eff_n")->Fill(track_d0_corr);
                }

               

            }

            // New trigger eff vs offline Electrons
            // already a signal track
            if (abs(pdgid) == 11 && L1_eEM26M_Passed) // is electron and pt > 26 GeV and d0>5 
            {   
                //offlineElectrons.push_back(track);

                // denominators
                if (track->pt()/1000. >30 && fabs(track_d0_corr) > 3. )
                {
                    hist("h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig_d")->Fill(track_prodR);
                    hist("h_prodR_eff_HLT_el26_r_offlinesig_d")->Fill(track_prodR);
                    hist("h_prodR_eff_HLT_el26_comb_r_offlinesig_d")->Fill(track_prodR);

                    hist("h_z0_eff_HLT_el30_LRTmedium_r_offlinesig_d")->Fill(track_z0);
                    hist("h_z0_eff_HLT_el26_r_offlinesig_d")->Fill(track_z0);
                    hist("h_z0_eff_HLT_el26_comb_r_offlinesig_d")->Fill(track_z0);

                    hist("h_eta_eff_HLT_el30_LRTmedium_r_offlinesig_d")->Fill(track->eta());
                    hist("h_eta_eff_HLT_el26_r_offlinesig_d")->Fill(track->eta());
                    hist("h_eta_eff_HLT_el26_comb_r_offlinesig_d")->Fill(track->eta());
                }

                if (fabs(track_d0_corr) > 3. )
                {
                    hist("h_pt_eff_HLT_el30_LRTmedium_r_offlinesig_d")->Fill(track->pt() / 1000.);

                    hist("h_pt_eff_HLT_el26_r_offlinesig_d")->Fill(track->pt() / 1000.);
                    hist("h_pt_eff_HLT_el26_comb_r_offlinesig_d")->Fill(track->pt() / 1000.);
                }

                if (track->pt()/1000. >30)
                {
                    hist("h_d0_eff_HLT_el26_r_offlinesig_d")->Fill(track_d0_corr);
                    hist("h_d0_eff_HLT_el30_LRTmedium_r_offlinesig_d")->Fill(track_d0_corr);
                    hist("h_d0_eff_HLT_el26_comb_r_offlinesig_d")->Fill(track_d0_corr);
                    hist("h_d0_zoom_eff_HLT_el26_r_offlinesig_d")->Fill(track_d0_corr);
                    hist("h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig_d")->Fill(track_d0_corr);
                    hist("h_d0_zoom_eff_HLT_el26_comb_r_offlinesig_d")->Fill(track_d0_corr);

                    
                

                    hist("h_pt_g20_r_offlinesig_eff_d")->Fill(track->pt() / 1000.);
                    hist("h_d0_g20_r_offlinesig_eff_d")->Fill(track_d0_corr);
                    hist("h_prodR_g20_r_offlinesig_eff_d")->Fill(track_prodR);
                }



                bool fillOR = false;
                // numerators
                if (m_offlinesig_to_ELPT_map.find(track) != m_offlinesig_to_ELPT_map.end()) // matched to roi electron track
                {
                    if (HLT_e26_lhtight_ivarloose_L1eEM26M_passed)
                    {
                        if (track->pt()/1000. >30 )
                        {
                            hist("h_d0_eff_HLT_el26_r_offlinesig_n")->Fill(track_d0_corr);
                            hist("h_d0_zoom_eff_HLT_el26_r_offlinesig_n")->Fill(track_d0_corr);
                            
                        }
                        if (track->pt()/1000. >30 && fabs(track_d0_corr) > 3. ) {
                            hist("h_prodR_eff_HLT_el26_r_offlinesig_n")->Fill(track_prodR);
                            hist("h_z0_eff_HLT_el26_r_offlinesig_n")->Fill(track_z0);
                            hist("h_eta_eff_HLT_el26_r_offlinesig_n")->Fill(track->eta());
                        }
                        if (fabs(track_d0_corr) > 3. ) hist("h_pt_eff_HLT_el26_r_offlinesig_n")->Fill(track->pt() / 1000.);

                        fillOR = true;                        
                    }
                }
                
                if (m_offlinesig_to_ELPTLRT_map.find(track) != m_offlinesig_to_ELPTLRT_map.end()) // matched to roi LRT electron track
                {
                    if (HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed) 
                    {
                        if (track->pt()/1000. >30 )
                        {
                            hist("h_d0_eff_HLT_el30_LRTmedium_r_offlinesig_n")->Fill(track_d0_corr);
                            hist("h_d0_zoom_eff_HLT_el30_LRTmedium_r_offlinesig_n")->Fill(track_d0_corr);
                        }
                        if (track->pt()/1000. >30 && fabs(track_d0_corr) > 3. ) {
                            hist("h_prodR_eff_HLT_el30_LRTmedium_r_offlinesig_n")->Fill(track_prodR);
                            hist("h_z0_eff_HLT_el30_LRTmedium_r_offlinesig_n")->Fill(track_z0);
                            hist("h_eta_eff_HLT_el30_LRTmedium_r_offlinesig_n")->Fill(track->eta());
                        }
                        if (fabs(track_d0_corr) > 3. ) hist("h_pt_eff_HLT_el30_LRTmedium_r_offlinesig_n")->Fill(track->pt() / 1000.);

                        fillOR = true;
                    }
                }


                if (fillOR) // matched to roi electron track , OR lrt track
                {
                    if (track->pt()/1000. >30 )
                    {
                        hist("h_d0_eff_HLT_el26_comb_r_offlinesig_n")->Fill(track_d0_corr);
                        hist("h_d0_zoom_eff_HLT_el26_comb_r_offlinesig_n")->Fill(track_d0_corr);                          
                    }
                    if (track->pt()/1000. >30 && fabs(track_d0_corr) > 3. )  {
                        hist("h_prodR_eff_HLT_el26_comb_r_offlinesig_n")->Fill(track_prodR); 
                        hist("h_z0_eff_HLT_el26_comb_r_offlinesig_n")->Fill(track_z0); 
                        hist("h_eta_eff_HLT_el26_comb_r_offlinesig_n")->Fill(track->eta()); 
                    }
                    if (fabs(track_d0_corr) > 3. ) hist("h_pt_eff_HLT_el26_comb_r_offlinesig_n")->Fill(track->pt() / 1000.);
             
                }


                if (ei->eventNumber() == 415 && m_debug == true)
                {
                    std::cout<<"elptlrtmap match "<<(m_offlinesig_to_ELPTLRT_map.find(track) != m_offlinesig_to_ELPTLRT_map.end())<<std::endl;
                    std::cout<<"trig pass test "<<HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed<<std::endl;
                }


                if (HLT_g20_loose_L1EM15VH_passed && track->pt()/1000. >30)
                {
                    hist("h_pt_g20_r_offlinesig_eff_n")->Fill(track->pt() / 1000.);
                    hist("h_d0_g20_r_offlinesig_eff_n")->Fill(track_d0_corr);
                    hist("h_prodR_g20_r_offlinesig_eff_n")->Fill(track_prodR);
                }




            }

            // L1 Loops
            bool inEMROI = false;
            bool inMUROI = false;
            float minDRoff_em = 999;
            float minDRoff_mu = 999;
            float saveTrack_d0_em = -999;
            float saveTrack_d0_mu = -999;

            // Again, loop through ROIs to see if tracks fall close
            double track_eta = track->eta();
            double track_phi = track->phi();
            if (abs(pdgid) == 11) // EM ROI
            {
                hist("h_pt_offline_L1eEM26M_eff_d")->Fill(track->pt()*0.001);
                hist("h_d0_offline_L1eEM26M_eff_d")->Fill(track_d0_corr);
                hist("h_vtxR_offline_L1eEM26M_eff_d")->Fill(track_prodR);


                for (auto em : m_roid_em)
                {
                    double tmp_dR = getDeltaR(track_eta, track_phi, em);
                    if (tmp_dR < m_ROISize) {
                        inEMROI = true;
                        hasTrackInROI_em = true;        

                        if ( bestMatch != nullptr ) // has FTF match
                        {
                            hasMatchedFTF_em = true;
                            hist("h2_offlineSignalwFTF_inROI_d0_v_pt_em")->Fill( fabs(track_d0_corr), track->pt()*0.001);
                            hist("h2_offlineSignalwFTF_inROI_d0sig_v_pt_em")->Fill( d0sig, track->pt()*0.001);

                        }
                    }

                    // Save the closest track info for later
                    if (tmp_dR < minDRoff_em) {
                        minDRoff_em = tmp_dR;
                        saveTrack_d0_em = track_d0_corr;
                    }
                }

                // Fill histos with closet match
                if (minDRoff_em < 999) 
                {
                    hist("h_deltaR_ROI_offlineSignal_em")->Fill(minDRoff_em);
                    hist("h2_deltaR_ROI_offlineSignal_v_d0_em")->Fill(minDRoff_em, fabs(saveTrack_d0_em) );
                }

                hist("h_vtxZ_offlineSignal_el")->Fill(  getProdZ(truthPart)  );
            } else if (abs(pdgid) == 13) // MU ROI
            {
                hist("h_pt_offline_L1MU14FCH_eff_d")->Fill(track->pt()*0.001);
                hist("h_d0_offline_L1MU14FCH_eff_d")->Fill(track_d0_corr);
                hist("h_vtxR_offline_L1MU14FCH_eff_d")->Fill(track_prodR);
                
                for (auto mu : m_roid_mu)
                {
                    double tmp_dR = getDeltaR(track_eta, track_phi, mu);       
                    if (tmp_dR < m_ROISize) 
                    {
                        inMUROI = true;  
                        hasTrackInROI_mu = true;      

                        if ( bestMatch != nullptr ) // has FTF match
                        {
                            hasMatchedFTF_mu = true;
                            hist("h2_offlineSignalwFTF_inROI_d0_v_pt_mu")->Fill( fabs(track_d0_corr), track->pt()*0.001);
                            hist("h2_offlineSignalwFTF_inROI_d0sig_v_pt_mu")->Fill( d0sig, track->pt()*0.001);
                        }
                    }
                    if (tmp_dR < minDRoff_mu) 
                    {
                        minDRoff_mu = tmp_dR;
                        saveTrack_d0_mu = track_d0_corr;
                    }
                }

                // If track has ftf match and is in roi, look for muon
                if (bestMatch != nullptr && inMUROI)
                {
                    for (const xAOD::Muon *muon : m_Muons)
                    {
                        float dr = getDeltaR(track_eta, track_phi, muon);
                        hist("h_deltaR_muon_offlineSignal")->Fill(dr);
                        if ( dr < m_matchSizeR)hasMatchedMuon_mu = true;
                    }
                }


                // Fill histos with closet match
                if (minDRoff_mu < 999)
                {
                    hist("h_deltaR_ROI_offlineSignal_mu")->Fill(minDRoff_mu);
                    hist("h2_deltaR_ROI_offlineSignal_v_d0_mu")->Fill(minDRoff_mu, fabs(saveTrack_d0_mu));
                }


                hist("h_vtxZ_offlineSignal_mu")->Fill(  getProdZ(truthPart)  );
            }



            // Fill numerators if there was a track in the ROI
            if (inEMROI && L1_eEM26M_Passed)
            {
                hist("h_pt_offline_L1eEM26M_eff_n")->Fill(track->pt()*0.001);
                hist("h_d0_offline_L1eEM26M_eff_n")->Fill(track_d0_corr);
                hist("h_vtxR_offline_L1eEM26M_eff_n")->Fill(track_prodR);
            }
            if (inMUROI && L1_MU14FCH_Passed)
            {
                hist("h_pt_offline_L1MU14FCH_eff_n")->Fill(track->pt()*0.001);
                hist("h_d0_offline_L1MU14FCH_eff_n")->Fill(track_d0_corr);
                hist("h_vtxR_offline_L1MU14FCH_eff_n")->Fill(track_prodR);
            }



            // Fill histos
            hist("h_pt_offlineSignal")->Fill(track->pt() / 1000. );
            hist("h_eta_offlineSignal")->Fill(track->eta() );
            hist("h_phi_offlineSignal")->Fill(track->phi() );
            hist("h_d0_offlineSignal")->Fill(track_d0_corr); 
            hist("h_z0_offlineSignal")->Fill(track->z0());
            hist("h_vtxR_offlineSignal")->Fill(  getProdR(truthPart)  );
            hist("h_vtxZ_offlineSignal")->Fill(  getProdZ(truthPart)  );
            hist("h_nhits_offlineSignal")->Fill(nPixHits+nSCTHits);
            hist("h_d0_res_offlineSignal_truth")->Fill( fabs(track_d0_corr) - fabs(getTruthPartD0_extrap(truthPart))   );
            hist("h_d0sig_offlineSignal")->Fill( d0sig );

            hist("h2_offlineSignal_d0_v_hitsSI")->Fill(fabs(track_d0_corr), nPixHits+nSCTHits+nDeadPix+nDeadSCT);
            hist("h2_offlineSignal_d0_v_hitsPix")->Fill(fabs(track_d0_corr), nPixHits+nDeadPix);
            hist("h2_offlineSignal_d0_v_hitsSCT")->Fill(fabs(track_d0_corr), nSCTHits+nDeadSCT);
            hist("h2_offlineSignal_vtxR_v_hitsSI")->Fill(getProdR(truthPart), nPixHits+nSCTHits+nDeadPix+nDeadSCT);

            hist("hp_offlineSignal_hits_v_d0")->Fill(fabs(track_d0_corr), nPixHits+nSCTHits );
            hist("hp_offlineSignal_pixhits_v_d0")->Fill(fabs(track_d0_corr), nPixHits+nDeadPix );
            hist("hp_offlineSignal_scthits_v_d0")->Fill(fabs(track_d0_corr), nSCTHits+nDeadSCT );


            //m_OfflineTracksSignal.push_back(track); 
    
            /*
            std::cout<<"==============="<<std::endl;
            std::cout<<"Offline d0 | truth d0 = " << track_d0_corr << " | " << getTruthPartD0(truthPart) <<std::endl;                
            std::cout <<"tida d0 | truth article = " << getTruthPartD0_straight_tida(truthPart)<< " | "<< getTruthPartD0_article(truthPart)<<std::endl;
            std::cout<<"Prod R = " << getProdR(truthPart) <<std::endl;
            std::cout<<"Offline pT | truth pT = " << track->pt()*0.001 << " | " << truthPart->pt()*0.001<<std::endl;
            std::cout<<"Offline eta | truth eta = " << track->eta() << " | " << truthPart->eta()<<std::endl;
            std::cout<<"Offline phi | truth phi = " << track->phi() << " | " << truthPart->phi()<<std::endl;
            std::cout<<"Offline charge | truth charge = " << track->charge() << " | " <<truthPart->charge()<<std::endl;
            */
            
        } // end truth matched offline track
    

    } // End Offline Track Loop
    // Sort electrons and muons by pT for msonly two leg trigger info
    //std::sort(offlineElectrons.begin(), offlineElectrons.end(), [](const xAOD::TrackParticle* a, const xAOD::TrackParticle* b){return (a->pt() > b->pt());} );
    //std::sort(offlineMuons.begin(), offlineMuons.end(), [](const xAOD::TrackParticle* a, const xAOD::TrackParticle* b){return (a->pt() > b->pt());} );
    std::sort(m_Muons.begin(), m_Muons.end(), [](const xAOD::Muon* a, const xAOD::Muon* b){return (a->pt() > b->pt());} );
    std::sort(m_Electrons.begin(), m_Electrons.end(), [](const xAOD::Electron* a, const xAOD::Electron* b){return (a->pt() > b->pt());} );
    //////////////////// offline

    // Check if muon is in L1 RoI since matching crashes
    // Fix thi sto be better and just loop over muons and roi
    bool leadMuInRoi = false;
    if (L1_MU14FCH_Passed and m_Muons.size()>0)
    {
        for (auto mu : m_roid_mu)
        {
            float mu_eta = m_Muons[0]->eta();
            float mu_phi = m_Muons[0]->phi();
            if (getDeltaR(mu_eta, mu_phi, mu) < m_ROISize)
            {
                leadMuInRoi = true;
                break;
            }
        }
    }

    //2mu50
    if (m_Muons.size() >= 2)
    {   
        if (L1_MU14FCH_Passed && leadMuInRoi && m_Muons[0]->pt()*0.001 > 50 )
        {

            hist("h_pt_sublead_HLT_2mu50_r_offline_eff_d")->Fill(m_Muons[1]->pt()*0.001);
            if (m_Muons[1]->pt()*0.001 > 50)
            {
                hist("h_prodR_HLT_2mu50_r_offline_d")->Fill(getProdR(m_Muons[1]->primaryTrackParticle()));
                hist("h_eta_sublead_HLT_2mu50_r_offline_eff_d")->Fill(m_Muons[1]->eta());
                hist("h_phi_sublead_HLT_2mu50_r_offline_eff_d")->Fill(m_Muons[1]->phi());
            }
            if(HLT_2mu50_msonly_L1MU14FCH_passed && m_trigMatchTool->match(*(xAOD::IParticle*)m_Muons[1], "HLT_2mu50_msonly_L1MU14FCH"))
            {
                hist("h_pt_sublead_HLT_2mu50_r_offline_eff_n")->Fill(m_Muons[1]->pt()*0.001);
                if (m_Muons[1]->pt()*0.001 > 50)
                {
                    hist("h_prodR_HLT_2mu50_r_offline_n")->Fill(getProdR(m_Muons[1]->primaryTrackParticle()));
                    hist("h_eta_sublead_HLT_2mu50_r_offline_eff_n")->Fill(m_Muons[1]->eta());
                    hist("h_phi_sublead_HLT_2mu50_r_offline_eff_n")->Fill(m_Muons[1]->phi());
                }
            }
            
        }
    }
    //g40mu40
    if (m_Muons.size() > 0 && m_Electrons.size() > 0)
    {
        if (HLT_g40_loose_L1EM20VH_passed)
        {

            hist("h_pt_mu_HLT_g40mu40_r_offline_eff_d")->Fill(m_Muons[0]->pt()*0.001);
            if (m_Muons[0]->pt()*0.001>40)
            {
                hist("h_prodR_mu_HLT_g40mu40_r_offline_d")->Fill(getProdR(m_Muons[0]->primaryTrackParticle()));
                hist("h_eta_mu_HLT_g40mu40_r_offline_eff_d")->Fill(m_Muons[0]->eta());
                hist("h_phi_mu_HLT_g40mu40_r_offline_eff_d")->Fill(m_Muons[0]->phi());
            }

            if (HLT_g40_loose_mu40_msonly_L1MU14FCH_passed && m_trigMatchTool->match(*(xAOD::IParticle*)m_Muons[0], "HLT_g40_loose_mu40_msonly_L1MU14FCH"))
            {
                hist("h_pt_mu_HLT_g40mu40_r_offline_eff_n")->Fill(m_Muons[0]->pt()*0.001);
                if (m_Muons[0]->pt()*0.001>40)
                {
                    hist("h_prodR_mu_HLT_g40mu40_r_offline_n")->Fill(getProdR(m_Muons[0]->primaryTrackParticle()));
                    hist("h_eta_mu_HLT_g40mu40_r_offline_eff_n")->Fill(m_Muons[0]->eta());
                    hist("h_phi_mu_HLT_g40mu40_r_offline_eff_n")->Fill(m_Muons[0]->phi());
                }

            }
        }

        /* this crashes
        bool trigMatchedMuon = false;
        for (auto muon: m_Muons)
        {
            if (muon->pt()*0.001 > 40 && m_trigMatchTool->match(*(xAOD::IParticle*)muon, "L1_MU14FCH")) trigMatchedMuon = true;
        }

        if (trigMatchedMuon)
        */
        if (HLT_mu20_msonly_Passed && m_Muons[0]->pt()*0.001 > 40 )
        {
            hist("h_pt_el_HLT_g40mu40_r_offline_eff_d")->Fill(m_Electrons[0]->pt()*0.001);
            if (m_Electrons[0]->pt()*0.001>40)
            {
                hist("h_prodR_el_HLT_g40mu40_r_offline_d")->Fill(getProdR(m_Electrons[0]->trackParticle()));
            }
            if(HLT_g40_loose_mu40_msonly_L1MU14FCH_passed) // && m_trigMatchTool->match(*(xAOD::IParticle*)m_Electrons[0], "HLT_g40_loose_mu40_msonly_L1MU14FCH_passed"))
            {
                hist("h_pt_el_HLT_g40mu40_r_offline_eff_n")->Fill(m_Electrons[0]->pt()*0.001);
                if (m_Electrons[0]->pt()*0.001>40)
                {
                    hist("h_prodR_el_HLT_g40mu40_r_offline_n")->Fill(getProdR(m_Electrons[0]->trackParticle()));
                }
            }
        }

    }
    //g40 single leg

    if (m_debug && false) 
    {
        std::cout<<"Getting electron chain features tnp"<<std::endl;
        getElectronChainFeatures("HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH", -1);
        getElectronChainTrackFeatures("HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH", -1);

        getPhotonChainFeatures("HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH", 1);
        
        std::cout<<"Getting electron chain features el lrt"<<std::endl;
        getElectronChainFeatures("HLT_e26_lhloose_nopix_lrttight_L1eEM26M");
        getElectronChainTrackFeatures("HLT_e26_lhloose_nopix_lrttight_L1eEM26M");

        

    }

    // debugging
    if (m_debug && false && !m_isDAOD) 
    {
        std::cout<<"Trig Calo Clusters size: "<<caloClusters->size()<<std::endl;
        for (const xAOD::CaloCluster *clust : *caloClusters)
        {
            std::cout<<"hlt cluster collection et/eta/phi: "<<clust->e()<<" / "<<clust->eta0()<<" / "<< clust->phi0()<<std::endl;
        }
    }

    //////////////////
    // HLT Electrons Loop
    for (const xAOD::Electron *electron: m_HLT_Electrons)
    {
        if (m_hlt_to_off_electrons_map.find(electron) == m_hlt_to_off_electrons_map.end() ) continue; // skip hlt electrons not matched to offline electron

        double electronProdR = getProdR(electron->trackParticle());
        double electron_d0 = electron->trackParticle() ? electron->trackParticle()->d0() : -999.;
        double electron_pt = electron->pt()*0.001;
        double electron_eta = electron->eta();
        double electron_phi = electron->phi();

        if (m_includeExtraLLH)
        {
            // llh vs truth
            hist("h_hltlrt_llh_El_d0_d")->Fill(abs(electron_d0));
            hist("h_hltlrt_llh_new_El_d0_d")->Fill(abs(electron_d0));

            hist("h_hltlrt_llh_El_pt_d")->Fill(electron_pt);
            hist("h_hltlrt_llh_new_El_pt_d")->Fill(electron_pt);

            if (m_ElectronLLHTool->accept(electron))
            {
                hist("h_hltlrt_llh_El_d0_n")->Fill(abs(electron_d0));
                hist("h_hltlrt_llh_El_pt_n")->Fill(electron_pt);
            }
            if (m_ElectronLLHTool_newtune->accept(electron)) 
            {
                hist("h_hltlrt_llh_new_El_d0_n")->Fill(abs(electron_d0));
                hist("h_hltlrt_llh_new_El_pt_n")->Fill(electron_pt);
            }

            // llh vs truth+offline
            /*if (m_ElectronLLHTool_offline->accept(electron))
            {
                hist("h_hltlrt_llh_El_d0_r_off_d")->Fill(abs(electron_d0));
                hist("h_hltlrt_llh_new_El_d0_r_off_d")->Fill(abs(electron_d0));

                hist("h_hltlrt_llh_El_pt_r_off_d")->Fill(electron_pt);
                hist("h_hltlrt_llh_new_El_pt_r_off_d")->Fill(electron_pt);

                if (m_ElectronLLHTool->accept(electron)) 
                {
                    hist("h_hltlrt_llh_El_d0_r_off_n")->Fill(abs(electron_d0));
                    hist("h_hltlrt_llh_El_pt_r_off_n")->Fill(electron_pt);
                }
                if (m_ElectronLLHTool_newtune->accept(electron)) 
                { 
                    hist("h_hltlrt_llh_new_El_d0_r_off_n")->Fill(abs(electron_d0));
                    hist("h_hltlrt_llh_new_El_pt_r_off_n")->Fill(electron_pt);
                }
            }*/
        }


    }

    //////////////////////
    // Electrons Loop
    for (const xAOD::Electron *electron: m_Electrons)
    {
        double electronProdR = getProdR(electron->trackParticle());
        double electron_d0 = electron->trackParticle()->d0();
        double electron_z0 = electron->trackParticle()->z0();
        double electron_pt = electron->pt()*0.001;
        double electron_eta = electron->eta();
        double electron_phi = electron->phi();

        //hist("h_dnn_El_d0_d")->Fill(abs(electron_d0));
        //if (m_ElectronDNNTool->accept(electron))  hist("h_dnn_El_d0_n")->Fill(abs(electron_d0));

        if (m_includeExtraLLH)
        {
            // llh vs truth
            hist("h_llh_El_d0_d")->Fill(abs(electron_d0));
            hist("h_llh_new_El_d0_d")->Fill(abs(electron_d0));
            hist("h_llh_off_El_d0_d")->Fill(abs(electron_d0));

            hist("h_llh_El_pt_d")->Fill(electron_pt);
            hist("h_llh_new_El_pt_d")->Fill(electron_pt);
            hist("h_llh_off_El_pt_d")->Fill(electron_pt);

            if (m_ElectronLLHTool->accept(electron))
            {
                hist("h_llh_El_d0_n")->Fill(abs(electron_d0));
                hist("h_llh_El_pt_n")->Fill(electron_pt);
            }
            if (m_ElectronLLHTool_newtune->accept(electron)) 
            {
                hist("h_llh_new_El_d0_n")->Fill(abs(electron_d0));
                hist("h_llh_new_El_pt_n")->Fill(electron_pt);
            }
            /*if (m_ElectronLLHTool_offline->accept(electron)) 
            { 
                hist("h_llh_off_El_d0_n")->Fill(abs(electron_d0));
                hist("h_llh_off_El_pt_n")->Fill(electron_pt);
            }*/

            // llh vs truth+offline
            /*if (m_ElectronLLHTool_offline->accept(electron))
            {
                hist("h_llh_El_d0_r_off_d")->Fill(abs(electron_d0));
                hist("h_llh_new_El_d0_r_off_d")->Fill(abs(electron_d0));

                hist("h_llh_El_pt_r_off_d")->Fill(electron_pt);
                hist("h_llh_new_El_pt_r_off_d")->Fill(electron_pt);

                if (m_ElectronLLHTool->accept(electron)) 
                {
                    hist("h_llh_El_d0_r_off_n")->Fill(abs(electron_d0));
                    hist("h_llh_El_pt_r_off_n")->Fill(electron_pt);
                }
                if (m_ElectronLLHTool_newtune->accept(electron)) 
                { 
                    hist("h_llh_new_El_d0_r_off_n")->Fill(abs(electron_d0));
                    hist("h_llh_new_El_pt_r_off_n")->Fill(electron_pt);
                }
            }*/
        }

        hist("h_electron_pt")->Fill(electron_pt);
        hist("h_electron_eta")->Fill(electron_eta);
        hist("h_electron_d0")->Fill(fabs(electron_d0));

        hist("h_pt_el_HLT_g40_r_offline_eff_d")->Fill(electron_pt);
        if (electron_pt>40)
        {
            hist("h_prodR_el_HLT_g40_r_offline_eff_d")->Fill(electronProdR);
            hist("h_eta_el_HLT_g40_r_offline_eff_d")->Fill(electron_eta);
        }

        bool electron_in_roi = false;
        for (auto el : m_roid_em)
        {
            if (getDeltaR(electron_eta, electron_phi, el) < m_ROISize)
            {
                electron_in_roi = true;
                break;
            }
        }

        if(HLT_g40_loose_L1EM20VH_passed && electron_in_roi) // && m_trigMatchTool->match(*(xAOD::IParticle*)electron, "HLT_g40_loose_L1EM20VH"))
        {
            hist("h_pt_el_HLT_g40_r_offline_eff_n")->Fill(electron_pt);
            if (electron_pt>40)
            {
                hist("h_prodR_el_HLT_g40_r_offline_eff_n")->Fill(electronProdR);
                hist("h_eta_el_HLT_g40_r_offline_eff_n")->Fill(electron_eta);
            }
        }

        // Electron LRT TnP chains
        //if (m_debug && m_trigMatchTool->match(*(xAOD::IParticle*)electron, "HLT_g25_medium_L1EM20VH")) std::cout<<"electron matches g25"<<std::endl;

        if (HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH_passed) // && m_trigMatchTool->match(*(xAOD::IParticle*)electron, "HLT_g25_medium_L1EM20VH") )
        { // need to match electron to photon chain, retrieve trig object from chain?



            if (m_debug && false)
            {
                std::vector<const xAOD::IParticle*> electronsMatch;
                electronsMatch.push_back((xAOD::IParticle*)electron);
                std::cout<<"matching HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH_passed " << m_trigMatchTool->match(electronsMatch, "HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH") << std::endl;
            }

            hist("h_pt_el_lrt_tnp_phtag_eff_d")->Fill(electron_pt);
            hist("h_prodR_el_lrt_tnp_phtag_eff_d")->Fill(electronProdR);

            if (HLT_e26_lhloose_nopix_lrttight_probe_g25_medium_L1EM20VH_passed)
            {
                hist("h_pt_el_lrt_tnp_phtag_eff_n")->Fill(electron_pt);
                hist("h_prodR_el_lrt_tnp_phtag_eff_n")->Fill(electronProdR);
            }
        }

        if (L1_eEM26M_Passed)
        {
            // Trigger efficiency wrt offline electrons
            // denominators
            if (electron_pt >30 && fabs(electron_d0) > 3. )
            {
                hist("h_prodR_eff_HLT_el30_LRTmedium_r_electron_d")->Fill(electronProdR);
                hist("h_prodR_eff_HLT_el26_r_electron_d")->Fill(electronProdR);
                hist("h_prodR_eff_HLT_el26_comb_r_electron_d")->Fill(electronProdR);

                hist("h_z0_eff_HLT_el30_LRTmedium_r_electron_d")->Fill(electron_z0);
                hist("h_z0_eff_HLT_el26_r_electron_d")->Fill(electron_z0);
                hist("h_z0_eff_HLT_el26_comb_r_electron_d")->Fill(electron_z0);

                hist("h_eta_eff_HLT_el30_LRTmedium_r_electron_d")->Fill(electron_eta);
                hist("h_eta_eff_HLT_el26_r_electron_d")->Fill(electron_eta);
                hist("h_eta_eff_HLT_el26_comb_r_electron_d")->Fill(electron_eta);
            }

            if (fabs(electron_d0) > 3. )
            {
                hist("h_pt_eff_HLT_el30_LRTmedium_r_electron_d")->Fill(electron_pt);

                hist("h_pt_eff_HLT_el26_r_electron_d")->Fill(electron_pt);
                hist("h_pt_eff_HLT_el26_comb_r_electron_d")->Fill(electron_pt);
            }

            if (electron_pt >30)
            {
                hist("h_d0_eff_HLT_el26_r_electron_d")->Fill(electron_d0);
                hist("h_d0_eff_HLT_el30_LRTmedium_r_electron_d")->Fill(electron_d0);
                hist("h_d0_eff_HLT_el26_comb_r_electron_d")->Fill(electron_d0);
                hist("h_d0_zoom_eff_HLT_el26_r_electron_d")->Fill(electron_d0);
                hist("h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron_d")->Fill(electron_d0);
                hist("h_d0_zoom_eff_HLT_el26_comb_r_electron_d")->Fill(electron_d0);

                //hist("h_d0_zoom_eff_HLT_el26dnn_r_electron_d")->Fill(electron_d0);
                //hist("h_d0_zoom_eff_HLT_el26dnn_nod0_r_electron_d")->Fill(electron_d0);
                //hist("h_d0_zoom_eff_HLT_el30dnn_nopix_LRTmedium_r_electron_d")->Fill(electron_d0);
                
            }



            bool fillOR = false;
            // numerators
            if (m_trigMatchTool->match(*electron, "HLT_e26_lhtight_ivarloose_L1eEM26M") ) // matched to roi electron track //todo replace this with trigger matching
            {
                if (HLT_e26_lhtight_ivarloose_L1eEM26M_passed)
                {
                    if (electron_pt >30 )
                    {
                        hist("h_d0_eff_HLT_el26_r_electron_n")->Fill(electron_d0);
                        hist("h_d0_zoom_eff_HLT_el26_r_electron_n")->Fill(electron_d0);
                        
                    }
                    if (electron_pt >30 && fabs(electron_d0) > 3. ) {
                        hist("h_prodR_eff_HLT_el26_r_electron_n")->Fill(electronProdR);
                        hist("h_z0_eff_HLT_el26_r_electron_n")->Fill(electron_z0);
                        hist("h_eta_eff_HLT_el26_r_electron_n")->Fill(electron_eta);
                    }
                    if (fabs(electron_d0) > 3. ) hist("h_pt_eff_HLT_el26_r_electron_n")->Fill(electron_pt);

                    fillOR = true;                        
                }


            }


            // DNN tests
            //if (m_trigMatchTool->match(*electron, "HLT_e26_dnnloose_L1eEM26M") && electron_pt >30 && m_trigDecTool->getChainGroup("HLT_e26_dnnloose_L1eEM26M")->isPassed() ) hist("h_d0_zoom_eff_HLT_el26dnn_r_electron_n")->Fill(electron_d0);
            //if (m_trigMatchTool->match(*electron, "HLT_e26_dnnloose_nod0_L1eEM26M") && electron_pt >30 && m_trigDecTool->getChainGroup("HLT_e26_dnnloose_nod0_L1eEM26M")->isPassed() ) hist("h_d0_zoom_eff_HLT_el26dnn_nod0_r_electron_n")->Fill(electron_d0);
            //if (m_trigMatchTool->match(*electron, "HLT_e30_dnnloose_nopix_lrtmedium_L1eEM26M") && electron_pt >30 && m_trigDecTool->getChainGroup("HLT_e30_dnnloose_nopix_lrtmedium_L1eEM26M")->isPassed() ) hist("h_d0_zoom_eff_HLT_el30dnn_nopix_LRTmedium_r_electron_n")->Fill(electron_d0);


            
            if (m_trigMatchTool->match(*electron, "HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M")) // matched to roi LRT electron track // todo replace this with trigger matching
            {
                if (HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed) 
                {
                    if (electron_pt >30 )
                    {
                        hist("h_d0_eff_HLT_el30_LRTmedium_r_electron_n")->Fill(electron_d0);
                        hist("h_d0_zoom_eff_HLT_el30_LRTmedium_r_electron_n")->Fill(electron_d0);
                    }
                    if (electron_pt >30 && fabs(electron_d0) > 3. ) {
                        hist("h_prodR_eff_HLT_el30_LRTmedium_r_electron_n")->Fill(electronProdR);
                        hist("h_z0_eff_HLT_el30_LRTmedium_r_electron_n")->Fill(electron_z0);
                        hist("h_eta_eff_HLT_el30_LRTmedium_r_electron_n")->Fill(electron_eta);
                    }
                    if (fabs(electron_d0) > 3. ) hist("h_pt_eff_HLT_el30_LRTmedium_r_electron_n")->Fill(electron_pt);

                    fillOR = true;
                }
            }


            if (fillOR) // matched to roi electron track , OR lrt track
            {
                if (electron_pt >30 )
                {
                    hist("h_d0_eff_HLT_el26_comb_r_electron_n")->Fill(electron_d0);
                    hist("h_d0_zoom_eff_HLT_el26_comb_r_electron_n")->Fill(electron_d0);                          
                }
                if (electron_pt >30 && fabs(electron_d0) > 3. )  {
                    hist("h_prodR_eff_HLT_el26_comb_r_electron_n")->Fill(electronProdR); 
                    hist("h_z0_eff_HLT_el26_comb_r_electron_n")->Fill(electron_z0); 
                    hist("h_eta_eff_HLT_el26_comb_r_electron_n")->Fill(electron_eta); 
                }
                if (fabs(electron_d0) > 3. ) hist("h_pt_eff_HLT_el26_comb_r_electron_n")->Fill(electron_pt);
            
            }
            // end trig eff wrt offline electron
        }
        


        // Matching checks
        if (HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed && false)
        //if (HLT_e26_lhtight_ivarloose_L1eEM26M_passed)
        {
            
            std::vector<const xAOD::Electron*> hlt_el_parts = getElectronChainFeatures("HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M", true); 
            //std::vector<const xAOD::Electron*> hlt_el_parts = getElectronChainFeatures("HLT_e26_lhtight_ivarloose_L1eEM26M", false); 
            const xAOD::Electron* el_save = nullptr;
            float mindr = 9999.;

            const xAOD::Electron* el_save_clust = nullptr;
            float mindr_clust = 9999.;

            const xAOD::Electron* el_save_clustBE = nullptr;
            float mindr_clustBE = 9999.;

            for (const xAOD::Electron* hlt_el_part : hlt_el_parts)
            {
                if (getDeltaR(electron, hlt_el_part) <  mindr)
                {
                    mindr = getDeltaR(electron, hlt_el_part);
                    el_save = hlt_el_part;

                }

                if (getDeltaR(electron->caloCluster(), hlt_el_part->caloCluster()) <  mindr_clust)
                {
                    mindr_clust = getDeltaR(electron->caloCluster(), hlt_el_part->caloCluster());
                    el_save_clust = hlt_el_part;
                }

                // cluster BE(2)
                if (getDeltaR(electron->caloCluster()->etaBE(2), electron->caloCluster()->phiBE(2), hlt_el_part->caloCluster()->etaBE(2), hlt_el_part->caloCluster()->phiBE(2)  ) <  mindr_clustBE )
                {
                    mindr_clustBE = getDeltaR(electron->caloCluster()->etaBE(2), electron->caloCluster()->phiBE(2), hlt_el_part->caloCluster()->etaBE(2), hlt_el_part->caloCluster()->phiBE(2)  );
                    el_save_clustBE = hlt_el_part;                
                }                

            }

            if (el_save != nullptr)
            {
                hist("h_deltaR_hlt_elec_reco_elec")->Fill(mindr);
                if (mindr < 0.07)
                {
                    hist("h_deltad0_hlt_elec_reco_elec_drcut")->Fill(electron->trackParticle()->d0() - el_save->trackParticle()->d0());
                    hist("h_deltapt_hlt_elec_reco_elec_drcut")->Fill( (electron->pt() - el_save->pt())/1000.);
                    hist("h_deltaE_hlt_elec_reco_elec_drcut")->Fill( (electron->e() - el_save->e())/1000.);
                }
                hist("h_deltad0_hlt_elec_reco_elec")->Fill(electron->trackParticle()->d0() - el_save->trackParticle()->d0());
                hist("h_deltapt_hlt_elec_reco_elec")->Fill( (electron->pt() - el_save->pt())/1000.);
                hist("h_deltaE_hlt_elec_reco_elec")->Fill( (electron->e() - el_save->e())/1000.);
            }

            if (el_save_clust != nullptr && !m_isDAOD)
            {
                hist("h_deltaR_clust_hlt_elec_reco_elec")->Fill(mindr_clust);
                if (m_debug && (mindr_clust > 0.5 || ei->eventNumber() == 21811)) 
                {
                    std::cout<<"electron dr cluster large mindr/mindrclust: "<<mindr <<"/"<<mindr_clust<< "   n hlt electrons= " <<hlt_el_parts.size()<<"   n electrons= " <<m_Electrons.size()<<std::endl;
                    std::cout<<"el_save "<<el_save<<"   el_save_clust "<<el_save_clust<<std::endl;
                    std::cout<<"el pt:            "<<electron->pt() <<"; el eta "<<electron->eta()<<"; el phi "<<electron->phi()<<"; el clust e "<<electron->caloCluster()->e()<<"; el clust eta "<<electron->caloCluster()->eta0()<<"; el clust phi "<<electron->caloCluster()->phi0()<<std::endl;
                    std::cout<<"el_save pt:       "<<el_save->pt() <<"; el_save eta "<<el_save->eta()<<"; el_save phi "<<el_save->phi()<<"; el_save clust e "<<el_save->caloCluster()->e()  <<"; el_save clust eta "<<el_save->caloCluster()->eta0()<<"; el_save clust phi "<<el_save->caloCluster()->phi0()<<std::endl;
                    std::cout<<"el_save_clust pt: "<<el_save_clust->pt() <<"; el_save_clust eta "<<el_save_clust->eta()<<"; el_save_clust phi "<<el_save_clust->phi()<<"; el_save_clust clust e "<<el_save_clust->caloCluster()->e() <<"; el_save_clust eta "<<el_save_clust->caloCluster()->eta0()<<"; el_save_clust phi "<<el_save_clust->caloCluster()->phi0()<<std::endl;

                    

                }
                if (mindr_clust < 0.07)
                {
                    hist("h_deltad0_clust_hlt_elec_reco_elec_drcut")->Fill(electron->trackParticle()->d0() - el_save_clust->trackParticle()->d0());
                    hist("h_deltapt_clust_hlt_elec_reco_elec_drcut")->Fill( (electron->pt() - el_save_clust->pt())/1000.);
                    hist("h_deltaE_clust_hlt_elec_reco_elec_drcut")->Fill( (electron->e() - el_save_clust->e())/1000.);
                }
                hist("h_deltad0_clust_hlt_elec_reco_elec")->Fill(electron->trackParticle()->d0() - el_save_clust->trackParticle()->d0());
                hist("h_deltapt_clust_hlt_elec_reco_elec")->Fill( (electron->pt() - el_save_clust->pt())/1000.);
                hist("h_deltaE_clust_hlt_elec_reco_elec")->Fill( (electron->e() - el_save_clust->e())/1000.);
            }

            if (el_save_clustBE != nullptr)
            {
                hist("h_deltaR_clustBE_hlt_elec_reco_elec")->Fill(mindr_clustBE);
            }

        }

    }
    //////// End Electrons Loop //////////////////

    //////////////////////
    // Muons Loop
    for (const xAOD::Muon *muon : m_Muons)
    {
        double muonProdR = getProdR(muon->primaryTrackParticle());
        double muon_d0 = muon->primaryTrackParticle()->d0();
        double muon_z0 = muon->primaryTrackParticle()->z0();
        double muon_pt = muon->pt()*0.001;
        double muon_eta = muon->eta();
        double muon_phi = muon->phi();

        hist("h_muon_pt")->Fill(muon_pt);
        hist("h_muon_eta")->Fill(muon_eta);
        hist("h_muon_auth")->Fill(muon->author());
        hist("h_muon_d0")->Fill( fabs(muon_d0) );
        if (!m_isDAOD) hist("h_muon_isLRT")->Fill( muon->primaryTrackParticle()->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) );

        // Matching checks
        if (HLT_mu20_LRT_d0loose_L1MU14FCH_Passed && !m_isDAOD && false)
        {

            std::vector<const xAOD::Muon*> hlt_mu_parts = getMuonChainFeatures("HLT_mu20_LRT_d0loose_L1MU14FCH", true); 
            //std::vector<const xAOD::TrackParticle*> hlt_mu_trk_parts = getMuonChainIDTrackFeatures("HLT_mu20_LRT_d0loose_L1MU14FCH", true);
            //std::vector<const xAOD::TrackParticle*> hlt_mu_trk_parts_test = getMuonChainIDTrackFeatures("HLT_mu24_ivarmedium_L1MU14FCH", false);

            // match track to muon
            //std::map<const xAOD::Muon*, const xAOD::TrackParticle*> hlt_muon_to_track = matchCollections_dR(hlt_mu_parts, hlt_mu_trk_parts, m_matchSizeR);


            const xAOD::Muon* mu_save = nullptr;
            float mindr = 9999.;

            const xAOD::Muon* mu_save_ms = nullptr;
            float mindr_ms = 9999.;
            for (const xAOD::Muon* hlt_mu_part : hlt_mu_parts)
            {

                if (getDeltaR(muon, hlt_mu_part) <  mindr)
                {
                    mindr = getDeltaR(muon, hlt_mu_part);
                    mu_save = hlt_mu_part;

                }

                if (getDeltaR(muon->extrapolatedMuonSpectrometerTrackParticleLink(), hlt_mu_part->extrapolatedMuonSpectrometerTrackParticleLink()) <  mindr_ms)
                {
                    mindr_ms = getDeltaR(muon->extrapolatedMuonSpectrometerTrackParticleLink(), hlt_mu_part->extrapolatedMuonSpectrometerTrackParticleLink());
                    mu_save_ms = hlt_mu_part;
                }

            }

            if (mu_save != nullptr)
            {
                hist("h_deltaR_hlt_muon_reco_muon")->Fill(mindr);
                if (mindr < 0.1)
                {
                    if (muon->primaryTrackParticle() == nullptr)           std::cout << "WARNING: offline muon missing primary track particle!"<<std::endl;
                    if (mu_save->primaryTrackParticle() == nullptr)   std::cout << "WARNING: hlt muon missing primary track particle!"<<std::endl;
                    
                    if (mu_save->primaryTrackParticle())  hist("h_deltad0_hlt_muon_reco_muon_drcut")->Fill(muon_d0 - mu_save->primaryTrackParticle()->d0());
                    hist("h_deltapt_hlt_muon_reco_muon_drcut")->Fill( (muon->pt() - mu_save->pt())/1000.);
                }
                if (mu_save->primaryTrackParticle())  hist("h_deltad0_hlt_muon_reco_muon")->Fill(muon_d0 - mu_save->primaryTrackParticle()->d0());
                hist("h_deltapt_hlt_muon_reco_muon")->Fill( (muon->pt() - mu_save->pt())/1000.);

                if (m_debug || mindr > 0.1)
                {
                    
                    std::cout<<"muon dr large mindr/mindr_ms: "<<mindr <<"/"<<mindr_ms<< "   n hlt muons= " <<hlt_mu_parts.size()<<"   n muons= " <<m_Muons.size()<<std::endl;
                    std::cout<<"mu_save "<<mu_save<<"   mu_save_clust "<<mu_save_ms<<std::endl;

                    std::cout<<"mu pt:            "<<muon->pt() <<"; mu eta "<<muon_eta<<"; mu phi "<<muon_phi;
                    auto muon_ms_link = muon->extrapolatedMuonSpectrometerTrackParticleLink();
                    if (muon_ms_link.isValid())
                    {
                        auto muon_ms = (*muon_ms_link);
                        std::cout<<"; mu extrap pt "<<muon_ms->pt()/1000.<<"; mu extrap eta "<<muon_ms->eta()<<"; mu extrap phi "<<muon_ms->phi()<<std::endl;
                    } else std::cout<<std::endl;
                    
                    std::cout<<"mu_save pt:            "<<mu_save->pt() <<"; mu eta "<<mu_save->eta()<<"; mu phi "<<mu_save->phi();
                    auto muon_save_ms_link = mu_save->extrapolatedMuonSpectrometerTrackParticleLink();
                    if (muon_save_ms_link.isValid())
                    {
                        auto muon_ms = (*muon_save_ms_link);
                        std::cout<<"; mu_save extrap pt "<<muon_ms->pt()/1000.<<"; mu extrap eta "<<muon_ms->eta()<<"; mu extrap phi "<<muon_ms->phi()<<std::endl;
                    } else std::cout<<std::endl;
                    
                }
            }

            if (mu_save_ms != nullptr)
            {
                hist("h_deltaR_ms_hlt_muon_reco_muon")->Fill(mindr_ms);
                if (mindr_ms < 0.1)
                {
                    //if (muon->primaryTrackParticle() == nullptr)           std::cout << "WARNING: offline muon missing primary track particle!"<<std::endl;
                    //else if (mu_save_ms->primaryTrackParticle() == nullptr)   std::cout << "WARNING: hlt muon missing primary track particle!"<<std::endl;
                    if (mu_save_ms->primaryTrackParticle()) hist("h_deltad0_ms_hlt_muon_reco_muon_drcut")->Fill(muon_d0 - mu_save_ms->primaryTrackParticle()->d0());
                    hist("h_deltapt_ms_hlt_muon_reco_muon_drcut")->Fill( (muon->pt() - mu_save_ms->pt())/1000.);
                }
                if (mu_save_ms->primaryTrackParticle()) hist("h_deltad0_ms_hlt_muon_reco_muon")->Fill(muon_d0 - mu_save_ms->primaryTrackParticle()->d0());
                hist("h_deltapt_ms_hlt_muon_reco_muon")->Fill( (muon->pt() - mu_save_ms->pt())/1000.);

                
            }

        }

        if (L1_MU14FCH_Passed) 
        {
            // muon trigger eff wrt offline muons
            // denominators
            if (muon_pt >20 && fabs(muon_d0) > 2. )
            {
                hist("h_prodR_eff_HLT_mu24_LRTloose_r_muon_d")->Fill(muonProdR);
                hist("h_prodR_eff_HLT_mu24_r_muon_d")->Fill(muonProdR);
                hist("h_prodR_eff_HLT_mu24_comb_r_muon_d")->Fill(muonProdR);

                hist("h_z0_eff_HLT_mu24_LRTloose_r_muon_d")->Fill(muon_z0);
                hist("h_z0_eff_HLT_mu24_r_muon_d")->Fill(muon_z0);
                hist("h_z0_eff_HLT_mu24_comb_r_muon_d")->Fill(muon_z0);

                hist("h_eta_eff_HLT_mu24_LRTloose_r_muon_d")->Fill(muon_eta);
                hist("h_eta_eff_HLT_mu24_r_muon_d")->Fill(muon_eta);
                hist("h_eta_eff_HLT_mu24_comb_r_muon_d")->Fill(muon_eta);

            }

            if (fabs(muon_d0) > 2. )
            {
                hist("h_pt_eff_HLT_mu24_LRTloose_r_muon_d")->Fill(muon_pt);
                hist("h_pt_eff_HLT_mu24_r_muon_d")->Fill(muon_pt);
                hist("h_pt_eff_HLT_mu24_comb_r_muon_d")->Fill(muon_pt);
            }

            if (muon_pt >20 )
            {
                hist("h_d0_eff_HLT_mu24_r_muon_d")->Fill(muon_d0);
                hist("h_d0_eff_HLT_mu24_LRTloose_r_muon_d")->Fill(muon_d0);
                hist("h_d0_eff_HLT_mu24_comb_r_muon_d")->Fill(muon_d0);
                hist("h_d0_zoom_eff_HLT_mu24_r_muon_d")->Fill(muon_d0);
                hist("h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon_d")->Fill(muon_d0);
                hist("h_d0_zoom_eff_HLT_mu24_comb_r_muon_d")->Fill(muon_d0);                  
            }




            bool fillOR = false;
            // numerators
            if (m_trigMatchTool->match(*muon, "HLT_mu24_ivarmedium_L1MU14FCH")) // matched to roi muon track
            {
                if (HLT_mu24_ivarmedium_L1MU14FCH_passed)
                {
                    if (muon_pt >20 )
                    {
                        hist("h_d0_eff_HLT_mu24_r_muon_n")->Fill(muon_d0);
                        hist("h_d0_zoom_eff_HLT_mu24_r_muon_n")->Fill(muon_d0);                            
                    }
                    if (muon_pt >20 && fabs(muon_d0) > 2. ) {
                        hist("h_prodR_eff_HLT_mu24_r_muon_n")->Fill(muonProdR);                        
                        hist("h_z0_eff_HLT_mu24_r_muon_n")->Fill(muon_z0);                        
                        hist("h_eta_eff_HLT_mu24_r_muon_n")->Fill(muon_eta);                        
                    }
                    if (fabs(muon_d0) > 2. ) hist("h_pt_eff_HLT_mu24_r_muon_n")->Fill(muon_pt);

                    fillOR = true;
                }
            }

            if (m_trigMatchTool->match(*muon, "HLT_mu20_LRT_d0loose_L1MU14FCH")) // matched to roi LRT muon track
            {
                if (HLT_mu20_LRT_d0loose_L1MU14FCH_Passed)
                {
                    if (muon_pt >20 )
                    {
                        hist("h_d0_eff_HLT_mu24_LRTloose_r_muon_n")->Fill(muon_d0);
                        hist("h_d0_zoom_eff_HLT_mu24_LRTloose_r_muon_n")->Fill(muon_d0);
                    }
                    if (muon_pt >20 && fabs(muon_d0) > 2. ) {
                        hist("h_prodR_eff_HLT_mu24_LRTloose_r_muon_n")->Fill(muonProdR);
                        hist("h_z0_eff_HLT_mu24_LRTloose_r_muon_n")->Fill(muon_z0);
                        hist("h_eta_eff_HLT_mu24_LRTloose_r_muon_n")->Fill(muon_eta);
                    }
                    if (fabs(muon_d0) > 2. ) hist("h_pt_eff_HLT_mu24_LRTloose_r_muon_n")->Fill(muon_pt);

                    fillOR = true;
                }
            }

            if (fillOR) // matched to roi muon track , OR lrt track
            {
                if (muon_pt >20 )
                {
                    hist("h_d0_eff_HLT_mu24_comb_r_muon_n")->Fill(muon_d0);
                    hist("h_d0_zoom_eff_HLT_mu24_comb_r_muon_n")->Fill(muon_d0);
                        
                } 
                if (muon_pt >20 && fabs(muon_d0) > 2. ) {
                    hist("h_prodR_eff_HLT_mu24_comb_r_muon_n")->Fill(muonProdR); 
                    hist("h_z0_eff_HLT_mu24_comb_r_muon_n")->Fill(muon_z0); 
                    hist("h_eta_eff_HLT_mu24_comb_r_muon_n")->Fill(muon_eta); 
                }
                if (fabs(muon_d0) > 2. ) hist("h_pt_eff_HLT_mu24_comb_r_muon_n")->Fill(muon_pt);
        
            }
            // end eff calc wrt offline muons
        }
    } // end muons loop



    hist("h_nTracks_offline")->Fill(m_OfflineTracks.size());
    hist("h_nTracks_offlineLRT")->Fill(n_offlineLRT_Tracks);
    if (m_debug) std::cout<<"Number of signal tracks in event = " <<m_OfflineTracksSignal.size()<<std::endl;


    ////// Cutflow filling based on Offline tracks
    m_cutflow(__LINE__, "Offline Track", m_OfflineTracks.size() > 0);
    m_cutflow(__LINE__, "Offline SUSY EL Track", passedSUSYTrackEL);
    m_cutflow(__LINE__, "Offline SUSY MU Track", passedSUSYTrackMU);
    m_cutflow(__LINE__, "Offline SUSY Other Track", passedSUSYTrackOther);


    m_cutflow(__LINE__, "High d0 Offline SUSY EL Track", passedHighD0SUSYTrackEL);
    m_cutflow(__LINE__, "High d0 Offline SUSY MU Track", passedHighD0SUSYTrackMU);
    m_cutflow(__LINE__, "High d0 Offline SUSY Other Track", passedHighD0SUSYTrackOther);
    m_cutflow(__LINE__, "Passed Disp Lep Triggers and Hd0 Off SUSY Track", (passedHighD0SUSYTrackOther || passedHighD0SUSYTrackEL || passedHighD0SUSYTrackMU) && passDisplacedLepTriggers );


    // Tracks in RoI 
    m_cutflow(__LINE__,"-------");
    m_cutflow(__LINE__, "Offline Signal Track in EM ROI", hasTrackInROI_em );
    m_cutflow(__LINE__, "Offline Signal Track in MU ROI", hasTrackInROI_mu);
    m_cutflow(__LINE__, "High d0 Off Sig Trk in EM ROI",  hasTrackInROI_em && passedHighD0SUSYTrackEL);
    m_cutflow(__LINE__, "High d0 Off Sig Trk in MU ROI", hasTrackInROI_mu && passedHighD0SUSYTrackMU);


    m_cutflow(__LINE__, "High d0 OffSig Trk w/FTF in EM ROI",  hasTrackInROI_em && hasMatchedFTF_em && passedHighD0SUSYTrackEL );
    m_cutflow(__LINE__, "High d0 OffSig Trk w/FTF in EM ROI && HLT_g25_loose_L1EM20VH",  hasTrackInROI_em && hasMatchedFTF_em && passedHighD0SUSYTrackEL && HLT_g25_loose_L1EM20VH_Passed );
    m_cutflow(__LINE__, "High d0 OffSig Trk w/FTF in EM ROI && HLT_g35_loose_L1eEM26M",  hasTrackInROI_em && hasMatchedFTF_em && passedHighD0SUSYTrackEL && HLT_g35_loose_L1eEM26M_Passed );
    m_cutflow(__LINE__, "High d0 OffSig Trk w/FTF in MU ROI",  hasTrackInROI_mu && hasMatchedFTF_mu && passedHighD0SUSYTrackMU );
    m_cutflow(__LINE__, "High d0 OffSig Trk w/FTF in MU ROI mu-matched",  hasTrackInROI_mu && hasMatchedFTF_mu && passedHighD0SUSYTrackMU && hasMatchedMuon_mu );
    m_cutflow(__LINE__, "High d0 OffSig Trk w/FTF in MU ROI && HLT_mu20_msonly_Passed",  hasTrackInROI_mu && hasMatchedFTF_mu && passedHighD0SUSYTrackMU && HLT_mu20_msonly_Passed );


    // Tracks in RoI and displaced lep triggers didn't fire
    m_cutflow(__LINE__,"--------");
    m_cutflow(__LINE__, "Offline Signal Track in EM ROI and !dispLepTrig",  hasTrackInROI_em && !passDisplacedLepTriggers);
    m_cutflow(__LINE__, "Offline Signal Track in MU ROI and !dispLepTrig",  hasTrackInROI_mu && !passDisplacedLepTriggers);
    m_cutflow(__LINE__, "High d0 Off Sig Trk in EM ROI && !dispLepTrig",  hasTrackInROI_em  && !passDisplacedLepTriggers && passedHighD0SUSYTrackEL );
    m_cutflow(__LINE__, "High d0 Off Sig Trk in MU ROI && !dispLepTrig",  hasTrackInROI_mu && !passDisplacedLepTriggers && passedHighD0SUSYTrackMU );

    m_cutflow(__LINE__, "High d0 OffSig Trk w/FTF in EM ROI && !dispLepTrig",  hasTrackInROI_em && hasMatchedFTF_em  && passedHighD0SUSYTrackEL && !passDisplacedLepTriggers);
    m_cutflow(__LINE__, "High d0 OffSig Trk w/FTF in MU ROI && !dispLepTrig",hasTrackInROI_mu && hasMatchedFTF_mu  && passedHighD0SUSYTrackMU && !passDisplacedLepTriggers);
     
    m_cutflow(__LINE__, "High d0 OffSig Trk w/FTF in EM ROI && !dispLepTrig && !HLT EL pass",hasTrackInROI_em  && !passDisplacedLepTriggers && passedHighD0SUSYTrackEL && !HLT_EL_chainPassed && hasMatchedFTF_em);
    m_cutflow(__LINE__, "High d0 OffSig Trk W/FTF in MU ROI && !dispLepTrig && !HLT MU pass", hasTrackInROI_mu && !passDisplacedLepTriggers && passedHighD0SUSYTrackMU && !HLT_MU_chainPassed && hasMatchedFTF_mu);
    m_cutflow(__LINE__, "High d0 OffSig Trk W/FTF in MU ROI matched-muon && !dispLepTrig && !HLT MU pass", hasTrackInROI_mu && !passDisplacedLepTriggers && passedHighD0SUSYTrackMU && !HLT_MU_chainPassed && hasMatchedFTF_mu && hasMatchedMuon_mu);
    m_cutflow(__LINE__,"---------");


    // Cutflow filling based on FTF tracks
    m_cutflow(__LINE__, "High d0 FTF Track in EM ROI", passedHighD0FTFTrackInROI_EM);
    m_cutflow(__LINE__, "High d0 FTF Track in MU ROI", passedHighD0FTFTrackInROI_MU);


    m_cutflow(__LINE__, "High d0 FTF Track in EM ROI && HLT_g25_loose_L1EM20VH", passedHighD0FTFTrackInROI_EM && HLT_g25_loose_L1EM20VH_Passed);
    m_cutflow(__LINE__, "High d0 FTF Track in EM ROI && HLT_g35_loose_L1eEM26M", passedHighD0FTFTrackInROI_EM && HLT_g35_loose_L1eEM26M_Passed);
    m_cutflow(__LINE__, "High d0 FTF Track in EM ROI && HLT_e26_etcut_L1eEM26M", passedHighD0FTFTrackInROI_EM && HLT_e26_etcut_L1eEM26M_Passed);
    m_cutflow(__LINE__, "High d0 FTF Track in MU ROI and HLT_mu20_msonly", passedHighD0FTFTrackInROI_MU && HLT_mu20_msonly_Passed);
    m_cutflow(__LINE__, "High d0 FTF Track in MU ROI matched to muon", passedHighD0FTFTrackInROI_MU && HighD0FTFMatchMuon_MU);

    m_cutflow(__LINE__, "High d0 FTF Track in EM ROI && !dispLepTrig && !HLT EL pass", passedHighD0FTFTrackInROI_EM && !HLT_EL_chainPassed && !passDisplacedLepTriggers);
    m_cutflow(__LINE__, "High d0 FTF Track in MU ROI && !dispLepTrig && !HLT MU pass", passedHighD0FTFTrackInROI_MU && !HLT_MU_chainPassed && !passDisplacedLepTriggers);


    // New mu lrt Triggers
    m_cutflow(__LINE__,"----------");
    m_cutflow(__LINE__, "HLT_mu6_msonly_L1MU6 && high d0 off signal", HLT_mu6_msonly_L1MU6_Passed && passedHighD0SUSYTrackMU);
    m_cutflow(__LINE__,"----------");
    m_cutflow(__LINE__, "mu24_L1MU14FCH_ivarm", HLT_mu24_ivarmedium_L1MU14FCH_passed);
    m_cutflow(__LINE__, "mu24_L1MU14FCH_ivarm && high d0 off signal", HLT_mu24_ivarmedium_L1MU14FCH_passed && passedHighD0SUSYTrackMU);

    m_cutflow(__LINE__, "mu24_LRT_pt d0loose", HLT_mu24_LRT_d0loose_L1MU14FCH_passed);
    m_cutflow(__LINE__, "mu24_LRT_pt d0loose && !mu24", HLT_mu24_LRT_d0loose_L1MU14FCH_passed && !HLT_mu24_ivarmedium_L1MU14FCH_passed);
    m_cutflow(__LINE__, "mu24_LRT_pt d0loose && high d0 off signal", HLT_mu24_LRT_d0loose_L1MU14FCH_passed && passedHighD0SUSYTrackMU);
    m_cutflow(__LINE__, "mu24_LRT_pt d0loose && high d0 off signal && !mu24", HLT_mu24_LRT_d0loose_L1MU14FCH_passed && passedHighD0SUSYTrackMU && !HLT_mu24_ivarmedium_L1MU14FCH_passed);

    m_cutflow(__LINE__, "mu24_LRT_pt d0medium", HLT_mu24_LRT_d0medium_L1MU14FCH_passed);
    m_cutflow(__LINE__, "mu24_LRT_pt d0medium && !mu24", HLT_mu24_LRT_d0medium_L1MU14FCH_passed && !HLT_mu24_ivarmedium_L1MU14FCH_passed);
    m_cutflow(__LINE__, "mu24_LRT_pt d0medium && high d0 off signal", HLT_mu24_LRT_d0medium_L1MU14FCH_passed && passedHighD0SUSYTrackMU);
    m_cutflow(__LINE__, "mu24_LRT_pt d0medium && high d0 off signal && !mu24", HLT_mu24_LRT_d0medium_L1MU14FCH_passed && passedHighD0SUSYTrackMU && !HLT_mu24_ivarmedium_L1MU14FCH_passed);

    m_cutflow(__LINE__, "mu24_LRT_pt d0tight", HLT_mu24_LRT_d0tight_L1MU14FCH_passed);
    m_cutflow(__LINE__, "mu24_LRT_pt d0tight && high d0 off signal", HLT_mu24_LRT_d0tight_L1MU14FCH_passed && passedHighD0SUSYTrackMU);
    m_cutflow(__LINE__, "mu24_LRT_pt d0tight && high d0 off signal && !mu24", HLT_mu24_LRT_d0tight_L1MU14FCH_passed && passedHighD0SUSYTrackMU && !HLT_mu24_ivarmedium_L1MU14FCH_passed);


    // Test muon lrt triggers
    m_cutflow(__LINE__, "mu20_LRT_pt d0loose", HLT_mu20_LRT_d0loose_L1MU14FCH_Passed);
    m_cutflow(__LINE__, "mu20_LRT_pt d0loose && high d0 off signal", HLT_mu20_LRT_d0loose_L1MU14FCH_Passed && passedHighD0SUSYTrackMU);
    m_cutflow(__LINE__, "mu20_LRT_pt d0loose && high d0 off signal && !mu24", HLT_mu20_LRT_d0loose_L1MU14FCH_Passed && passedHighD0SUSYTrackMU && !HLT_mu24_ivarmedium_L1MU14FCH_passed);
    
    m_cutflow(__LINE__, "mu20_LRT_pt d0tight", HLT_mu20_LRT_d0tight_L1MU14FCH_Passed);
    m_cutflow(__LINE__, "mu20_LRT_pt d0tight && high d0 off signal", HLT_mu20_LRT_d0tight_L1MU14FCH_Passed && passedHighD0SUSYTrackMU);
    m_cutflow(__LINE__, "mu20_LRT_pt d0tight && high d0 off signal && !mu24", HLT_mu20_LRT_d0tight_L1MU14FCH_Passed && passedHighD0SUSYTrackMU && !HLT_mu24_ivarmedium_L1MU14FCH_passed);

    // New el lrt triggers 
    m_cutflow(__LINE__,"-----------");
    m_cutflow(__LINE__, "e26_lht", HLT_e26_lhtight_ivarloose_L1eEM26M_passed);
    m_cutflow(__LINE__, "e26_lht && high d0 off signal", HLT_e26_lhtight_ivarloose_L1eEM26M_passed && passedHighD0SUSYTrackEL);

    m_cutflow(__LINE__,"--");
    m_cutflow(__LINE__, "el26_LRTtight lhloose_nopix", HLT_e26_lhloose_nopix_lrttight_L1eEM26M_passed);
    m_cutflow(__LINE__, "el26_LRTtight lhloose_nopix && high d0 off signal", HLT_e26_lhloose_nopix_lrttight_L1eEM26M_passed && passedHighD0SUSYTrackEL);
    m_cutflow(__LINE__, "el26_LRTtight lhloose_nopix && hd0 offsig !e26", HLT_e26_lhloose_nopix_lrttight_L1eEM26M_passed && passedHighD0SUSYTrackEL && !HLT_e26_lhtight_ivarloose_L1eEM26M_passed);

    m_cutflow(__LINE__, "el26_LRTtight lhmedium_nopix", HLT_e26_lhmedium_nopix_lrttight_L1eEM26M_passed);
    m_cutflow(__LINE__, "el26_LRTtight lhmedium_nopix && high d0 off signal", HLT_e26_lhmedium_nopix_lrttight_L1eEM26M_passed && passedHighD0SUSYTrackEL);
    m_cutflow(__LINE__, "el26_LRTtight lhmedium_nopix && hd0 offsig !e26", HLT_e26_lhmedium_nopix_lrttight_L1eEM26M_passed && passedHighD0SUSYTrackEL && !HLT_e26_lhtight_ivarloose_L1eEM26M_passed);

    m_cutflow(__LINE__,"--pt/d0 check--");
    m_cutflow(__LINE__, "el30_LRTmedium lhloose_nopix", HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed);
    m_cutflow(__LINE__, "el30_LRTmedium lhloose_nopix && high d0 off signal", HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed && passedHighD0SUSYTrackEL);
    m_cutflow(__LINE__, "el30_LRTmedium lhloose_nopix && hd0 offsig !e26", HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed && passedHighD0SUSYTrackEL && !HLT_e26_lhtight_ivarloose_L1eEM26M_passed);


    // // msonly trigger validation
    m_cutflow(__LINE__,"--msonly");
    m_cutflow(__LINE__, ">0 reco muons pt>60 eta<1.05", m_Muons.size() > 0 && muon_pt60_eta105);
    m_cutflow(__LINE__, ">0 reco muons pt>60 eta<1.05 && L1MU14FCH", m_Muons.size() > 0 && muon_pt60_eta105 && L1_MU14FCH_Passed);
    m_cutflow(__LINE__, ">0 reco muons pt>60 eta<1.05 && mu60msonly", m_Muons.size() > 0 && muon_pt60_eta105 && HLT_mu60_0eta105_msonly_L1MU14FCH_passed);
    m_cutflow(__LINE__, ">0 reco muons pt>60 eta<1.05 && mu60msonly match", m_Muons.size() > 0 && muon_pt60_eta105 && muon_matches_HLT_mu60_0eta105_msonly_L1MU14FCH);

    m_cutflow(__LINE__, "2 reco muons pt>50", m_Muons.size() > 1 && m_Muons[1]->pt()*0.001 > 50);
    m_cutflow(__LINE__, "2 reco muons pt>50 && L1MU14FCH", m_Muons.size() > 1 && m_Muons[1]->pt()*0.001 > 50 && L1_MU14FCH_Passed);
    m_cutflow(__LINE__, "2 reco muons && hlt_2mu50 passed", m_Muons.size() > 1 && m_Muons[1]->pt()*0.001 > 50 && HLT_2mu50_msonly_L1MU14FCH_passed);
    m_cutflow(__LINE__, "2 reco muons && hlt_2mu50 passed match", m_Muons.size() > 1 && m_Muons[1]->pt()*0.001 > 50 && muon_matches_HLT_2mu50_msonly_L1MU14FCH);

    m_cutflow(__LINE__, "1 reco el 1", m_Electrons.size() > 0 && m_Electrons[0]->pt()*0.001 > 40);
    m_cutflow(__LINE__, "1 reco el 1 && hlt_g40", m_Electrons.size() > 0 && m_Electrons[0]->pt()*0.001 > 40 && HLT_g40_loose_L1EM20VH_passed);

    m_cutflow(__LINE__, "+1 reco mu pt>40", m_Muons.size() > 0 && m_Electrons.size() > 0 && m_Muons[0]->pt()*0.001 > 40 && m_Electrons[0]->pt()*0.001 > 40 && HLT_g40_loose_L1EM20VH_passed);
    m_cutflow(__LINE__, "+L1MU14FCH", m_Muons.size() > 0 && m_Electrons.size() > 0 && m_Muons[0]->pt()*0.001 > 40 && m_Electrons[0]->pt()*0.001 > 40 && L1_MU14FCH_Passed && HLT_g40_loose_L1EM20VH_passed);
    m_cutflow(__LINE__, "+hlt_g40mu40 passed", m_Muons.size() > 0 && m_Electrons.size() > 0 && m_Muons[0]->pt()*0.001 > 40 && m_Electrons[0]->pt()*0.001 > 40 && HLT_g40_loose_mu40_msonly_L1MU14FCH_passed);



    /////////////////////////
    // Truth particle loop
    ////////////////////////
    //findTruthParticles(truthParticles, {211, 11,  13}, m_signalParentPDGIDs , status, selectTracks)
    //if (m_debug) findTruthParticles(truthParticles, {11,13}, m_signalParentPDGIDs, {} , 0, false);


    if (m_makeNtuple)
    {
        truthSignal_pt->clear();
        truthSignal_eta->clear();
        truthSignal_phi->clear();
        truthSignal_d0->clear();
        truthSignal_pdgid->clear();
        truthSignal_motherpdgid->clear();
    }

    double minDRtruth_em = 999;
    double minDRtruth_mu = 999;
    double minDEtatruth_em = 999;
    double minDEtatruth_mu = 999;
    double minDPhitruth_em = 999;
    double minDPhitruth_mu = 999;

    bool hasTruthMatchedFTF = false;
    bool hasTruthMatchedFTF_highd0 = false;

    if (m_isSimulation && m_doTruth)
    {
        // Containers to save electrons and muons
        std::vector<const xAOD::TruthParticle*> truthElectrons;
        std::vector<const xAOD::TruthParticle*> truthMuons;


        // Truth particle loop with specific parent
        for (const xAOD::TruthParticle *part : m_TruthParticlesSignal)
        {     
            // msonly validation info from Truth classifier info (could comment later to spee dup)
            MCTruthClassifier::Info info;
            std::pair<unsigned int, unsigned int> res;
            ParticleDef partDef;
            res = m_truthClassifier->particleTruthClassifier(part, &info);
            unsigned int iTypeOfPart = res.first;
            //unsigned int iPartOrig   = res.second;
            //ParticleOutCome iPartOutCome = info.particleOutCome;


            double part_d0 = getTruthPartD0_extrap(part);
            double part_prodR = getProdR(part);
            double part_prodZ = getProdZ(part);
            int part_pdgid = abs(part->pdgId());
            double part_eta = getTruthEta(part);
            double part_phi = getTruthPhi(part);

            // Check if an offline track is matched to this particle
            bool offlineMatched = false;
            if (m_debug) std::cout<<"part = "<<part->pt()<<std::endl;

            for (const xAOD::TrackParticle *offSigTrack : m_OfflineTracksSignal)
            {
                if ( (abs(offlineTrackIsSignal[offSigTrack]->pt() - part->pt() ) / part->pt() ) < 0.01 ) 
                {
                    offlineMatched = true;
                    break;
                }
            }


            if (m_makeNtuple)
            {
                truthSignal_pt->push_back(part->pt()*0.001);
                truthSignal_eta->push_back( part_eta );
                truthSignal_phi->push_back( part_phi );
                truthSignal_d0->push_back(part_d0);
                truthSignal_pdgid->push_back(part->pdgId());
            
                //m_truthClassifier->particleTruthClassifier(part);
                truthSignal_motherpdgid->push_back(part->parent()->pdgId() );
            }

            hist("h_pt_truthSignal")->Fill(part->pt() *0.001 );
            hist("h_d0_truthSignal")->Fill( fabs(part_d0) );
            hist("h_eta_truthSignal")->Fill( part_eta );
            hist("h_pdgid_truthSignal")->Fill( part_pdgid );
            hist("h_vtxR_truthSignal")->Fill(part_prodR);
            hist("h_vtxZ_truthSignal")->Fill(part_prodZ);
            if(abs(part_pdgid)==11) hist("h_vtxZ_truthSignal_el")->Fill(part_prodZ);
            else if(abs(part_pdgid)==13) hist("h_vtxZ_truthSignal_mu")->Fill(part_prodZ);

            //hist("h2_truthSignal_d0_v_l1MET")->Fill(fabs(part_d0), l1_met);


            // Offline track eff
            if (m_trackTruth_to_Offline_map.find(part) != m_trackTruth_to_Offline_map.end())
            {
                // Fill numerators
                hist("h_d0_offline_eff_n")->Fill( part_d0 );
                hist("h_pt_offline_eff_n")->Fill(part->pt()*0.001 );
                hist("h_vtxR_offline_eff_n")->Fill(part_prodR);
            }
            // Fill denominators
            hist("h_d0_offline_eff_d")->Fill( part_d0 );
            hist("h_pt_offline_eff_d")->Fill(part->pt()*0.001 );
            hist("h_vtxR_offline_eff_d")->Fill(part_prodR);


            // Match FTF tracks to Truth
            double bestDeltaR = 999;
            const xAOD::TrackParticle *bestMatch = nullptr;

            if (m_trackTruth_to_FTF_map.find(part) != m_trackTruth_to_FTF_map.end())
            {
                bestMatch = m_trackTruth_to_FTF_map[part];
                bestDeltaR = getDeltaR(part_eta, part_phi, bestMatch);
    
                hist("h_deltaR_truthsignal_FTFcomb")->Fill(bestDeltaR);

                // Fill numerators
                hist("h_d0_truth_eff_n")->Fill( part_d0 );
                hist("h_pt_truth_eff_n")->Fill(part->pt()*0.001 );
                hist("h_vtxR_truth_eff_n")->Fill(part_prodR);

                if (part_pdgid==11)
                {
                    hist("h_d0_truth_el_eff_n")->Fill( part_d0 );
                }
                else if (part_pdgid==13)
                {
                    hist("h_d0_truth_mu_eff_n")->Fill( part_d0 );
                }

            }
            // Fill denominators
            hist("h_d0_truth_eff_d")->Fill( part_d0 );
            hist("h_pt_truth_eff_d")->Fill(part->pt()*0.001 );
            hist("h_vtxR_truth_eff_d")->Fill(part_prodR);
            if (part_pdgid==11)
            {
                hist("h_d0_truth_el_eff_d")->Fill( part_d0 );
            }
            else if (part_pdgid==13)
            {
                hist("h_d0_truth_mu_eff_d")->Fill( part_d0 );
            }
            

            hist("h_deltaR_FTF_truthSignal")->Fill(bestDeltaR);
            if (bestMatch != nullptr)  // has ftf match
            {
                hist("h_d0_res_FTF_truthSignal")->Fill( bestMatch->d0() - part_d0  );
                hist("h_pt_res_FTF_truthSignal")->Fill( (bestMatch->pt() - part->pt())*0.001  );
                hist("h_eta_res_FTF_truthSignal")->Fill( getDeltaEta(bestMatch, part)  );
                hist("h_phi_res_FTF_truthSignal")->Fill( getDeltaPhi(bestMatch, part)  );
            }

            bool inEMROI = false;
            bool inMUROI = false;
            // L1 Efficiencies, loop through ROIs          
            if (part_pdgid == 11) // EM ROI
            {
                //if (iTypeOfPart==2 ) truthElectrons.push_back(part);
                truthElectrons.push_back(part); // already from signal decay chain

                hist("h_pt_truth_L1eEM26M_eff_d")->Fill(part->pt()*0.001);
                hist("h_d0_truth_L1eEM26M_eff_d")->Fill(part_d0);
                hist("h_vtxR_truth_L1eEM26M_eff_d")->Fill(part_prodR);

                for (auto em : m_roid_em)
                {
                    double tmp_dR = getDeltaR(part_eta, part_phi, em);
                    if (tmp_dR < m_ROISize) 
                    {
                        if (bestMatch!=nullptr)
                        {
                            hist("h2_truthSignalwFTF_inROI_d0_v_pt_em")->Fill( fabs(part_d0), part->pt()*0.001);
                            //hist("h2_truthSignalwFTF_inROI_d0sig_v_pt_em")->Fill( part_d0sig, part->pt()*0.001);
                        }
                        inEMROI = true;
                    }
                    if (tmp_dR < minDRtruth_em) minDRtruth_em = tmp_dR;
                }

                // Fill histo with closet match
                if (minDRtruth_em < 999) hist("h_deltaR_ROI_truthSignal_em")->Fill(minDRtruth_em);

                // HLT LRT chain efficiency
                if (part->pt()*0.001 > 30 && fabs(part_d0) > 5)
                {
                    hist("h_d0_HLT_el30_LRT_r_truth_eff_d")->Fill(part_d0);
                    hist("h_pt_HLT_el30_LRT_r_truth_eff_d")->Fill(part->pt()*0.001);
                    hist("h_eta_HLT_el30_LRT_r_truth_eff_d")->Fill(part_eta);
                    hist("h_phi_HLT_el30_LRT_r_truth_eff_d")->Fill(part_phi);
                    hist("h_vtxR_HLT_el30_LRT_r_truth_eff_d")->Fill(part_prodR);
                }

                if (part->pt()*0.001 > 30)
                {
                    hist("h_d0_HLT_el26_r_truth_eff_d")->Fill(part_d0);
                    hist("h_pt_HLT_el26_r_truth_eff_d")->Fill(part->pt()*0.001);
                    hist("h_eta_HLT_el26_r_truth_eff_d")->Fill(part_eta);
                    hist("h_phi_HLT_el26_r_truth_eff_d")->Fill(part_phi);
                    hist("h_vtxR_HLT_el26_r_truth_eff_d")->Fill(part_prodR);
                }

                if (HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed && m_trackTruth_to_ELPTLRT_map.find(part) != m_trackTruth_to_ELPTLRT_map.end() && part->pt()*0.001 > 30 && part_d0 > 5) 
                {
                    hist("h_d0_HLT_el30_LRT_r_truth_eff_n")->Fill(part_d0);
                    hist("h_pt_HLT_el30_LRT_r_truth_eff_n")->Fill(part->pt()*0.001);
                    hist("h_eta_HLT_el30_LRT_r_truth_eff_n")->Fill(part_eta);
                    hist("h_phi_HLT_el30_LRT_r_truth_eff_n")->Fill(part_phi);
                    hist("h_vtxR_HLT_el30_LRT_r_truth_eff_n")->Fill(part_prodR);
                }
                if (HLT_e26_lhtight_ivarloose_L1eEM26M_passed && m_trackTruth_to_ELPT_map.find(part) != m_trackTruth_to_ELPT_map.end() && part->pt()*0.001 > 30)
                { 
                    hist("h_d0_HLT_el26_r_truth_eff_n")->Fill(part_d0);
                    hist("h_pt_HLT_el26_r_truth_eff_n")->Fill(part->pt()*0.001);
                    hist("h_eta_HLT_el26_r_truth_eff_n")->Fill(part_eta);
                    hist("h_phi_HLT_el26_r_truth_eff_n")->Fill(part_phi);
                    hist("h_vtxR_HLT_el26_r_truth_eff_n")->Fill(part_prodR);
                }

                

            } else if (part_pdgid == 13) // MU ROI
            {       
                //f (iTypeOfPart==6) truthMuons.push_back(part); // isomuons only
                truthMuons.push_back(part); // already from the signal decay chain

                hist("h_pt_truth_L1MU14FCH_eff_d")->Fill(part->pt()*0.001);
                hist("h_d0_truth_L1MU14FCH_eff_d")->Fill(part_d0);
                hist("h_vtxR_truth_L1MU14FCH_eff_d")->Fill(part_prodR);

                for (auto mu : m_roid_mu)
                {                    
                    double tmp_dR = getDeltaR(part_eta, part_phi, mu);
                    if (tmp_dR < m_ROISize) 
                    {
                        if (bestMatch!=nullptr)
                        {
                            hist("h2_truthSignalwFTF_inROI_d0_v_pt_mu")->Fill( fabs(part_d0), part->pt()*0.001);
                            //hist("h2_truthSignalwFTF_inROI_d0sig_v_pt_mu")->Fill( part_d0sig, part->pt()*0.001);
                        }   
                        inMUROI = true;
                    }
                    if (tmp_dR < minDRtruth_mu) 
                    {
                        minDRtruth_mu = tmp_dR;
                        minDEtatruth_mu = fabs( getDeltaEta(part,mu) );
                        minDPhitruth_mu = fabs( getDeltaPhi(part, mu) );
                    }
                }

                // Fill histo with closet match
                if (minDRtruth_mu < 999)
                {
                    hist("h_deltaR_ROI_truthSignal_mu")->Fill(minDRtruth_mu);
                    hist("h_deltaEta_ROI_truthSignal_mu")->Fill(minDEtatruth_mu);
                    hist("h_deltaPhi_ROI_truthSignal_mu")->Fill(minDPhitruth_mu);                   
                }



                // HLT LRT chain efficiency
                if (part->pt()*0.001 > 20 && fabs(part_d0) > 2)
                {
                    hist("h_d0_HLT_mu24_LRT_r_truth_eff_d")->Fill(part_d0);
                    hist("h_pt_HLT_mu24_LRT_r_truth_eff_d")->Fill(part->pt()*0.001);
                    hist("h_eta_HLT_mu24_LRT_r_truth_eff_d")->Fill(part_eta);
                    hist("h_phi_HLT_mu24_LRT_r_truth_eff_d")->Fill(part_phi);
                    hist("h_vtxR_HLT_mu24_LRT_r_truth_eff_d")->Fill(part_prodR);
                }

                if (part->pt()*0.001 > 20)
                {
                    hist("h_d0_HLT_mu24_r_truth_eff_d")->Fill(part_d0);
                    hist("h_pt_HLT_mu24_r_truth_eff_d")->Fill(part->pt()*0.001);
                    hist("h_eta_HLT_mu24_r_truth_eff_d")->Fill(part_eta);
                    hist("h_phi_HLT_mu24_r_truth_eff_d")->Fill(part_phi);
                    hist("h_vtxR_HLT_mu24_r_truth_eff_d")->Fill(part_prodR);
                }

                if (HLT_mu20_LRT_d0loose_L1MU14FCH_Passed && m_trackTruth_to_MUPTLRT_map.find(part) != m_trackTruth_to_MUPTLRT_map.end() && part->pt()*0.001 > 20 && part_d0 > 2) 
                //if (offlineMatched)  
                {
                    hist("h_d0_HLT_mu24_LRT_r_truth_eff_n")->Fill(part_d0);
                    hist("h_pt_HLT_mu24_LRT_r_truth_eff_n")->Fill(part->pt()*0.001);
                    hist("h_eta_HLT_mu24_LRT_r_truth_eff_n")->Fill(part_eta);
                    hist("h_phi_HLT_mu24_LRT_r_truth_eff_n")->Fill(part_phi);
                    hist("h_vtxR_HLT_mu24_LRT_r_truth_eff_n")->Fill(part_prodR);
                }

                if (HLT_mu24_ivarmedium_L1MU14FCH_passed && m_trackTruth_to_MUPT_map.find(part) != m_trackTruth_to_MUPT_map.end() && part->pt()*0.001 > 20) 
                //if (offlineMatched)  
                {
                    hist("h_d0_HLT_mu24_r_truth_eff_n")->Fill(part_d0);
                    hist("h_pt_HLT_mu24_r_truth_eff_n")->Fill(part->pt()*0.001);
                    hist("h_eta_HLT_mu24_r_truth_eff_n")->Fill(part_eta);
                    hist("h_phi_HLT_mu24_r_truth_eff_n")->Fill(part_phi);
                    hist("h_vtxR_HLT_mu24_r_truth_eff_n")->Fill(part_prodR);
                }


            }

            // Fill numerators
            if (inEMROI && L1_eEM26M_Passed)
            {
                hist("h_pt_truth_L1eEM26M_eff_n")->Fill(part->pt()*0.001);
                hist("h_d0_truth_L1eEM26M_eff_n")->Fill(part_d0);
                hist("h_vtxR_truth_L1eEM26M_eff_n")->Fill(part_prodR);
            }
            if (inMUROI && L1_MU14FCH_Passed)
            {
                hist("h_pt_truth_L1MU14FCH_eff_n")->Fill(part->pt()*0.001);
                hist("h_d0_truth_L1MU14FCH_eff_n")->Fill(part_d0);
                hist("h_vtxR_truth_L1MU14FCH_eff_n")->Fill(part_prodR);
            }


        } // End Truth Particle Loop



        hist("h_nTracks_truthSignal")->Fill(m_TruthParticlesSignal.size());




        // msonly two leg triggers
        // Sort vectors by pt
        std::sort(truthElectrons.begin(), truthElectrons.end(), [](const xAOD::TruthParticle* a, const xAOD::TruthParticle* b){return (a->pt() > b->pt());} );
        std::sort(truthMuons.begin(), truthMuons.end(), [](const xAOD::TruthParticle* a, const xAOD::TruthParticle* b){return (a->pt() > b->pt());} );


        // // msonly trigger validation
        m_cutflow(__LINE__,"---msonly");
        m_cutflow(__LINE__, "1 truth muons pt>60 eta<1.05", truthMuons.size() > 0 && truthMuons[0]->pt()*0.001 > 60 && fabs(truthMuons[0]->eta()) < 1.05);
        m_cutflow(__LINE__, "1 truth muons pt>60 eta<1.05 && mu60msonly", truthMuons.size() > 0 && truthMuons[0]->pt()*0.001 > 60 && fabs(truthMuons[0]->eta()) < 1.05 && HLT_mu60_0eta105_msonly_L1MU14FCH_passed);

        m_cutflow(__LINE__, "2 truth muons pt>50", truthMuons.size() > 1 && truthMuons[1]->pt()*0.001 > 50);
        m_cutflow(__LINE__, "2 truth muons pt>50 and l1", truthMuons.size() > 1 && truthMuons[1]->pt()*0.001 > 50 && L1_MU14FCH_Passed);
        m_cutflow(__LINE__, "truth: 2mu50 passed", truthMuons.size() > 1 && truthMuons[1]->pt()*0.001 > 50 && L1_MU14FCH_Passed && HLT_2mu50_msonly_L1MU14FCH_passed);

        m_cutflow(__LINE__, "1 truth el 1", truthElectrons.size() > 0 && truthElectrons[0]->pt()*0.001 > 40);
        m_cutflow(__LINE__, "1 truth el 1 && hlt_g40", truthElectrons.size() > 0 && truthElectrons[0]->pt()*0.001 > 40 &&  HLT_g40_loose_L1EM20VH_passed);

        m_cutflow(__LINE__, "1 truth el 1 truth mu pt>40", truthMuons.size() > 0 && truthElectrons.size() > 0 && truthMuons[0]->pt()*0.001 > 40 && truthElectrons[0]->pt()*0.001 > 40);
        m_cutflow(__LINE__, "1 truth el 1 truth mu pt>40 and l1", truthMuons.size() > 0 && truthElectrons.size() > 0 && truthMuons[0]->pt()*0.001 > 40 && truthElectrons[0]->pt()*0.001 > 40 && L1_MU14FCH_Passed && L1_EM20VH_Passed);
        m_cutflow(__LINE__, "truth: g40mu40 passed", truthMuons.size() > 0 && truthElectrons.size() > 0 && truthMuons[0]->pt()*0.001 > 40 && truthElectrons[0]->pt()*0.001 > 40 && L1_MU14FCH_Passed && L1_EM20VH_Passed && HLT_g40_loose_mu40_msonly_L1MU14FCH_passed);


        // Run 2 vs Run 3 trigger scheme
        m_cutflow(__LINE__,"Run 2 vs Run 3 trigger scheme");
        m_cutflow(__LINE__, "2 Truth Muons pt>(24,10)", truthMuons.size() > 1 && truthMuons[0]->pt()*0.001 > 24 && truthMuons[1]->pt()*0.001 > 10);
        m_cutflow(__LINE__, "2 Truth Electrons pt>(26,10)", truthElectrons.size() > 1 && truthElectrons[0]->pt()*0.001 > 26 && truthElectrons[1]->pt()*0.001 > 10);
        m_cutflow(__LINE__, "1 Truth e-mu pt>(26,10)", truthElectrons.size() > 0 && truthMuons.size() > 0 && truthElectrons[0]->pt()*0.001 > 26 && truthMuons[0]->pt()*0.001 > 10);
        m_cutflow(__LINE__, "1 Truth mu-e pt>(24,10-26)", truthElectrons.size() > 0 && truthMuons.size() > 0 && truthElectrons[0]->pt()*0.001 < 26 && truthElectrons[0]->pt()*0.001 > 10 && truthMuons[0]->pt()*0.001 > 24 );
        m_cutflow(__LINE__,"--+Run2+--");
        //ee
        if (truthElectrons.size() > 1 && truthElectrons[0]->pt()*0.001 > 26 && truthElectrons[1]->pt()*0.001 > 10)
        {
            bool pass = false;
            if (truthElectrons[0]->pt()*0.001 > 160) pass = HLT_g140_loose_L1eEM26M_passed;
            else if (truthElectrons[1]->pt()*0.001 > 60) pass = HLT_2g50_loose_L12EM20VH_pased;

            m_cutflow(__LINE__, "Run 2: ee", pass);
        } // mm
        else if (truthMuons.size() > 1 && truthMuons[0]->pt()*0.001 > 24 && truthMuons[1]->pt()*0.001 > 10)
        {
            bool pass = false;
            if (truthMuons[0]->pt()*0.001 > 60) pass = HLT_mu60_0eta105_msonly_L1MU14FCH_passed;

            m_cutflow(__LINE__, "Run 2: mm", pass); 
        } //em
        else if (truthElectrons.size() > 0 && truthMuons.size() > 0 && ( (truthElectrons[0]->pt()*0.001 > 26 && truthMuons[0]->pt()*0.001 > 10) || (truthElectrons[0]->pt()*0.001 < 26 && truthElectrons[0]->pt()*0.001 > 10 && truthMuons[0]->pt()*0.001 > 24) )    )
        {
            bool pass = false;

            if (truthElectrons[0]->pt()*0.001 > 160) pass =  HLT_g140_loose_L1eEM26M_passed;
            else if (truthElectrons[0]->pt()*0.001 > 60) pass = HLT_2g50_loose_L12EM20VH_pased;
            else if (truthMuons[0]->pt()*0.001 > 60) pass = HLT_mu60_0eta105_msonly_L1MU14FCH_passed;

            m_cutflow(__LINE__, "Run 2: em", pass);
        }
        ////////////////////////////////////////////
        m_cutflow(__LINE__,"--+Run3+--");
        //ee
        if (truthElectrons.size() > 1 && truthElectrons[0]->pt()*0.001 > 26 && truthElectrons[1]->pt()*0.001 > 10)
        {
            bool pass = false;

            if (truthElectrons[0]->pt()*0.001 > 160 && getTruthPartD0_extrap(truthElectrons[0]) ) pass = HLT_g140_loose_L1eEM26M_passed;
            else if (truthElectrons[0]->pt()*0.001 > 60 && truthElectrons[1]->pt()*0.001 > 60 ) pass = HLT_2g50_loose_L12EM20VH_pased;
            else if (truthElectrons[0]->pt()*0.001 > 28 && getTruthPartD0_extrap(truthElectrons[0]) > 6 ) pass = HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed;
            else if (truthElectrons[0]->pt()*0.001 > 28 && getTruthPartD0_extrap(truthElectrons[0]) < 6) pass = HLT_e26_lhtight_ivarloose_L1eEM26M_passed;            

            m_cutflow(__LINE__, "Run 3: ee", pass);
        }//mm
        else if (truthMuons.size() > 1 && truthMuons[0]->pt()*0.001 > 24 && truthMuons[1]->pt()*0.001 > 10)
        {      
            bool pass = false;

            if (truthMuons[0]->pt()*0.001 > 60  ) pass = HLT_mu60_0eta105_msonly_L1MU14FCH_passed;
            else if (truthMuons[0]->pt()*0.001 > 50 && truthMuons[1]->pt()*0.001 > 50 ) pass = HLT_2mu50_msonly_L1MU14FCH_passed;
            else if (truthMuons[0]->pt()*0.001 > 26 && getTruthPartD0_extrap(truthMuons[0]) > 3 ) pass = HLT_mu24_LRT_d0loose_L1MU14FCH_passed;
            else if (truthMuons[0]->pt()*0.001 > 26 && getTruthPartD0_extrap(truthMuons[0]) < 3 ) pass = HLT_mu24_ivarmedium_L1MU14FCH_passed;

            m_cutflow(__LINE__, "Run 3: mm", pass);
        }//em
        else if (truthElectrons.size() > 0 && truthMuons.size() > 0 && ( (truthElectrons[0]->pt()*0.001 > 26 && truthMuons[0]->pt()*0.001 > 10) || (truthElectrons[0]->pt()*0.001 < 26 && truthElectrons[0]->pt()*0.001 > 10 && truthMuons[0]->pt()*0.001 > 24) )    )
        {
            bool pass = false;

            if (truthElectrons[0]->pt()*0.001 > 28)
            {
                if (truthElectrons[0]->pt()*0.001 > 160 ) pass = HLT_g140_loose_L1eEM26M_passed;
                else if (truthElectrons[0]->pt()*0.001 > 50 && truthMuons[0]->pt()*0.001 > 40  ) pass = HLT_g40_loose_mu40_msonly_L1MU14FCH_passed;
                else if (truthElectrons[0]->pt()*0.001 > 28 && getTruthPartD0_extrap(truthElectrons[0]) > 6 ) pass = HLT_e30_lhloose_nopix_lrtmedium_L1eEM26M_passed;
                else if (truthElectrons[0]->pt()*0.001 > 28 && getTruthPartD0_extrap(truthElectrons[0]) < 6) pass = HLT_e26_lhtight_ivarloose_L1eEM26M_passed;
                
            } 
            else
            {
                if (truthMuons[0]->pt()*0.001 > 60 ) pass = HLT_mu60_0eta105_msonly_L1MU14FCH_passed;
                else if (truthMuons[0]->pt()*0.001 > 26 && getTruthPartD0_extrap(truthMuons[0]) > 3 ) pass = HLT_mu24_LRT_d0loose_L1MU14FCH_passed;
                else if (truthMuons[0]->pt()*0.001 > 26 && getTruthPartD0_extrap(truthMuons[0]) < 3 ) pass = HLT_mu24_ivarmedium_L1MU14FCH_passed;

            }

            m_cutflow(__LINE__, "Run 3: em", pass);
        }


    } // End doTruth



    // Precision tracking loop
    for (const xAOD::TrackParticle* track :m_muonPTLRT)
    {
        hist("h_muon_lrtPT_d0")->Fill( fabs(track->d0()) );
    }

    /*
    for (const xAOD::TrackParticle* track : m_electronPTLRT)
    {
        hist("h_electron_lrtPT_pt")->Fill(track->pt() * 0.001);
        hist("h_electron_lrtPT_d0")->Fill(track->d0());
    }
    */


    ///////////////



    // Cutflow filling for compressed SUSY scenario
    /*
    m_cutflow_met(__LINE__, "High d0 FTF");
    if (passedHighD0FTF)  m_cutflow_met(__LINE__, "High d0 FTF")++;
    m_cutflow_met(__LINE__, "Truth Matched FTF");
    if (hasTruthMatchedFTF) m_cutflow_met(__LINE__, "Truth Matched FTF")++;
    m_cutflow_met(__LINE__, "L1_XE50 && Truth Matched FTF");
    if (L1_XE50_Passed && hasTruthMatchedFTF) m_cutflow_met(__LINE__, "L1_XE50 && Truth Matched FTF")++;
    m_cutflow_met(__LINE__, "L1_XE50 && HLT MET && Truth Matched FTF");
    if (L1_XE50_Passed && hasTruthMatchedFTF && hlt_met_trkmht > m_minHLTtrkmht && hlt_met_pufit > m_minHLTpufit ) m_cutflow_met(__LINE__, "L1_XE50 && HLT MET && Truth Matched FTF")++;
    m_cutflow_met(__LINE__, "L1_XE50 && HLT MET && Jet && Truth Matched FTF");
    if (L1_XE50_Passed && hasTruthMatchedFTF && hlt_met_trkmht > m_minHLTtrkmht && hlt_met_pufit > m_minHLTpufit && jet_n>0 ) m_cutflow_met(__LINE__, "L1_XE50 && HLT MET && Jet && Truth Matched FTF")++;
    if (L1_XE50_Passed && hasTruthMatchedFTF && hlt_met_trkmht > m_minHLTtrkmht && hlt_met_pufit > m_minHLTpufit && jet_n>0 && hasTruthMatchedFTF_highd0) m_cutflow_met(__LINE__, "L1_XE50 && HLT MET && Jet && high d0 Truth Matched FTF")++;
    m_cutflow_met(__LINE__, "L1_XE50 && HLT MET && Jet && high d0 Truth Matched FTF");

    m_cutflow_met(__LINE__, "L1_XE50 && HLT MET && High d0 FTF");
    if (L1_XE50_Passed && hlt_met_trkmht > m_minHLTtrkmht && hlt_met_pufit > m_minHLTpufit && passedHighD0FTF ) m_cutflow_met(__LINE__, "L1_XE50 && HLT MET && High d0 FTF")++;
    m_cutflow_met(__LINE__, "L1_XE50 && HLT MET && Jet && High d0 FTF");
    if (L1_XE50_Passed && hlt_met_trkmht > m_minHLTtrkmht && hlt_met_pufit > m_minHLTpufit && passedHighD0FTF && jet_n>0) m_cutflow_met(__LINE__, "L1_XE50 && HLT MET && Jet && High d0 FTF")++;
    */

    
    ////////////////////////////////////////////
    // Analysis on track collections
    ////////////////////////////////////////////

    // This must be after the above loops selecting tracks and doing matching
    fillPurHistos();


    m_firstEvent = false;

    if (m_makeNtuple) tree("utt_tree")->Fill();

    return StatusCode::SUCCESS;
}



StatusCode simplexAODReaderEL :: finalize ()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.
    

    // Divide numerator and denominator to make efficiency

    h_d0_el_eff_FTFcomb_r_offline->Divide( hist("h_d0_el_eff_n"), hist("h_d0_el_eff_d") );    
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_el_eff_FTFcomb_r_offline", h_d0_el_eff_FTFcomb_r_offline));

    h_d0_mu_eff_FTFcomb_r_offline->Divide( hist("h_d0_mu_eff_n"), hist("h_d0_mu_eff_d") );    
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_mu_eff_FTFcomb_r_offline", h_d0_mu_eff_FTFcomb_r_offline));

    h_pt_eff_FTFcomb_r_offline->Divide( hist("h_pt_eff_n"), hist("h_pt_eff_d") );    
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_pt_eff_FTFcomb_r_offline", h_pt_eff_FTFcomb_r_offline));

    h_eta_eff_FTFcomb_r_offline->Divide( hist("h_eta_eff_n"), hist("h_eta_eff_d") );    
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_eta_eff_FTFcomb_r_offline", h_eta_eff_FTFcomb_r_offline));

    h_vtxR_eff_FTFcomb_r_offline->Divide( hist("h_vtxR_eff_n"), hist("h_vtxR_eff_d") );    
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_vtxR_eff_FTFcomb_r_offline", h_vtxR_eff_FTFcomb_r_offline));


    // truth
    h_d0_eff_FTFcomb_r_truth->Divide( hist("h_d0_truth_eff_n"), hist("h_d0_truth_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_eff_FTFcomb_r_truth", h_d0_eff_FTFcomb_r_truth));

    h_d0_eff_FTFcomb_r_truth_el->Divide( hist("h_d0_truth_el_eff_n"), hist("h_d0_truth_el_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_eff_FTFcomb_r_truth_el", h_d0_eff_FTFcomb_r_truth_el));

    h_d0_eff_FTFcomb_r_truth_mu->Divide( hist("h_d0_truth_mu_eff_n"), hist("h_d0_truth_mu_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_eff_FTFcomb_r_truth_mu", h_d0_eff_FTFcomb_r_truth_mu));

    h_pt_eff_FTFcomb_r_truth->Divide( hist("h_pt_truth_eff_n"), hist("h_pt_truth_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_pt_eff_FTFcomb_r_truth", h_pt_eff_FTFcomb_r_truth));

    h_vtxR_eff_FTFcomb_r_truth->Divide( hist("h_vtxR_truth_eff_n"), hist("h_vtxR_truth_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_vtxR_eff_FTFcomb_r_truth", h_vtxR_eff_FTFcomb_r_truth));

    // offline
    h_d0_eff_offline_r_truth->Divide( hist("h_d0_offline_eff_n"), hist("h_d0_offline_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_eff_offline_r_truth", h_d0_eff_offline_r_truth));

    h_pt_eff_offline_r_truth->Divide( hist("h_pt_offline_eff_n"), hist("h_pt_offline_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_pt_eff_offline_r_truth", h_pt_eff_offline_r_truth));

    h_vtxR_eff_offline_r_truth->Divide( hist("h_vtxR_offline_eff_n"), hist("h_vtxR_offline_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_vtxR_eff_offline_r_truth", h_vtxR_eff_offline_r_truth));

    // purity
    h_d0_pur_FTFcomb_r_offline->Divide( hist("h_d0_pur_n"), hist("h_d0_pur_d") );    
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_pur_FTFcomb_r_offline", h_d0_pur_FTFcomb_r_offline));

    h_pt_pur_FTFcomb_r_offline->Divide( hist("h_pt_pur_n"), hist("h_pt_pur_d") );    
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_pt_pur_FTFcomb_r_offline", h_pt_pur_FTFcomb_r_offline));

    h_eta_pur_FTFcomb_r_offline->Divide( hist("h_eta_pur_n"), hist("h_eta_pur_d") );    
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_eta_pur_FTFcomb_r_offline", h_eta_pur_FTFcomb_r_offline));

    //h_vtxR_pur_FTFcomb_r_offline->Divide(hist("h_vtxR_pur_n"), hist("h_vtxR_pur_d") )
    ////wk()->addOutput(h_vtxR_pur_FTFcomb_r_offline);


    // L1 Eff Offline
    
    h_pt_L1eEM26M_r_offline->Divide(hist("h_pt_offline_L1eEM26M_eff_n"), hist("h_pt_offline_L1eEM26M_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_pt_L1eEM26M_r_offline", h_pt_L1eEM26M_r_offline));

    h_d0_L1eEM26M_r_offline->Divide(hist("h_d0_offline_L1eEM26M_eff_n"), hist("h_d0_offline_L1eEM26M_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_L1eEM26M_r_offline", h_d0_L1eEM26M_r_offline));

    h_vtxR_L1eEM26M_r_offline->Divide(hist("h_vtxR_offline_L1eEM26M_eff_n"), hist("h_vtxR_offline_L1eEM26M_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_vtxR_L1eEM26M_r_offline", h_vtxR_L1eEM26M_r_offline));

    h_pt_L1MU14FCH_r_offline->Divide(hist("h_pt_offline_L1MU14FCH_eff_n"), hist("h_pt_offline_L1MU14FCH_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_pt_L1MU14FCH_r_offline", h_pt_L1MU14FCH_r_offline));

    h_d0_L1MU14FCH_r_offline->Divide(hist("h_d0_offline_L1MU14FCH_eff_n"), hist("h_d0_offline_L1MU14FCH_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_L1MU14FCH_r_offline", h_d0_L1MU14FCH_r_offline));

    h_vtxR_L1MU14FCH_r_offline->Divide(hist("h_vtxR_offline_L1MU14FCH_eff_n"), hist("h_vtxR_offline_L1MU14FCH_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_vtxR_L1MU14FCH_r_offline", h_vtxR_L1MU14FCH_r_offline));

    // L1 Eff Truth
    h_pt_L1eEM26M_r_truth->Divide(hist("h_pt_truth_L1eEM26M_eff_n"), hist("h_pt_truth_L1eEM26M_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_pt_L1eEM26M_r_truth", h_pt_L1eEM26M_r_truth));

    h_d0_L1eEM26M_r_truth->Divide(hist("h_d0_truth_L1eEM26M_eff_n"), hist("h_d0_truth_L1eEM26M_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_L1eEM26M_r_truth", h_d0_L1eEM26M_r_truth));

    h_vtxR_L1eEM26M_r_truth->Divide(hist("h_vtxR_truth_L1eEM26M_eff_n"), hist("h_vtxR_truth_L1eEM26M_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_vtxR_L1eEM26M_r_truth", h_vtxR_L1eEM26M_r_truth));

    h_pt_L1MU14FCH_r_truth->Divide(hist("h_pt_truth_L1MU14FCH_eff_n"), hist("h_pt_truth_L1MU14FCH_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_pt_L1MU14FCH_r_truth", h_pt_L1MU14FCH_r_truth));

    h_d0_L1MU14FCH_r_truth->Divide(hist("h_d0_truth_L1MU14FCH_eff_n"), hist("h_d0_truth_L1MU14FCH_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_L1MU14FCH_r_truth", h_d0_L1MU14FCH_r_truth));

    h_vtxR_L1MU14FCH_r_truth->Divide(hist("h_vtxR_truth_L1MU14FCH_eff_n"), hist("h_vtxR_truth_L1MU14FCH_eff_d"));
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_vtxR_L1MU14FCH_r_truth", h_vtxR_L1MU14FCH_r_truth));


    // muon ftf over fs ftf
    h_pt_MUFTF_r_FSFTF->Divide( hist("h_pt_MUFTF_r_FSFTF_n"), hist("h_pt_MUFTF_r_FSFTF_d") );
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_pt_MUFTF_r_FSFTF", h_pt_MUFTF_r_FSFTF));

    h_d0_MUFTF_r_FSFTF->Divide( hist("h_d0_MUFTF_r_FSFTF_n"), hist("h_d0_MUFTF_r_FSFTF_d") );
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_MUFTF_r_FSFTF", h_d0_MUFTF_r_FSFTF));

    h_dEta_MUFTF_r_FSFTF->Divide( hist("h_dEta_MUFTF_r_FSFTF_n"), hist("h_dEta_MUFTF_r_FSFTF_d") );
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_dEta_MUFTF_r_FSFTF", h_dEta_MUFTF_r_FSFTF));

    h_dPhi_MUFTF_r_FSFTF->Divide( hist("h_dPhi_MUFTF_r_FSFTF_n"), hist("h_dPhi_MUFTF_r_FSFTF_d") );
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_dPhi_MUFTF_r_FSFTF", h_dPhi_MUFTF_r_FSFTF));


    // electron ftf over fs ftf
    h_pt_ELFTF_r_FSFTF->Divide( hist("h_pt_ELFTF_r_FSFTF_n"), hist("h_pt_ELFTF_r_FSFTF_d") );
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_pt_ELFTF_r_FSFTF", h_pt_ELFTF_r_FSFTF));

    h_d0_ELFTF_r_FSFTF->Divide( hist("h_d0_ELFTF_r_FSFTF_n"), hist("h_d0_ELFTF_r_FSFTF_d") );
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_d0_ELFTF_r_FSFTF", h_d0_ELFTF_r_FSFTF));

    h_dEta_ELFTF_r_FSFTF->Divide( hist("h_dEta_ELFTF_r_FSFTF_n"), hist("h_dEta_ELFTF_r_FSFTF_d") );
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_dEta_ELFTF_r_FSFTF", h_dEta_ELFTF_r_FSFTF));

    h_dPhi_ELFTF_r_FSFTF->Divide( hist("h_dPhi_ELFTF_r_FSFTF_n"), hist("h_dPhi_ELFTF_r_FSFTF_d") );
    ANA_CHECK(histSvc()->regGraph("/ANALYSIS/h_dPhi_ELFTF_r_FSFTF", h_dPhi_ELFTF_r_FSFTF));

 
    finalizeEfficiencies();
    
    //////// Print Trigger Decision Info
    if (m_printTriggerDecCount)
    {
        //flip map
        std::multimap<int, std::string> flip_L1, flip_HLT;
        for (auto &it: m_L1_Trig_Pass) flip_L1.insert({it.second, it.first});
    
        for (auto &it: m_HLT_Trig_Pass) flip_HLT.insert({it.second, it.first});


        std::cout<<"Total Number of events = " << m_totalEventCounter << std::endl;    

        for (auto &it: flip_L1)
        {
            std::cout<<it.second << " nPassed = " << it.first << " prescale = " << m_L1_Trig_Prescale[it.second] << std::endl;
        }

        for (auto &it: flip_HLT)
        {
            std::cout<<it.second << " nPassed = " << it.first << " prescale = " << m_HLT_Trig_Prescale[it.second] << std::endl;
        }
    }

    m_cutflow.printSummary();

    m_cutflow_met.printSummary();

    std::cout<<"I'm finished!"<<std::endl;

    return StatusCode::SUCCESS;
}















/******************
  Helper functions
*******************/







// Wrapper to retrieve collections
template <class A>
void simplexAODReaderEL :: retrieveCollection(bool &flag, A*& collection, std::string collectionName)
{
    if (flag && !evtStore()->retrieve(collection, collectionName) )
    {
        ANA_MSG_ERROR("Couldn't retrieve " + collectionName + ", won't try again!");
        collection = new A;
        flag = false;
    }
}

// Wrapper to book histograms
//template<class A>
StatusCode::ErrorCode simplexAODReaderEL ::  bookHisto(std::string info, std::vector<double> params)
{

    std::string name = info.substr(0, info.find(";") );
    
    if (params.size()==3) // TH1
    {
        ANA_CHECK(book(TH1F(name.c_str(), name.c_str(), int(params[0]), params[1], params[2]) ) );
    }   
    else if (params.size()==6) // TH2
    {
        ANA_CHECK(book(TH2F(name.c_str(), name.c_str(), int(params[0]), params[1], params[2], int(params[3]), params[4], params[5]) ) );
    }

    hist(name)->SetTitle(info.c_str());

    return StatusCode::SUCCESS;
}

// Helper to generate efficiencies
StatusCode::ErrorCode simplexAODReaderEL ::  efficiencyHelper(std::string nameTitle, std::vector<double> params)
{

    std::string name = nameTitle.substr(0, nameTitle.find(";") );

    if (params.size()==3)
    {
        // Book numerator and denominators
        ANA_CHECK(book(TH1F((name+"_n").c_str(), (name+"_n").c_str(), int(params[0]), params[1], params[2]) ) );
        ANA_CHECK(book(TH1F((name+"_d").c_str(), (name+"_d").c_str(), int(params[0]), params[1], params[2]) ) );

        TGraphAsymmErrors *tempTG = new TGraphAsymmErrors();
        tempTG->SetNameTitle(name.c_str(), nameTitle.c_str());

        efficiencyGraphs.push_back(tempTG);
    }
    else if (params.size()>3)
    {
        // Variable width histogram, params are bin edges

        int nbins = params.size()-1;

        //double bins[params.size()];
        std::vector<double> bins_v(params.size());
        for (unsigned int i = 0; i<params.size(); i++)
        {
            bins_v[i] = params[i];
        }

        double *bins = bins_v.data();

        // Book numerator and denominators
        ANA_CHECK(book(TH1F((name+"_n").c_str(), (name+"_n").c_str(), nbins, bins ) ) );
        ANA_CHECK(book(TH1F((name+"_d").c_str(), (name+"_d").c_str(), nbins, bins ) ) );

        TGraphAsymmErrors *tempTG = new TGraphAsymmErrors();
        tempTG->SetNameTitle(name.c_str(), nameTitle.c_str());

        efficiencyGraphs.push_back(tempTG);

    }
    else 
    {
        ANA_MSG_ERROR("Params for efficiency must be 3 values for normal behavior, or at least 4 for variable width binning.");

    }

    return StatusCode::SUCCESS;
}

// helper to finalize efficiencies
StatusCode::ErrorCode simplexAODReaderEL ::  finalizeEfficiencies()
{
    // Loop through graphs, divide, and save
    for (auto tg : efficiencyGraphs)
    {
        std::string name = tg->GetName();
        tg->Divide( hist(name+"_n"), hist(name+"_d") );
        ANA_CHECK(histSvc()->regGraph("/ANALYSIS/"+name, tg));
    }

    return StatusCode::SUCCESS;
}


// Apply basic selection to muon
bool simplexAODReaderEL :: selectMuon(const xAOD::Muon *muon, bool isTrigger)
{
    if ( !(muon->muonType() == xAOD::Muon::Combined) ) return false; // only allow combined

    if (fabs(muon->eta()>m_maxTrackEta)) return false;
    if ( muon->pt()*0.001 < m_minTrackpT ) return false; // don't want threshold effects in pT

    if (muon->primaryTrackParticle()== nullptr) 
    {
        std::cout<<"Warning muon in selectmuon doesn't have primaryTrackParticle()!"<<std::endl;
        std::cout<<"Muon author: "<<muon->author()<<std::endl;
        std::cout<<"Muon pt: "<<muon->pt()<<std::endl;
    }
    else if (fabs(muon->primaryTrackParticle()->d0()) < m_minTrackd0 ) return false; 

    try
    {
        if (!isTrigger && !m_isDAOD && !m_muonSelectionTool->accept(*muon) ) return false; // medium muons
    } catch (const std::runtime_error &e) 
    {
        std::cout<<e.what()<<std::endl;;
        std::cout<<"Caught exception in muon selector tool.  Muon summary:"<<std::endl;
        std::cout<<"muon author "<<muon->author()<< " / type "<<muon->muonType()<<std::endl;
        std::cout<<"mu pt:            "<<muon->pt() <<"; el eta "<<muon->eta()<<"; el phi "<<muon->phi()<<std::endl;
        if (muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle)!=nullptr) std::cout<<"mu islrt track: "<<muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle)->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0)<<std::endl;
        std::cout<<"returning false"<<std::endl;
        return false;
    }

    //if ( !(muon->isAuthor(xAOD::Muon::MuidSA) ) return false; // select standalone muons
    //if ( !(muon->muonType() == xAOD::Muon::MuonStandAlone || muon->muonType() == xAOD::Muon::Combined) ) return false; // allow standalone muons

    

    //const xAOD::TruthParticle *tempPart;
    //if ( !selectOfflineTrackSignal(muon->primaryTrackParticle(), ParticleOrigin::SUSY, tempPart ) ) return false;

    return true;
}

// Apply basic selection to electron
bool simplexAODReaderEL :: selectElectron(const xAOD::Electron *electron, bool isTrigger)
{
    if (fabs(electron->eta()>m_maxTrackEta)) return false;
    if ( electron->pt()*0.001 < m_minTrackpT ) return false; // don't want threshold effects in pT
    if (electron->trackParticle() == nullptr) 
    {
        std::cout<<"Warning electron with pt "  << electron->pt()/1000. << " in select electron is missing track particle!"<<std::endl;
    } else if ( fabs(electron->trackParticle()->d0()) < m_minTrackd0 ) return false; 
    //if (! electron->passSelection("LHVLoose") ) return false; // maybe not in AOD

    const xAOD::TruthParticle *tempPart;
    // jdl todo, isdaod flag here?
    if (!isTrigger && !selectOfflineTrackSignal(electron->trackParticle(), ParticleOrigin::SUSY, tempPart) ) return false;

    //if (!m_isDAOD && !m_ElectronLLHTool->accept(electron)) return false;

    return true;
}

// Apply basic selection to offline tracks
bool simplexAODReaderEL ::  selectOfflineTrack(const xAOD::TrackParticle *track)
{

    bool isLargeD0 = false;
    if (!m_isDAOD) isLargeD0 = track->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0);


    // Pix radii  33.5, 50.5, 88.5, 122.5; fig 1 https://arxiv.org/pdf/1707.02826.pdf

    if ( fabs(track->d0()) < m_minTrackd0 ) return false;

    if (isLargeD0)
    {
        if ( track->pt()*0.001 < m_minTrackpT ) return false;   
        if ( fabs(track->eta()) >  m_maxTrackEta) return false;

         //was used in rel21.3
        /*int nPixHits = track->auxdata<unsigned char>("numberOfPixelHits");
        int nSCTHits = track->auxdata<unsigned char>("numberOfSCTHits");
        int nDeadPix = track->auxdata<unsigned char>("numberOfPixelDeadSensors");
        int nDeadSCT = track->auxdata<unsigned char>("numberOfSCTDeadSensors");
        if ( (nPixHits + nSCTHits + nDeadPix + nDeadSCT) < 7 ) return false;

        int nSharedPix = track->auxdata<unsigned char>("numberOfPixelSharedHits");
        int nSharedSCT = track->auxdata<unsigned char>("numberOfSCTSharedHits");
        if ( (nSharedPix + nSharedSCT/2) >1 ) return false;

        int nSCTHoles = track->auxdata<unsigned char>("numberOfSCTHoles");
        int nPixHoles = track->auxdata<unsigned char>("numberOfPixelHoles");
        if ( (nPixHoles + nSCTHoles) > 2 ) return false;
        if ( (nPixHoles) > 1 ) return false;
        */
    } 
    else // Normal track
    {
        // Loose from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPRecsEarly2018#Selection_Criteria
        // Apply some quality cuts to tracks
        if ( track->pt()*0.001 < m_minTrackpT ) return false;   
        if ( fabs(track->eta()) > m_maxTrackEta ) return false;

        
        /*int nPixHits = track->auxdata<unsigned char>("numberOfPixelHits");
        int nSCTHits = track->auxdata<unsigned char>("numberOfSCTHits");
        int nDeadPix = track->auxdata<unsigned char>("numberOfPixelDeadSensors");
        int nDeadSCT = track->auxdata<unsigned char>("numberOfSCTDeadSensors");
        if ( (nPixHits + nSCTHits + nDeadPix + nDeadSCT) < 7 ) return false;

        int nSharedPix = track->auxdata<unsigned char>("numberOfPixelSharedHits");
        int nSharedSCT = track->auxdata<unsigned char>("numberOfSCTSharedHits");
        if ( (nSharedPix + nSharedSCT/2) >1 ) return false;

        int nSCTHoles = track->auxdata<unsigned char>("numberOfSCTHoles");
        int nPixHoles = track->auxdata<unsigned char>("numberOfPixelHoles");
        if ( (nPixHoles + nSCTHoles) > 2 ) return false;
        if ( (nPixHoles) > 1 ) return false;
        */
    }

    return true;
}

// True for offline tracks with signal parent
// Tries the mcTruthClassifier origin type first (matchParticleOrigin), e.g. SUSY, otherwise it loops through the associated truth particles parents
bool simplexAODReaderEL :: selectOfflineTrackSignal(const xAOD::TrackParticle *track, uint matchParticleOrigin, const xAOD::TruthParticle *&part)
{
    std::pair<unsigned int, unsigned int> res;
    ParticleDef partDef;

    // Truth Classification
    MCTruthClassifier::Info info;    
    res = m_truthClassifier->particleTruthClassifier(track, &info);
    unsigned int iTypeOfPart = res.first;
    unsigned int iPartOrig   = res.second;
    const xAOD::TruthParticle *thePart = m_truthClassifier->getGenPart(track);
    part = thePart;
    ParticleOutCome iPartOutCome = info.particleOutCome;

    if (!thePart) {
        if (m_debug) std::cout<<"No truth link for track with pt = " << track->pt()/1000. <<".  iTypeOfPart = " << iTypeOfPart <<", iPartOrig = " << iPartOrig << std::endl;
        return false; // there is no truth link
    }
    if( !findInVec(m_signalPDGIDs, abs(thePart->pdgId()))) return false; // only consider part if it's a particle we accept as signal

    if (m_debug && thePart) std::cout<<" pdg  "<<thePart->pdgId()<<" particle  type        "<<partDef.sParticleType[iTypeOfPart] <<"  particle origin      "<<partDef.sParticleOrigin[iPartOrig] <<"  particle outcome "<<partDef.sParticleOutCome[iPartOutCome]<<std::endl;
    if (m_debug && thePart) std::cout << "nChildren = "<< thePart->nChildren() << ";  Parent PDGID = " << thePart->parent()->pdgId()<< std::endl;
    if (m_debug && thePart && iPartOrig == matchParticleOrigin) std::cout<<"SUSY decay part pdgid = "<<thePart->pdgId()<<std::endl;
    
    // It's already labled as signal, so return true
    if (iPartOrig == matchParticleOrigin)  return true; 

    // do search on truth heritage
    bool hasSignalParent = testTruthParticle(thePart, m_signalPDGIDs, m_signalParentPDGIDs, m_excludeParentPDGIDs, 0);
    if (m_debug) std::cout<<"Offline has signal parent? "<<hasSignalParent<<std::endl; 

    return hasSignalParent;
}

// Apply basic selection to FTF tracks
bool simplexAODReaderEL ::  selectFTFTrack(const xAOD::TrackParticle *track)
{
    if ( fabs(track->d0()) < m_minTrackd0 ) return false;

    if ( track->pt()*0.001 < m_minTrackpT ) return false;
    if ( fabs(track->eta()) > m_maxTrackEta ) return false;

    // Apply some quality cuts to tracks
    //int nPixHits = track->auxdata<unsigned char>("numberOfPixelHits");
    //int nSCTHits = track->auxdata<unsigned char>("numberOfSCTHits");
    //int nDeadPix = track->auxdata<unsigned char>("numberOfPixelDeadSensors");
    //int nDeadSCT = track->auxdata<unsigned char>("numberOfSCTDeadSensors");

    //if ( (nPixHits + nSCTHits) < m_minHitsFTF ) return false;


    return true;
}

// Apply basic selection to LRT FTF tracks
bool simplexAODReaderEL ::  selectlrtFTFTrack(const xAOD::TrackParticle *track)
{
    if ( fabs(track->d0()) < m_minTrackd0 ) return false;

    if ( track->pt()*0.001 < m_minTrackpT ) return false;
    if ( fabs(track->eta()) > m_maxTrackEta ) return false;

    // Apply some quality cuts to tracks
    //int nPixHits = track->auxdata<unsigned char>("numberOfPixelHits");
    //int nSCTHits = track->auxdata<unsigned char>("numberOfSCTHits");
    //int nDeadPix = track->auxdata<unsigned char>("numberOfPixelDeadSensors");
    //int nDeadSCT = track->auxdata<unsigned char>("numberOfSCTDeadSensors");

    //if ( (nPixHits + nSCTHits) < m_minHitsFTF ) return false;


    //int nSCTHoles = track->auxdata<unsigned char>("numberOfSCTHoles");
    //int nPixHoles = track->auxdata<unsigned char>("numberOfPixelHoles");

    //if ( (nSCTHoles + nPixHoles) > 0 ) return false;


    return true;
}

// Apply basic selection to truth track
bool simplexAODReaderEL ::  selectTruthTrack(const xAOD::TruthParticle *part)
{
    //if (m_debug) std::cout<<"Checking truth part with eta="<<fabs(part->eta())<<", pt="<<part->pt()*0.001<<", d0="<<fabs( getTruthPartD0_tida(part) )<<", prodr="<<getProdR(part)<<std::endl;

    if ( fabs( getTruthEta(part)) > m_maxTrackEta) return false; 
    if ( part->pt()*0.001 < m_minTrackpT) return false;

    if ( fabs( getTruthPartD0_extrap(part) ) < m_minTrackd0 ) return false;

    if (getProdR(part) > m_truthMaxProdR) return false;

    if (getDecayR(part) < m_truthMinDecayR) return false;

    if (countTruthLayers(part) < m_minTruthLayers) return false;

    if (m_maxBarcode > 0 && part->barcode() > m_maxBarcode) return false;

    return true;
}

int simplexAODReaderEL ::  countTruthLayers(const xAOD::TruthParticle *part)
{

    auto start = std::upper_bound(m_SiLayers.begin(), m_SiLayers.end(), getProdR(part));
    auto end   = std::lower_bound(m_SiLayers.begin(), m_SiLayers.end(), getDecayR(part));

    if (start == m_SiLayers.end()) return 0;    

    int firstLayer = std::distance(m_SiLayers.begin(), start);
    int lastLayer  = std::distance(m_SiLayers.begin(), end) + 1; // 0 indexing needs a +1 to count layers
    

    /*
    if (m_debug)
    {
        std::cout<<"counting layers: \n" <<
        getProdR(part) << "\n" <<
        getDecayR(part) << "\n" <<   
        "n layers = " << lastLayer - firstLayer <<std::endl;
    }
    */
    return lastLayer - firstLayer;

}



// Function to test if truth particle is of given pdgid and has correct parent pdgid
bool simplexAODReaderEL ::  testTruthParticle(const xAOD::TruthParticle* part, std::vector<int> pdgid_vec, std::vector<int> parentPdgid_vec, std::vector<int> excludeParentPdgid_vec, int status)
{
    // Only select particles of certain pdgid
    if ( ! (findInVec(pdgid_vec, abs(part->pdgId()) ) || pdgid_vec.empty()) ) return false;
 
    //if (m_debug) std::cout<<"Found particle of type: "<<part->pdgId()<< " and status " <<part->status()<<" and barcode "<<part->barcode()<< std::endl;

    // Select status
    if (status !=0 && part->status() != status) return false;

    //if (m_debug) std::cout<<"Particle has parent with signal PDGID? " << bool(particleHasParentPdgid(part, parentPdgid_vec, false)!=nullptr) << ", parent pdgid = " << part->parent()->pdgId() << std::endl;

    // Select parents
    if (parentPdgid_vec.size() !=0 && particleHasParentPdgid(part, parentPdgid_vec, false)==nullptr) return false;

    // Exclude parents
    if (excludeParentPdgid_vec.size()!=0 && particleHasParentPdgid(part, excludeParentPdgid_vec, true)!=nullptr) return false;

    //if (m_debug && abs(part->pdgId())==211 && parentPdgid_vec.size() !=0 && !particleHasParentPdgid(part, {15}, false, status)) std::cout<<"PION didn't come from tau!"<<std::endl;

    return true;

}




// return something if the particle has a parent with a given pdgid, where this is a vector of acceptable pdgids.  directParent requires the immediate parent to be the desired pdgid, otherwise it looks all the way up the chain
const xAOD::TruthParticle* simplexAODReaderEL ::  particleHasParentPdgid(const xAOD::TruthParticle *part, std::vector<int> parentPdgid_vec, bool directParent, int status, bool firstLevel)
{

    if (!part) return nullptr;

    //if (m_debug) std::cout<<"Current particle pdgid="<<part->pdgId()<<" and status="<<part->status()<<std::endl;

    // Don't return part "true" if the input particle is the one we're looking for, it's not a parent
    /*if (findInVec(parentPdgid_vec, abs(part->pdgId()) ) && firstLevel )
    {
        return nullptr;
    }
    else*/ 
    if ( findInVec(parentPdgid_vec, abs(part->pdgId()) ) && !firstLevel )
    {
        if (status!=0 && part->status() == status)
        {
            return part; 
        } 
        else if(status==0)
        {
            return part;  
        } 
      
    }


    if (!part->hasProdVtx()) return nullptr; // No production vertex


    const xAOD::TruthVertex *prodVtx = part->prodVtx();       
    if (prodVtx->nIncomingParticles() < 1 ) return nullptr; // no parent particles in vertex
    //std::cout<<"Number of incoming particles = "<<prodVtx->nIncomingParticles()<<std::endl;
    //std::cout<<prodVtx->incomingParticleLinks()[0]<<std::endl;
   
    const xAOD::TruthParticle * parent = nullptr;
    for (const ElementTruthLink_t &link : prodVtx->incomingParticleLinks())
    {
        if (!link.isValid()) 
        {
            //std::cout<<"Part with pdigd: "<<part->pdgId()<< " has empty truth link"<<std::endl;
            continue;
        }

        const xAOD::TruthParticle *tp_part = (*link);
        if (directParent && findInVec(parentPdgid_vec, abs(tp_part->pdgId()) ) )
        {
            //std::cout<<"Found direct match!"<<__LINE__<<std::endl;
            return tp_part;
        }
        else if (!directParent)
        {
            parent = particleHasParentPdgid(tp_part, parentPdgid_vec, directParent, status, false);

            if (parent && findInVec(parentPdgid_vec, abs(parent->pdgId()) ))
            {
                if( status!=0 && parent->status() == status)
                {
                    //std::cout<<"Found match!"<<__LINE__<<std::endl;
                    return parent;
                }
                else if (status==0) 
                {
                    //std::cout<<"Found match!"<<__LINE__<<std::endl;
                    return parent;
                }
             
            }
        }
        

    }


    return nullptr;
}



// Return vector of truth particles which are of pdgid and possibly with parents of parentPdgid, where these are vectors of acceptable pdgids
const std::vector<const xAOD::TruthParticle*>  simplexAODReaderEL ::  findTruthParticles(const xAOD::TruthParticleContainer* particles, std::vector<int> pdgid_vec, std::vector<int> parentPdgid_vec, std::vector<int> excludeParentPdgid_vec, int status, bool selectTracks)
{
    if (m_debug) std::cout<<"DEBUG: simplexAODReaderEL::findTruthParticles"<<std::endl;
    

    std::vector<const xAOD::TruthParticle*> passingParticles; 
    if (m_debug) std::cout<<"Number of truth particles = "<<particles->size()<<std::endl;
    int counter = 0;
    for (const xAOD::TruthParticle *part : *particles)
    {
        // Check pdgid and parents for signal
        if (!testTruthParticle(part, pdgid_vec, parentPdgid_vec, excludeParentPdgid_vec, status)) continue;

        if (selectTracks && !selectTruthTrack(part)) continue; 

        //m_TruthParticlesSignal.push_back(part);
        passingParticles.push_back(part);

        if (m_debug) 
        {
            std::cout<<"\n--------------\nParticle #"<<counter<<std::endl;
            std::cout<<"Pdgid="<<part->pdgId()<<std::endl;
            std::cout<<"status="<<part->status()<<std::endl;
            std::cout<<"barcode="<<part->barcode()<<std::endl;       
            std::cout<<"pt="<<part->pt()/1000.<<std::endl;       


            // Truth Classification
            MCTruthClassifier::Info info;
            std::pair<unsigned int, unsigned int> res;
            ParticleDef partDef;
            res = m_truthClassifier->particleTruthClassifier(part, &info);
            unsigned int iTypeOfPart = res.first;
            unsigned int iPartOrig   = res.second;
            ParticleOutCome iPartOutCome = info.particleOutCome;

            std::cout<<" pdg  "<<part->pdgId()<<" particle  type            "<<partDef.sParticleType[iTypeOfPart] <<"  particle origin      "<<partDef.     sParticleOrigin[iPartOrig] <<"  particle outcome "<<partDef.sParticleOutCome  [iPartOutCome]<<std::endl;
            
            if (part->parent() != nullptr && part->parent()->pdgId() !=0)
            {
                std::cout<<"Parent PDGID = " << part->parent()->pdgId()  <<std::endl;
            }
            else 
            {
                const xAOD::TruthVertex *prodVtx = part->prodVtx();
                if (prodVtx != nullptr)
                {
                    std::cout<<"Has prod vertex = "<< prodVtx << " with "<<prodVtx->nIncomingParticles()<<" incoming parts"<<std::endl;
                    for (const ElementTruthLink_t &link : prodVtx->incomingParticleLinks())
                    {
                        if (link.isValid())
                        {
                            const xAOD::TruthParticle *tp_part = (*link);
                            std::cout<<"Parent pdgid " <<tp_part->pdgId()<<std::endl;
                        }
                    }
                }
                else
                {
                    std::cout<<"No parent or production vertex"<<std::endl;
                }
            }
        }
        counter++;
    }

    return passingParticles;
    //return m_TruthParticlesSignal;
}



// Calculate production vertex radius
double simplexAODReaderEL ::  getProdR(const xAOD::TruthParticle *part)
{
    if (!part) return -999;

    if (!part->hasProdVtx()) return -999;

    const xAOD::TruthVertex *prodVtx = part->prodVtx();
    double xprod = prodVtx->x();
    double yprod = prodVtx->y();

    double R = sqrt( pow(xprod - m_xbeam,2) + pow(yprod - m_ybeam,2) );

    return R;

}

// Calculate production vertex Z
double simplexAODReaderEL ::  getProdZ(const xAOD::TrackParticle *track)
{
    const xAOD::TruthParticle *part = m_truthClassifier->getGenPart(track);

    return getProdR(part);
}
// Calculate production vertex Z
double simplexAODReaderEL ::  getProdZ(const xAOD::TruthParticle *part)
{
    if (!part) return -999;

    if (!part->hasProdVtx()) return -999;

    const xAOD::TruthVertex *prodVtx = part->prodVtx();
    
    return prodVtx->z();

}


double simplexAODReaderEL ::  getProdR(const xAOD::TrackParticle *track)
{
    const xAOD::TruthParticle *part = m_truthClassifier->getGenPart(track);

    return getProdR(part);
}


// Calculate decay vertex radius
double simplexAODReaderEL ::  getDecayR(const xAOD::TruthParticle *part)
{
    if (!part) return -999;

    if (!part->hasDecayVtx()) return 1e16;

    const xAOD::TruthVertex *decayVtx = part->decayVtx();
    double xdecay = decayVtx->x();
    double ydecay = decayVtx->y();

    double R = sqrt( pow(xdecay - m_xbeam,2) + pow(ydecay - m_ybeam,2) );

    return R;

}


// Match two collections (ie tracks) uniquely
template <class A, class B>
std::map<A,B> simplexAODReaderEL ::  matchCollections_dR(std::vector<A> ref, std::vector<B> test, double maxdR)
{
    
    std::map<A,B> refToTest;
    std::multimap<double, std::pair<A, B> > raw_matches;

    // Loop through refs and look for best matches to test objects, need to keep everything within max dR
    for (auto &r: ref)
    {
        double r_eta = r->eta();
        double r_phi = r->phi();
        for (auto &t: test)
        {
            double deltaR = getDeltaR(r_eta, r_phi, t);
            if (deltaR < maxdR )
            {
                raw_matches.emplace( deltaR, std::make_pair(r, t) );
            }
        }
    }

    // Now make the match unique
    // These are sorted by the best dR by the map already
    std::set<A> usedRef;
    std::set<B> usedTest;
    // loop through each match, taking the smallest dR first
    for (auto &m: raw_matches)
    {
        // Already used these objects in a better match
        if (usedRef.find(m.second.first) != usedRef.end()) continue;
        if (usedTest.find(m.second.second) != usedTest.end()) continue;

        refToTest.emplace(m.second.first, m.second.second);
        usedRef.insert(m.second.first);
        usedTest.insert(m.second.second);
    }

    return refToTest;

}

// specialize for truth
template <class B>
std::map<const xAOD::TruthParticle*,B> simplexAODReaderEL ::  matchCollections_dR(std::vector<const xAOD::TruthParticle*> ref, std::vector<B> test, double maxdR)
{
    
    std::map<const xAOD::TruthParticle*,B> refToTest;
    std::multimap<double, std::pair<const xAOD::TruthParticle*, B> > raw_matches;

    // Loop through refs and look for best matches to test objects, need to keep everything within max dR
    for (auto &r: ref)
    {
        double r_eta = getTruthEta(r);
        double r_phi = getTruthPhi(r);
        for (auto &t: test)
        {
            double deltaR = getDeltaR(r_eta, r_phi, t);
            if (deltaR < maxdR )
            {
                raw_matches.emplace( deltaR, std::make_pair(r, t) );
            }
        }
    }

    // Now make the match unique
    // These are sorted by the best dR by the map already
    std::set<const xAOD::TruthParticle*> usedRef;
    std::set<B> usedTest;
    // loop through each match, taking the smallest dR first
    for (auto &m: raw_matches)
    {
        // Already used these objects in a better match
        if (usedRef.find(m.second.first) != usedRef.end()) continue;
        if (usedTest.find(m.second.second) != usedTest.end()) continue;

        refToTest.emplace(m.second.first, m.second.second);
        usedRef.insert(m.second.first);
        usedTest.insert(m.second.second);
    }

    return refToTest;

}


// Fill histos for purity after doing matching
void simplexAODReaderEL :: fillPurHistos()
{
//m_FTFTracksComb, m_OfflineTracks, m_OfflineTracksSignal

    // Loop through FTF tracks to fill purity histos
    for( const xAOD::TrackParticle *ftfPart: m_FTFTracksComb)
    {

        // Count number of elements in map for ftfPart, i.e. how many times this track matched to 
        //int nTracks = std::distance(m_trackFTF_to_Offline_map.equal_range(ftfPart).first, m_trackFTF_to_Offline_map.equal_range(ftfPart).second);
        bool hasMatch = m_trackFTF_to_Offline_map.find(ftfPart) != m_trackFTF_to_Offline_map.end();

        hist("h_d0_pur_d")->Fill( ftfPart->d0() );
        hist("h_pt_pur_d")->Fill( ftfPart->pt() / 1000. );
        hist("h_eta_pur_d")->Fill( ftfPart->eta() );
        //hist("h_vtxR_pur_d")->Fill( );

        // If FTF track was matched to at least one offline track, fill numerator
        if (hasMatch)
        {
            // Fill numerators
            hist("h_d0_pur_n")->Fill( ftfPart->d0() );
            hist("h_pt_pur_n")->Fill( ftfPart->pt() / 1000. );
            hist("h_eta_pur_n")->Fill( ftfPart->eta() );
            //hist("h_vtxR_pur_n")->Fill( );
        
        }

    }



}

// Adjust offline track d0 to primary vertex position
double simplexAODReaderEL :: getCorrectedD0(const xAOD::TrackParticle *offPart)
{
    // From TIDA here https://gitlab.cern.ch/atlas/athena/-/blob/21.3/Trigger/TrigAnalysis/TrigInDetAnalysisUtils/src/TrigTrackSelector.cxx#L1077
    // Again, like below, haven't worked out geometry

    return offPart->d0() +  std::sin(offPart->phi())*m_xbeam - std::cos(offPart->phi())*m_ybeam;

}

// Adjust offline track d0 to primary vertex position and compute sig
double simplexAODReaderEL :: getCorrectedD0Sig(const xAOD::TrackParticle *offPart)
{
    //xAOD::TrackingHelpers::checkTPAndDefiningParamCov(offPart);
    // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODTracking/Root/TrackParticlexAODHelpers.cxx?v=21.3
    double d0_corr = getCorrectedD0(offPart);
    double beamspot_uncert2 = xAOD::TrackingHelpers::d0UncertaintyBeamSpot2(offPart->phi(), ei->beamPosSigmaX(), ei->beamPosSigmaY(), ei->beamPosSigmaXY() );   
    double sigma_d0 = offPart->definingParametersCovMatrixVec().at(0);
    if (sigma_d0<=0.) std::cout<<"Error sigma d0 <0"<<std::endl;

    return d0_corr / sqrt( beamspot_uncert2 + sigma_d0 );

}



// Compute truth particle d0
double simplexAODReaderEL :: getTruthPartD0(const xAOD::TrackParticle *offPart)
{
    const xAOD::TruthParticle *truthPart = m_truthClassifier->getGenPart(offPart);
    return getTruthPartD0(truthPart);
}

// Compute truth particle d0
double simplexAODReaderEL :: getTruthPartD0(const xAOD::TruthParticle *part)
{

    if (!part) return -999;
    if (!part->hasProdVtx()) return -999;

    double phi = part->phi();
    double B = 2; // Tesla
    double pt = part->pt() / 1000.; // GeV
    double R = pt / (0.3 * B) * 1000.; // millimeters
    
    double x_prod = part->prodVtx()->x();
    double y_prod = part->prodVtx()->y();

    int charge = part->charge();
    double phiToCenter = phi - charge * M_PI / 2; // angle to center of circle from prod vertex

    double x_cent = x_prod + R * cos(phiToCenter);
    double y_cent = y_prod + R * sin(phiToCenter);

    double d0 = sqrt(pow(x_cent - m_xbeam, 2) + pow(y_cent - m_ybeam, 2) ) - R; // millimeters

    return d0;
}

// Use extrapolated truth info if requested
double simplexAODReaderEL :: getTruthPartD0_extrap(const xAOD::TrackParticle *offPart)
{
    const xAOD::TruthParticle *truthPart = m_truthClassifier->getGenPart(offPart);
    return getTruthPartD0_extrap(truthPart);
}
double simplexAODReaderEL :: getTruthPartD0_extrap(const xAOD::TruthParticle *part)
{
    if (m_extrapolateTruth)
    {
        return part->auxdataConst<float>("d0");
    }
    else return getTruthPartD0(part);
}


double simplexAODReaderEL :: getTruthPartD0_tida(const xAOD::TrackParticle *offPart)
{
    const xAOD::TruthParticle *truthPart = m_truthClassifier->getGenPart(offPart);
    return getTruthPartD0_tida(truthPart);
}


double simplexAODReaderEL :: getTruthPartD0_tida(const xAOD::TruthParticle *part)
{
    // https://gitlab.cern.ch/atlas/athena/-/blob/21.3/Trigger/TrigAnalysis/TrigInDetAnalysisUtils/src/TrigTrackSelector.cxx#L370
    // From TIDA, still don't understand the geometry of this

    if (!part) return -999;
    if (!part->hasProdVtx()) return -999;

    double d0 = (part->prodVtx()->y() - m_ybeam) * cos(part->phi()) - (part->prodVtx()->x() - m_xbeam) * sin(part->phi()) ;

    return d0;
}



/*
double simplexAODReaderEL :: getTruthPartD0_article(const xAOD::TruthParticle *part)
{
    // See section 1.4 here https://www.phys.ufl.edu/~avery/fitting/fitting4.pdf 

    if (!part) return -999;
    if (!part->hasProdVtx()) return -999;

    const xAOD::TruthVertex *prodVtx = part->prodVtx();
    double xprod = (prodVtx->x() - m_xbeam) / 1000.; // meters
    double yprod = (prodVtx->y() - m_ybeam) / 1000.; // meters


    double px = part->px() / 1000.; // GeV
    double py = part->py() / 1000.; // GeV
    double pt = part->pt() / 1000.; // GeV

    int Q = part->charge();

    double B = 2; // Tesla

    double a = -0.3 * B * Q;

    double T = sqrt( pt*pt - 2 * a * ( xprod * py -yprod * px) + a*a*(xprod*xprod + yprod*yprod) );

    double d0 = (-2 * (xprod * py - yprod * px) + a * (xprod*xprod + yprod*yprod) ) / (T + pt) * 1000.; // millimeters

    return d0;

}
*/

/*
double simplexAODReaderEL :: getTruthPartD0_straight_displep(const xAOD::TruthParticle *part)
{
    // https://gitlab.cern.ch/atlas-phys-susy-wg/Common/FactoryTools/-/blob/highd0/Root/RegionVarCalculator_highd0.cxx#L1389

    if (!part) return -999;
    if (!part->hasProdVtx()) return -999;

    TVector3 beamToProd(part->prodVtx()->x() - m_xbeam, part->prodVtx()->y() - m_ybeam, 0 );
    TVector3 track;
    track.SetMagThetaPhi(1.0, part->p4().Theta(), part->phi());

    double d0 = beamToProd.Perp() * sin( fabs(track.DeltaPhi(beamToProd)) );

    return d0;
}
*/



std::vector<const xAOD::Electron*>  simplexAODReaderEL :: getElectronChainFeatures(std::string chainName, bool isLRT, int leg)
{
    std::string sg_key;
    //if (chainName.find("nogsf") != std::string::npos)
    //    sg_key = isLRT ? "HLT_egamma_Electrons_LRT" : "HLT_egamma_Electrons";
    //else
    sg_key = isLRT ? "HLT_egamma_Electrons_LRTGSF" : "HLT_egamma_Electrons_GSF";

    std::vector< TrigCompositeUtils::LinkInfo<xAOD::ElectronContainer> > fc = m_trigDecTool->template features<xAOD::ElectronContainer>( 
        chainName,
        TrigDefs::Physics, // decision type: TrigDefs::alsoDeactivateTEs; //TrigDefs::Physics;   TrigDefs::alsoDeactivateTEs;  TrigDefs::L1_isPassedBeforePrescale;
        sg_key, // storegate key
        TrigDefs::lastFeatureOfType, // feature type: TrigDefs::lastFeatureOfType   TrigDefs::allFeaturesOfType
        "feature",
        leg);

    if (m_debug) std::cout<<"electron size = "<<fc.size()<<std::endl;
    std::vector<const xAOD::Electron*> out;

    for (const auto& electronLinkInfo : fc)
    {
        if (!electronLinkInfo.isValid())
        {
            ATH_MSG_ERROR("Electron link info is invalid");
        } else {
            const xAOD::Electron *el = *electronLinkInfo.link;
            if (m_debug and !m_isDAOD) 
            {
                std::cout<<"JDL TEST: Electron pt,eta,phi = "<<el->pt() <<", "<<el->eta()<<", "<<el->phi()<<std::endl;
                std::cout<<" el->caloCluster() "<<el->caloCluster()<<std::endl;
                if (el->caloCluster()!=nullptr) std::cout<<"Electron cluster et,eta,phi = "<<el->caloCluster()->e() <<", "<<el->caloCluster()->eta0()<<", "<<el->caloCluster()->phi0()<<std::endl;
                std::cout<<"el ncaloclusters() "<<el->nCaloClusters()<<std::endl;
                std::cout<<"HLT Electron link dataID and index "<< electronLinkInfo.link.dataID() << " and "<< electronLinkInfo.link.index()<<std::endl;

                const ElementLink< xAOD::CaloClusterContainer > calclustlink = el->caloClusterLink();
                if (calclustlink.isValid()) std::cout<<"cluster link dataID and index "<<calclustlink.dataID() << " and "<< calclustlink.index()<<std::endl;
                else std::cout<<"calclustlink is invalid"<<std::endl;
            }
            //out.push_back(dynamic_cast<const xAOD::IParticle*>(*electronLinkInfo.link));
            out.push_back(*electronLinkInfo.link);
        }

    }

    return out;
}



std::vector<const xAOD::TrackParticle*>  simplexAODReaderEL :: getElectronChainTrackFeatures(std::string chainName, int leg)
{
    std::vector< TrigCompositeUtils::LinkInfo<xAOD::TrackParticleContainer> > fc = m_trigDecTool->template features<xAOD::TrackParticleContainer>( 
        chainName,
        TrigDefs::includeFailedDecisions, // decision type: TrigDefs::alsoDeactivateTEs; //TrigDefs::Physics;   TrigDefs::alsoDeactivateTEs;  TrigDefs::L1_isPassedBeforePrescale;
        "HLT_IDTrack_ElecLRT_IDTrig", // storegate key
        TrigDefs::lastFeatureOfType, // feature type: TrigDefs::lastFeatureOfType   TrigDefs::allFeaturesOfType
        "feature",
        leg);

    std::cout<<"track size = "<<fc.size()<<std::endl;
    std::vector<const xAOD::TrackParticle*> out;

    for (const auto& TrackLinkInfo : fc)
    {
        if (!TrackLinkInfo.isValid())
        {
            ATH_MSG_ERROR("Track link info is invalid");
        } else {
            const xAOD::TrackParticle *el = *TrackLinkInfo.link;
            std::cout<<"JDL TEST: Track pt,eta,phi = "<<el->pt() <<", "<<el->eta()<<", "<<el->phi()<<std::endl;
            out.push_back(*TrackLinkInfo.link);
        }

    }

    return out;
}


std::vector<const xAOD::IParticle*>  simplexAODReaderEL :: getPhotonChainFeatures(std::string chainName, int leg)
{
    std::vector< TrigCompositeUtils::LinkInfo<xAOD::PhotonContainer> > fc = m_trigDecTool->template features<xAOD::PhotonContainer>( 
        chainName,
        TrigDefs::includeFailedDecisions, // decision type: TrigDefs::alsoDeactivateTEs; //TrigDefs::Physics;   TrigDefs::alsoDeactivateTEs;  TrigDefs::L1_isPassedBeforePrescale;
        "HLT_egamma_Photons", // storegate key
        TrigDefs::allFeaturesOfType, // feature type: TrigDefs::lastFeatureOfType   TrigDefs::allFeaturesOfType
        "feature",
        leg);

    std::cout<<"photon size = "<<fc.size()<<std::endl;
    std::vector<const xAOD::IParticle*> out;

    for (const auto& PhotonLinkInfo : fc)
    {
        if (!PhotonLinkInfo.isValid())
        {
            ATH_MSG_ERROR("Photon link info is invalid");
        } else {
            const xAOD::Photon *el = *PhotonLinkInfo.link;
            std::cout<<"JDL TEST: Photon pt,eta,phi = "<<el->pt() <<", "<<el->eta()<<", "<<el->phi()<<std::endl;
            out.push_back(dynamic_cast<const xAOD::IParticle*>(*PhotonLinkInfo.link));
        }

    }

    return out;
}


std::vector<const TrigRoiDescriptor*>  simplexAODReaderEL :: getElectronTnPRoI()
{

    std::vector<const xAOD::IParticle*> photons = getPhotonChainFeatures("HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH", 1);

    std::vector<const TrigRoiDescriptor*> roid_em;


    unsigned feature_type = TrigDefs::allFeaturesOfType; // TrigDefs::lastFeatureOfType   TrigDefs::allFeaturesOfType
    unsigned decisiontype = TrigDefs::Physics; //TrigDefs::alsoDeactivateTEs; //TrigDefs::Physics;   TrigDefs::alsoDeactivateTEs;  TrigDefs::L1_isPassedBeforePrescale;
    std::string roi_key = "HLT_Roi_FastElectron_LRT"; // HLT_Roi_FastElectron   HLT_EMRoIs   HLT_MURoIs HLT_MuonsCB_RoI  MuonCandidates_FS_ROIs  HLT_Roi_L2SAMuon
    std::string electron_chaingroup_forROI = "HLT_e5_lhvloose_nopix_lrtloose_idperf_probe_g25_medium_L1EM20VH"; //HLT_e26_lhloose_lrtloose_L1eEM26M; HLT_e26_lhtight_ivarloose_L1eEM26M . HACK "(HLT_e25_lhvloose_nod0_L1EM15|HLT_e60_lhmedium_nod0)" "(HLT_e26_lhmedium_nod0|HLT_e60_lhmedium)" HLT_e5_idperf_L1EM3 HLT_g25_loose_L1EM20VH 



    std::vector< TrigCompositeUtils::LinkInfo<TrigRoiDescriptorCollection> > rois_em_fc = m_trigDecTool->template features<TrigRoiDescriptorCollection>( 
        electron_chaingroup_forROI,
        decisiontype,
        roi_key,
        feature_type,
        "roi");

    // Debug
    //auto cg =  m_trigDecTool->getChainGroup("HLT_e26_lhtight_ivarloose_L1eEM26M");
    //std::cout<<"HLT_e26_lhtight_ivarloose_L1eEM26M passed "<<cg->isPassed()<<std::endl;
    ANA_MSG_ALWAYS(" N " << rois_em_fc.size());
    for (auto& r : rois_em_fc) {
        ANA_MSG_ALWAYS( r.link.dataID() << " #" << r.link.index() << " eta:" << (*r.link)->eta() << " phi:" << (*r.link)->phi());
    }



    // Pull out trig roi descriptor
    for (const TrigCompositeUtils::LinkInfo<TrigRoiDescriptorCollection> &RoI_info : rois_em_fc)
    {
        if (RoI_info.isValid()) 
        {
            roid_em.push_back(*RoI_info.link);
        }
        else std::cout<<"Warning: em roi descriptor link wasn't valid"<<std::endl;
    }


    electron_chaingroup_forROI = "HLT_e26_lhloose_nopix_lrttight_probe_g25_medium_L1EM20VH";
    rois_em_fc = m_trigDecTool->template features<TrigRoiDescriptorCollection>( 
        electron_chaingroup_forROI,
        TrigDefs::alsoDeactivateTEs,
        roi_key,
        feature_type,
        "roi");

    // Debug
    //auto cg =  m_trigDecTool->getChainGroup("HLT_e26_lhtight_ivarloose_L1eEM26M");
    //std::cout<<"HLT_e26_lhtight_ivarloose_L1eEM26M passed "<<cg->isPassed()<<std::endl;
    ANA_MSG_ALWAYS(" N " << rois_em_fc.size());
    for (auto& r : rois_em_fc) {
        ANA_MSG_ALWAYS( r.link.dataID() << " #" << r.link.index() << " eta:" << (*r.link)->eta() << " phi:" << (*r.link)->phi());
    }

    return roid_em;

}


std::vector<const xAOD::Muon*>  simplexAODReaderEL :: getMuonChainFeatures(std::string chainName, bool isLRT, int leg)
{
    std::string sg_key = isLRT ? "HLT_MuonsCB_LRT" : "HLT_Muons_RoI";

    std::vector< TrigCompositeUtils::LinkInfo<xAOD::MuonContainer> > fc = m_trigDecTool->template features<xAOD::MuonContainer>( 
        chainName,
        TrigDefs::Physics, // decision type: TrigDefs::alsoDeactivateTEs; //TrigDefs::Physics;   TrigDefs::alsoDeactivateTEs;  TrigDefs::L1_isPassedBeforePrescale;
        sg_key, // storegate key
        TrigDefs::lastFeatureOfType, // feature type: TrigDefs::lastFeatureOfType   TrigDefs::allFeaturesOfType
        "feature",
        leg);

    if (m_debug) std::cout<<"muon size = "<<fc.size()<<std::endl;
    std::vector<const xAOD::Muon*> out;

    for (const auto& muonLinkInfo : fc)
    {
        if (!muonLinkInfo.isValid())
        {
            ATH_MSG_ERROR("Muon link info is invalid");
        } else {
            const xAOD::Muon *mu = *muonLinkInfo.link;
            if (m_debug) std::cout<<"JDL TEST: Muon pt,eta,phi = "<<mu->pt() <<", "<<mu->eta()<<", "<<mu->phi()<<std::endl;
            //out.push_back(dynamic_cast<const xAOD::IParticle*>(*muonLinkInfo.link));
            out.push_back(*muonLinkInfo.link);
        }

    }

    return out;
}


std::vector<const xAOD::TrackParticle*>  simplexAODReaderEL :: getMuonChainTrackFeatures(std::string chainName, bool isLRT, int leg)
{
    std::string sg_key = isLRT ? "HLT_CBCombinedMuon_RoITrackParticles" : "HLT_CBCombinedMuon_RoITrackParticles";

    std::vector< TrigCompositeUtils::LinkInfo<xAOD::TrackParticleContainer> > fc = m_trigDecTool->template features<xAOD::TrackParticleContainer>( 
        chainName,
        TrigDefs::Physics, // decision type: TrigDefs::alsoDeactivateTEs; //TrigDefs::Physics;   TrigDefs::alsoDeactivateTEs;  TrigDefs::L1_isPassedBeforePrescale;
        sg_key, // storegate key
        TrigDefs::allFeaturesOfType, // feature type: TrigDefs::lastFeatureOfType   TrigDefs::allFeaturesOfType
        "feature",
        leg);

    if (m_debug) std::cout<<"muon trk size = "<<fc.size()<<std::endl;
    std::vector<const xAOD::TrackParticle*> out;

    for (const auto& trkLinkInfo : fc)
    {
        if (!trkLinkInfo.isValid())
        {
            ATH_MSG_ERROR("Muon trk link info is invalid");
        } else {
            const xAOD::TrackParticle *mu = *trkLinkInfo.link;
            if (m_debug) std::cout<<"JDL TEST: Muon trk pt,eta,phi = "<<mu->pt() <<", "<<mu->eta()<<", "<<mu->phi()<<std::endl;
            //out.push_back(dynamic_cast<const xAOD::IParticle*>(*trkLinkInfo.link));
            out.push_back(*trkLinkInfo.link);
        }

    }

    return out;
}

std::vector<const xAOD::TrackParticle*>  simplexAODReaderEL :: getMuonChainIDTrackFeatures(std::string chainName, bool isLRT, int leg)
{
    std::string sg_key = isLRT ? "HLT_IDTrack_MuonLRT_IDTrig" : "HLT_IDTrack_Muon_IDTrig";

    std::vector< TrigCompositeUtils::LinkInfo<xAOD::TrackParticleContainer> > fc = m_trigDecTool->template features<xAOD::TrackParticleContainer>( 
        chainName,
        TrigDefs::Physics, // decision type: TrigDefs::alsoDeactivateTEs; //TrigDefs::Physics;   TrigDefs::alsoDeactivateTEs;  TrigDefs::L1_isPassedBeforePrescale;
        sg_key, // storegate key
        TrigDefs::allFeaturesOfType, // feature type: TrigDefs::lastFeatureOfType   TrigDefs::allFeaturesOfType
        "feature",
        leg);

    if (m_debug) std::cout<<"muon id trk size = "<<fc.size()<<std::endl;
    std::vector<const xAOD::TrackParticle*> out;

    for (const auto& trkLinkInfo : fc)
    {
        if (!trkLinkInfo.isValid())
        {
            ATH_MSG_ERROR("Muon trk link info is invalid");
        } else {
            const xAOD::TrackParticle *mu = *trkLinkInfo.link;
            if (m_debug) std::cout<<"JDL TEST: Muon trk pt,eta,phi = "<<mu->pt() <<", "<<mu->eta()<<", "<<mu->phi()<<std::endl;
            //out.push_back(dynamic_cast<const xAOD::IParticle*>(*trkLinkInfo.link));
            out.push_back(*trkLinkInfo.link);
        }

    }

    return out;
}
